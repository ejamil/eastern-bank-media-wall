﻿package code
{
	import org.tuio.*;
	import org.tuio.adapters.*;
	import org.tuio.debug.TuioDebug;
	import org.tuio.TuioManager;
	import org.tuio.connectors.*;
	
	import code.Document;
	
	import flash.display.*;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
    import fl.text.TLFTextField;
    import flash.text.TextFieldType;
    import flash.text.TextFormat;
    import flash.text.TextFormatAlign;
	import flashx.textLayout.formats.TextAlign;
	import flashx.textLayout.formats.TextLayoutFormat;
	import flashx.textLayout.elements.TextFlow;
    import flash.text.engine.TextBaseline;
	import flash.text.Font;

	public class Pong extends MovieClip
	{
		private var yDirection:Number = 20;
		private var xDirection:Number = -20;
		private var targetX:Number;
		private var easing:Number = 24;
		private var playerScore:Number;
		private var enemyScore:Number;
		private var winScore:Number = 10;
		private var paddle_mc:Paddle;
		private var enemy_mc:Enemy;
		private var ball_mc:Ball;
		private var owner:Document;
		public var whichPong:Number;
		public var whichScreen:Number;
		private var bg:Bg;
		//private var closeoutTimer:Timer;
		private var closeLarge:CloseLarge;
		
		private var scoreEnemyText:TLFTextField = new TLFTextField();
		private var scoreEnemyFormat:TextLayoutFormat = new TextLayoutFormat();
		private var scoreEnemyTextFlow:TextFlow;
		
		private var scorePaddleText:TLFTextField = new TLFTextField();
		private var scorePaddleFormat:TextLayoutFormat = new TextLayoutFormat();
		private var scorePaddleTextFlow:TextFlow;
		
		private var ballSpeed:Number = 1;
		private var closeoutTimer:Timer;

		public function Pong(xPos:Number, yPos:Number, myOwner:Document, w:Number, s:Number)
		{
			owner = myOwner;
			
			bg = new Bg();
			addChildAt(bg, 0);
			bg.alpha = .7;
			
			
			whichPong = w;
			whichScreen = s;
			this.x = xPos;
			this.y = yPos;
			addEventListener(TuioTouchEvent.TOUCH_DOWN, startGame);
			enemy_mc = new Enemy();
			addChild(enemy_mc);
			enemy_mc.x = owner.loadXML.screenWidth/8 - enemy_mc.width/2;
			enemy_mc.y = 200;
			paddle_mc = new Paddle(this.x, this.y, this.width);
			addChild(paddle_mc);
			paddle_mc.x = owner.loadXML.screenWidth/8 - paddle_mc.width/2;
			paddle_mc.y = owner.loadXML.screenHeight - 200 - paddle_mc.height;
			
			ball_mc = new Ball();
			addChild(ball_mc);
			ball_mc.x = owner.loadXML.screenWidth/8 - ball_mc.width/2;
			ball_mc.y = owner.loadXML.screenHeight/2 - ball_mc.height/2;
			ball_mc.scaleX = .3;
			ball_mc.scaleY = .3;
			
			targetX = paddle_mc.x;
			
			closeLarge = new CloseLarge();
			addChild(closeLarge);
			closeLarge.x = 1080 - closeLarge.width;
			closeLarge.endTxt.text == " "
			closeLarge.addEventListener(TuioTouchEvent.TOUCH_DOWN, endClickX);
			
			setTextFormat(scoreEnemyFormat, 100, 0xC0112F, 2, 0, true, 2);
			setText(scoreEnemyText, null, 0xC0112F, "0", scoreEnemyFormat, scoreEnemyTextFlow);
			addChild(scoreEnemyText);
			scoreEnemyText.x = 46;
			scoreEnemyText.y = 738;
			scoreEnemyText.width = 200;
			scoreEnemyText.height = 200;
			scoreEnemyText.alpha = .5;
			
			setTextFormat(scorePaddleFormat, 100, 0x00467F, 2, 0, true, 2);
			setText(scorePaddleText, null, 0x00467F, "0", scorePaddleFormat, scorePaddleTextFlow);
			addChild(scorePaddleText);
			scorePaddleText.x = 46;
			scorePaddleText.y = 1014;
			scorePaddleText.width = 200;
			scorePaddleText.height = 200;
			scorePaddleText.alpha = .5;
			
			closeoutTimer = new Timer(180000, 1);
			closeoutTimer.start();
			closeoutTimer.addEventListener(TimerEvent.TIMER_COMPLETE, timeout);
		}

		private function timeout(e:TimerEvent):void
		{
			closeoutTimer.reset();
			closeoutTimer.stop();
			closeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
			owner.removeNewPong(whichPong, whichScreen);
		
		}
		
		private function endClickX(e:TuioTouchEvent):void
		{
			
			
			//reset all assets and play again!
			//isEnding = false;
			if (closeoutTimer != null)
			{
				closeoutTimer.reset();
				closeoutTimer.stop();
				closeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
			}
				
			owner.removeNewPong(whichPong, whichScreen);
		}


		
		/*private function timeout(e:TimerEvent):void
		{
			if (isEnding == true)
			{
				closeoutTimer.reset();
				closeoutTimer.stop();
				closeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
				owner.removeNewPong(whichPong, whichScreen);
			}
		}*/


		//turn off all menu buttons
		//start_mc.visible = false;
		//instr_mc.visible = false;
		//back_mc.visible = false;

		//turn on button mode for the background and turn it into a button


		//start the game
		private function startGame(e:TuioTouchEvent)
		{
			playerScore = 0;
			enemyScore = 0;
			showScore();
			//background_mc.buttonMode = false;
			removeEventListener(TuioTouchEvent.TOUCH_DOWN, startGame);
			ball_mc.addEventListener(Event.ENTER_FRAME, moveBall);
			addEventListener(TuioTouchEvent.TOUCH_DOWN, movePaddle);
			enemy_mc.addEventListener(Event.ENTER_FRAME, moveEnemy);
			//Mouse.hide();
			//click_mc.visible = false;
			//Tweener.addTween(win_mc, {x:1162.25, y:-12.9, time:3, transition:"easeIn"});
			//Tweener.addTween(lose_mc, {x:-403.75, y:843.9, time:3, transition:"easeIn"});
			//ball_mc.ball.addEventListener(Event.ENTER_FRAME, rotateCell);
		}

		private function rotateCell(e:Event)
		{
			ball_mc.ball.rotation +=  3;
		}

		private function moveBall(e:Event)
		{
			//trace("ball_mc x = " + ball_mc.x + ", y = " + ball_mc.y);
			//trace("this x = " + this.x + ", y = " + this.y);
			//made the ball bounce off the top and bottom walls, paddles, and reset/score when it goes past the left/right walls
			if (ball_mc.x <= 0)
			{
				xDirection *=  -1;
				ball_mc.x = 1;
			}
			else if (ball_mc.x >= owner.loadXML.screenWidth/4 - ball_mc.width)
			{
				xDirection *=  -1;
				ball_mc.x = owner.loadXML.screenWidth/4 - ball_mc.width - 1;
			}
			if (ball_mc.hitTestObject(paddle_mc))
			{
				yDirection *=  -1;
				//ball_mc.ball.play();
				//paddle_mc.play();
				ball_mc.y = paddle_mc.y - ball_mc.height;
				checkHitLocation(paddle_mc);
			}
			if (ball_mc.hitTestObject(enemy_mc))
			{
				yDirection *=  -1;
				//ball_mc.ball.play();
				//enemy_mc.play();
				ball_mc.y = enemy_mc.y + enemy_mc.height;
				checkHitLocation(enemy_mc);
			}
			if (ball_mc.y <= 0)
			{
				resetBallPosition();
				playerScore++;
				showScore();
			}
			else if (ball_mc.y >= owner.loadXML.screenHeight - ball_mc.height)
			{
				resetBallPosition();
				enemyScore++;
				showScore();
			}
			ball_mc.x +=  xDirection * ballSpeed;
			ball_mc.y +=  yDirection * ballSpeed;
		}

		private function movePaddle(e:TuioTouchEvent)
		{
			closeoutTimer.reset();
			closeoutTimer.start();
		}

		private function resetBallPosition()
		{
			yDirection = 20;
			xDirection = -20;
			ball_mc.gotoAndStop(1);
			ball_mc.x = paddle_mc.x + paddle_mc.width / 2 - ball_mc.width / 2;
			ball_mc.y = paddle_mc.y - paddle_mc.height -ball_mc.height - 1;
		}

		private function checkHitLocation(paddle_mc:MovieClip)
		{
			var hitPercent:Number;
			var ballPosition:Number = ball_mc.x - paddle_mc.x;
			hitPercent = (ballPosition/(paddle_mc.width - ball_mc.width)) - .5;
			xDirection = hitPercent * 30;
			yDirection *=  1.01;
		}

		private function moveEnemy(e:Event)
		{
			var enemytargetX:Number = ball_mc.x - enemy_mc.width / 2;
			if (enemytargetX <= 0)
			{
				enemytargetX = 0;
			}
			else if (enemytargetX >= owner.loadXML.screenWidth/4 - enemy_mc.width)
			{
				enemytargetX = owner.loadXML.screenWidth/4 - enemy_mc.width;
			}
			enemy_mc.x +=  (enemytargetX - enemy_mc.x) / easing;
		}

		private function showScore()
		{
			/*if(playerScore >= winScore) {
			Tweener.addTween(win_mc, {x:400, y:400, time:3, transition:"easeIn"});
			endGame();
			}else if(enemyScore >= winScore) {
			Tweener.addTween(lose_mc, {x:350, y:400, time:3, transition:"easeIn"});
			endGame();
			}*/
			scorePaddleText.htmlText = String(playerScore);
			scoreEnemyText.htmlText = String(enemyScore);
		}

		private function endGame()
		{
			//background_mc.buttonMode = true;
			addEventListener(TuioTouchEvent.TOUCH_DOWN, startGame);
			removeEventListener(TuioTouchEvent.TOUCH_DOWN, movePaddle);
			enemy_mc.removeEventListener(Event.ENTER_FRAME, moveEnemy);
			ball_mc.removeEventListener(Event.ENTER_FRAME, moveBall);
		}
		
		private function setTextFormat(f:TextLayoutFormat, s:Number, c:uint, a:Number = 0, lh:Number = 0, r:Boolean = true, su:Number = 0):void
		{
			f.alignmentBaseline = TextBaseline.ASCENT;
			f.dominantBaseline = TextBaseline.ASCENT;
			f.firstBaselineOffset = "ascent";
			
			if (lh != 0)
			{
				/*if (owner.leftToRight == false)
				{
					lh -= lh * .3;
				}*/
				f.lineHeight = lh;
			}
			
			//f.locale = owner.locale;
			if (r)
			{
				//f.fontFamily = gotham.fontName;
			}
			else
			{
				//f.fontFamily = gothamBold.fontName;
			}
				
			//if (owner.leftToRight == false)
			//{
				f.textAlign = TextAlign.RIGHT;
				//f.fontFamily = arabicRiyadh.fontName;
				f.fontSize = s + su;
				f.color = c;
				//textDirection = "flashx.textLayout.formats.Direction.RTL";
				//f.paddingTop = 20;
				//f.paragraphSpaceBefore = 100;
			//}
			
			/*else
			{
				f.textAlign = TextAlign.LEFT;
				f.fontSize = s;
				f.color = c;
				textDirection = "flashx.textLayout.formats.Direction.LTR";
			}*/
			
			
			switch (a)
			{
				case 0:
				break;
				case 1:
				f.textAlign = TextAlign.LEFT;
				break;
				case 2:
				f.textAlign = TextAlign.CENTER;
				break;
				case 3:
				f.textAlign = TextAlign.RIGHT;
				break;
			}
		}
		
		private function setText(t:TLFTextField, o:MovieClip, c:uint, s:String, f:TextLayoutFormat, tf:TextFlow, offset:Number = 0, textBelow:Boolean = false, textBelowBuffer:Number = 0, replacementY:Number = 0, tHeight:Number = 0):void
		{
			t.selectable = false;
			//t.direction = textDirection;
			t.wordWrap = true;
			t.multiline = true;
			t.textColor = c;
			t.mouseChildren = false;
			t.mouseEnabled = false;
			
			if (o != null)
			{
				t.width = o.width;
				t.height = o.height; 
				t.x = o.x;
				if (!textBelow)
				{
					t.y = o.y - offset;
					t.verticalAlign = flashx.textLayout.formats.VerticalAlign.MIDDLE;
				}
				else
				{
					t.y = o.y + o.height + textBelowBuffer;
					t.verticalAlign = flashx.textLayout.formats.VerticalAlign.TOP;
				}
			}
			else
			{
				t.verticalAlign = flashx.textLayout.formats.VerticalAlign.TOP;
				t.width = 1680;
				t.height = tHeight;
				t.x = 0;
				t.y = replacementY;
				
				//if (owner.leftToRight == false)
				//{
					//t.y += 10;
				//}
			}
			
			
			
			t.htmlText = s;
			
			tf = t.textFlow;
			tf.hostFormat = f;
	 		tf.flowComposer.updateAllControllers();
		}
		
		private function setTextFlow(tf:TextFlow, t:TLFTextField, f:TextLayoutFormat):void
		{
			tf = t.textFlow;
			tf.hostFormat = f;
	 		tf.flowComposer.updateAllControllers();
		}
	}
}
