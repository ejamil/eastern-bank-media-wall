﻿package code 
{
	
	import flash.display.MovieClip;
	
	import org.tuio.*;
	import org.tuio.adapters.*;
	import org.tuio.gestures.*;
	import org.tuio.debug.TuioDebug;
	import org.tuio.TuioManager;
	import org.tuio.connectors.*;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	
	
	public class CrGm extends MovieClip 
	{		
		public var score:Number = 0;
		
		public var whichAnswer:Number;
		public var correctAnswer:Number;
		public var whichPlayer:Number;
		public var thisCorrect:Boolean = false;
		public var didNotAnswer:Boolean = true;
		
		public var whichQuest:Number = 1;
		
		public var owner:CreditGame;
		
		public var whichOne:String;
		
		public var player1Score:Number = 3; //between 1 and 5
		public var player2Score:Number = 3; //between 1 and 5
		
		public var midX:Number;
		public var midY:Number;
		
		public var placeHolder:Boolean = false;
		
		public var exitBtn:CloseBlue;
		
		public var l:Number = 0;
		
		public var whatSound:Number;
		
		public function CrGm(myOwner:CreditGame, s:String) 
		{
			owner = myOwner;
			if (owner.whatLang == "english")
			{
				l = 0;
			}
			else
			{
				l = 1;
			}
			whichOne = s;
			
		}
		
		public function addExitTop():void
		{
			exitBtn = new CloseBlue();
			addChild(exitBtn);
			setChildIndex(exitBtn, numChildren - 1);
			exitBtn.x = 1080 - exitBtn.width;
			exitBtn.addEventListener(TuioTouchEvent.TOUCH_DOWN, exitGame);
		}
		
		public function setExitTop():void
		{
			setChildIndex(exitBtn, numChildren - 1);
		}
		
		
		public function exitGame(e:TuioTouchEvent = null):void
		{
			//playerGo = false;
			this.visible = false;
			//SoundAS.stopAll();
			didNotAnswer = true;
			stopTweens();
			owner.tryToRemoveThis(this);
		}
		
		public function calc():void
		{
			
		}
		
		public function setScore(n:Number):void
		{
			trace("setScore");
			thisCorrect = false;
			switch (n)
			{
				case 5:
					if (q1.currentFrame == 2)
					{
						score -= 1;
						thisCorrect = false;
						owner.playResultNoise(2);
						whatSound = 2;
					}
					if (q2.currentFrame == 2)
					{
						score -= 1;
						thisCorrect = false;
						owner.playResultNoise(2);
						whatSound = 2;
					}
					if (q3.currentFrame == 2)
					{
						//trace("****************************************** CORRECT");
						score += 1;
						thisCorrect = true;
						owner.playResultNoise(1);
						whatSound = 1;
					}
					if (q4.currentFrame == 2)
					{
						score -= 1;
						thisCorrect = false;
						owner.playResultNoise(2);
						whatSound = 2;
					}
				break;
				case 8:
					if (q1.currentFrame == 2)
					{
						score -= 1;
						thisCorrect = false;
						owner.playResultNoise(2);
						whatSound = 2;
					}
					if (q2.currentFrame == 2)
					{
						//trace("****************************************** CORRECT");
						score += 1;
						thisCorrect = true;
						owner.playResultNoise(1);
						whatSound = 1;
					}
					if (q3.currentFrame == 2)
					{
						score -= 1;
						thisCorrect = false;
						owner.playResultNoise(2);
						whatSound = 2;
					}
					if (q4.currentFrame == 2)
					{
						score -= 1;
						thisCorrect = false;
						owner.playResultNoise(2);
						whatSound = 2;
					}
				break;
				case 11:
					if (q1.currentFrame == 2)
					{
						score -= 1;
						thisCorrect = false;
						owner.playResultNoise(2);
						whatSound = 2;
					}
					if (q2.currentFrame == 2)
					{
						//trace("****************************************** CORRECT");
						score += 1;
						thisCorrect = true;
						owner.playResultNoise(1);
						whatSound = 1;
					}
					if (q3.currentFrame == 2)
					{
						score -= 1;
						thisCorrect = false;
						owner.playResultNoise(2);
						whatSound = 2;
					}
					if (q4.currentFrame == 2)
					{
						score -= 1;
						thisCorrect = false;
						owner.playResultNoise(2);
						whatSound = 2;
					}
				break;
				case 14:
					if (q1.currentFrame == 2)
					{
						score -= 1;
						thisCorrect = false;
						owner.playResultNoise(2);
						whatSound = 2;
					}
					if (q2.currentFrame == 2)
					{
						score -= 1;
						thisCorrect = false;
						owner.playResultNoise(2);
						whatSound = 2;
					}
					if (q3.currentFrame == 2)
					{
						score -= 1;
						thisCorrect = false;
						owner.playResultNoise(2);
						whatSound = 2;
					}
					if (q4.currentFrame == 2)
					{
						//trace("****************************************** CORRECT");
						score += 1;
						thisCorrect = true;
						owner.playResultNoise(1);
						whatSound = 1;
					}
				break;
				case 17:
					if (q1.currentFrame == 2)
					{
						//trace("****************************************** CORRECT");
						score += 1;
						thisCorrect = true;
						owner.playResultNoise(1);
						whatSound = 1;
					}
					if (q2.currentFrame == 2)
					{
						score -= 1;
						thisCorrect = false;
						owner.playResultNoise(2);
						whatSound = 2;
					}
					if (q3.currentFrame == 2)
					{
						score -= 1;
						thisCorrect = false;
						owner.playResultNoise(2);
						whatSound = 2;
					}
					if (q4.currentFrame == 2)
					{
						score -= 1;
						thisCorrect = false;
						owner.playResultNoise(2);
						whatSound = 2;
					}
				break;
				case 22:
					switch (whichQuest)
					{
						case 1:
							questTwo();
						break;
						
						case 2:
							questThree();
						break;
						
						case 3:
							
						break;
				}
				break;
				
				default:
					trace("score set on wrong frame");
				break;
			}
			trace(whichOne + " score = " + score);
			trace(whichOne + " thisCorrect = " + thisCorrect);
			//resetQuestions();
		}
		
		public function hideMeters(both:Boolean = true):void
		{
			if (both == true)
			{
				rW1.visible = false;
				rW2.visible = false;
			}
			else
			{
				rW2.visible = false;
			}
		}
		
		public function showMeters():void
		{
			rW1.visible = true;
			rW2.visible = true;
			rW1.rw.visible = false;
			rW2.rw.visible = false;
			rW2.n.text = "2";
			rW1.mid.gotoAndStop(1);
			rW2.mid.gotoAndStop(1);
			rW1.m.gotoAndStop(1);
			rW2.m.gotoAndStop(1);
			TweenMax.to(rW1.p, 0, {tint:0x0096B2});
			TweenMax.to(rW2.p, 0, {tint:0x0096B2});
			TweenMax.to(rW1.n, 0, {tint:0x0096B2});
			TweenMax.to(rW2.n, 0, {tint:0x0096B2});
		}
		//player 1 score, player 2 score, player 1 correct, player 2 correct, player 1 didn't answer, player 2 didn't answer, just calculate score
		public function setMeters(a:Number = 0, b:Number = 0, c:Boolean = false, d:Boolean = false, e:Boolean = true, f:Boolean = true, part2:Boolean = false):void 
		{
			//trace("a = " + a + ", b = " + b + ", c = " + c + ", d = " + d + ", e = " + e + ", f = " + f);
			//a = player 1's score
			//b = player 2's score
			
			if (a >= 12)
			{
				player1Score = 1;
			}
			if (a >= 4 && a < 12)
			{
				player1Score = 2;
			}
			if (a >= -3 && a < 4)
			{
				player1Score = 3;
			}
			if (a >= -11 && a < -3)
			{
				player1Score = 4;
			}
			if (a < -11)
			{
				player1Score = 5;
			}
			
			if (b >= 12)
			{
				player2Score = 1;
			}
			if (b >= 4 && b < 12)
			{
				player2Score = 2;
			}
			if (b >= -3 && b < 4)
			{
				player2Score = 3;
			}
			if (b >= -11 && b < -3)
			{
				player2Score = 4;
			}
			if (b < -11)
			{
				player2Score = 5;
			}
			
			owner.player1ScoreFrame = player1Score;
			owner.player2ScoreFrame = player2Score;
			
			//trace("player 1 score, player 2 score = " + player1Score + ", " + player2Score);
			
			//player1Score = a;
			//player2Score = b;
			
			//rW1.met.b1.gotoAndStop(1);
			//rW1.met.b2.gotoAndStop(3);
			//rW1.met.b3.gotoAndStop(5);
			//rW1.met.b4.gotoAndStop(7);
			//rW1.met.b5.gotoAndStop(9);
			if (part2 == false)
			{
				rW1.met.gotoAndStop(1);
				rW1.met.dot1.visible = false;
				rW1.met.dot2.visible = false;
				rW1.met.dot3.visible = false;
				rW1.met.dot4.visible = false;
				rW1.met.dot5.visible = false;
				
				switch (player1Score)
				{
					case 1:
						//rW1.met.b1.gotoAndStop(2);
						rW1.met.dot1.visible = true;
						rW1.met.play();
					break;
					case 2:
						//rW1.met.b2.gotoAndStop(4);
						rW1.met.dot2.visible = true;
						rW1.met.play();
					break;
					case 3:
						//rW1.met.b3.gotoAndStop(6);
						rW1.met.dot3.visible = true;
						rW1.met.play();
					break;
					case 4:
						//rW1.met.b4.gotoAndStop(8);
						rW1.met.dot4.visible = true;
						rW1.met.play();
					break;
					case 5:
						//rW1.met.b5.gotoAndStop(10);
						rW1.met.dot5.visible = true;
						rW1.met.play();
					break;
				}
				
				//rW2.met.b1.gotoAndStop(1);
				//rW2.met.b2.gotoAndStop(3);
				//rW2.met.b3.gotoAndStop(5);
				//rW2.met.b4.gotoAndStop(7);
				//rW2.met.b5.gotoAndStop(9);
				rW2.met.gotoAndStop(1);
				rW2.met.dot1.visible = false;
				rW2.met.dot2.visible = false;
				rW2.met.dot3.visible = false;
				rW2.met.dot4.visible = false;
				rW2.met.dot5.visible = false;
				
				switch (player2Score)
				{
					case 1:
						//rW2.met.b1.gotoAndStop(2);
						rW2.met.dot1.visible = true;
						rW2.met.play();
					break;
					case 2:
						//rW2.met.b2.gotoAndStop(4);
						rW2.met.dot2.visible = true;
						rW2.met.play();
					break;
					case 3:
						//rW2.met.b3.gotoAndStop(6);
						rW2.met.dot3.visible = true;
						rW2.met.play();
					break;
					case 4:
						//rW2.met.b4.gotoAndStop(8);
						rW2.met.dot4.visible = true;
						rW2.met.play();
					break;
					case 5:
						//rW2.met.b5.gotoAndStop(10);
						rW2.met.dot5.visible = true;
						rW2.met.play();
					break;
				}
				
				rW1.m.gotoAndStop(player1Score + 1);
				rW2.m.gotoAndStop(player2Score + 1);
				rW1.mid.gotoAndStop(player1Score + 1);
				rW2.mid.gotoAndStop(player2Score + 1);
				
				
				
				if (c == true)
				{
					rW1.rw.gotoAndStop(1);
				}
				else
				{
					rW1.rw.gotoAndStop(2);
				}
				if (d == true)
				{
					rW2.rw.gotoAndStop(1);
				}
				else
				{
					rW2.rw.gotoAndStop(2);
				}
				
				
				if (e == false)
				{
					//trace("e == false");
					rW1.rw.visible = true;
					TweenMax.to(rW1.p, 0, {tint:0xffffff});
					TweenMax.to(rW1.n, 0, {tint:0xffffff});
				}
				else
				{
					rW1.rw.visible = false;
					rW1.mid.gotoAndStop(1);
				}
				if (f == false)
				{
					rW2.rw.visible = true;
					TweenMax.to(rW2.p, 0, {tint:0xffffff});
					TweenMax.to(rW2.n, 0, {tint:0xffffff});
				}
				else
				{
					rW2.rw.visible = false;
					rW2.mid.gotoAndStop(1);
				}
			}
		}
		
		public function initMeters():void
		{
			/*rW1.met.b1.gotoAndStop(1);
			rW1.met.b2.gotoAndStop(3);
			rW1.met.b3.gotoAndStop(6);
			rW1.met.b4.gotoAndStop(7);
			rW1.met.b5.gotoAndStop(9);
			rW2.met.b1.gotoAndStop(1);
			rW2.met.b2.gotoAndStop(3);
			rW2.met.b3.gotoAndStop(6);
			rW2.met.b4.gotoAndStop(7);
			rW2.met.b5.gotoAndStop(9);*/
			rW1.met.gotoAndStop(1);
			rW1.met.dot1.visible = false;
			rW1.met.dot2.visible = false;
			rW1.met.dot3.visible = false;
			rW1.met.dot4.visible = false;
			rW1.met.dot5.visible = false;
			rW2.met.gotoAndStop(1);
			rW2.met.dot1.visible = false;
			rW2.met.dot2.visible = false;
			rW2.met.dot3.visible = false;
			rW2.met.dot4.visible = false;
			rW2.met.dot5.visible = false;
			rW1.m.gotoAndStop(1);
			rW2.m.gotoAndStop(1);
			if (whichPlayer == 1)
			{
				rW1.m.visible = true;
				rW2.m.visible = false;
			}
			if (whichPlayer == 2)
			{
				rW1.m.visible = false;
				rW2.m.visible = true;
			}
		}
		
		public function showScore():void
		{
			if (sc != null)
			{
				sc.text = String(score);
			}
		}
		
		public function hideQ():void
		{
			//trace("hideQ");
			if (q1 != null)
			{
				q1.visible = false;
				q2.visible = false;
				q3.visible = false;
				q4.visible = false;
			}
		}
		
		public function showQ():void
		{
			//trace("showQ");
			if (q1 != null)
			{
				//trace("q1 != null");
				q1.visible = true;
				q2.visible = true;
				q3.visible = true;
				q4.visible = true;
				
				q1.ans.mouseChildren = false;
				q1.ans.mouseEnabled = false;
				q2.ans.mouseChildren = false;
				q2.ans.mouseEnabled = false;
				q3.ans.mouseChildren = false;
				q3.ans.mouseEnabled = false;
				q4.ans.mouseChildren = false;
				q4.ans.mouseEnabled = false;
			}
		}
		
		public function resetQuestions():void
		{
			if (q1 != null)
			{
				q1.gotoAndStop(1);
				q2.gotoAndStop(1);
				q3.gotoAndStop(1);
				q4.gotoAndStop(1);
				didNotAnswer = true;
				if (placeHolder == true)
				{
					didNotAnswer = true;
				}
			}
		}
		
		public function addQuestListeners():void
		{
			if (q1 != null)
			{
				q1.addEventListener(TuioTouchEvent.TOUCH_DOWN, loChosen);
				q2.addEventListener(TuioTouchEvent.TOUCH_DOWN, loChosen);
				q3.addEventListener(TuioTouchEvent.TOUCH_DOWN, loChosen);
				q4.addEventListener(TuioTouchEvent.TOUCH_DOWN, loChosen);
			}
		}
		
		public function removeQuestListeners():void
		{
			if (q1 != null)
			{
				q1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, loChosen);
				q2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, loChosen);
				q3.removeEventListener(TuioTouchEvent.TOUCH_DOWN, loChosen);
				q4.removeEventListener(TuioTouchEvent.TOUCH_DOWN, loChosen);
			}
		}
		
		
		private function loChosen(e:TuioTouchEvent):void
		{
			removeQuestListeners();
			//trace("loChosen");
			didNotAnswer = false;
			//trace("DID NOT ANSWER = " + didNotAnswer);
			q1.gotoAndStop(1);
			q2.gotoAndStop(1);
			q3.gotoAndStop(1);
			q4.gotoAndStop(1);
			e.currentTarget.gotoAndStop(2);
			owner.playClick();
			setScore(currentFrame);
			owner.bothHaveAnswered(whatSound);
			switch (e.currentTarget.name)
			{
				case "q1":
				
				break;
				
				case "q2":
				
				break;
				
				case "q3":
				
				break;
				
				case "q4":
				
				break;
			}
		}
		
		public function resetGs():void
		{
			if (c1 != null)
			{
				c1.gotoAndStop(1);
				c2.gotoAndStop(1);
				c3.gotoAndStop(1);
				c4.gotoAndStop(1);
				c5.gotoAndStop(1);
				c6.gotoAndStop(1);
				c7.gotoAndStop(1);
				i1.gotoAndStop(1);
				i2.gotoAndStop(1);
				i3.gotoAndStop(1);
				i4.gotoAndStop(1);
				i5.gotoAndStop(1);
				i6.gotoAndStop(1);
				i7.gotoAndStop(1);
			}
		}
		
		public function addGListeners():void
		{
			c1.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			c2.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			c3.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			c4.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			c5.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			c6.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			i1.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			i2.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			i3.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			i4.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			i5.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			i6.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			resetGs();
			mid.t.text = owner.owner.loadXML.lifeR2Question1[l];
			mid.q.text = owner.owner.loadXML.lifeR1Question[l] + " 1/3:";
			c1.t.text = owner.owner.loadXML.lifeR2Q1Answer1[l];
			c2.t.text = owner.owner.loadXML.lifeR2Q1Answer2[l];
			c3.t.text = owner.owner.loadXML.lifeR2Q1Answer3[l];
			c4.t.text = owner.owner.loadXML.lifeR2Q1Answer4[l];
			c5.t.text = owner.owner.loadXML.lifeR2Q1Answer5[l];
			c6.t.text = owner.owner.loadXML.lifeR2Q1Answer6[l];
			c7.t.text = "";
			i1.t.text = owner.owner.loadXML.lifeR2Q1Answer7[l];
			i2.t.text = owner.owner.loadXML.lifeR2Q1Answer8[l];
			i3.t.text = owner.owner.loadXML.lifeR2Q1Answer9[l];
			i4.t.text = owner.owner.loadXML.lifeR2Q1Answer10[l];
			i5.t.text = owner.owner.loadXML.lifeR2Q1Answer11[l];
			i6.t.text = owner.owner.loadXML.lifeR2Q1Answer12[l];
			i7.t.text = "";
			
			
			t1.text = owner.owner.loadXML.lifeMainTitle1[l];
			t2.text = owner.owner.loadXML.lifeMainTitle2[l];
			
			/*mid.t.text = "A Good Credit Score Will Get You...";
			mid.q.text = "Question 1:";
			c1.t.text = "Low interest rates";
			c2.t.text = "Loan approval";
			c3.t.text = "Lower insurance premiums";
			c4.t.text = "Better utility plans";
			c5.t.text = "Hassle-free rental approvals";
			c6.t.text = "Higher credit card limits";
			c7.t.text = "";
			i1.t.text = "High interest rates";
			i2.t.text = "no loans";
			i3.t.text = "Large outstanding debt";
			i4.t.text = "Lower credit card limits";
			i5.t.text = "Difficulty borrowing money";
			i6.t.text = "Less negotiating power";
			i7.t.text = "";*/
			
			//trace("owner.owner.loadXML.credit2Speed = " + owner.owner.loadXML.credit2Speed);
			
			c1.x = c2.x = c3.x = c4.x = c5.x = c6.x = c7.x = i1.x = i2.x = i3.x = i4.x = i5.x = i6.x = i7.x = mid.x;
			c1.y = c2.y = c3.y = c4.y = c5.y = c6.y = c7.y = i1.y = i2.y = i3.y = i4.y = i5.y = i6.y = i7.y = mid.y;
			c1.scaleX = c1.scaleY = c2.scaleX = c2.scaleY = c3.scaleX = c3.scaleY = c4.scaleX = c4.scaleY = c5.scaleX = c5.scaleY = c6.scaleX = c6.scaleY = c7.scaleX = c7.scaleY = i1.scaleX = i1.scaleY = i2.scaleX = i2.scaleY = i3.scaleX = i3.scaleY = i4.scaleX = i4.scaleY = i5.scaleX = i5.scaleY = i6.scaleX = i6.scaleY = i7.scaleX = i7.scaleY = .1;
			c1.alpha = c2.alpha = c3.alpha = c4.alpha = c5.alpha = c6.alpha = c7.alpha = i1.alpha = i2.alpha = i3.alpha = i4.alpha = i5.alpha = i6.alpha = i7.alpha = 0;
			
			midX = mid.x;
			midY = mid.y;
			
			//mid.x = -mid.width;
			mid.scaleX = .1;
			mid.scaleY = .1;
			TweenMax.to(mid, 1, {scaleX:.5, scaleY:.5, ease:Elastic.easeOut});
			TweenMax.to(c1, owner.owner.loadXML.credit2Speed, {delay:.5, alpha:1, scaleX:1, scaleY:1, x:550, y:-170, ease:Linear.easeNone});
			TweenMax.to(i1, owner.owner.loadXML.credit2Speed, {delay:1, alpha:1, scaleX:1, scaleY:1, x:940, y:2090, ease:Linear.easeNone});
			TweenMax.to(c2, owner.owner.loadXML.credit2Speed, {delay:1.5, alpha:1, scaleX:1, scaleY:1, x:-240, y:150, ease:Linear.easeNone});
			TweenMax.to(i2, owner.owner.loadXML.credit2Speed, {delay:2, alpha:1, scaleX:1, scaleY:1, x:300, y:-170, ease:Linear.easeNone});
			TweenMax.to(c3, owner.owner.loadXML.credit2Speed, {delay:2.5, alpha:1, scaleX:1, scaleY:1, x:1330, y:1880, ease:Linear.easeNone});
			TweenMax.to(i3, owner.owner.loadXML.credit2Speed, {delay:3, alpha:1, scaleX:1, scaleY:1, x:1330, y:150, ease:Linear.easeNone});
			TweenMax.to(c4, owner.owner.loadXML.credit2Speed, {delay:3.5, alpha:1, scaleX:1, scaleY:1, x:-240, y:1880, ease:Linear.easeNone});
			TweenMax.to(i4, owner.owner.loadXML.credit2Speed, {delay:4, alpha:1, scaleX:1, scaleY:1, x:550, y:2090, ease:Linear.easeNone});
			TweenMax.to(c5, owner.owner.loadXML.credit2Speed, {delay:4.5, alpha:1, scaleX:1, scaleY:1, x:-50, y:2090, ease:Linear.easeNone});
			TweenMax.to(i5, owner.owner.loadXML.credit2Speed, {delay:5, alpha:1, scaleX:1, scaleY:1, x:-50, y:-170, ease:Linear.easeNone});
			TweenMax.to(c6, owner.owner.loadXML.credit2Speed, {delay:5.5, alpha:1, scaleX:1, scaleY:1, x:300, y:2090, ease:Linear.easeNone});
			TweenMax.to(i6, owner.owner.loadXML.credit2Speed, {delay:6, alpha:1, scaleX:1, scaleY:1, x:940, y:-170, ease:Linear.easeNone, onComplete:setScore, onCompleteParams:[22]});
			TweenMax.to(mid, .5, {delay:owner.owner.loadXML.credit2Speed + 5.5, scaleX:.01, scaleY:.01});
		}
		
		
		public function setQText(n:Number):void
		{
			switch (n)
			{
				case 1:
					//trace("setQText =case 1");
					q.text = owner.owner.loadXML.lifeR1Question[l] + " 1/5:";
					quest.text = owner.owner.loadXML.lifeR1Question1[l];
					q1.ans.text = owner.owner.loadXML.lifeR1Q1Answer1[l];
					q2.ans.text = owner.owner.loadXML.lifeR1Q1Answer2[l];
					q3.ans.text = owner.owner.loadXML.lifeR1Q1Answer3[l];
					q4.ans.text = owner.owner.loadXML.lifeR1Q1Answer4[l];
					t1.text = owner.owner.loadXML.lifeMainTitle1[l];
					t2.text = owner.owner.loadXML.lifeMainTitle2[l];
					rW1.p.text = owner.owner.loadXML.lifePlayer[l];
					rW2.p.text = owner.owner.loadXML.lifePlayer[l];
				break;
				
				case 2:
					//trace("setQText =case 2");
					q.text = owner.owner.loadXML.lifeR1Question[l] + " 2/5:";
					quest.text = owner.owner.loadXML.lifeR1Question2[l];
					q1.ans.text = owner.owner.loadXML.lifeR1Q2Answer1[l];
					q2.ans.text = owner.owner.loadXML.lifeR1Q2Answer2[l];
					q3.ans.text = owner.owner.loadXML.lifeR1Q2Answer3[l];
					q4.ans.text = owner.owner.loadXML.lifeR1Q2Answer4[l];
					t1.text = owner.owner.loadXML.lifeMainTitle1[l];
					t2.text = owner.owner.loadXML.lifeMainTitle2[l];
				break;
				
				case 3:
					//trace("setQText =case 3");
					q.text = owner.owner.loadXML.lifeR1Question[l] + " 3/5:";
					quest.text = owner.owner.loadXML.lifeR1Question3[l];
					q1.ans.text = owner.owner.loadXML.lifeR1Q3Answer1[l];
					q2.ans.text = owner.owner.loadXML.lifeR1Q3Answer2[l];
					q3.ans.text = owner.owner.loadXML.lifeR1Q3Answer3[l];
					q4.ans.text = owner.owner.loadXML.lifeR1Q3Answer4[l];
					t1.text = owner.owner.loadXML.lifeMainTitle1[l];
					t2.text = owner.owner.loadXML.lifeMainTitle2[l];
				break;
				
				case 4:
					//trace("setQText =case 4");
					q.text = owner.owner.loadXML.lifeR1Question[l] + " 4/5:";
					quest.text = owner.owner.loadXML.lifeR1Question4[l];
					q1.ans.text = owner.owner.loadXML.lifeR1Q4Answer1[l];
					q2.ans.text = owner.owner.loadXML.lifeR1Q4Answer2[l];
					q3.ans.text = owner.owner.loadXML.lifeR1Q4Answer3[l];
					q4.ans.text = owner.owner.loadXML.lifeR1Q4Answer4[l];
					t1.text = owner.owner.loadXML.lifeMainTitle1[l];
					t2.text = owner.owner.loadXML.lifeMainTitle2[l];
				break;
				
				case 5:
					//trace("setQText =case 5");
					q.text = owner.owner.loadXML.lifeR1Question[l] + " 5/5:";
					quest.text = owner.owner.loadXML.lifeR1Question5[l];
					q1.ans.text = owner.owner.loadXML.lifeR1Q5Answer1[l];
					q2.ans.text = owner.owner.loadXML.lifeR1Q5Answer2[l];
					q3.ans.text = owner.owner.loadXML.lifeR1Q5Answer3[l];
					q4.ans.text = owner.owner.loadXML.lifeR1Q5Answer4[l];
					t1.text = owner.owner.loadXML.lifeMainTitle1[l];
					t2.text = owner.owner.loadXML.lifeMainTitle2[l];
				break;
				
				case 6:
					//trace("setQText =case 6");
					q.text = owner.owner.loadXML.lifeR1Question[l] + " 1/5:";
					quest.text = owner.owner.loadXML.lifeR1Question1[l];
					ans.text = owner.owner.loadXML.lifeR1Q1CorrectAnswer[l];
					inf.text = owner.owner.loadXML.lifeR1Q1AnswerExplanation1[l] + "\n" + owner.owner.loadXML.lifeR1Q1AnswerExplanation2[l];
					t1.text = owner.owner.loadXML.lifeMainTitle1[l];
					t2.text = owner.owner.loadXML.lifeMainTitle2[l];
				break;
				
				case 7:
					//trace("setQText =case 7");
					q.text = owner.owner.loadXML.lifeR1Question[l] + " 2/5:";
					quest.text = owner.owner.loadXML.lifeR1Question2[l];
					ans.text = owner.owner.loadXML.lifeR1Q2CorrectAnswer[l];
					inf.text = owner.owner.loadXML.lifeR1Q2AnswerExplanation1[l] + "\n" + owner.owner.loadXML.lifeR1Q2AnswerExplanation2[l];
					t1.text = owner.owner.loadXML.lifeMainTitle1[l];
					t2.text = owner.owner.loadXML.lifeMainTitle2[l];
				break;
				
				case 8:
					//trace("setQText =case 8");
					q.text = owner.owner.loadXML.lifeR1Question[l] + " 3/5:";
					quest.text = owner.owner.loadXML.lifeR1Question3[l];
					ans.text = owner.owner.loadXML.lifeR1Q3CorrectAnswer[l];
					inf.text = owner.owner.loadXML.lifeR1Q3AnswerExplanation1[l] + "\n" + owner.owner.loadXML.lifeR1Q3AnswerExplanation2[l] + "\n" + owner.owner.loadXML.lifeR1Q3AnswerExplanation3[l];
					t1.text = owner.owner.loadXML.lifeMainTitle1[l];
					t2.text = owner.owner.loadXML.lifeMainTitle2[l];
				break;
				
				case 9:
					//trace("setQText =case 9");
					q.text = owner.owner.loadXML.lifeR1Question[l] + " 4/5:";
					quest.text = owner.owner.loadXML.lifeR1Question4[l];
					ans.text = owner.owner.loadXML.lifeR1Q4CorrectAnswer[l];
					inf.text = owner.owner.loadXML.lifeR1Q4AnswerExplanation1[l] + "\n" + owner.owner.loadXML.lifeR1Q4AnswerExplanation2[l];
					t1.text = owner.owner.loadXML.lifeMainTitle1[l];
					t2.text = owner.owner.loadXML.lifeMainTitle2[l];
				break;
				
				case 10:
					//trace("setQText =case 10");
					q.text = owner.owner.loadXML.lifeR1Question[l] + " 5/5:";
					quest.text = owner.owner.loadXML.lifeR1Question5[l];
					ans.text = owner.owner.loadXML.lifeR1Q5CorrectAnswer[l];
					inf.text = owner.owner.loadXML.lifeR1Q5AnswerExplanation1[l] + "\n" + owner.owner.loadXML.lifeR1Q5AnswerExplanation2[l] + "\n" + owner.owner.loadXML.lifeR1Q5AnswerExplanation3[l];
					t1.text = owner.owner.loadXML.lifeMainTitle1[l];
					t2.text = owner.owner.loadXML.lifeMainTitle2[l];
				break;
				case 11:
					//trace("setQText =case 11");
					t1.text = owner.owner.loadXML.lifeMainTitle1[l];
					t2.text = owner.owner.loadXML.lifeMainTitle2[l];
					rW1.p.text = owner.owner.loadXML.lifePlayer[l];
					rW2.p.text = owner.owner.loadXML.lifePlayer[l];
				break;
				case 12:
					//trace("setQText =case 12");
					creditEnd.winner.p.text = owner.owner.loadXML.lifePlayer[l];
					creditEnd.loser.p.text = owner.owner.loadXML.lifePlayer[l];
					creditEnd.t1.text = owner.owner.loadXML.lifeMainTitle1[l];
					creditEnd.t2.text = owner.owner.loadXML.lifeMainTitle2[l];
					creditEnd.wins.text = owner.owner.loadXML.lifeWins[l];
					creditEndTie.winner.p.text = owner.owner.loadXML.lifePlayer[l];
					creditEndTie.loser.p.text = owner.owner.loadXML.lifePlayer[l];
					creditEndTie.t1.text = owner.owner.loadXML.lifeMainTitle1[l];
					creditEndTie.t2.text = owner.owner.loadXML.lifeMainTitle2[l];
					creditEndTie.tie.text = owner.owner.loadXML.lifeTies[l];
				break;
				case 13:
					//trace("setQText =case 13");
					creditStairs.ft.t.text = owner.owner.loadXML.dreamCheckOutOther[l];
					creditStairs.t1.text = owner.owner.loadXML.lifeHomeEquity[l];
					creditStairs.t2.text = owner.owner.loadXML.lifeReserveCredit[l];
					creditStairs.t3.text = owner.owner.loadXML.lifeCreditCard[l];
					creditStairs.t4.text = owner.owner.loadXML.lifeCarLoan[l];
					creditStairs.t5.text = owner.owner.loadXML.lifeMortgage[l];
				break;
			}
		}
		
		public function questTwo():void
		{
			whichQuest = 2;
			removeGListeners();
			c1.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			c2.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			c3.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			i1.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			i2.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			i3.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			resetGs();
			mid.t.text = owner.owner.loadXML.lifeR2Question2[l];
			mid.q.text = owner.owner.loadXML.lifeR1Question[l] + " 2/3:";
			c1.t.text = owner.owner.loadXML.lifeR2Q2Answer1[l];
			c2.t.text = owner.owner.loadXML.lifeR2Q2Answer2[l];
			c3.t.text = owner.owner.loadXML.lifeR2Q2Answer3[l];
			c4.t.text = "";
			c5.t.text = "";
			c6.t.text = "";
			c7.t.text = "";
			i1.t.text = owner.owner.loadXML.lifeR2Q2Answer4[l];
			i2.t.text = owner.owner.loadXML.lifeR2Q2Answer5[l];
			i3.t.text = owner.owner.loadXML.lifeR2Q2Answer6[l];
			i4.t.text = "";
			i5.t.text = "";
			i6.t.text = "";
			i7.t.text = "";
			
			c1.x = c2.x = c3.x = c4.x = c5.x = c6.x = c7.x = i1.x = i2.x = i3.x = i4.x = i5.x = i6.x = i7.x = midX;
			c1.y = c2.y = c3.y = c4.y = c5.y = c6.y = c7.y = i1.y = i2.y = i3.y = i4.y = i5.y = i6.y = i7.y = midY;
			c1.scaleX = c1.scaleY = c2.scaleX = c2.scaleY = c3.scaleX = c3.scaleY = c4.scaleX = c4.scaleY = c5.scaleX = c5.scaleY = c6.scaleX = c6.scaleY = c7.scaleX = c7.scaleY = i1.scaleX = i1.scaleY = i2.scaleX = i2.scaleY = i3.scaleX = i3.scaleY = i4.scaleX = i4.scaleY = i5.scaleX = i5.scaleY = i6.scaleX = i6.scaleY = i7.scaleX = i7.scaleY = .1;
			c1.alpha = c2.alpha = c3.alpha = c4.alpha = c5.alpha = c6.alpha = c7.alpha = i1.alpha = i2.alpha = i3.alpha = i4.alpha = i5.alpha = i6.alpha = i7.alpha = 0;
			
			//mid.x = -mid.width;
			TweenMax.to(mid, 1, {scaleX:.5, scaleY:.5, ease:Elastic.easeOut});
			TweenMax.to(c1, owner.owner.loadXML.credit2Speed, {delay:.5, alpha:1, scaleX:1, scaleY:1, x:550, y:-170, ease:Linear.easeNone});
			TweenMax.to(i1, owner.owner.loadXML.credit2Speed, {delay:1, alpha:1, scaleX:1, scaleY:1, x:940, y:2090, ease:Linear.easeNone});
			TweenMax.to(c2, owner.owner.loadXML.credit2Speed, {delay:1.5, alpha:1, scaleX:1, scaleY:1, x:-240, y:150, ease:Linear.easeNone});
			TweenMax.to(i2, owner.owner.loadXML.credit2Speed, {delay:2, alpha:1, scaleX:1, scaleY:1, x:300, y:-170, ease:Linear.easeNone});
			TweenMax.to(c3, owner.owner.loadXML.credit2Speed, {delay:2.5, alpha:1, scaleX:1, scaleY:1, x:1330, y:1880, ease:Linear.easeNone});
			TweenMax.to(i3, owner.owner.loadXML.credit2Speed, {delay:3, alpha:1, scaleX:1, scaleY:1, x:1330, y:150, ease:Linear.easeNone, onComplete:setScore, onCompleteParams:[22]});
			TweenMax.to(mid, .5, {delay:owner.owner.loadXML.credit2Speed + 2.5, scaleX:.01, scaleY:.01});
		}
		
		public function questThree():void
		{
			whichQuest = 3;
			
			removeGListeners();
			c1.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			c2.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			c3.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			c4.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			i1.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			i2.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			i3.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			i4.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			resetGs();
			mid.t.text = owner.owner.loadXML.lifeR2Question3[l];
			mid.q.text = owner.owner.loadXML.lifeR1Question[l] + " 3/3:";
			c1.t.text = owner.owner.loadXML.lifeR2Q3Answer1[l];
			c2.t.text = owner.owner.loadXML.lifeR2Q3Answer2[l];
			c3.t.text = owner.owner.loadXML.lifeR2Q3Answer3[l];
			c4.t.text = owner.owner.loadXML.lifeR2Q3Answer4[l];
			c5.t.text = "";
			c6.t.text = "";
			c7.t.text = "";
			i1.t.text = owner.owner.loadXML.lifeR2Q3Answer5[l];
			i2.t.text = owner.owner.loadXML.lifeR2Q3Answer6[l];
			i3.t.text = owner.owner.loadXML.lifeR2Q3Answer7[l];
			i4.t.text = owner.owner.loadXML.lifeR2Q3Answer8[l];
			i5.t.text = "";
			i6.t.text = "";
			i7.t.text = "";
			
			c1.x = c2.x = c3.x = c4.x = c5.x = c6.x = c7.x = i1.x = i2.x = i3.x = i4.x = i5.x = i6.x = i7.x = midX;
			c1.y = c2.y = c3.y = c4.y = c5.y = c6.y = c7.y = i1.y = i2.y = i3.y = i4.y = i5.y = i6.y = i7.y = midY;
			c1.scaleX = c1.scaleY = c2.scaleX = c2.scaleY = c3.scaleX = c3.scaleY = c4.scaleX = c4.scaleY = c5.scaleX = c5.scaleY = c6.scaleX = c6.scaleY = c7.scaleX = c7.scaleY = i1.scaleX = i1.scaleY = i2.scaleX = i2.scaleY = i3.scaleX = i3.scaleY = i4.scaleX = i4.scaleY = i5.scaleX = i5.scaleY = i6.scaleX = i6.scaleY = i7.scaleX = i7.scaleY = .1;
			c1.alpha = c2.alpha = c3.alpha = c4.alpha = c5.alpha = c6.alpha = c7.alpha = i1.alpha = i2.alpha = i3.alpha = i4.alpha = i5.alpha = i6.alpha = i7.alpha = 0;
			
			//mid.x = -mid.width;
			TweenMax.to(mid, 1, {scaleX:.5, scaleY:.5, ease:Elastic.easeOut});
			TweenMax.to(c1, owner.owner.loadXML.credit2Speed, {delay:.5, alpha:1, scaleX:1, scaleY:1, x:550, y:-170, ease:Linear.easeNone});
			TweenMax.to(i1, owner.owner.loadXML.credit2Speed, {delay:1, alpha:1, scaleX:1, scaleY:1, x:940, y:2090, ease:Linear.easeNone});
			TweenMax.to(c2, owner.owner.loadXML.credit2Speed, {delay:1.5, alpha:1, scaleX:1, scaleY:1, x:-240, y:150, ease:Linear.easeNone});
			TweenMax.to(i2, owner.owner.loadXML.credit2Speed, {delay:2, alpha:1, scaleX:1, scaleY:1, x:300, y:-170, ease:Linear.easeNone});
			TweenMax.to(c3, owner.owner.loadXML.credit2Speed, {delay:2.5, alpha:1, scaleX:1, scaleY:1, x:1330, y:1880, ease:Linear.easeNone});
			TweenMax.to(i3, owner.owner.loadXML.credit2Speed, {delay:3, alpha:1, scaleX:1, scaleY:1, x:1330, y:150, ease:Linear.easeNone});
			TweenMax.to(c4, owner.owner.loadXML.credit2Speed, {delay:3.5, alpha:1, scaleX:1, scaleY:1, x:-240, y:1880, ease:Linear.easeNone});
			TweenMax.to(i4, owner.owner.loadXML.credit2Speed, {delay:4, alpha:1, scaleX:1, scaleY:1, x:550, y:2090, ease:Linear.easeNone, onComplete:setScore, onCompleteParams:[22]});
			TweenMax.to(c5, owner.owner.loadXML.credit2Speed, {delay:5, alpha:1, onComplete:moveOnGame});
			TweenMax.to(mid, .5, {delay:owner.owner.loadXML.credit2Speed + 3.5, scaleX:.01, scaleY:.01, alpha:0});
		}
		
		public function moveOnGame():void
		{
			owner.moveOnGame();
		}
		
		public function stopTweens():void
		{
			if (mid != null)
			{
				TweenMax.killTweensOf(mid);
				TweenMax.killTweensOf(c1);
				TweenMax.killTweensOf(i1);
				TweenMax.killTweensOf(c2);
				TweenMax.killTweensOf(i2);
				TweenMax.killTweensOf(c3);
				TweenMax.killTweensOf(i3);
				TweenMax.killTweensOf(c4);
				TweenMax.killTweensOf(i4);
				TweenMax.killTweensOf(c5);
				TweenMax.killTweensOf(i5);
				TweenMax.killTweensOf(c6);
				TweenMax.killTweensOf(i6);
				TweenMax.killTweensOf(c6);
				TweenMax.killTweensOf(i6);
			}
		}
		
		public function removeGListeners():void
		{
			if (c1 != null)
			{
				c1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
				c2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
				c3.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
				c4.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
				c5.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
				c6.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
				c7.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
				i1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
				i2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
				i3.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
				i4.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
				i5.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
				i6.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
				i7.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			}
			
		}
		
		private function setG(e:TuioTouchEvent):void
		{
			//e.currentTarget.gotoAndStop(2);
			e.currentTarget.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			
			switch (whichQuest)
			{
				case 1:
					if (e.currentTarget == c1 || e.currentTarget == c2 || e.currentTarget == c3 || e.currentTarget == c4 || e.currentTarget == c5 || e.currentTarget == c6)
					{
						e.currentTarget.gotoAndStop(2);
						score++;
						owner.playResultNoise(3);
					}
					if (e.currentTarget == i1 || e.currentTarget == i2 || e.currentTarget == i3 || e.currentTarget == i4 || e.currentTarget == i5 || e.currentTarget == i6)
					{
						e.currentTarget.gotoAndStop(3);
						score--;
						owner.playResultNoise(4);
					}
				break;
				
				case 2:
					if (e.currentTarget == c1 || e.currentTarget == c2 || e.currentTarget == c3)
					{
						e.currentTarget.gotoAndStop(2);
						score++;
						owner.playResultNoise(3);
					}
					if (e.currentTarget == i1 || e.currentTarget == i2 || e.currentTarget == i3)
					{
						e.currentTarget.gotoAndStop(3);
						score--;
						owner.playResultNoise(4);
					}
				break;
				
				case 3:
					if (e.currentTarget == c1 || e.currentTarget == c2 || e.currentTarget == c3 || e.currentTarget == c4)
					{
						e.currentTarget.gotoAndStop(2);
						score++;
						owner.playResultNoise(3);
					}
					if (e.currentTarget == i1 || e.currentTarget == i2 || e.currentTarget == i3 || e.currentTarget == i4)
					{
						e.currentTarget.gotoAndStop(3);
						score--;
						owner.playResultNoise(4);
					}
				break;
			}
			
				//trace("************************************* score = " + score);
		}
	}
	
}
