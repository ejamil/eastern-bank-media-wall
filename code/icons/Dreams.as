﻿package code.icons {
	
	import flash.display.MovieClip;
	
	import com.greensock.TweenMax;
	import com.greensock.TimelineMax;
	import com.greensock.easing.*
	import com.greensock.events.*;
	
	public class Dreams extends MovieClip {
		
		private var playTimeline:TimelineMax;
		private var sWidth:Number;
		private var sHeight:Number;
		private var buffer:Number = 200;
		private var whichWay:Number;
		private var yOffset:Number;
		private var speed1:Number;
		private var speed2:Number;
		private var speed3:Number;
		
		public function Dreams(w:Number, h:Number, s1:Number, s2:Number, s3:Number)
		{
			speed1 = s1;
			speed2 = s2;
			speed3 = s3;
			
			var temp:Number = Math.round(Math.random() * 100);
			if (temp < 50)
			{
				whichWay = 1;
			}
			else
			{
				whichWay  = -1;
			}
			sWidth = w;
			sHeight = h;
			if (whichWay == 1)
			{
				this.x = -this.width;
			}
			else
			{
				this.x = sWidth + this.width;
			}
			setNumbers();
			playTimeline = new TimelineMax();
			playTimeline.repeat = -1;
			if (whichWay == 1)
			{
				playTimeline.appendMultiple([new TweenMax(this, speed1, {x:sWidth, ease:Quad.easeInOut, repeat:-1}),
									 new TweenMax(this, speed2, {y:this.y + yOffset, yoyo:true, repeat:-1, ease:Quad.easeInOut}),
									 new TweenMax(this, speed3, {scaleX:.5, scaleY:.5, yoyo:true, repeat:-1, ease:Quad.easeInOut})]);
			}
			else
			{
				playTimeline.appendMultiple([new TweenMax(this, speed1, {x:-this.width, ease:Quad.easeInOut, repeat:-1}),
									 new TweenMax(this, speed2, {y:this.y + yOffset, yoyo:true, repeat:-1, ease:Quad.easeInOut}),
									 new TweenMax(this, speed3, {scaleX:.5, scaleY:.5, yoyo:true, repeat:-1, ease:Quad.easeInOut})]);
			}
			
			//playTimeline.insertMultiple([(new TweenMax(this, 1, {y:this.y + 30, ease:Quad.easeInOut, yoyo:true, repeat:-1}))])
			
			
			playTimeline.addEventListener(TweenEvent.REPEAT, resetX);
		}
		
		private function setNumbers():void
		{
			this.y = Math.random() * sHeight;
			if (this.y > sHeight - buffer)
			{
				this.y -= buffer;
			}
			if (this.y < buffer)
			{
				this.y += buffer;
			}
			yOffset = Math.random() * 300;
			
			speed1 = speed1 * Math.random() + speed1;
			speed2 = speed2 * Math.random() + speed2;
			speed3 = speed3 * Math.random() + speed3;
		}
		
		private function resetX(e:TweenEvent):void
		{
			setNumbers();
		}
		
		public function pauseThis():void
		{
			playTimeline.pause();
		}
		
		public function playThis():void
		{
			playTimeline.play();
		}
	}
	
}