﻿package  code {
	
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.display.Image;
	import starling.display.Stage;
	import starling.utils.deg2rad;
	import starling.events.Event;
	import starling.core.Starling;
	import flash.display.Bitmap;
	import starling.textures.Texture;
	import starling.animation.Juggler;
	import starling.animation.Tween;
	import starling.animation.Transitions;
	import flash.display.MovieClip;
	import starling.filters.BlurFilter;
    import starling.display.Quad;
    import starling.filters.BlurFilter;
	import starling.extensions.pixelmask.PixelMaskDisplayObject;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	import com.greensock.motionPaths.*;
	import code.Background;
	
	import flash.display.BlendMode;
	
	import flash.display.Loader;
    import flash.geom.Point;
    import flash.geom.Rectangle;
	import flash.xml.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import code.Document;
	
	public class BackgroundContainer extends Sprite
	{
		[Embed(source = "../assets/background/logo.png")]
		private static const LOGO:Class;
		
		[Embed(source = "../assets/background/touch.png")]
		private static const TOUCH:Class;
		
		[Embed(source = "../assets/background/h_bake.png")]
		private static const HBAKE:Class;
		
		[Embed(source = "../assets/background/h_beach.png")]
		private static const HBEACH:Class;
		
		[Embed(source = "../assets/background/h_littleboy.png")]
		private static const HBOY:Class;
		
		[Embed(source = "../assets/background/h_daddaughters.png")]
		private static const HDADDAUGHTERS:Class;
		
		[Embed(source = "../assets/background/h_dog.png")]
		private static const HDOG:Class;
		
		[Embed(source = "../assets/background/h_fishing.png")]
		private static const HFISHING:Class;
		
		[Embed(source = "../assets/background/h_girldog.png")]
		private static const HGIRLDOG:Class;
		
		[Embed(source = "../assets/background/h_girlonrings.png")]
		private static const HGIRLONIONRINGS:Class;
		
		[Embed(source = "../assets/background/h_girls.png")]
		private static const HGIRLS:Class;
		
		[Embed(source = "../assets/background/h_house.png")]
		private static const HHOUSE:Class;
		
		[Embed(source = "../assets/background/h_johnsonkids.png")]
		private static const HJOHNSONKIDS:Class;
		
		[Embed(source = "../assets/background/h_johnsonmom.png")]
		private static const HJOHNSONMOM:Class;
		
		[Embed(source = "../assets/background/h_planting.png")]
		private static const HPLANTING:Class;
		
		[Embed(source = "../assets/background/h_smile.png")]
		private static const HSMILE:Class;
		
		[Embed(source = "../assets/background/speck.png")]
		private static const SPECK:Class;
		
		[Embed(source = "../assets/background/band.png")]
		private static const BAND:Class;
		
		[Embed(source = "../assets/background/star.png")]
		private static const STAR:Class;
		
		[Embed(source = "../assets/background/wave.png")]
		private static const WAVE:Class;
		
		[Embed(source = "../assets/background/b_bg.png")]
		private static const BBG:Class;
		[Embed(source = "../assets/background/d_bg.png")]
		private static const DBG:Class;
		[Embed(source = "../assets/background/h_bg.png")]
		private static const HBG:Class;
		[Embed(source = "../assets/background/l_bg.png")]
		private static const LBG:Class;
		[Embed(source = "../assets/background/p_bg.png")]
		private static const PBG:Class;
		
		
		[Embed(source = "../assets/background/clouds.png")]
		private static const CLOUDS:Class;
		[Embed(source = "../assets/background/bubble.png")]
		private static const BUBBLE:Class;
		[Embed(source = "../assets/background/feathers.png")]
		private static const FEATHERS:Class;
		[Embed(source = "../assets/background/flower.png")]
		private static const FLOWER:Class;
		[Embed(source = "../assets/background/leaves.png")]
		private static const LEAVES:Class;
		
		[Embed(source = "../assets/background/flare.png")]
		private static const FLARE:Class;
		[Embed(source = "../assets/background/flaremove.png")]
		private static const FLAREROT:Class;
		
		[Embed(source = "../assets/background/b_register.png")]
		private static const BREGISTER:Class;
		[Embed(source = "../assets/background/b_clipboard.png")]
		private static const BCLIPBOARD:Class;
		[Embed(source = "../assets/background/b_bike.png")]
		private static const BBIKE:Class;
		[Embed(source = "../assets/background/b_shake.png")]
		private static const BSHAKE:Class;
		[Embed(source = "../assets/background/b_men.png")]
		private static const BMEN:Class;
		[Embed(source = "../assets/background/b_colab.png")]
		private static const BCOLAB:Class;
		[Embed(source = "../assets/background/b_crossed.png")]
		private static const BCROSSED:Class;
		[Embed(source = "../assets/background/b_hardware.png")]
		private static const BHARDWARE:Class;
		[Embed(source = "../assets/background/b_books.png")]
		private static const BBOOKS:Class;
		[Embed(source = "../assets/background/b_plates.png")]
		private static const BPLATES:Class;
		[Embed(source = "../assets/background/b_smile.png")]
		private static const BSMILE:Class;
		[Embed(source = "../assets/background/b_restaurant.png")]
		private static const BRESTAURANT:Class;
		
		[Embed(source = "../assets/background/d_baby.png")]
		private static const DBABY:Class;
		[Embed(source = "../assets/background/d_boys.png")]
		private static const DBOYS:Class;
		[Embed(source = "../assets/background/d_chicks.png")]
		private static const DCHICKS:Class;
		[Embed(source = "../assets/background/d_smile.png")]
		private static const DSMILE:Class;
		[Embed(source = "../assets/background/d_swing.png")]
		private static const DSWING:Class;
		[Embed(source = "../assets/background/d_team.png")]
		private static const DTEAM:Class;
		[Embed(source = "../assets/background/d_ladies.png")]
		private static const DLADIES:Class;
		[Embed(source = "../assets/background/d_plane.png")]
		private static const DPLANE:Class;
		[Embed(source = "../assets/background/d_fly.png")]
		private static const DFLY:Class;
		[Embed(source = "../assets/background/d_look.png")]
		private static const DLOOK:Class;
		[Embed(source = "../assets/background/d_twirl.png")]
		private static const DTWIRL:Class;
		[Embed(source = "../assets/background/d_coffee.png")]
		private static const DCOFFEE:Class;
		[Embed(source = "../assets/background/d_couple.png")]
		private static const DCOUPLE:Class;
		[Embed(source = "../assets/background/d_family.png")]
		private static const DFAMILY:Class;
		[Embed(source = "../assets/background/d_holding.png")]
		private static const DHOLDING:Class;
		[Embed(source = "../assets/background/d_blond.png")]
		private static const DBLOND:Class;
		
		[Embed(source = "../assets/background/business.png")]
		private static const BUSINESS:Class;
		[Embed(source = "../assets/background/dreams.png")]
		private static const DREAMS:Class;
		[Embed(source = "../assets/background/home.png")]
		private static const HOME:Class;
		[Embed(source = "../assets/background/life.png")]
		private static const LIFE:Class;
		[Embed(source = "../assets/background/play.png")]
		private static const PLAY:Class;
		
		[Embed(source = "../assets/background/p_ballpit.png")]
		private static const PBALLPIT:Class;
		[Embed(source = "../assets/background/p_baseball.png")]
		private static const PBASEBALL:Class;
		[Embed(source = "../assets/background/p_bike.png")]
		private static const PBIKE:Class;
		[Embed(source = "../assets/background/p_camera.png")]
		private static const PCAMERA:Class;
		[Embed(source = "../assets/background/p_catch.png")]
		private static const PCATCH:Class;
		[Embed(source = "../assets/background/p_fetch.png")]
		private static const PFETCH:Class;
		[Embed(source = "../assets/background/p_peek.png")]
		private static const PPEEK:Class;
		[Embed(source = "../assets/background/p_shoulders.png")]
		private static const PSHOULDERS:Class;
		[Embed(source = "../assets/background/p_soccer.png")]
		private static const PSOCCER:Class;
		[Embed(source = "../assets/background/p_swim.png")]
		private static const PSWIM:Class;
		[Embed(source = "../assets/background/p_tire.png")]
		private static const PTIRE:Class;
		[Embed(source = "../assets/background/p_toys.png")]
		private static const PTOYS:Class;
		
		
		[Embed(source = "../assets/background/l_baby.png")]
		private static const LBABY:Class;
		[Embed(source = "../assets/background/l_blow.png")]
		private static const LBLOW:Class;
		[Embed(source = "../assets/background/l_garden.png")]
		private static const LGARDEN:Class;
		[Embed(source = "../assets/background/l_grads.png")]
		private static const LGRADS:Class;
		[Embed(source = "../assets/background/l_hike.png")]
		private static const LHIKE:Class;
		[Embed(source = "../assets/background/l_lean.png")]
		private static const LLEAN:Class;
		[Embed(source = "../assets/background/l_lift.png")]
		private static const LLIFT:Class;
		[Embed(source = "../assets/background/l_make.png")]
		private static const LMAKE:Class;
		[Embed(source = "../assets/background/l_marry.png")]
		private static const LMARRY:Class;
		[Embed(source = "../assets/background/l_raise.png")]
		private static const LRAISE:Class;
		[Embed(source = "../assets/background/l_read.png")]
		private static const LREAD:Class;
		[Embed(source = "../assets/background/l_school.png")]
		private static const LSCHOOL:Class;
		
		[Embed(source = "../assets/background/dreamWords1.png")]
		private static const DREAMWORDS1:Class;
		[Embed(source = "../assets/background/dreamWords2.png")]
		private static const DREAMWORDS2:Class;
		
		[Embed(source = "../assets/background/businessWords1.png")]
		private static const BUSINESSWORDS1:Class;
		[Embed(source = "../assets/background/businessWords2.png")]
		private static const BUSINESSWORDS2:Class;
		
		[Embed(source = "../assets/background/homeWords1.png")]
		private static const HOMEWORDS1:Class;
		[Embed(source = "../assets/background/homeWords2.png")]
		private static const HOMEWORDS2:Class;
		
		[Embed(source = "../assets/background/lifeWords1.png")]
		private static const LIFEWORDS1:Class;
		
		[Embed(source = "../assets/background/playWords1.png")]
		private static const PLAYWORDS1:Class;
		
		
		private var dreamWords1:Image;
		private var dreamWords2:Image;
		private var businessWords1:Image;
		private var businessWords2:Image;
		private var homeWords1:Image;
		private var homeWords2:Image;
		private var lifeWords1:Image;
		private var lifeWords2:Image;
		private var playWords1:Image;
		private var playWords2:Image;
		
		private var b_bg:Image;
		private var d_bg:Image;
		private var h_bg:Image;
		private var l_bg:Image;
		private var p_bg:Image;
		private var b2_bg:Image;
		private var d2_bg:Image;
		private var h2_bg:Image;
		private var l2_bg:Image;
		private var p2_bg:Image;
		
		private var touch:Image;
		private var touch2:Image;
		
		private var clouds:Image;
		private var feathers:Image;
		//private var drops:Image;
		private var leaves:Image;
		private var flower:Image;
		
		private var bubble1:Image;
		private var bubble2:Image;
		private var bubble3:Image;
		private var bubble4:Image;
		private var bubble5:Image;
		private var bubble6:Image;
		private var bubble7:Image;
		private var bubble8:Image;
		private var bubble9:Image;
		private var bubble10:Image;
		
		private var flare:Image;
		private var flareRot:Image;
		private var flareRot2:Image;
		
		private var b_bike:Image;
		private var b_clipboard:Image;
		private var b_register:Image;
		private var b_shake:Image;
		private var b_men:Image;
		private var b_colab:Image;
		private var b_crossed:Image;
		private var b_hardware:Image;
		private var b_books:Image;
		private var b_plates:Image;
		private var b_smile:Image;
		private var b_restaurant:Image;
		
		private var d_baby:Image;
		private var d_boys:Image;
		private var d_chicks:Image;
		private var d_smile:Image;
		private var d_swing:Image;
		private var d_team:Image;
		private var d_ladies:Image;
		private var d_plane:Image;
		private var d_fly:Image;
		private var d_look:Image;
		private var d_twirl:Image;
		private var d_coffee:Image;
		private var d_couple:Image;
		private var d_family:Image;
		private var d_holding:Image;
		private var d_blond:Image;
		
		
		private var p_ballpit:Image;
		private var p_baseball:Image;
		private var p_bike:Image;
		private var p_camera:Image;
		private var p_catch:Image;
		private var p_fetch:Image;
		private var p_peek:Image;
		private var p_shoulders:Image;
		private var p_soccer:Image;
		private var p_swim:Image;
		private var p_tire:Image;
		private var p_toys:Image;
		
		private var pBallpitTimeline:TimelineMax;
		private var pBaseballTimeline:TimelineMax;
		private var pBikeTimeline:TimelineMax;
		private var pCameraTimeline:TimelineMax;
		private var pCatchTimeline:TimelineMax;
		private var pFetchTimeline:TimelineMax;
		private var pPeekTimeline:TimelineMax;
		private var pShouldersTimeline:TimelineMax;
		private var pSoccerTimeline:TimelineMax;
		private var pTireTimeline:TimelineMax;
		private var pSwimTimeline:TimelineMax;
		private var pToysTimeline:TimelineMax;
		
		
		private var h_bake:Image;
		private var h_beach:Image;
		private var h_boy:Image;
		private var h_daddaughters:Image;
		private var h_dog:Image;
		private var h_fishing:Image;
		private var h_girldog:Image;
		private var h_girlonrings:Image;
		private var h_girls:Image;
		private var h_house:Image;
		private var h_johnsonkids:Image;
		private var h_johnsonmom:Image;
		private var h_planting:Image;
		private var h_smile:Image;
		
		private var l_baby:Image;
		private var l_blow:Image;
		private var l_garden:Image;
		private var l_grads:Image;
		private var l_hike:Image;
		private var l_lean:Image;
		private var l_lift:Image;
		private var l_make:Image;
		private var l_marry:Image;
		private var l_raise:Image;
		private var l_read:Image;
		private var l_school:Image;
		
		private var lBabyTimeline:TimelineMax;
		private var lBlowTimeline:TimelineMax;
		private var lGardenTimeline:TimelineMax;
		private var lGradsTimeline:TimelineMax;
		private var lHikeTimeline:TimelineMax;
		private var lLeanTimeline:TimelineMax;
		private var lLiftTimeline:TimelineMax;
		private var lMakeTimeline:TimelineMax;
		private var lMarryTimeline:TimelineMax;
		private var lRaiseTimeline:TimelineMax;
		private var lReadTimeline:TimelineMax;
		private var lSchoolTimeline:TimelineMax;
		
		private var speck1:Image;
		private var speck2:Image;
		private var speck3:Image;
		private var speck4:Image;
		private var speck5:Image;
		
		private var wave1:Image;
		private var wave2:Image;
		private var wave3:Image;
		private var wave4:Image;
		private var wave5:Image;
		
		private var star1:Image;
		private var star2:Image;
		private var star3:Image;
		private var star4:Image;
		private var star5:Image;
		
		private var band1:Image;
		private var band2:Image;
		private var band3:Image;
		private var band4:Image;
		private var band5:Image;
		
		private var speck6:Image;
		private var speck7:Image;
		private var speck8:Image;
		private var speck9:Image;
		private var speck10:Image;
		
		private var wave6:Image;
		private var wave7:Image;
		private var wave8:Image;
		private var wave9:Image;
		private var wave10:Image;
		
		private var star6:Image;
		private var star7:Image;
		private var star8:Image;
		private var star9:Image;
		private var star10:Image;
		
		private var band6:Image;
		private var band7:Image;
		private var band8:Image;
		private var band9:Image;
		private var band10:Image;
		
		private var hbakeTimeline:TimelineMax;
		private var hbeachTimeline:TimelineMax;
		private var hboyTimeline:TimelineMax;
		private var hdaddaughtersTimeline:TimelineMax;
		private var hdogTimeline:TimelineMax;
		private var hfishingTimeline:TimelineMax;
		private var hgirldogTimeline:TimelineMax;
		private var hgirlonringsTimeline:TimelineMax;
		private var hgirlsTimeline:TimelineMax;
		private var hhouseTimeline:TimelineMax;
		private var hjohnsonkidsTimeline:TimelineMax;
		private var hjohnsonmomTimeline:TimelineMax;
		private var hplantingTimeline:TimelineMax;
		private var hSmileTimeline:TimelineMax;
		
		private var business:Image;
		private var dreams:Image;
		private var home:Image;
		private var life:Image;
		private var games:Image;
		private var logo:Image;
		
		private var bgTimeline:TimelineMax;
		private var bTimeline:TimelineMax;
		private var dTimeline:TimelineMax;
		private var hTimeline:TimelineMax;
		private var lTimeline:TimelineMax;
		private var pTimeline:TimelineMax;
		
		private var bBikeTimeline:TimelineMax;
		private var bClipboardTimeline:TimelineMax;
		private var bRegisterTimeline:TimelineMax;
		private var bShakeTimeline:TimelineMax;
		private var bMenTimeline:TimelineMax;
		private var bColabTimeline:TimelineMax;
		private var bCrossedTimeline:TimelineMax;
		private var bHardwareTimeline:TimelineMax;
		private var bBooksTimeline:TimelineMax;
		private var bPlatesTimeline:TimelineMax;
		private var bSmileTimeline:TimelineMax;
		private var bRestuarantTimeline:TimelineMax;
		
		private var dSmileTimeline:TimelineMax;
		private var dBoysTimeline:TimelineMax;
		private var dTeamTimeline:TimelineMax;
		private var dChicksTimeline:TimelineMax;
		private var dSwingTimeline:TimelineMax;
		private var dBabyTimeline:TimelineMax;
		private var dLadiesTimeline:TimelineMax;
		private var dPlaneTimeline:TimelineMax;
		private var dFlyTimeline:TimelineMax;
		private var dLookTimeline:TimelineMax;
		private var dTwirlTimeline:TimelineMax;
		private var dCoffeeTimeline:TimelineMax;
		private var dCoupleTimeline:TimelineMax;
		private var dFamilyTimeline:TimelineMax;
		private var dHoldingTimeline:TimelineMax;
		private var dBlondTimeline:TimelineMax;
		
		private var cloudsTimeline:TimelineMax;
		private var flowerTimeline:TimelineMax;
		private var feathersTimeline:TimelineMax;
		//private var dropsTimeline:TimelineMax;
		private var leavesTimeline:TimelineMax;
	
		private var businessTimeline:TimelineMax;
		private var dreamsTimeline:TimelineMax;
		private var homeTimeline:TimelineMax;
		private var lifeTimeline:TimelineMax;
		private var playTimeline:TimelineMax;
		
		//private var touchTimeline:TimelineMax;
		
		/*private var businessAnimTimeline:TimelineMax;
		private var dreamAnimTimeline:TimelineMax;
		private var homeAnimTimeline:TimelineMax;
		private var lifeAnimTimeline:TimelineMax;
		private var playAnimTimeline:TimelineMax;*/
		
		private var picAlpha:Number = .4;
		private var trans:Number = 10;
		private var del:Number = 80;
		private var picTime:Number = 10;
		private var bRect:Rectangle;
        private var bContents:Sprite;
		private var bRect2:Rectangle;
        private var bContents2:Sprite;
		private var dRect:Rectangle;
        private var dContents:Sprite;
		private var hRect:Rectangle;
        private var hContents:Sprite;
		private var lRect:Rectangle;
        private var lContents:Sprite;
		private var pRect:Rectangle;
        private var pContents:Sprite;
		private var dRect2:Rectangle;
        private var dContents2:Sprite;
		private var hRect2:Rectangle;
        private var hContents2:Sprite;
		private var lRect2:Rectangle;
        private var lContents2:Sprite;
		private var pRect2:Rectangle;
        private var pContents2:Sprite;
		private var maskSpeed:Number = 3;
		private var maskStart1:Number = 4720;
		private var maskStart2:Number = 4920;
		private var wordsStart1:Number = 250;
		private var wordsStart2:Number = 500;
		private var wordsAlpha:Number = .5;
		
		public var configXML:XML;
		private var configLoader:URLLoader;
		
		private var loadXML:LoadXML;
		
		private var isDone:Boolean = false;
		
		private var maskVal:Number = 1;
		
		private var filt:Number = 10;
		
		
		private var _particleContainer:PixelMaskDisplayObject;
		//private var owner:Document;
		
		/*public var businessAnim:BusinessAnim;
		public var dreamAnim:DreamAnim;
		public var homeAnim:HomeAnim;
		public var lifeAnim:LifeAnim;
		public var playAnim:PlayAnim;*/

		public function BackgroundContainer() 
		{
			loadXML = new LoadXML();
			
			/*while(isDone == false)
			{
				trace("check isDone");
				checkIsDone();
			}*/
			init();
		}
		
		private function checkIsDone():void
		{
			if (loadXML.isDone == true)
			{
				isDone = true;
				picAlpha = loadXML.picAlpha;
				trans = loadXML.trans;
				del = loadXML.del;
				picTime = loadXML.picTime;
				maskSpeed = loadXML.maskSpeed;
				maskStart1 = loadXML.maskStart1;
				maskStart2 = loadXML.maskStart2;
				wordsStart1 = loadXML.wordsStart1;
				wordsStart2 = loadXML.wordsStart2;
				wordsAlpha = loadXML.wordsAlpha;
				
				init();
			}
			
		}
		
		private function init():void
		{
			//owner = myOwner;
			var b_bgBitmap:Bitmap = new BBG();
			var texture:Texture = Texture.fromBitmap(b_bgBitmap);
			b_bg = new Image(texture);
			addChild(b_bg);
			b_bg.height = 1920;
			b_bg.width = 4320;
			b2_bg = new Image(texture);
			addChild(b2_bg);
			b2_bg.height = 1920;
			b2_bg.width = 4320;
			b2_bg.blendMode = BlendMode.MULTIPLY;
			
			//texture.dispose();
			
			var flowerBitmap:Bitmap = new FLOWER();
			var texture1:Texture = Texture.fromBitmap(flowerBitmap);
			flower = new Image(texture1);
			addChild(flower);
			flower.width = 4720;
			flower.height = 2097;
			flower.blendMode = BlendMode.MULTIPLY;
			
			var d_bgBitmap:Bitmap = new DBG();
			var texture2:Texture = Texture.fromBitmap(d_bgBitmap);
			d_bg = new Image(texture2);
			addChild(d_bg);
			d_bg.height = 1920;
			d_bg.width = 4320;
			d2_bg = new Image(texture2);
			addChild(d2_bg);
			d2_bg.height = 1920;
			d2_bg.width = 4320;
			d2_bg.blendMode = BlendMode.MULTIPLY;
			
			//texture2.dispose();
			
			var feathersBitmap:Bitmap = new FEATHERS();
			var texture3:Texture = Texture.fromBitmap(feathersBitmap);
			feathers = new Image(texture3);
			addChild(feathers);
			feathers.width = 4720;
			feathers.height = 2097;
			feathers.blendMode = BlendMode.MULTIPLY;
			
			var h_bgBitmap:Bitmap = new HBG();
			var texture4:Texture = Texture.fromBitmap(h_bgBitmap);
			h_bg = new Image(texture4);
			addChild(h_bg);
			h_bg.height = 1920;
			h_bg.width = 4320;
			h2_bg = new Image(texture4);
			addChild(h2_bg);
			h2_bg.height = 1920;
			h2_bg.width = 4320;
			h2_bg.blendMode = BlendMode.MULTIPLY;
			
			//texture4.dispose();
			
			var cloudsBitmap:Bitmap = new CLOUDS();
			var texture5:Texture = Texture.fromBitmap(cloudsBitmap);
			clouds = new Image(texture5);
			addChild(clouds);
			clouds.width = 4720;
			clouds.height = 2097;
			clouds.blendMode = BlendMode.SCREEN;
			
			
			var l_bgBitmap:Bitmap = new LBG();
			var texture6:Texture = Texture.fromBitmap(l_bgBitmap);
			l_bg = new Image(texture6);
			addChild(l_bg);
			l_bg.height = 1920;
			l_bg.width = 4320;
			l2_bg = new Image(texture6);
			addChild(l2_bg);
			l2_bg.height = 1920;
			l2_bg.width = 4320;
			l2_bg.blendMode = BlendMode.MULTIPLY;
			
			//texture6.dispose();
			
			var leavesBitmap:Bitmap = new LEAVES();
			var texture7:Texture = Texture.fromBitmap(leavesBitmap);
			leaves = new Image(texture7);
			addChild(leaves);
			leaves.width = 4720;
			leaves.height = 2097;
			leaves.blendMode = BlendMode.MULTIPLY;
			
			var p_bgBitmap:Bitmap = new PBG();
			var texture8:Texture = Texture.fromBitmap(p_bgBitmap);
			p_bg = new Image(texture8);
			addChild(p_bg);
			p_bg.height = 1920;
			p_bg.width = 4320;	
			p2_bg = new Image(texture8);
			addChild(p2_bg);
			p2_bg.height = 1920;
			p2_bg.width = 4320;
			p2_bg.blendMode = BlendMode.MULTIPLY;
			
			var bubbleBitmap:Bitmap = new BUBBLE();
			var textureBubble:Texture = Texture.fromBitmap(bubbleBitmap);
			bubble1 = new Image(textureBubble);
			addChild(bubble1);
			bubble1.blendMode = BlendMode.MULTIPLY;
			bubble1.x = Math.random() * (4320 - bubble1.width);
			bubble1.y = Math.random() * (1920 - bubble1.height);
			animSpeck(bubble1);
			bubble1.alpha = 0;
			
			bubble2 = new Image(textureBubble);
			addChild(bubble2);
			bubble2.blendMode = BlendMode.MULTIPLY;
			bubble2.x = Math.random() * (4320 - bubble2.width);
			bubble2.y = Math.random() * (1920 - bubble2.height);
			animSpeck(bubble2);
			bubble2.alpha = 0;
			
			bubble3 = new Image(textureBubble);
			addChild(bubble3);
			bubble3.blendMode = BlendMode.MULTIPLY;
			bubble3.x = Math.random() * (4320 - bubble3.width);
			bubble3.y = Math.random() * (1920 - bubble3.height);
			animSpeck(bubble3);
			bubble3.alpha = 0;
			
			bubble4 = new Image(textureBubble);
			addChild(bubble4);
			bubble4.blendMode = BlendMode.MULTIPLY;
			bubble4.x = Math.random() * (4320 - bubble4.width);
			bubble4.y = Math.random() * (1920 - bubble4.height);
			animSpeck(bubble4);
			bubble4.alpha = 0;
			
			bubble5 = new Image(textureBubble);
			addChild(bubble5);
			bubble5.blendMode = BlendMode.MULTIPLY;
			bubble5.x = Math.random() * (4320 - bubble5.width);
			bubble5.y = Math.random() * (1920 - bubble5.height);
			animSpeck(bubble5);
			bubble5.alpha = 0;
			
			bubble6 = new Image(textureBubble);
			addChild(bubble6);
			bubble6.blendMode = BlendMode.MULTIPLY;
			bubble6.x = Math.random() * (4320 - bubble6.width);
			bubble6.y = Math.random() * (1920 - bubble6.height);
			animSpeck(bubble6);
			bubble6.alpha = 0;
			
			bubble7 = new Image(textureBubble);
			addChild(bubble7);
			bubble7.blendMode = BlendMode.MULTIPLY;
			bubble7.x = Math.random() * (4320 - bubble7.width);
			bubble7.y = Math.random() * (1920 - bubble7.height);
			animSpeck(bubble7);
			bubble7.alpha = 0;
			
			bubble8 = new Image(textureBubble);
			addChild(bubble8);
			bubble8.blendMode = BlendMode.MULTIPLY;
			bubble8.x = Math.random() * (4320 - bubble8.width);
			bubble8.y = Math.random() * (1920 - bubble8.height);
			animSpeck(bubble8);
			bubble8.alpha = 0;
			
			bubble9 = new Image(textureBubble);
			addChild(bubble9);
			bubble9.blendMode = BlendMode.MULTIPLY;
			bubble9.x = Math.random() * (4320 - bubble9.width);
			bubble9.y = Math.random() * (1920 - bubble9.height);
			animSpeck(bubble9);
			bubble9.alpha = 0;
			
			bubble10 = new Image(textureBubble);
			addChild(bubble10);
			bubble10.blendMode = BlendMode.MULTIPLY;
			bubble10.x = Math.random() * (4320 - bubble10.width);
			bubble10.y = Math.random() * (1920 - bubble10.height);
			animSpeck(bubble10);
			bubble10.alpha = 0;
			
			
			var businessBitmap:Bitmap = new BUSINESS();
			var texture10:Texture = Texture.fromBitmap(businessBitmap);
			business = new Image(texture10);
			addChild(business);
			business.pivotX = business.width/2;
			business.pivotY = business.height;
			business.blendMode = BlendMode.ADD;
			business.alpha = .5;
			business.scaleX = 2.5;
			business.scaleY = 2.5;
			business.x = 4320/2;
			business.y = 3020;
			business.rotation = deg2rad(90);
			business.alpha = 0;
			
			//texture10.dispose();
			
			
			var dreamsBitmap:Bitmap = new DREAMS();
			var texture11:Texture = Texture.fromBitmap(dreamsBitmap);
			dreams = new Image(texture11);
			addChild(dreams);
			dreams.pivotX = dreams.width/2;
			dreams.pivotY = dreams.height;
			dreams.blendMode = BlendMode.ADD;
			dreams.alpha = .5;
			dreams.scaleX = 2.5;
			dreams.scaleY = 2.5;
			dreams.x = 4320/2;
			dreams.y = 3020;
			dreams.rotation = deg2rad(90);
			dreams.alpha = 0;
			
			//texture11.dispose();
			
			
			var homeBitmap:Bitmap = new HOME();
			var texture12:Texture = Texture.fromBitmap(homeBitmap);
			home = new Image(texture12);
			addChild(home);
			home.pivotX = home.width/2;
			home.pivotY = home.height;
			home.blendMode = BlendMode.ADD;
			home.alpha = .5;
			home.scaleX = 2.5;
			home.scaleY = 2.5;
			home.x = 4320/2;
			home.y = 3020;
			home.rotation = deg2rad(90);
			home.alpha = 0;
			
			//texture12.dispose();
			
			
			var lifeBitmap:Bitmap = new LIFE();
			var texture13:Texture = Texture.fromBitmap(lifeBitmap);
			life = new Image(texture13);
			addChild(life);
			life.pivotX = life.width/2;
			life.pivotY = life.height;
			life.blendMode = BlendMode.ADD;
			life.alpha = .5;
			life.scaleX = 2.5;
			life.scaleY = 2.5;
			life.x = 4320/2;
			life.y = 3020;
			life.rotation = deg2rad(90);
			life.alpha = 0;
			
			//texture13.dispose();
			
			
			var gamesBitmap:Bitmap = new PLAY();
			var texture14:Texture = Texture.fromBitmap(gamesBitmap);
			games = new Image(texture14);
			addChild(games);
			games.pivotX = games.width/2;
			games.pivotY = games.height;
			games.blendMode = BlendMode.ADD;
			games.alpha = .5;
			games.scaleX = 2.5;
			games.scaleY = 2.5;
			games.x = 4320/2;
			games.y = 3020;
			games.rotation = deg2rad(90);
			games.alpha = 0;
			
			//texture14.dispose();
			
			var speckBitmap:Bitmap = new SPECK();
			var textureSpeck:Texture = Texture.fromBitmap(speckBitmap);
			speck1 = new Image(textureSpeck);
			addChild(speck1);
			speck1.pivotX = speck1.width/2;
			speck1.pivotY = speck1.height/2;
			speck1.blendMode = BlendMode.ADD;
			speck1.x = Math.random() * 4320;
			speck1.y = Math.random() * 1920;
			animSpeck(speck1);
			//speck1.alpha = 0;
			
			speck2 = new Image(textureSpeck);
			addChild(speck2);
			speck2.pivotX = speck2.width/2;
			speck2.pivotY = speck2.height/2;
			speck2.blendMode = BlendMode.ADD;
			speck2.x = Math.random() * 4320;
			speck2.y = Math.random() * 1920;
			animSpeck(speck2);
			//speck2.alpha = 0;
			
			speck3 = new Image(textureSpeck);
			addChild(speck3);
			speck3.pivotX = speck3.width/2;
			speck3.pivotY = speck3.height/2;
			speck3.blendMode = BlendMode.ADD;
			speck3.x = Math.random() * 4320;
			speck3.y = Math.random() * 1920;
			animSpeck(speck3);
			//speck3.alpha = 0;
			
			speck4 = new Image(textureSpeck);
			addChild(speck1);
			speck4.pivotX = speck4.width/2;
			speck4.pivotY = speck4.height/2;
			speck4.blendMode = BlendMode.ADD;
			speck4.x = Math.random() * 4320;
			speck4.y = Math.random() * 1920;
			animSpeck(speck4);
			//speck4.alpha = 0;
			
			speck5 = new Image(textureSpeck);
			addChild(speck5);
			speck5.pivotX = speck5.width/2;
			speck5.pivotY = speck5.height/2;
			speck5.blendMode = BlendMode.ADD;
			speck5.x = Math.random() * 4320;
			speck5.y = Math.random() * 1920;
			animSpeck(speck5);
			//speck5.alpha = 0;
			
			//textureSpeck.dispose();
			
			speck6 = new Image(textureSpeck);
			addChild(speck6);
			speck6.pivotX = speck6.width/2;
			speck6.pivotY = speck6.height/2;
			speck6.blendMode = BlendMode.ADD;
			speck6.x = Math.random() * 4320;
			speck6.y = Math.random() * 1920;
			animSpeck(speck6);
			//speck1.alpha = 0;
			
			speck7 = new Image(textureSpeck);
			addChild(speck7);
			speck7.pivotX = speck7.width/2;
			speck7.pivotY = speck7.height/2;
			speck7.blendMode = BlendMode.ADD;
			speck7.x = Math.random() * 4320;
			speck7.y = Math.random() * 1920;
			animSpeck(speck7);
			//speck2.alpha = 0;
			
			speck8 = new Image(textureSpeck);
			addChild(speck8);
			speck8.pivotX = speck8.width/2;
			speck8.pivotY = speck8.height/2;
			speck8.blendMode = BlendMode.ADD;
			speck8.x = Math.random() * 4320;
			speck8.y = Math.random() * 1920;
			animSpeck(speck8);
			//speck3.alpha = 0;
			
			speck9 = new Image(textureSpeck);
			addChild(speck9);
			speck9.pivotX = speck9.width/2;
			speck9.pivotY = speck9.height/2;
			speck9.blendMode = BlendMode.ADD;
			speck9.x = Math.random() * 4320;
			speck9.y = Math.random() * 1920;
			animSpeck(speck9);
			//speck4.alpha = 0;
			
			speck10 = new Image(textureSpeck);
			addChild(speck10);
			speck10.pivotX = speck10.width/2;
			speck10.pivotY = speck10.height/2;
			speck10.blendMode = BlendMode.ADD;
			speck10.x = Math.random() * 4320;
			speck10.y = Math.random() * 1920;
			animSpeck(speck10);
			//speck5.alpha = 0;
			
			var starBitmap:Bitmap = new STAR();
			var textureStar:Texture = Texture.fromBitmap(starBitmap);
			star1 = new Image(textureStar);
			addChild(star1);
			star1.pivotX = star1.width/2;
			star1.pivotY = star1.height/2;
			star1.blendMode = BlendMode.ADD;
			star1.x = Math.random() * 4320;
			star1.y = Math.random() * 1920;
			star1.rotation = deg2rad(Math.random()*360);
			animSpeck(star1);
			star1.alpha = 0;
			
			star2 = new Image(textureStar);
			addChild(star2);
			star2.pivotX = star2.width/2;
			star2.pivotY = star2.height/2;
			star2.blendMode = BlendMode.ADD;
			star2.x = Math.random() * 4320;
			star2.y = Math.random() * 1920;
			star2.rotation = deg2rad(Math.random()*360);
			animSpeck(star2);
			star2.alpha = 0;
			
			star3 = new Image(textureStar);
			addChild(star3);
			star3.pivotX = star3.width/2;
			star3.pivotY = star3.height/2;
			star3.blendMode = BlendMode.ADD;
			star3.x = Math.random() * 4320;
			star3.y = Math.random() * 1920;
			star3.rotation = deg2rad(Math.random()*360);
			animSpeck(star3);
			star3.alpha = 0;
			
			star4 = new Image(textureStar);
			addChild(star4);
			star4.pivotX = star4.width/2;
			star4.pivotY = star4.height/2;
			star4.blendMode = BlendMode.ADD;
			star4.x = Math.random() * 4320;
			star4.y = Math.random() * 1920;
			star4.rotation = deg2rad(Math.random()*360);
			animSpeck(star4);
			star4.alpha = 0;
			
			star5 = new Image(textureStar);
			addChild(star5);
			star5.pivotX = star5.width/2;
			star5.pivotY = star5.height/2;
			star5.rotation = deg2rad(Math.random()*360);
			star5.blendMode = BlendMode.ADD;
			star5.x = Math.random() * 4320;
			star5.y = Math.random() * 1920;
			animSpeck(star5);
			star5.alpha = 0;
			
			//textureStar.dispose();
			
			star6 = new Image(textureStar);
			addChild(star6);
			star6.pivotX = star6.width/2;
			star6.pivotY = star6.height/2;
			star6.blendMode = BlendMode.ADD;
			star6.x = Math.random() * 4320;
			star6.y = Math.random() * 1920;
			star6.rotation = deg2rad(Math.random()*360);
			animSpeck(star6);
			star6.alpha = 0;
			
			star7 = new Image(textureStar);
			addChild(star7);
			star7.pivotX = star7.width/2;
			star7.pivotY = star7.height/2;
			star7.blendMode = BlendMode.ADD;
			star7.x = Math.random() * 4320;
			star7.y = Math.random() * 1920;
			star7.rotation = deg2rad(Math.random()*360);
			animSpeck(star7);
			star7.alpha = 0;
			
			star8 = new Image(textureStar);
			addChild(star8);
			star8.pivotX = star8.width/2;
			star8.pivotY = star8.height/2;
			star8.blendMode = BlendMode.ADD;
			star8.x = Math.random() * 4320;
			star8.y = Math.random() * 1920;
			star8.rotation = deg2rad(Math.random()*360);
			animSpeck(star8);
			star8.alpha = 0;
			
			star9 = new Image(textureStar);
			addChild(star9);
			star9.pivotX = star9.width/2;
			star9.pivotY = star9.height/2;
			star9.blendMode = BlendMode.ADD;
			star9.x = Math.random() * 4320;
			star9.y = Math.random() * 1920;
			star9.rotation = deg2rad(Math.random()*360);
			animSpeck(star9);
			star9.alpha = 0;
			
			star10 = new Image(textureStar);
			addChild(star10);
			star10.pivotX = star10.width/2;
			star10.pivotY = star10.height/2;
			star10.rotation = deg2rad(Math.random()*360);
			star10.blendMode = BlendMode.ADD;
			star10.x = Math.random() * 4320;
			star10.y = Math.random() * 1920;
			animSpeck(star10);
			star10.alpha = 0;
			
			var bandBitmap:Bitmap = new BAND();
			var textureBand:Texture = Texture.fromBitmap(bandBitmap);
			band1 = new Image(textureBand);
			addChild(band1);
			band1.pivotX = band1.width/2;
			band1.pivotY = band1.height/2;
			band1.rotation = deg2rad(Math.random()*360);
			band1.blendMode = BlendMode.ADD;
			band1.x = Math.random() * 4320;
			band1.y = Math.random() * 1920;
			animSpeck(band1);
			band1.alpha = 0;
			
			band2 = new Image(textureBand);
			addChild(band2);
			band2.pivotX = band2.width/2;
			band2.pivotY = band2.height/2;
			band2.rotation = deg2rad(Math.random()*360);
			band2.blendMode = BlendMode.ADD;
			band2.x = Math.random() * 4320;
			band2.y = Math.random() * 1920;
			animSpeck(band2);
			band2.alpha = 0;
			
			band3 = new Image(textureBand);
			addChild(band3);
			band3.pivotX = band3.width/2;
			band3.pivotY = band3.height/2;
			band3.rotation = deg2rad(Math.random()*360);
			band3.blendMode = BlendMode.ADD;
			band3.x = Math.random() * 4320;
			band3.y = Math.random() * 1920;
			animSpeck(band3);
			band3.alpha = 0;
			
			band4 = new Image(textureBand);
			addChild(band4);
			band4.pivotX = band4.width/2;
			band4.pivotY = band4.height/2;
			band4.rotation = deg2rad(Math.random()*360);
			band4.blendMode = BlendMode.ADD;
			band4.x = Math.random() * 4320;
			band4.y = Math.random() * 1920;
			animSpeck(band4);
			band4.alpha = 0;
			
			band5 = new Image(textureBand);
			addChild(band5);
			band5.pivotX = band5.width/2;
			band5.pivotY = band5.height/2;
			band5.rotation = deg2rad(Math.random()*360);
			band5.blendMode = BlendMode.ADD;
			band5.x = Math.random() * 4320;
			band5.y = Math.random() * 1920;
			animSpeck(band5);
			band5.alpha = 0;
			
			//textureBand.dispose();
			
			band6 = new Image(textureBand);
			addChild(band6);
			band6.pivotX = band6.width/2;
			band6.pivotY = band6.height/2;
			band6.rotation = deg2rad(Math.random()*360);
			band6.blendMode = BlendMode.ADD;
			band6.x = Math.random() * 4320;
			band6.y = Math.random() * 1920;
			animSpeck(band6);
			band6.alpha = 0;
			
			band7 = new Image(textureBand);
			addChild(band7);
			band7.pivotX = band7.width/2;
			band7.pivotY = band7.height/2;
			band7.rotation = deg2rad(Math.random()*360);
			band7.blendMode = BlendMode.ADD;
			band7.x = Math.random() * 4320;
			band7.y = Math.random() * 1920;
			animSpeck(band7);
			band7.alpha = 0;
			
			band8 = new Image(textureBand);
			addChild(band8);
			band8.pivotX = band8.width/2;
			band8.pivotY = band8.height/2;
			band8.rotation = deg2rad(Math.random()*360);
			band8.blendMode = BlendMode.ADD;
			band8.x = Math.random() * 4320;
			band8.y = Math.random() * 1920;
			animSpeck(band8);
			band8.alpha = 0;
			
			band9 = new Image(textureBand);
			addChild(band9);
			band9.pivotX = band9.width/2;
			band9.pivotY = band9.height/2;
			band9.rotation = deg2rad(Math.random()*360);
			band9.blendMode = BlendMode.ADD;
			band9.x = Math.random() * 4320;
			band9.y = Math.random() * 1920;
			animSpeck(band9);
			band9.alpha = 0;
			
			band10 = new Image(textureBand);
			addChild(band10);
			band10.pivotX = band10.width/2;
			band10.pivotY = band10.height/2;
			band10.rotation = deg2rad(Math.random()*360);
			band10.blendMode = BlendMode.ADD;
			band10.x = Math.random() * 4320;
			band10.y = Math.random() * 1920;
			animSpeck(band10);
			band10.alpha = 0;
			
			var waveBitmap:Bitmap = new WAVE();
			var textureWave:Texture = Texture.fromBitmap(waveBitmap);
			wave1 = new Image(textureWave);
			addChild(wave1);
			wave1.pivotX = wave1.width/2;
			wave1.pivotY = wave1.height/2;
			wave1.blendMode = BlendMode.ADD;
			wave1.x = Math.random() * 4320;
			wave1.y = 1000 + (Math.random() * 920);
			animBand(wave1);
			wave1.alpha = 0;
			
			wave2 = new Image(textureWave);
			addChild(wave2);
			wave2.pivotX = wave2.width/2;
			wave2.pivotY = wave2.height/2;
			wave2.blendMode = BlendMode.ADD;
			wave2.x = Math.random() * 4320;
			wave2.y = 1000 + (Math.random() * 920);
			animBand(wave2);
			wave2.alpha = 0;
			
			wave3 = new Image(textureWave);
			addChild(wave3);
			wave3.pivotX = wave3.width/2;
			wave3.pivotY = wave3.height/2;
			wave3.blendMode = BlendMode.ADD;
			wave3.x = Math.random() * 4320;
			wave3.y = 1000 + (Math.random() * 920);
			animBand(wave3);
			wave3.alpha = 0;
		
			wave4 = new Image(textureWave);
			addChild(wave4);
			wave4.pivotX = wave4.width/2;
			wave4.pivotY = wave4.height/2;
			wave4.blendMode = BlendMode.ADD;
			wave4.x = Math.random() * 4320;
			wave4.y = 1000 + (Math.random() * 920);
			animBand(wave4);
			wave4.alpha = 0;
			
			wave5 = new Image(textureWave);
			addChild(wave5);
			wave5.pivotX = wave5.width/2;
			wave5.pivotY = wave5.height/2;
			wave5.blendMode = BlendMode.ADD;
			wave5.x = Math.random() * 4320;
			wave5.y = 1000 + (Math.random() * 920);
			animBand(wave5);
			wave5.alpha = 0;
			
			var touchBitmap:Bitmap = new TOUCH();
			var textureTouch:Texture = Texture.fromBitmap(touchBitmap);
			touch = new Image(textureTouch);
			addChild(touch);
			//touch.pivotX = touch.width/2;
			//touch.pivotY = touch.height/2;
			touch.alpha = .3;
			touch.y = 1920 - touch.height;
			touch.x = -touch.width;
			//wave1.blendMode = BlendMode.ADD;
			TweenMax.to(touch, 400, {x:4320, repeat:-1});
			
			touch2 = new Image(textureTouch);
			addChild(touch2);
			//touch.pivotX = touch.width/2;
			//touch.pivotY = touch.height/2;
			touch2.alpha = .3;
			touch2.y = 0;
			touch2.x = 4320;
			//wave1.blendMode = BlendMode.ADD;
			TweenMax.to(touch2, 400, {x:-touch2.width, repeat:-1});
			
			
			bContents = new Sprite();
            addChild(bContents);
			bContents2 = new Sprite();
            addChild(bContents2);
			
			var businessWords1Bitmap:Bitmap = new BUSINESSWORDS1();
			var texturebusinessWords1:Texture = Texture.fromBitmap(businessWords1Bitmap);
			businessWords1 = new Image(texturebusinessWords1);
			bContents.addChild(businessWords1);
			businessWords1.width = 4320;
			businessWords1.height = 4320/2;
			businessWords1.x = wordsStart1;
			businessWords1.y = -100;
			businessWords1.alpha = wordsAlpha;
			//businessWords1.blendMode = BlendMode.MULTIPLY;
			
			var businessWords2Bitmap:Bitmap = new BUSINESSWORDS2();
			var texturebusinessWords2:Texture = Texture.fromBitmap(businessWords2Bitmap);
			businessWords2 = new Image(texturebusinessWords2);
			bContents2.addChild(businessWords2);
			businessWords2.width = 4320;
			businessWords2.height = 4320/2;
			businessWords2.x = wordsStart2 + 175;
			businessWords2.y = businessWords1.y - 442;
			businessWords2.alpha = wordsAlpha;
			//businessWords2.blendMode = BlendMode.MULTIPLY;
			
			/*var mask:MaskSprite = new MaskSprite();
			mask.x = 500; //maskStart1;
			mask.y = 0;
			mask.width = 4320;
			mask.height = 1920;*/
			//addEventListener(TouchEvent.TOUCH, handleClick);
			
			//_particleContainer = new PixelMaskDisplayObject();
			
			// NOTE: to test non-animated mode, assign "false" to the second constructor parameter:
			//_particleContainer = new PixelMaskDisplayObject(-1, false);
			
			// uncomment this to see inverted mode:
			//_particleContainer.inverted = true;
			
			//addChild(_particleContainer);
			//_particleContainer.mask = mask;
			//_particleContainer.addChild(businessWords2);
			
			bRect2 = new Rectangle(0, 0, 4320, 1920); 
            bRect2.x = maskStart1;
            bRect2.y = 0;
			//bRect2.BlurFilter(filt, filt);
           	bContents2.clipRect = bRect2;
			bRect = new Rectangle(0, 0, 4320, 1920); 
            bRect.x = maskStart2;
            bRect.y = 0;
			//bRect.BlurFilter(filt, filt);
           	bContents.clipRect = bRect;
			//addMaskB();
			
			
			/*bRect2 = new Rectangle(0, 0, 6720, 5742); 
            bRect2.x = 8387; //go to -1970
            bRect2.y = -2150;
			bRect2.rotate = -45;
			//bRect2.BlurFilter(filt, filt);
           	bContents2.clipRect = bRect2;
			bRect = new Rectangle(0, 0, 6720, 5742);  
            bRect.x = 8387;
            bRect.y = -2150;
			bRect.rotate = -45;
			//bRect.BlurFilter(filt, filt);
           	bContents.clipRect = bRect;*/
            
   
			
			//texture1.dispose();
			
			
			
			dContents = new Sprite();
            addChild(dContents);
			dContents2 = new Sprite();
            addChild(dContents2);
			
			var dreamWords1Bitmap:Bitmap = new DREAMWORDS1();
			var textureDreamWords1:Texture = Texture.fromBitmap(dreamWords1Bitmap);
			dreamWords1 = new Image(textureDreamWords1);
			dContents.addChild(dreamWords1);
			dreamWords1.width = 4320;
			dreamWords1.height = 4320/4;
			dreamWords1.x = wordsStart1;
			dreamWords1.y = 500;
			//dreamWords1.blendMode = BlendMode.MULTIPLY;
			dreamWords1.alpha = wordsAlpha;
			
			var dreamWords2Bitmap:Bitmap = new DREAMWORDS2();
			var textureDreamWords2:Texture = Texture.fromBitmap(dreamWords2Bitmap);
			dreamWords2 = new Image(textureDreamWords2);
			dContents2.addChild(dreamWords2);
			dreamWords2.width = 4320;
			dreamWords2.height = 4320/4;
			dreamWords2.x = wordsStart2 + 62;
			dreamWords2.y = dreamWords1.y + 225;
			//dreamWords2.blendMode = BlendMode.MULTIPLY;
			dreamWords2.alpha = wordsAlpha;
			
			dRect = new Rectangle(0, 0, 4320, 1920); 
            dRect.x = maskStart1;
            dRect.y = 0;
			//dRect.BlurFilter(filt, filt);
            dContents.clipRect = dRect;
			dRect2 = new Rectangle(0, 0, 4320, 1920); 
            dRect2.x = maskStart2;
            dRect2.y = 0;
			//dRect2.BlurFilter(filt, filt);
            dContents2.clipRect = dRect2;
			//addMaskD();
			
			//texture3.dispose();
			
			
			
			hContents = new Sprite();
            addChild(hContents);
			hContents2 = new Sprite();
            addChild(hContents2);
			
			var homeWords1Bitmap:Bitmap = new HOMEWORDS1();
			var texturehomeWords1:Texture = Texture.fromBitmap(homeWords1Bitmap);
			homeWords1 = new Image(texturehomeWords1);
			hContents.addChild(homeWords1);
			homeWords1.width = 4320;
			homeWords1.height = 4320/2;
			homeWords1.x = wordsStart1;
			homeWords1.y = 0;
			homeWords1.alpha = wordsAlpha;
			//homeWords1.blendMode = BlendMode.MULTIPLY;
			
			var homeWords2Bitmap:Bitmap = new HOMEWORDS2();
			var texturehomeWords2:Texture = Texture.fromBitmap(homeWords2Bitmap);
			homeWords2 = new Image(texturehomeWords2);
			hContents2.addChild(homeWords2);
			homeWords2.width = 4320;
			homeWords2.height = 4320/2;
			homeWords2.x = wordsStart2;
			homeWords2.y = 200;
			homeWords2.alpha = wordsAlpha;
			//homeWords2.blendMode = BlendMode.MULTIPLY;
			
			hRect = new Rectangle(0, 0, 4320, 1920); 
            hRect.x = maskStart1;
            hRect.y = 0;
			//hRect.BlurFilter(filt, filt);
            hContents.clipRect = hRect;
			hRect2 = new Rectangle(0, 0, 4320, 1920); 
            hRect2.x = maskStart2;
            hRect2.y = 0;
			//hRect2.BlurFilter(filt, filt);
            hContents2.clipRect = hRect2;
			//addMaskH();
			
			//texture5.dispose();
			
			
			
			lContents = new Sprite();
            addChild(lContents);
			lContents2 = new Sprite();
            addChild(lContents2);
			
			var lifeWords1Bitmap:Bitmap = new LIFEWORDS1();
			var texturelifeWords1:Texture = Texture.fromBitmap(lifeWords1Bitmap);
			lifeWords1 = new Image(texturelifeWords1);
			lContents.addChild(lifeWords1);
			lifeWords1.width = 4320;
			lifeWords1.height = 4320/2;
			lifeWords1.x = wordsStart1;
			lifeWords1.y = -300;
			lifeWords1.alpha = wordsAlpha;
			//lifeWords1.blendMode = BlendMode.MULTIPLY;
			
			lRect = new Rectangle(0, 0, 4320, 1920); 
            lRect.x = maskStart1;
            lRect.y = 0;
			//lRect.BlurFilter(filt, filt);
            lContents.clipRect = lRect;
			//addMaskL();
			
			//texture7.dispose();
			
			
			
			//texture8.dispose();
			
			/*var dropsBitmap:Bitmap = new DROPS();
			var texture9:Texture = Texture.fromBitmap(dropsBitmap);
			drops = new Image(texture9);
			addChild(drops);
			drops.width = 4720;
			drops.height = 2097;
			drops.blendMode = BlendMode.MULTIPLY;*/
			
			
			
			pContents = new Sprite();
            addChild(pContents);
			pContents2 = new Sprite();
            addChild(pContents2);
			
			var playWords1Bitmap:Bitmap = new PLAYWORDS1();
			var textureplayWords1:Texture = Texture.fromBitmap(playWords1Bitmap);
			playWords1 = new Image(textureplayWords1);
			pContents.addChild(playWords1);
			playWords1.width = 4320;
			playWords1.height = 4320/2;
			playWords1.x = wordsStart1;
			playWords1.y = -400;
			playWords1.alpha = wordsAlpha;
			//playWords1.blendMode = BlendMode.MULTIPLY;
			
			pRect = new Rectangle(0, 0, 4320, 1920); 
            pRect.x = maskStart1;
            pRect.y = 0;
			//pRect.BlurFilter(filt, filt);
            pContents.clipRect = pRect;
			//addMaskP();
			
			//texture9.dispose();
			
			
			
			
			
			var logoBitmap:Bitmap = new LOGO();
			var texture15:Texture = Texture.fromBitmap(logoBitmap);
			logo = new Image(texture15);
			addChild(logo);
			logo.width = 3450;
			logo.height = 862;
			logo.x = (4320 - logo.width)/2;
			logo.y = (1920 - logo.height)/2;
			
			//texture15.dispose();
			
			
			
			var flareRotBitmap:Bitmap = new FLAREROT();
			var texture16:Texture = Texture.fromBitmap(flareRotBitmap);
			flareRot = new Image(texture16);
			addChild(flareRot);
			//flareRot.width = 1024;
			//flareRot.height = 1024;
			flareRot.pivotX = flareRot.width/2;
			flareRot.pivotY = flareRot.height/2;
			flareRot.x = 100;
			flareRot.y = 100;
			flareRot.scaleX = -1;
			flareRot.alpha = 0;
			flareRot.blendMode = BlendMode.ADD;
			TweenMax.to(flareRot, 50, {rotation:deg2rad(360), repeat:-1, ease:Linear.easeNone});
			
			
			flareRot2 = new Image(texture16);
			addChild(flareRot2);
			//flareRot.width = 1024;
			//flareRot.height = 1024;
			flareRot2.pivotX = flareRot2.width/2;
			flareRot2.pivotY = flareRot2.height/2;
			flareRot2.x = 100;
			flareRot2.y = 100;
			flareRot2.alpha = 0;
			flareRot2.blendMode = BlendMode.ADD;
			TweenMax.to(flareRot2, 55, {rotation:deg2rad(-360), repeat:-1, ease:Linear.easeNone});
			
			//texture16.dispose();
			
			
			var flareBitmap:Bitmap = new FLARE();
			var texture17:Texture = Texture.fromBitmap(flareBitmap);
			flare = new Image(texture17);
			addChild(flare);
			flare.width = 4320;
			flare.height = 1920;
			flare.alpha = 0;
			flare.blendMode = BlendMode.ADD;
			
			//texture17.dispose();
			
			
			
			
			var b_bikeBitmap:Bitmap = new BBIKE();
			var texture18:Texture = Texture.fromBitmap(b_bikeBitmap);
			b_bike = new Image(texture18);
			addChild(b_bike);
			b_bike.width = 2048;
			b_bike.height = 1024;
			b_bike.x = -550;
			b_bike.y = -80;
			
			//texture18.dispose();
			
			
			var b_clipboardBitmap:Bitmap = new BCLIPBOARD();
			var texture19:Texture = Texture.fromBitmap(b_clipboardBitmap);
			b_clipboard = new Image(texture19);
			addChild(b_clipboard);
			b_clipboard.width = 2048;
			b_clipboard.height = 2048;
			b_clipboard.x = 800;
			b_clipboard.y = 470;
			
			//texture19.dispose();
			
			
			var b_registerBitmap:Bitmap = new BREGISTER();
			var texture20:Texture = Texture.fromBitmap(b_registerBitmap);
			b_register = new Image(texture20);
			addChild(b_register);
			b_register.width = 2048;
			b_register.height = 2048;
			b_register.x = 3000;
			b_register.y = 500;
			
			//texture20.dispose();
			
			//-------------------------------------
			var b_shakeBitmap:Bitmap = new BSHAKE();
			var texture21:Texture = Texture.fromBitmap(b_shakeBitmap);
			b_shake = new Image(texture21);
			addChild(b_shake);
			b_shake.width = 2276;
			b_shake.height = 2276;
			b_shake.x = 2500;
			b_shake.y = 460;
			
			//texture21.dispose();
			
			
			var b_menBitmap:Bitmap = new BMEN();
			var texture22:Texture = Texture.fromBitmap(b_menBitmap);
			b_men = new Image(texture22);
			addChild(b_men);
			b_men.width = 2171;
			b_men.height = 1085;
			b_men.x = -485;
			b_men.y = 666;
			
			//texture22.dispose();
			
			
			var b_colabBitmap:Bitmap = new BCOLAB();
			var texture23:Texture = Texture.fromBitmap(b_colabBitmap);
			b_colab = new Image(texture23);
			addChild(b_colab);
			b_colab.width = 1155;
			b_colab.height = 1155;
			b_colab.x = 1818;
			b_colab.y = -195;
			
			//texture23.dispose();
			
			
			var b_crossedBitmap:Bitmap = new BCROSSED();
			var texture24:Texture = Texture.fromBitmap(b_crossedBitmap);
			b_crossed = new Image(texture24);
			addChild(b_crossed);
			b_crossed.width = 2424;
			b_crossed.height = 2424;
			b_crossed.x = -165;
			b_crossed.y = -15;
			
			//texture24.dispose();
			
			
			var b_smileBitmap:Bitmap = new BSMILE();
			var texture25:Texture = Texture.fromBitmap(b_smileBitmap);
			b_smile = new Image(texture25);
			addChild(b_smile);
			b_smile.width = 1010;
			b_smile.height = 1010;
			b_smile.x = 2070;
			b_smile.y = -15;
			
			//texture25.dispose();
			
			
			var b_platesBitmap:Bitmap = new BPLATES();
			var texture26:Texture = Texture.fromBitmap(b_platesBitmap);
			b_plates = new Image(texture26);
			addChild(b_plates);
			b_plates.width = 2067;
			b_plates.height = 2067;
			b_plates.x = 2222;
			b_plates.y = 58;
			
			//texture26.dispose();
			
			
			var b_booksBitmap:Bitmap = new BBOOKS();
			var texture27:Texture = Texture.fromBitmap(b_booksBitmap);
			b_books = new Image(texture27);
			addChild(b_books);
			b_books.width = 1301;
			b_books.height = 2621;
			b_books.x = -80;
			b_books.y = 150;
			
			//texture27.dispose();
			
			
			var b_restaurantBitmap:Bitmap = new BRESTAURANT();
			var texture28:Texture = Texture.fromBitmap(b_restaurantBitmap);
			b_restaurant = new Image(texture28);
			addChild(b_restaurant);
			b_restaurant.width = 1495;
			b_restaurant.height = 1495;
			b_restaurant.x = 980;
			b_restaurant.y = -142;
			
			//texture28.dispose();
			
			
			var b_hardwareBitmap:Bitmap = new BHARDWARE();
			var texture29:Texture = Texture.fromBitmap(b_hardwareBitmap);
			b_hardware = new Image(texture29);
			addChild(b_hardware);
			b_hardware.width = 2660;
			b_hardware.height = 2660;
			b_hardware.x = 1846;
			b_hardware.y = -275;
			
			//texture29.dispose();
			
			//---------------------------------------------
			
			var d_babyBitmap:Bitmap = new DBABY();
			var texture30:Texture = Texture.fromBitmap(d_babyBitmap);
			d_baby = new Image(texture30);
			addChild(d_baby);
			d_baby.width = 1920;
			d_baby.height = 1920;
			d_baby.x = 3485;
			d_baby.y = 0;
			
			//texture30.dispose();
			
			
			var d_boysBitmap:Bitmap = new DBOYS();
			var texture31:Texture = Texture.fromBitmap(d_boysBitmap);
			d_boys = new Image(texture31);
			addChild(d_boys);
			d_boys.width = 1477;
			d_boys.height = 1477;
			d_boys.x = -280;
			d_boys.y = 765;
			
			//texture31.dispose();
			
			
			var d_chicksBitmap:Bitmap = new DCHICKS();
			var texture32:Texture = Texture.fromBitmap(d_chicksBitmap);
			d_chicks = new Image(texture32);
			addChild(d_chicks);
			d_chicks.width = 2048;
			d_chicks.height = 1024;
			d_chicks.x = 1860;
			d_chicks.y = 1042;
			
			//texture32.dispose();
			
			
			var d_smileBitmap:Bitmap = new DSMILE();
			var texture33:Texture = Texture.fromBitmap(d_smileBitmap);
			d_smile = new Image(texture33);
			addChild(d_smile);
			d_smile.width = 1920;
			d_smile.height = 1920;
			d_smile.x = -975;
			d_smile.y = 0;
			
			//texture33.dispose();
			
			
			var d_swingBitmap:Bitmap = new DSWING();
			var texture34:Texture = Texture.fromBitmap(d_swingBitmap);
			d_swing = new Image(texture34);
			addChild(d_swing);
			d_swing.width = 2048;
			d_swing.height = 2048;
			d_swing.x = 1675;
			d_swing.y = -85;
			
			//texture34.dispose();
			
			
			var d_teamBitmap:Bitmap = new DTEAM();
			var texture35:Texture = Texture.fromBitmap(d_teamBitmap);
			d_team = new Image(texture35);
			addChild(d_team);
			d_team.width = 1600;
			d_team.height = 1600;
			d_team.x = 970;
			d_team.y = -190;
			
			//texture35.dispose();
			
			//-------------------------------------	
			var d_ladiesBitmap:Bitmap = new DLADIES();
			var texture36:Texture = Texture.fromBitmap(d_ladiesBitmap);
			d_ladies = new Image(texture36);
			addChild(d_ladies);
			d_ladies.width = 1661;
			d_ladies.height = 830;
			d_ladies.x = -168;
			d_ladies.y = 654;
			
			//texture36.dispose();
			
			
			var d_planeBitmap:Bitmap = new DPLANE();
			var texture37:Texture = Texture.fromBitmap(d_planeBitmap);
			d_plane = new Image(texture37);
			addChild(d_plane);
			d_plane.width = 2551;
			d_plane.height = 2551;
			d_plane.x = 875;
			d_plane.y = -415;
			
			//texture37.dispose();
			
			
			var d_flyBitmap:Bitmap = new DFLY();
			var texture38:Texture = Texture.fromBitmap(d_flyBitmap);
			d_fly = new Image(texture38);
			addChild(d_fly);
			d_fly.width = 1731;
			d_fly.height = 866;
			d_fly.x = 2276;
			d_fly.y = 124;
			
			//texture38.dispose();
			
			
			var d_lookBitmap:Bitmap = new DLOOK();
			var texture39:Texture = Texture.fromBitmap(d_lookBitmap);
			d_look = new Image(texture39);
			addChild(d_look);
			d_look.width = 1720;
			d_look.height = 1720;
			d_look.x = 0;
			d_look.y = 530;
			
			//texture39.dispose();
			
			
			var d_twirlBitmap:Bitmap = new DTWIRL();
			var texture40:Texture = Texture.fromBitmap(d_twirlBitmap);
			d_twirl = new Image(texture40);
			addChild(d_twirl);
			d_twirl.width = 1308;
			d_twirl.height = 1308;
			d_twirl.x = 80;
			d_twirl.y = -176;
			
			//texture40.dispose();
			
			
			var d_coupleBitmap:Bitmap = new DCOUPLE();
			var texture41:Texture = Texture.fromBitmap(d_coupleBitmap);
			d_couple = new Image(texture41);
			addChild(d_couple);
			d_couple.width = 2467;
			d_couple.height = 2467;
			d_couple.x = 2120;
			d_couple.y = 50;
			
			//texture41.dispose();
			
			
			var d_coffeeBitmap:Bitmap = new DCOFFEE();
			var texture42:Texture = Texture.fromBitmap(d_coffeeBitmap);
			d_coffee = new Image(texture42);
			addChild(d_coffee);
			d_coffee.width = 1315;
			d_coffee.height = 1315;
			d_coffee.x = -230;
			d_coffee.y = 20;
			
			//texture42.dispose();
			
			//---------------------------------
			var h_bakeBitmap:Bitmap = new HBAKE();
			var texture43:Texture = Texture.fromBitmap(h_bakeBitmap);
			h_bake = new Image(texture43);
			addChild(h_bake);
			h_bake.width = 870;
			h_bake.height = 870;
			h_bake.x = 1215;
			h_bake.y = 0;
			h_bake.alpha = 0;
			
			//texture43.dispose();
			
			
			var h_beachBitmap:Bitmap = new HBEACH();
			var texture44:Texture = Texture.fromBitmap(h_beachBitmap);
			h_beach = new Image(texture44);
			addChild(h_beach);
			h_beach.width = 6420;
			h_beach.height = 3210;
			h_beach.x = -1120;
			h_beach.y = -220;
			h_beach.alpha = 0;
			
			//texture44.dispose();
			
			
			var h_boyBitmap:Bitmap = new HBOY();
			var texture45:Texture = Texture.fromBitmap(h_boyBitmap);
			h_boy = new Image(texture45);
			addChild(h_boy);
			h_boy.width = 1201;
			h_boy.height = 1210;
			h_boy.x = 1170;
			h_boy.y = 1225;
			h_boy.alpha = 0;
			
			//texture45.dispose();
			
			
			var h_daddaughtersBitmap:Bitmap = new HDADDAUGHTERS();
			var texture14a:Texture = Texture.fromBitmap(h_daddaughtersBitmap);
			h_daddaughters = new Image(texture14a);
			addChild(h_daddaughters);
			h_daddaughters.width = 1990;
			h_daddaughters.height = 995;
			h_daddaughters.x = 2160;
			h_daddaughters.y = -15;
			h_daddaughters.alpha = 0;
			
			//texture14a.dispose();
			
			
			var h_dogBitmap:Bitmap = new HDOG();
			var texture14b:Texture = Texture.fromBitmap(h_dogBitmap);
			h_dog = new Image(texture14b);
			addChild(h_dog);
			h_dog.width = 1970;
			h_dog.height = 985;
			h_dog.x = 40;
			h_dog.y = 1110;
			h_dog.alpha = 0;
			
			//texture14b.dispose();
			
			
			var h_fishingBitmap:Bitmap = new HFISHING();
			var texture14c:Texture = Texture.fromBitmap(h_fishingBitmap);
			h_fishing = new Image(texture14c);
			addChild(h_fishing);
			h_fishing.width = 4260;
			h_fishing.height = 2080;
			h_fishing.x = 480;
			h_fishing.y = -16;
			h_fishing.alpha = 0;
			
			//texture14c.dispose();
			
			
			var h_girldogBitmap:Bitmap = new HGIRLDOG();
			var texture14d:Texture = Texture.fromBitmap(h_girldogBitmap);
			h_girldog = new Image(texture14d);
			addChild(h_girldog);
			h_girldog.width = 1740;
			h_girldog.height = 870;
			h_girldog.x = 1850;
			h_girldog.y = 1090;
			h_girldog.alpha = 0;
			
			//texture14d.dispose();
			
			
			var h_girlonringsBitmap:Bitmap = new HGIRLONIONRINGS();
			var texture14e:Texture = Texture.fromBitmap(h_girlonringsBitmap);
			h_girlonrings = new Image(texture14e);
			addChild(h_girlonrings);
			h_girlonrings.pivotX = h_girlonrings.width/2;
			h_girlonrings.pivotY = 0;
			h_girlonrings.width = 508;
			h_girlonrings.height = 1016;
			h_girlonrings.x = 1555;
			h_girlonrings.y = -595;
			h_girlonrings.alpha = 0;
			h_girlonrings.blendMode = BlendMode.MULTIPLY;
			
			//texture14e.dispose();
			
			
			var h_girlsBitmap:Bitmap = new HGIRLS();
			var texture14f:Texture = Texture.fromBitmap(h_girlsBitmap);
			h_girls = new Image(texture14f);
			addChild(h_girls);
			h_girls.width = 1460;
			h_girls.height = 1460;
			h_girls.x = -130;
			h_girls.y = 1070;
			h_girls.alpha = 0;
			
			//texture14f.dispose();
			
			
			var h_houseBitmap:Bitmap = new HHOUSE();
			var texture14g:Texture = Texture.fromBitmap(h_houseBitmap);
			h_house = new Image(texture14g);
			addChild(h_house);
			h_house.width = 2050;
			h_house.height = 2050;
			h_house.x = 3130;
			h_house.y = -125;
			h_house.alpha = 0;
			
			//texture14g.dispose();
			
			
			var h_johnsonkidsBitmap:Bitmap = new HJOHNSONKIDS();
			var texture14h:Texture = Texture.fromBitmap(h_johnsonkidsBitmap);
			h_johnsonkids = new Image(texture14h);
			addChild(h_johnsonkids);
			h_johnsonkids.width = 1430;
			h_johnsonkids.height = 1430;
			h_johnsonkids.x = 2940;
			h_johnsonkids.y = 860;
			h_johnsonkids.alpha = 0;
			
			//texture14h.dispose();
			
			
			var h_johnsonmomBitmap:Bitmap = new HJOHNSONMOM();
			var texture14i:Texture = Texture.fromBitmap(h_johnsonmomBitmap);
			h_johnsonmom = new Image(texture14i);
			addChild(h_johnsonmom);
			h_johnsonmom.width = 1070;
			h_johnsonmom.height = 1070;
			h_johnsonmom.x = 230;
			h_johnsonmom.y = 650;
			h_johnsonmom.alpha = 0;
			
			//texture14i.dispose();
			
			
			var h_plantingBitmap:Bitmap = new HPLANTING();
			var texture14j:Texture = Texture.fromBitmap(h_plantingBitmap);
			h_planting = new Image(texture14j);
			addChild(h_planting);
			h_planting.width = 2080;
			h_planting.height = 1040;
			h_planting.x = 2310;
			h_planting.y = 790;
			h_planting.alpha = 0;
			
			//texture14j.dispose();
			
			
			var h_smileBitmap:Bitmap = new HSMILE();
			var texture14k:Texture = Texture.fromBitmap(h_smileBitmap);
			h_smile = new Image(texture14k);
			addChild(h_smile);
			h_smile.width = 1855;
			h_smile.height = 1855;
			h_smile.x = -650;
			h_smile.y = 30;
			h_smile.alpha = 0;
			
			//texture14k.dispose();
			
			//------------------------
			
			var p_ballpitBitmap:Bitmap = new PBALLPIT();
			var texturePBallpit:Texture = Texture.fromBitmap(p_ballpitBitmap);
			p_ballpit = new Image(texturePBallpit);
			addChild(p_ballpit);
			p_ballpit.width = 1792;
			p_ballpit.height = 1792;
			p_ballpit.x = -80;
			p_ballpit.y = 108;
			p_ballpit.alpha = 0;
			
			//texturePBallpit.dispose();
			
			
			var p_baseballBitmap:Bitmap = new PBASEBALL();
			var texturePBaseball:Texture = Texture.fromBitmap(p_baseballBitmap);
			p_baseball = new Image(texturePBaseball);
			addChild(p_baseball);
			p_baseball.width = 2290;
			p_baseball.height = 2290;
			p_baseball.x = 1333;
			p_baseball.y = -100;
			p_baseball.alpha = 0;
			
			//texturePBaseball.dispose();
			
			
			var p_bikeBitmap:Bitmap = new PBIKE();
			var texturePBike:Texture = Texture.fromBitmap(p_bikeBitmap);
			p_bike = new Image(texturePBike);
			addChild(p_bike);
			p_bike.width = 1393;
			p_bike.height = 1393;
			p_bike.x = 3040;
			p_bike.y = -80;
			p_bike.alpha = 0;
			
			//texturePBike.dispose();
			
			
			var p_cameraBitmap:Bitmap = new PCAMERA();
			var texturePCamera:Texture = Texture.fromBitmap(p_cameraBitmap);
			p_camera = new Image(texturePCamera);
			addChild(p_camera);
			p_camera.width = 1430;
			p_camera.height = 1430;
			p_camera.x = 12;
			p_camera.y = 746;
			p_camera.alpha = 0;
			
			//texturePCamera.dispose();
			
			
			var p_catchBitmap:Bitmap = new PCATCH();
			var texturePCatch:Texture = Texture.fromBitmap(p_catchBitmap);
			p_catch = new Image(texturePCatch);
			addChild(p_catch);
			p_catch.width = 1817;
			p_catch.height = 1817;
			p_catch.x = 1734;
			p_catch.y = -110;
			p_catch.alpha = 0;
			
			//texturePCatch.dispose();
			
			
			var p_fetchBitmap:Bitmap = new PFETCH();
			var texturePFetch:Texture = Texture.fromBitmap(p_fetchBitmap);
			p_fetch = new Image(texturePFetch);
			addChild(p_fetch);
			p_fetch.width = 3533;
			p_fetch.height = 3533;
			p_fetch.x = 453;
			p_fetch.y = -370;
			p_fetch.alpha = 0;
			
			//texturePFetch.dispose();
			
			
			var p_peekBitmap:Bitmap = new PPEEK();
			var texturePPeek:Texture = Texture.fromBitmap(p_peekBitmap);
			p_peek = new Image(texturePPeek);
			addChild(p_peek);
			p_peek.width = 1690;
			p_peek.height = 1690;
			p_peek.x = -100;
			p_peek.y = 412;
			p_peek.alpha = 0;
			
			//texturePPeek.dispose();
			
			
			var p_shouldersBitmap:Bitmap = new PSHOULDERS();
			var texturePShoulders:Texture = Texture.fromBitmap(p_shouldersBitmap);
			p_shoulders = new Image(texturePShoulders);
			addChild(p_shoulders);
			p_shoulders.width = 1217;
			p_shoulders.height = 1217;
			p_shoulders.x = 3091;
			p_shoulders.y = -210;
			p_shoulders.alpha = 0;
			
			//texturePShoulders.dispose();
			
			
			var p_soccerBitmap:Bitmap = new PSOCCER();
			var texturePSoccer:Texture = Texture.fromBitmap(p_soccerBitmap);
			p_soccer = new Image(texturePSoccer);
			addChild(p_soccer);
			p_soccer.width = 2448;
			p_soccer.height = 2448;
			p_soccer.x = 2231;
			p_soccer.y = 70;
			p_soccer.alpha = 0;
			
			//texturePSoccer.dispose();
			
			var p_swimBitmap:Bitmap = new PSWIM();
			var texturePSwim:Texture = Texture.fromBitmap(p_swimBitmap);
			p_swim = new Image(texturePSwim);
			addChild(p_swim);
			p_swim.width = 1325;
			p_swim.height = 1325;
			p_swim.x = 0;
			p_swim.y = 70;
			p_swim.alpha = 0;
			
			//texturePSwim.dispose();
			
			var p_tireBitmap:Bitmap = new PTIRE();
			var texturePTire:Texture = Texture.fromBitmap(p_tireBitmap);
			p_tire = new Image(texturePTire);
			addChild(p_tire);
			p_tire.width = 1458;
			p_tire.height = 2916;
			p_tire.x = 2964;
			p_tire.y = -268;
			p_tire.alpha = 0;
			p_tire.blendMode = BlendMode.MULTIPLY;
			
			//texturePTire.dispose();
			
			var p_toysBitmap:Bitmap = new PTOYS();
			var texturePToys:Texture = Texture.fromBitmap(p_toysBitmap);
			p_toys = new Image(texturePToys);
			addChild(p_toys);
			p_toys.width = 1100;
			p_toys.height = 1100;
			p_toys.x = 1518;
			p_toys.y = -130;
			p_toys.alpha = 0;
			
			//texturePToys.dispose();
			
			var l_babyBitmap:Bitmap = new LBABY();
			var textureLBaby:Texture = Texture.fromBitmap(l_babyBitmap);
			l_baby = new Image(textureLBaby);
			addChild(l_baby);
			l_baby.width = 2480;
			l_baby.height = 2480;
			l_baby.x = 1861;
			l_baby.y = 10;
			l_baby.alpha = 0;
			
			//textureLBaby.dispose();
			
			
			var l_blowBitmap:Bitmap = new LBLOW();
			var textureLBlow:Texture = Texture.fromBitmap(l_blowBitmap);
			l_blow = new Image(textureLBlow);
			addChild(l_blow);
			l_blow.width = 1584;
			l_blow.height = 1584;
			l_blow.x = -125;
			l_blow.y = 557;
			l_blow.alpha = 0;
			
			//textureLBlow.dispose();
			
			
			var l_gardenBitmap:Bitmap = new LGARDEN();
			var textureLGarden:Texture = Texture.fromBitmap(l_gardenBitmap);
			l_garden = new Image(textureLGarden);
			addChild(l_garden);
			l_garden.width = 2195;
			l_garden.height = 2195;
			l_garden.x = -449;
			l_garden.y = 237;
			l_garden.alpha = 0;
			
			//textureLGarden.dispose();
			
			
			var l_gradsBitmap:Bitmap = new LGRADS();
			var textureLGrads:Texture = Texture.fromBitmap(l_gradsBitmap);
			l_grads = new Image(textureLGrads);
			addChild(l_grads);
			l_grads.width = 1362;
			l_grads.height = 1362;
			l_grads.x = 1450;
			l_grads.y = -215;
			l_grads.alpha = 0;
			
			//textureLGrads.dispose();
			
			var l_hikeBitmap:Bitmap = new LHIKE();
			var textureLHike:Texture = Texture.fromBitmap(l_hikeBitmap);
			l_hike = new Image(textureLHike);
			addChild(l_hike);
			l_hike.width = 1331;
			l_hike.height = 1331;
			l_hike.x = 1320;
			l_hike.y = -222;
			l_hike.alpha = 0;
			
			//textureLHike.dispose();
			
			
			var l_leanBitmap:Bitmap = new LLEAN();
			var textureLLean:Texture = Texture.fromBitmap(l_leanBitmap);
			l_lean = new Image(textureLLean);
			addChild(l_lean);
			l_lean.width = 2009;
			l_lean.height = 2009;
			l_lean.x = 2416;
			l_lean.y = -89;
			l_lean.alpha = 0;
			
			//textureLLean.dispose();
			
			
			var l_liftBitmap:Bitmap = new LLIFT();
			var textureLLift:Texture = Texture.fromBitmap(l_liftBitmap);
			l_lift = new Image(textureLLift);
			addChild(l_lift);
			l_lift.width = 1973;
			l_lift.height = 1973;
			l_lift.x = 2600;
			l_lift.y = 120;
			l_lift.alpha = 0;
			
			//textureLLift.dispose();
			
			var l_makeBitmap:Bitmap = new LMAKE();
			var textureLMake:Texture = Texture.fromBitmap(l_makeBitmap);
			l_make = new Image(textureLMake);
			addChild(l_make);
			l_make.width = 1201;
			l_make.height = 1201;
			l_make.x = 1062;
			l_make.y = -120;
			l_make.alpha = 0;
			
			//textureLMake.dispose();
			
			var l_marryBitmap:Bitmap = new LMARRY();
			var texturePLMarry:Texture = Texture.fromBitmap(l_marryBitmap);
			l_marry = new Image(texturePLMarry);
			addChild(l_marry);
			l_marry.width = 2158;
			l_marry.height = 2158;
			l_marry.x = 2336;
			l_marry.y = 165;
			l_marry.alpha = 0;
			
			//texturePLMarry.dispose();
			
			var l_raiseBitmap:Bitmap = new LRAISE();
			var textureLRaise:Texture = Texture.fromBitmap(l_raiseBitmap);
			l_raise = new Image(textureLRaise);
			addChild(l_raise);
			l_raise.width = 1211;
			l_raise.height = 1211;
			l_raise.x = 1780;
			l_raise.y = -166;
			l_raise.alpha = 0;
			
			//textureLRaise.dispose();
			
			var l_readBitmap:Bitmap = new LREAD();
			var textureLRead:Texture = Texture.fromBitmap(l_readBitmap);
			l_read = new Image(textureLRead);
			addChild(l_read);
			l_read.width = 1974;
			l_read.height = 1974;
			l_read.x = -217;
			l_read.y = 257;
			l_read.alpha = 0;
			
			//textureLRead.dispose();
			
			var l_schoolBitmap:Bitmap = new LSCHOOL();
			var textureLSchool:Texture = Texture.fromBitmap(l_schoolBitmap);
			l_school = new Image(textureLSchool);
			addChild(l_school);
			l_school.width = 1877;
			l_school.height = 1877;
			l_school.x = -336;
			l_school.y = 297;
			l_school.alpha = 0;
			
			//textureLSchool.dispose();
			
			//-------------------------------
			
			
			
			//textureWave.dispose();
			
			
			logo.alpha = 0;
			
			d_bg.alpha = 0;
			h_bg.alpha = 0;
			l_bg.alpha = 0;
			p_bg.alpha = 0;
			d2_bg.alpha = 0;
			h2_bg.alpha = 0;
			l2_bg.alpha = 0;
			p2_bg.alpha = 0;
			feathers.alpha = 0;
			clouds.alpha = 0;
			leaves.alpha = 0;
			//drops.alpha = 0;
			
			b_bike.alpha = 0;
			b_clipboard.alpha = 0;
			b_register.alpha = 0;
			b_shake.alpha = 0;
			b_men.alpha = 0;
			b_colab.alpha = 0;
			b_crossed.alpha = 0;
			b_hardware.alpha = 0;
			b_books.alpha = 0;
			b_plates.alpha = 0;
			b_restaurant.alpha = 0;
			b_smile.alpha = 0;
			
			d_baby.alpha = 0;
			d_boys.alpha = 0;
			d_chicks.alpha = 0;
			d_smile.alpha = 0;
			d_swing.alpha = 0;
			d_team.alpha = 0;
			d_ladies.alpha = 0;
			d_fly.alpha = 0;
			d_plane.alpha = 0;
			d_look.alpha = 0;
			d_twirl.alpha = 0;
			d_coffee.alpha = 0;
			d_couple.alpha = 0;
		
			bgTimeline = new TimelineMax();
			bgTimeline.repeat = -1;
			
			cloudsTimeline = new TimelineMax();
			cloudsTimeline.repeat = -1;
			
			cloudsTimeline.append(new TweenLite(clouds, del/2, {y:-100}));
			cloudsTimeline.append(new TweenLite(clouds, del/2, {x:-200}));
			cloudsTimeline.append(new TweenLite(clouds, del/2, {y:0, x:0}));
			
			leavesTimeline = new TimelineMax();
			leavesTimeline.repeat = -1;
			
			leavesTimeline.append(new TweenLite(leaves, del/2, {y:-100}));
			leavesTimeline.append(new TweenLite(leaves, del/2, {x:-200}));
			leavesTimeline.append(new TweenLite(leaves, del/2, {y:0, x:0}));
			
			feathersTimeline = new TimelineMax();
			feathersTimeline.repeat = -1;
			
			feathersTimeline.append(new TweenLite(feathers, del/2, {y:-100}));
			feathersTimeline.append(new TweenLite(feathers, del/2, {x:-200}));
			feathersTimeline.append(new TweenLite(feathers, del/2, {y:0, x:0}));
			
			/*dropsTimeline = new TimelineMax();
			dropsTimeline.repeat = -1;
			
			dropsTimeline.append(new TweenLite(drops, del/2, {y:-100}));
			dropsTimeline.append(new TweenLite(drops, del/2, {x:-200}));
			dropsTimeline.append(new TweenLite(drops, del/2, {y:0, x:0}));*/
			
			flowerTimeline = new TimelineMax();
			flowerTimeline.repeat = -1;
			
			flowerTimeline.append(new TweenLite(flower, del/2, {y:-100}));
			flowerTimeline.append(new TweenLite(flower, del/2, {x:-200}));
			flowerTimeline.append(new TweenLite(flower, del/2, {y:0, x:0}));
			
			bTimeline = new TimelineMax();
			dTimeline = new TimelineMax();
			hTimeline = new TimelineMax();
			lTimeline = new TimelineMax();
			pTimeline = new TimelineMax();
			
			//background words
			businessTimeline = new TimelineMax();
			dreamsTimeline = new TimelineMax();
			homeTimeline = new TimelineMax();
			lifeTimeline = new TimelineMax();
			playTimeline = new TimelineMax();
			
			businessTimeline.appendMultiple([new TweenMax(business, del * .8, {rotation:deg2rad(-90)}), new TweenMax(business, del * .4, {alpha:.5, ease:Cubic.easeOut}), new TweenMax(business, del * .4, {alpha:0, delay:del * .4, ease:Cubic.easeIn})]);
			dreamsTimeline.appendMultiple([new TweenMax(dreams, del * .9, {rotation:deg2rad(-90)}), new TweenMax(dreams, del * .45, {alpha:.5, ease:Cubic.easeOut}), new TweenMax(dreams, del * .45, {alpha:0, delay:del * .45, ease:Cubic.easeIn})]);
			homeTimeline.appendMultiple([new TweenMax(home, del, {rotation:deg2rad(-90)}), new TweenMax(home, del/2, {alpha:.5, ease:Cubic.easeOut}), new TweenMax(home, del/2, {alpha:0, delay:del/2, ease:Cubic.easeIn})]);
			lifeTimeline.appendMultiple([new TweenMax(life, del, {rotation:deg2rad(-90)}), new TweenMax(life, del/2, {alpha:.5, ease:Cubic.easeOut}), new TweenMax(life, del/2, {alpha:0, delay:del/2, ease:Cubic.easeIn})]);
			playTimeline.appendMultiple([new TweenMax(games, del, {rotation:deg2rad(-90)}), new TweenMax(games, del/2, {alpha:.5, ease:Cubic.easeOut}), new TweenMax(games, del/2, {alpha:0, delay:del/2, ease:Cubic.easeIn})]);
			
			//business assets
			bBikeTimeline = new TimelineMax();
			bClipboardTimeline = new TimelineMax();
			bRegisterTimeline = new TimelineMax();
			bSmileTimeline = new TimelineMax();
			bCrossedTimeline = new TimelineMax();
			bPlatesTimeline = new TimelineMax();
			bBooksTimeline = new TimelineMax();
			bRestuarantTimeline = new TimelineMax();
			bHardwareTimeline = new TimelineMax();
			bMenTimeline = new TimelineMax();
			bColabTimeline = new TimelineMax();
			bShakeTimeline = new TimelineMax();
			
			bBikeTimeline.append(new TweenLite(b_bike, picTime, {alpha:picAlpha, y:b_bike.y + 50, x:b_bike.x + 50, ease:Sine.easeOut}));
			bBikeTimeline.append(new TweenLite(b_bike, picTime, {alpha:0, y:b_bike.y + 100, x:b_bike.x + 100, ease:Sine.easeIn}));
			bClipboardTimeline.append(new TweenLite(b_clipboard, picTime, {alpha:picAlpha, y:b_clipboard.y - 50, ease:Sine.easeOut}));
			bClipboardTimeline.append(new TweenLite(b_clipboard, picTime, {alpha:0, y:b_clipboard.y - 100, ease:Sine.easeIn}));
			bRegisterTimeline.append(new TweenLite(b_register, picTime, {alpha:picAlpha, y:b_register.y - 50, x:b_register.x - 50, ease:Sine.easeOut}));
			bRegisterTimeline.append(new TweenLite(b_register, picTime, {alpha:0, y:b_register.y - 100, x:b_register.x - 100, ease:Sine.easeIn}));
			
			
			bPlatesTimeline.append(new TweenLite(b_plates, picTime, {alpha:picAlpha, y:b_plates.y - 50, x:b_plates.x - 50, ease:Sine.easeOut}));
			bPlatesTimeline.append(new TweenLite(b_plates, picTime, {alpha:0, y:b_plates.y - 100, x:b_plates.x - 100, ease:Sine.easeIn}));
			
			bSmileTimeline.append(new TweenLite(b_smile, picTime, {alpha:picAlpha, scaleX:b_smile.scaleX + .5, scaleY:b_smile.scaleY + .5, ease:Sine.easeOut}));
			bSmileTimeline.append(new TweenLite(b_smile, picTime, {alpha:0, scaleX:b_smile.scaleX + 1, scaleY:b_smile.scaleY + 1, ease:Sine.easeIn}));
			
			bRestuarantTimeline.append(new TweenLite(b_restaurant, picTime, {alpha:picAlpha, y:b_restaurant.y - 50, x:b_restaurant.x - 50, ease:Sine.easeOut}));
			bRestuarantTimeline.append(new TweenLite(b_restaurant, picTime, {alpha:0, y:b_restaurant.y - 100, x:b_restaurant.x - 100, ease:Sine.easeIn}));
			
			bCrossedTimeline.append(new TweenLite(b_crossed, picTime, {alpha:picAlpha, y:b_crossed.y - 50, x:b_crossed.x + 50, ease:Sine.easeOut}));
			bCrossedTimeline.append(new TweenLite(b_crossed, picTime, {alpha:0, y:b_crossed.y - 100, x:b_crossed.x+ 100, ease:Sine.easeIn}));
			
			bHardwareTimeline.append(new TweenLite(b_hardware, picTime, {alpha:picAlpha, y:b_hardware.y - 50, x:b_hardware.x - 50, ease:Sine.easeOut}));
			bHardwareTimeline.append(new TweenLite(b_hardware, picTime, {alpha:0, y:b_hardware.y - 100, x:b_hardware.x - 100, ease:Sine.easeIn}));
			
			bBooksTimeline.append(new TweenLite(b_books, picTime, {alpha:picAlpha, y:b_books.y + 50, x:b_books.x + 50, ease:Sine.easeOut}));
			bBooksTimeline.append(new TweenLite(b_books, picTime, {alpha:0, y:b_books.y+ 100, x:b_books.x+ 100, ease:Sine.easeIn}));
			
			
			bShakeTimeline.append(new TweenLite(b_shake, picTime, {alpha:picAlpha, y:b_shake.y - 50, x:b_shake.x - 50, ease:Sine.easeOut}));
			bShakeTimeline.append(new TweenLite(b_shake, picTime, {alpha:0, y:b_shake.y - 100, x:b_shake.x - 100, ease:Sine.easeIn}));
			
			bMenTimeline.append(new TweenLite(b_men, picTime, {alpha:picAlpha, y:b_men.y - 50, x:b_men.x - 50, ease:Sine.easeOut}));
			bMenTimeline.append(new TweenLite(b_men, picTime, {alpha:0, y:b_men.y - 100, x:b_men.x - 100, ease:Sine.easeIn}));
			
			bColabTimeline.append(new TweenLite(b_colab, picTime, {alpha:picAlpha, y:b_colab.y - 50, x:b_colab.x - 50, ease:Sine.easeOut}));
			bColabTimeline.append(new TweenLite(b_colab, picTime, {alpha:0, y:b_colab.y - 100, x:b_colab.x - 100, ease:Sine.easeIn}));
			
			bTimeline.appendMultiple([bBikeTimeline, bClipboardTimeline, bRegisterTimeline]);
			bTimeline.appendMultiple([bSmileTimeline, bCrossedTimeline, bPlatesTimeline]);
			bTimeline.appendMultiple([bBooksTimeline, bRestuarantTimeline, bHardwareTimeline]);
			bTimeline.appendMultiple([bMenTimeline, bColabTimeline, bShakeTimeline]);
			
			//dreams assets
			dBabyTimeline = new TimelineMax();
			dBoysTimeline = new TimelineMax();
			dChicksTimeline = new TimelineMax();
			//dSmileTimeline = new TimelineMax();
			dSwingTimeline = new TimelineMax();
			dTeamTimeline = new TimelineMax();
			dCoffeeTimeline = new TimelineMax();
			dLookTimeline = new TimelineMax();
			dTwirlTimeline = new TimelineMax();
			dCoupleTimeline = new TimelineMax();
			dLadiesTimeline = new TimelineMax();
			dPlaneTimeline = new TimelineMax();
			dFlyTimeline = new TimelineMax();
			
			dBabyTimeline.append(new TweenLite(d_baby, picTime, {alpha:picAlpha, x:d_baby.x - 50, ease:Sine.easeOut}));
			dBabyTimeline.append(new TweenLite(d_baby, picTime, {alpha:0, x:d_baby.x - 100, ease:Sine.easeIn}));
			dBoysTimeline.append(new TweenLite(d_boys, picTime, {alpha:picAlpha, x:d_boys.x - 50, y:d_boys.y - 50, ease:Sine.easeOut}));
			dBoysTimeline.append(new TweenLite(d_boys, picTime, {alpha:0, x:d_boys.x - 100, y:d_boys.y - 100,  ease:Sine.easeIn}));
			dChicksTimeline.append(new TweenLite(d_chicks, picTime, {alpha:picAlpha, y:d_chicks.y - 50, ease:Sine.easeOut}));
			dChicksTimeline.append(new TweenLite(d_chicks, picTime, {alpha:0, y:d_chicks.y - 100, ease:Sine.easeIn}));
			dCoffeeTimeline.append(new TweenLite(d_coffee, picTime, {alpha:picAlpha, x:d_coffee.x + 50, ease:Sine.easeOut}));
			dCoffeeTimeline.append(new TweenLite(d_coffee, picTime, {alpha:0, x:d_coffee.x+ 100, ease:Sine.easeIn}));
			dSwingTimeline.append(new TweenLite(d_swing, picTime, {alpha:picAlpha, width:2448, height:2448, ease:Sine.easeOut}));
			dSwingTimeline.append(new TweenLite(d_swing, picTime, {alpha:0, width:2848, height:2848, ease:Sine.easeIn}));
			dTeamTimeline.append(new TweenLite(d_team, picTime, {alpha:picAlpha, width:1700, height:1700, ease:Sine.easeOut}));
			dTeamTimeline.append(new TweenLite(d_team, picTime, {alpha:0, width:1800, height:1800, ease:Sine.easeIn}));
			
			dLadiesTimeline.append(new TweenLite(d_ladies, picTime, {alpha:picAlpha, x:d_ladies.x + 50, ease:Sine.easeOut}));
			dLadiesTimeline.append(new TweenLite(d_ladies, picTime, {alpha:0, x:d_ladies.x+ 100, ease:Sine.easeIn}));
			dPlaneTimeline.append(new TweenLite(d_plane, picTime, {alpha:picAlpha, y:d_plane.y - 50, ease:Sine.easeOut}));
			dPlaneTimeline.append(new TweenLite(d_plane, picTime, {alpha:0, y:d_plane.y - 100, ease:Sine.easeIn}));
			dFlyTimeline.append(new TweenLite(d_fly, picTime, {alpha:picAlpha, x:d_fly.x - 50, scaleX:d_fly.scaleX + .5, scaleY:d_fly.scaleY + .5, ease:Sine.easeOut}));
			dFlyTimeline.append(new TweenLite(d_fly, picTime, {alpha:0, x:d_fly.x - 100, scaleX:d_fly.scaleX + 1, scaleY:d_fly.scaleY + 1, ease:Sine.easeIn}));
			
			dLookTimeline.append(new TweenLite(d_look, picTime, {alpha:picAlpha, x:d_look.x+ 100, y:d_look.y + 50, ease:Sine.easeOut}));
			dLookTimeline.append(new TweenLite(d_look, picTime, {alpha:0, x:d_look.x+ 100, y:d_look.y+ 100, ease:Sine.easeIn}));
			dTwirlTimeline.append(new TweenLite(d_twirl, picTime, {alpha:picAlpha, x:d_twirl.x + 50, scaleX:d_twirl.scaleX + .5, scaleY:d_twirl.scaleY + .5, ease:Sine.easeOut}));
			dTwirlTimeline.append(new TweenLite(d_twirl, picTime, {alpha:0, x:d_twirl.x+ 100, scaleX:d_twirl.scaleX + .5, scaleY:d_twirl.scaleY + 1, ease:Sine.easeIn}));
			dCoupleTimeline.append(new TweenLite(d_couple, picTime, {alpha:picAlpha, x:d_couple.x - 50, y:d_couple.y - 50, ease:Sine.easeOut}));
			dCoupleTimeline.append(new TweenLite(d_couple, picTime, {alpha:0, x:d_couple.x - 100, y:d_couple.y - 100, ease:Sine.easeIn}));
			
			dTimeline.appendMultiple([dBabyTimeline, dSwingTimeline, dBoysTimeline]);
			dTimeline.appendMultiple([dCoffeeTimeline, dChicksTimeline, dTeamTimeline]);
			dTimeline.appendMultiple([dLookTimeline, dTwirlTimeline, dCoupleTimeline]);
			dTimeline.appendMultiple([dLadiesTimeline, dPlaneTimeline, dFlyTimeline]);
			
			//home assets
			
			hbakeTimeline = new TimelineMax();
			hbeachTimeline = new TimelineMax();
			hboyTimeline = new TimelineMax();
			hdaddaughtersTimeline = new TimelineMax();
			hdogTimeline = new TimelineMax();
			hfishingTimeline = new TimelineMax();
			hgirldogTimeline = new TimelineMax();
			hgirlonringsTimeline = new TimelineMax();
			hgirlsTimeline = new TimelineMax();
			hhouseTimeline = new TimelineMax();
			hjohnsonkidsTimeline = new TimelineMax();
			hjohnsonmomTimeline = new TimelineMax();
			hplantingTimeline = new TimelineMax();
			hSmileTimeline = new TimelineMax();
			
			hgirlsTimeline.append(new TweenLite(h_girls, 12, {alpha:picAlpha, x:h_girls.x + 20, ease:Sine.easeOut}));
			hgirlsTimeline.append(new TweenLite(h_girls, 12, {alpha:0, x:h_girls.x + 40, ease:Sine.easeIn}));
			hfishingTimeline.append(new TweenLite(h_fishing, 14, {alpha:picAlpha, ease:Sine.easeOut}));
			hfishingTimeline.append(new TweenLite(h_fishing, 14, {alpha:0, ease:Sine.easeIn}));
			hboyTimeline.delay = 2;
			hboyTimeline.append(new TweenLite(h_boy, 11, {alpha:picAlpha, y:h_boy.y - 200, ease:Sine.easeOut}));
			hboyTimeline.append(new TweenLite(h_boy, 11, {alpha:0, y:h_boy.y - 400, ease:Sine.easeIn}));
			hgirlonringsTimeline.delay = 3;
			hgirlonringsTimeline.append(new TweenLite(h_girlonrings, 6, {alpha:picAlpha, y:h_girlonrings.y + 230, scaleX:h_girlonrings.scaleX + .5, scaleY:h_girlonrings.scaleY + .5, ease:Sine.easeOut}));
			hgirlonringsTimeline.append(new TweenLite(h_girlonrings, 6, {alpha:0, y:-(h_girlonrings.height * 2), scaleX:h_girlonrings.scaleX + 1, scaleY:h_girlonrings.scaleY + 1, ease:Sine.easeIn}));
			hdaddaughtersTimeline.delay = 26;
			hdaddaughtersTimeline.append(new TweenLite(h_daddaughters, 13, {alpha:picAlpha, ease:Sine.easeOut}));
			hdaddaughtersTimeline.append(new TweenLite(h_daddaughters, 13, {alpha:0, ease:Sine.easeIn}));
			hgirldogTimeline.delay = 28;
			hgirldogTimeline.append(new TweenLite(h_girldog, 13, {alpha:picAlpha, ease:Sine.easeOut}));
			hgirldogTimeline.append(new TweenLite(h_girldog, 13, {alpha:0, ease:Sine.easeIn}));
			hbakeTimeline.delay = 32;
			hbakeTimeline.append(new TweenLite(h_bake, 13, {alpha:picAlpha, ease:Sine.easeOut}));
			hbakeTimeline.append(new TweenLite(h_bake, 13, {alpha:0, ease:Sine.easeIn}));
			hjohnsonmomTimeline.delay = 34;
			hjohnsonmomTimeline.append(new TweenLite(h_johnsonmom, 13, {alpha:picAlpha, ease:Sine.easeOut}));
			hjohnsonmomTimeline.append(new TweenLite(h_johnsonmom, 13, {alpha:0, ease:Sine.easeIn}));
			hjohnsonkidsTimeline.delay = 39;
			hjohnsonkidsTimeline.append(new TweenLite(h_johnsonkids, 11, {alpha:picAlpha, ease:Sine.easeOut}));
			hjohnsonkidsTimeline.append(new TweenLite(h_johnsonkids, 11, {alpha:0, ease:Sine.easeIn}));
			hbeachTimeline.delay = 48;
			hbeachTimeline.append(new TweenLite(h_beach, 14, {alpha:picAlpha, ease:Sine.easeOut}));
			hbeachTimeline.append(new TweenLite(h_beach, 14, {alpha:0, ease:Sine.easeIn}));
			hhouseTimeline.delay = 59;
			hhouseTimeline.append(new TweenLite(h_house, 13, {alpha:picAlpha, ease:Sine.easeOut}));
			hhouseTimeline.append(new TweenLite(h_house, 13, {alpha:0, ease:Sine.easeIn}));
			hdogTimeline.delay = 62;
			hdogTimeline.append(new TweenLite(h_dog, 12, {alpha:picAlpha, ease:Sine.easeOut}));
			hdogTimeline.append(new TweenLite(h_dog, 12, {alpha:0, ease:Sine.easeIn}));
			hplantingTimeline.delay = 64;
			hplantingTimeline.append(new TweenLite(h_planting, 11, {alpha:picAlpha, ease:Sine.easeOut}));
			hplantingTimeline.append(new TweenLite(h_planting, 11, {alpha:0, ease:Sine.easeIn}));
			hSmileTimeline.delay = 70;
			hSmileTimeline.append(new TweenLite(h_smile, 10, {alpha:picAlpha, x:h_smile.x + 50, ease:Sine.easeOut}));
			hSmileTimeline.append(new TweenLite(h_smile, 10, {alpha:0, x:h_smile.x + 100, ease:Sine.easeIn}));
		
			hTimeline.appendMultiple([hgirlsTimeline, hfishingTimeline, hboyTimeline, hgirlonringsTimeline, hdaddaughtersTimeline, hgirldogTimeline, 
									  hbakeTimeline, hjohnsonmomTimeline, hjohnsonkidsTimeline, hbeachTimeline, hhouseTimeline, hdogTimeline, 
									  hplantingTimeline, hSmileTimeline]);
			
			//life assets
			lBabyTimeline = new TimelineMax();
			lBlowTimeline = new TimelineMax();
			lGardenTimeline = new TimelineMax();
			lGradsTimeline = new TimelineMax();
			lHikeTimeline = new TimelineMax();
			lLeanTimeline = new TimelineMax();
			lLiftTimeline = new TimelineMax();
			lMakeTimeline = new TimelineMax();
			lMarryTimeline = new TimelineMax();
			lRaiseTimeline = new TimelineMax();
			lReadTimeline = new TimelineMax();
			lSchoolTimeline = new TimelineMax();
			
			lReadTimeline.append(new TweenLite(l_read, picTime, {alpha:picAlpha, x:l_read.x + 50, y:l_read.y - 50, ease:Sine.easeOut}));
			lReadTimeline.append(new TweenLite(l_read, picTime, {alpha:0, x:l_read.x + 100, y:l_read.y - 100, ease:Sine.easeIn}));
			lHikeTimeline.append(new TweenLite(l_hike, picTime, {alpha:picAlpha, scaleX:l_hike.scaleX + .5, scaleY:l_hike.scaleX + .5, ease:Sine.easeOut}));
			lHikeTimeline.append(new TweenLite(l_hike, picTime, {alpha:0, scaleX:l_hike.scaleX + 1, scaleY:l_hike.scaleY + 1, ease:Sine.easeIn}));
			lLeanTimeline.append(new TweenLite(l_lean, picTime, {alpha:picAlpha, x:l_lean.x - 50, ease:Sine.easeOut}));
			lLeanTimeline.append(new TweenLite(l_lean, picTime, {alpha:0, x:l_lean.x - 100, ease:Sine.easeIn}));
			
			lSchoolTimeline.append(new TweenLite(l_school, picTime, {alpha:picAlpha, y:l_school.y - 50, ease:Sine.easeOut}));
			lSchoolTimeline.append(new TweenLite(l_school, picTime, {alpha:0, y:l_school.y - 100, ease:Sine.easeIn}));
			lGradsTimeline.append(new TweenLite(l_grads, picTime, {alpha:picAlpha, x:l_grads.x - 50, y:l_grads.y + 50, ease:Sine.easeOut}));
			lGradsTimeline.append(new TweenLite(l_grads, picTime, {alpha:0, x:l_grads.x - 100, y:l_grads.y + 100, ease:Sine.easeIn}));
			lMarryTimeline.append(new TweenLite(l_marry, picTime, {alpha:picAlpha, scaleX:l_marry.scaleX + .5, scaleY:l_marry.scaleX + .5, ease:Sine.easeOut}));
			lMarryTimeline.append(new TweenLite(l_marry, picTime, {alpha:0, scaleX:l_marry.scaleX + 1, scaleY:l_marry.scaleY + 1, ease:Sine.easeIn}));
			
			lGardenTimeline.append(new TweenLite(l_garden, picTime, {alpha:picAlpha, x:l_garden.x + 50, y:l_garden.y + 50, ease:Sine.easeOut}));
			lGardenTimeline.append(new TweenLite(l_garden, picTime, {alpha:0, x:l_garden.x + 100, y:l_garden.y + 100, ease:Sine.easeIn}));
			lRaiseTimeline.append(new TweenLite(l_raise, picTime, {alpha:picAlpha, x:l_raise.x - 50, y:l_raise.y + 50, scaleX:l_raise.scaleX + .5, scaleY:l_raise.scaleX + .5, ease:Sine.easeOut}));
			lRaiseTimeline.append(new TweenLite(l_raise, picTime, {alpha:0, x:l_raise.x - 100, y:l_raise.y + 100, scaleX:l_raise.scaleX + 1, scaleY:l_raise.scaleY + 1, ease:Sine.easeIn}));
			lLiftTimeline.append(new TweenLite(l_lift, picTime, {alpha:picAlpha, x:l_lift.x - 50, ease:Sine.easeOut}));
			lLiftTimeline.append(new TweenLite(l_lift, picTime, {alpha:0, x:l_lift.x - 100, ease:Sine.easeIn}));
			
			lBlowTimeline.append(new TweenLite(l_blow, picTime, {alpha:picAlpha, x:l_blow.x + 50, ease:Sine.easeOut}));
			lBlowTimeline.append(new TweenLite(l_blow, picTime, {alpha:0, x:l_blow.x + 100, ease:Sine.easeIn}));
			lMakeTimeline.append(new TweenLite(l_make, picTime, {alpha:picAlpha, scaleX:l_make.scaleX + .5, scaleY:l_make.scaleX + .5, ease:Sine.easeOut}));
			lMakeTimeline.append(new TweenLite(l_make, picTime, {alpha:0, scaleX:l_make.scaleX + 1, scaleY:l_make.scaleY + 1, ease:Sine.easeIn}));
			lBabyTimeline.append(new TweenLite(l_baby, picTime, {alpha:picAlpha, x:l_baby.x - 50, ease:Sine.easeOut}));
			lBabyTimeline.append(new TweenLite(l_baby, picTime, {alpha:0, x:l_baby.x - 100, ease:Sine.easeIn}));
			
			lTimeline.appendMultiple([lReadTimeline, lHikeTimeline, lLeanTimeline]);
			lTimeline.appendMultiple([lSchoolTimeline, lGradsTimeline, lMarryTimeline]);
			lTimeline.appendMultiple([lGardenTimeline, lRaiseTimeline, lLiftTimeline]);
			lTimeline.appendMultiple([lBlowTimeline, lMakeTimeline, lBabyTimeline]);
			
			
			//play assets
			pBallpitTimeline = new TimelineMax();
			pBaseballTimeline = new TimelineMax();
			pBikeTimeline = new TimelineMax();
			pCameraTimeline = new TimelineMax();
			pCatchTimeline = new TimelineMax();
			pFetchTimeline = new TimelineMax();
			pPeekTimeline = new TimelineMax();
			pShouldersTimeline = new TimelineMax();
			pSoccerTimeline = new TimelineMax();
			pTireTimeline = new TimelineMax();
			pSwimTimeline = new TimelineMax();
			pToysTimeline = new TimelineMax();
			
			pPeekTimeline.append(new TweenLite(p_peek, picTime, {alpha:picAlpha, x:p_peek.x + 50, ease:Sine.easeOut}));
			pPeekTimeline.append(new TweenLite(p_peek, picTime, {alpha:0, x:p_peek.x + 100, ease:Sine.easeIn}));
			pFetchTimeline.append(new TweenLite(p_fetch, picTime, {alpha:picAlpha, y:p_fetch.y + 50, ease:Sine.easeOut}));
			pFetchTimeline.append(new TweenLite(p_fetch, picTime, {alpha:0, y:p_fetch.y + 100, ease:Sine.easeIn}));
			pBikeTimeline.append(new TweenLite(p_bike, picTime, {alpha:picAlpha, scaleX:p_bike.scaleX + .5, scaleY:p_bike.scaleX + .5, ease:Sine.easeOut}));
			pBikeTimeline.append(new TweenLite(p_bike, picTime, {alpha:0, scaleX:p_bike.scaleX + 1, scaleY:p_bike.scaleY + 1, ease:Sine.easeIn}));
			
			pBallpitTimeline.append(new TweenLite(p_ballpit, picTime, {alpha:picAlpha, scaleX:p_ballpit.scaleX + .5, scaleY:p_ballpit.scaleY + .5, ease:Sine.easeOut}));
			pBallpitTimeline.append(new TweenLite(p_ballpit, picTime, {alpha:0, scaleX:p_ballpit.scaleX + 1, scaleY:p_ballpit.scaleY + 1, ease:Sine.easeIn}));
			pToysTimeline.append(new TweenLite(p_toys, picTime, {alpha:picAlpha, y:p_toys.y + 50, x:p_toys.x + 50, ease:Sine.easeOut}));
			pToysTimeline.append(new TweenLite(p_toys, picTime, {alpha:0, y:p_toys.y + 100, x:p_toys.x + 100, ease:Sine.easeIn}));
			pSoccerTimeline.append(new TweenLite(p_soccer, picTime, {alpha:picAlpha, y:p_soccer.y - 50, ease:Sine.easeOut}));
			pSoccerTimeline.append(new TweenLite(p_soccer, picTime, {alpha:0, y:p_soccer.y - 100, ease:Sine.easeIn}));
			
			pSwimTimeline.append(new TweenLite(p_swim, picTime, {alpha:picAlpha, scaleX:p_swim.scaleX + .5, scaleY:p_swim.scaleY + .5, ease:Sine.easeOut}));
			pSwimTimeline.append(new TweenLite(p_swim, picTime, {alpha:0, scaleX:p_swim.scaleX + 1, scaleY:p_swim.scaleY + 1, ease:Sine.easeIn}));
			pBaseballTimeline.append(new TweenLite(p_baseball, picTime, {alpha:picAlpha, y:p_baseball.y - 50, ease:Sine.easeOut}));
			pBaseballTimeline.append(new TweenLite(p_baseball, picTime, {alpha:0, y:p_baseball.y - 100, ease:Sine.easeIn}));
			pShouldersTimeline.append(new TweenLite(p_shoulders, picTime, {alpha:picAlpha,  ease:Sine.easeOut}));
			pShouldersTimeline.append(new TweenLite(p_shoulders, picTime, {alpha:0, ease:Sine.easeIn}));
			
			pCameraTimeline.append(new TweenLite(p_camera, picTime, {alpha:picAlpha, y:p_camera.y - 50, scaleX:p_camera.scaleX + .5, scaleY:p_camera.scaleY + .5, ease:Sine.easeOut}));
			pCameraTimeline.append(new TweenLite(p_camera, picTime, {alpha:0, y:p_camera.y - 100, scaleX:p_camera.scaleX + 1, scaleY:p_camera.scaleY + 1, ease:Sine.easeIn}));
			pCatchTimeline.append(new TweenLite(p_catch, picTime, {alpha:picAlpha, x:p_catch.x - 50, scaleX:p_catch.scaleX + .5, scaleY:p_catch.scaleY + .5, ease:Sine.easeOut}));
			pCatchTimeline.append(new TweenLite(p_catch, picTime, {alpha:0, x:p_catch.x - 100, scaleX:p_catch.scaleX + 1, scaleY:p_catch.scaleY + 1, ease:Sine.easeIn}));
			pTireTimeline.append(new TweenLite(p_tire, picTime, {alpha:picAlpha, x:p_tire.x - 50, ease:Sine.easeOut}));
			pTireTimeline.append(new TweenLite(p_tire, picTime, {alpha:0, x:p_tire.x - 100, ease:Sine.easeIn}));
			
			pTimeline.appendMultiple([pPeekTimeline, pFetchTimeline, pBikeTimeline]);
			pTimeline.appendMultiple([pBallpitTimeline, pToysTimeline, pSoccerTimeline]);
			pTimeline.appendMultiple([pSwimTimeline, pBaseballTimeline, pShouldersTimeline]);
			pTimeline.appendMultiple([pCameraTimeline, pCatchTimeline, pTireTimeline]);
			
			/*businessAnim = new BusinessAnim();
			addChild(businessAnim);
			businessAnim.width = 4380;
			businessAnim.x = -20;
			businessAnim.height = 1920;
			businessAnim.alpha = 0;
			businessAnim.stop();
			
			dreamAnim = new DreamAnim();
			addChild(dreamAnim);
			dreamAnim.width = 4380;
			dreamAnim.x = -20;
			dreamAnim.height = 1920;
			dreamAnim.alpha = 0;
			dreamAnim.stop();
			
			homeAnim = new HomeAnim();
			addChild(homeAnim);
			homeAnim.width = 4380;
			homeAnim.x = -20;
			homeAnim.height = 1920;
			homeAnim.alpha = 0;
			homeAnim.stop();
			
			lifeAnim = new LifeAnim();
			addChild(lifeAnim);
			lifeAnim.width = 4380;
			lifeAnim.x = -20;
			lifeAnim.height = 1920;
			lifeAnim.alpha = 0;
			lifeAnim.stop();
			
			playAnim = new PlayAnim();
			addChild(playAnim);
			playAnim.width = 4380;
			playAnim.x = -20;
			playAnim.height = 1920;
			playAnim.alpha = 0;
			playAnim.stop();
			
			businessAnimTimeline = new TimelineMax();
			businessAnimTimeline.append(new TweenMax(businessAnim, trans/2, {alpha:.5}));
			businessAnimTimeline.append(new TweenMax(businessAnim, del, {frame:businessAnim.totalFrames, ease:Linear.easeNone}));
			businessAnimTimeline.append(new TweenMax(businessAnim, trans/2, {alpha:0}));
			
			dreamAnimTimeline = new TimelineMax();
			dreamAnimTimeline.append(new TweenMax(dreamAnim, trans/2, {alpha:.5}));
			dreamAnimTimeline.append(new TweenMax(dreamAnim, del, {frame:dreamAnim.totalFrames, ease:Linear.easeNone}));
			dreamAnimTimeline.append(new TweenMax(dreamAnim, trans/2, {alpha:0}));
			
			homeAnimTimeline = new TimelineMax();
			homeAnimTimeline.append(new TweenMax(homeAnim, trans/2, {alpha:.5}));
			homeAnimTimeline.append(new TweenMax(homeAnim, del, {frame:homeAnim.totalFrames, ease:Linear.easeNone}));
			homeAnimTimeline.append(new TweenMax(homeAnim, trans/2, {alpha:0}));
			
			lifeAnimTimeline = new TimelineMax();
			lifeAnimTimeline.append(new TweenMax(lifeAnim, trans/2, {alpha:.5}));
			lifeAnimTimeline.append(new TweenMax(lifeAnim, del, {frame:lifeAnim.totalFrames, ease:Linear.easeNone}));
			lifeAnimTimeline.append(new TweenMax(lifeAnim, trans/2, {alpha:0}));
			
			playAnimTimeline = new TimelineMax();
			playAnimTimeline.append(new TweenMax(playAnim, trans/2, {alpha:.5}));
			playAnimTimeline.append(new TweenMax(playAnim, del, {frame:playAnim.totalFrames, ease:Linear.easeNone}));
			playAnimTimeline.append(new TweenMax(playAnim, trans/2, {alpha:0}));*/
			
			//master timeline assembly
			bgTimeline.appendMultiple([new TweenLite(d_bg, trans, {delay:del, alpha:1}), 
									   new TweenLite(d2_bg, trans, {delay:del, alpha:1}), 
									   new TweenLite(feathers, trans, {delay:del, alpha:1}), 
									   new TweenMax(logo, trans, {alpha:0, onComplete:addMaskB}), 
									   new TweenMax(logo, trans, {delay:del, alpha:picAlpha}), 
									   new TweenMax(businessWords1, del + trans, {x:0}), 
									   new TweenMax(businessWords1, trans, {alpha:1}),  
									   new TweenMax(businessWords1, trans, {delay:del, alpha:0, onComplete:removeMaskB}), 
									   new TweenMax(businessWords2, del + trans, {x:175}), 
									   new TweenMax(businessWords2, trans, {alpha:1}),  
									   new TweenMax(businessWords2, trans, {delay:del, alpha:0}),
									   new TweenMax(logo, del, {onComplete:endBusinessStartDream}),
									   bTimeline, 
									   businessTimeline]);
			bgTimeline.appendMultiple([new TweenLite(h_bg, trans, {delay:del, alpha:1}), 
									   new TweenLite(h2_bg, trans, {delay:del, alpha:1}), 
									   new TweenLite(clouds, trans, {delay:del, alpha:1}), 
									   new TweenMax(logo, trans, {alpha:0, onComplete:addMaskD}), 
									   new TweenMax(logo, trans, {delay:del, alpha:picAlpha}), 
									   new TweenLite(d_bg, .1, {delay:del + trans, alpha:0}), 
									   new TweenLite(d2_bg, .1, {delay:del + trans, alpha:0}), 
									   new TweenLite(feathers, .1, {delay:del + trans, alpha:0}), 
									   new TweenMax(dreamWords1, del + trans, {x:0}), 
									   new TweenMax(dreamWords1, trans, {alpha:1}),  
									   new TweenMax(dreamWords1, trans, {delay:del, alpha:0, onComplete:removeMaskD}), 
									   new TweenMax(dreamWords2, del + trans, {x:62}), 
									   new TweenMax(dreamWords2, trans, {alpha:1}),  
									   new TweenMax(dreamWords2, trans, {delay:del, alpha:0}),
									   new TweenMax(logo, del, {onComplete:endDreamStartHome}),
									   dTimeline, 
									   dreamsTimeline]);
			bgTimeline.appendMultiple([new TweenLite(l_bg, trans, {delay:del, alpha:1}), 
									   new TweenLite(l2_bg, trans, {delay:del, alpha:1}),
									   new TweenLite(leaves, trans, {delay:del, alpha:1}),
									   new TweenMax(logo, trans, {alpha:0, onComplete:addMaskH}), 
									   new TweenMax(logo, trans, {delay:del, alpha:picAlpha}), 
									   new TweenLite(h_bg, .1, {delay:del + trans, alpha:0}), 
									   new TweenLite(h2_bg, .1, {delay:del + trans, alpha:0}), 
									   new TweenLite(clouds, .1, {delay:del + trans, alpha:0}),
									   new TweenMax(homeWords1, del + trans, {x:0}), 
									   new TweenMax(homeWords1, trans, {alpha:1}),  
									   new TweenMax(homeWords1, trans, {delay:del, alpha:0, onComplete:removeMaskH}), 
									   new TweenMax(homeWords2, del + trans, {x:0}), 
									   new TweenMax(homeWords2, trans, {alpha:1}),  
									   new TweenMax(homeWords2, trans, {delay:del, alpha:0}),
									   new TweenMax(logo, del, {onComplete:endHomeStartLife}),
									   hTimeline, 
									   homeTimeline]);
			bgTimeline.appendMultiple([new TweenLite(p_bg, trans, {delay:del, alpha:1}), 
									   new TweenLite(p2_bg, trans, {delay:del, alpha:1}), 
									 //  new TweenLite(drops, trans, {delay:del, alpha:1}), 
									   new TweenMax(logo, trans, {alpha:0, onComplete:addMaskL}), 
									   new TweenMax(logo, trans, {delay:del, alpha:picAlpha}), 
									   new TweenLite(l_bg, .1, {delay:del + trans, alpha:0}), 
									   new TweenLite(l2_bg, .1, {delay:del + trans, alpha:0}), 
									   new TweenLite(leaves, .1, {delay:del + trans, alpha:0}), 
									   new TweenMax(lifeWords1, del + trans, {x:0}), 
									   new TweenMax(lifeWords1, trans, {alpha:1}),  
									   new TweenMax(lifeWords1, trans, {delay:del, alpha:0, onComplete:removeMaskL}),
									   new TweenMax(logo, del, {onComplete:endLifeStartPlay}),
									   lTimeline, 
									   lifeTimeline]);
			bgTimeline.appendMultiple([new TweenLite(p_bg, trans, {delay:del, alpha:0}), 
									   new TweenLite(p2_bg, trans, {delay:del, alpha:0}), 
									  // new TweenLite(drops, trans, {delay:del, alpha:0}),
									   new TweenMax(logo, trans, {alpha:0, onComplete:addMaskP}), 
									   new TweenMax(logo, trans, {delay:del, alpha:picAlpha}), 
									   new TweenMax(logo, trans, {delay:del + trans, alpha:0}), 
									   new TweenMax(playWords1, del + trans, {x:0}), 
									   new TweenMax(playWords1, trans, {alpha:1}),  
									   new TweenMax(playWords1, trans, {delay:del, alpha:0, onComplete:removeMaskP}), 
									   new TweenMax(logo, del, {onComplete:endPlayStartBusiness}),
									   pTimeline, 
									   playTimeline]);
			//bgTimeline.appendMultiple([new TweenLite(p_bg, trans, {alpha:0}), 
									   //new TweenLite(p2_bg, trans, {alpha:0}),
									   //new TweenLite(flower, trans, {alpha:0})]);
			//bgTimeline.onComplete
		}
		
		private function addMaskB():void
		{
			addEventListener(Event.ENTER_FRAME, moveMaskB);
		}
		
		private function moveMaskB(e:Event):void
		{
			//maskVal *= -1;
			//if (maskVal == 1)
			//{
				bRect.x -= maskSpeed;
				bContents.clipRect = bRect;
				if(bRect.x < 0)
				{
					bRect.x = 0;
					bContents.clipRect = bRect;
				}
				bRect2.x -= maskSpeed;
				bContents2.clipRect = bRect2;
				if(bRect2.x < 0)
				{
					bRect2.x = 0;
					bContents2.clipRect = bRect2;
				}
			//}
		}
		
		private function removeMaskB():void
		{
			removeEventListener(Event.ENTER_FRAME, moveMaskB);
			bRect.x = maskStart2;
			bRect2.x = maskStart1;
			bContents.clipRect = bRect;
			bContents2.clipRect = bRect2;
		}
		
		private function addMaskD():void
		{
			addEventListener(Event.ENTER_FRAME, moveMaskD);
		}
		
		private function moveMaskD(e:Event):void
		{
			//maskVal *= -1;
			//if (maskVal == 1)
			//{
				dRect.x -= maskSpeed;
				dContents.clipRect = dRect;
				if(dRect.x < 0)
				{
					dRect.x = 0;
					dContents.clipRect = dRect;
				}
				dRect2.x -= maskSpeed;
				dContents2.clipRect = dRect2;
				if(dRect2.x < 0)
				{
					dRect2.x = 0;
					dContents2.clipRect = dRect2;
				}
			//}
		}
		
		private function removeMaskD():void
		{
			removeEventListener(Event.ENTER_FRAME, moveMaskD);
			dRect.x = maskStart1;
			dRect2.x = maskStart2;
			dContents.clipRect = dRect;
			dContents2.clipRect = dRect2;
		}
		
		private function addMaskH():void
		{
			addEventListener(Event.ENTER_FRAME, moveMaskH);
		}
		
		private function moveMaskH(e:Event):void
		{
			//maskVal *= -1;
			//if (maskVal == 1)
			//{
				hRect.x -= maskSpeed;
				hContents.clipRect = hRect;
				if(hRect.x < 0)
				{
					hRect.x = 0;
					hContents.clipRect = hRect;
				}
				hRect2.x -= maskSpeed;
				hContents2.clipRect = hRect2;
				if(hRect2.x < 0)
				{
					hRect2.x = 0;
					hContents2.clipRect = hRect2;
				}
			//}
		}
		
		private function removeMaskH():void
		{
			removeEventListener(Event.ENTER_FRAME, moveMaskH);
			hRect.x = maskStart1;
			hRect2.x = maskStart2;
			hContents.clipRect = hRect;
			hContents2.clipRect = hRect2;
		}
		
		private function addMaskL():void
		{
			addEventListener(Event.ENTER_FRAME, moveMaskL);
		}
		
		private function moveMaskL(e:Event):void
		{
			//maskVal *= -1;
			//if (maskVal == 1)
			////{
				lRect.x -= maskSpeed;
				lContents.clipRect = lRect;
				if(lRect.x < 0)
				{
					lRect.x = 0;
					lContents.clipRect = lRect;
				}
			//}
		}
		
		private function removeMaskL():void
		{
			removeEventListener(Event.ENTER_FRAME, moveMaskL);
			lRect.x = maskStart1;
			lContents.clipRect = lRect;
		}
		
		private function addMaskP():void
		{
			addEventListener(Event.ENTER_FRAME, moveMaskP);
		}
		
		private function moveMaskP(e:Event):void
		{
			//maskVal *= -1;
			//if (maskVal == 1)
			//{
				pRect.x -= maskSpeed;
				pContents.clipRect = pRect;
				if(pRect.x < 0)
				{
					pRect.x = 0;
					pContents.clipRect = pRect;
				}
			//}
			
		}
		
		private function removeMaskP():void
		{
			removeEventListener(Event.ENTER_FRAME, moveMaskP);
			pRect.x = maskStart1;
			pContents.clipRect = pRect;
		}
		
		private function animSpeck(s:Image, v:Number = 1, h:Number = 1):void
		{
			var sc:Number = (Math.random() * 2.5) + .5;
			TweenMax.to(s, (Math.random() * 60) + 60, {x:(Math.random() * 4320 * v) - (s.x * (v - 1)), y:(Math.random() * 1920 * h) - (s.y * (h - 1)), scaleX:sc, scaleY:sc, onComplete:speckAgain, onCompleteParams:[s, v, h], ease:Linear.easeNone});
			
		}
		
		private function animBand(s:Image):void
		{
			var sc:Number = Math.random() * 3;
			TweenMax.to(s, (Math.random() * 60) + 60, {x:Math.random() * 4320, onComplete:bandAgain, onCompleteParams:[s], ease:Linear.easeNone});
			
		}
		
		private function bandAgain(s:Image):void
		{
			animBand(s);
		}
		
		private function speckAgain(s:Image, v:Number = 1, h:Number = 1):void
		{
			animSpeck(s, v, h);
		}
		
		private function endBusinessStartDream():void
		{
			trace("endBusinessStartDream");
			TweenMax.to(speck1, 3, {alpha:0});
			TweenMax.to(speck2, 3, {alpha:0});
			TweenMax.to(speck3, 3, {alpha:0});
			TweenMax.to(speck4, 3, {alpha:0});
			TweenMax.to(speck5, 3, {alpha:0});
			TweenMax.to(speck6, 3, {alpha:0});
			TweenMax.to(speck7, 3, {alpha:0});
			TweenMax.to(speck8, 3, {alpha:0});
			TweenMax.to(speck9, 3, {alpha:0});
			TweenMax.to(speck10, 3, {alpha:0});
			TweenMax.to(star1, 6, {delay:10, alpha:1});
			TweenMax.to(star2, 6, {delay:10, alpha:1});
			TweenMax.to(star3, 6, {delay:10, alpha:1});
			TweenMax.to(star4, 6, {delay:10, alpha:1});
			TweenMax.to(star5, 6, {delay:10, alpha:1});
			TweenMax.to(star6, 6, {delay:10, alpha:1});
			TweenMax.to(star7, 6, {delay:10, alpha:1});
			TweenMax.to(star8, 6, {delay:10, alpha:1});
			TweenMax.to(star9, 6, {delay:10, alpha:1});
			TweenMax.to(star10, 6, {delay:10, alpha:1});
		}
		
		private function endDreamStartHome():void
		{
			TweenMax.to(star1, 3, {alpha:0});
			TweenMax.to(star2, 3, {alpha:0});
			TweenMax.to(star3, 3, {alpha:0});
			TweenMax.to(star4, 3, {alpha:0});
			TweenMax.to(star5, 3, {alpha:0});
			TweenMax.to(star6, 3, {alpha:0});
			TweenMax.to(star7, 3, {alpha:0});
			TweenMax.to(star8, 3, {alpha:0});
			TweenMax.to(star9, 3, {alpha:0});
			TweenMax.to(star10, 3, {alpha:0});
			TweenMax.to(flare, 3, {alpha:.3});
			TweenMax.to(flareRot, 6, {delay:10, alpha:.1});
			TweenMax.to(flareRot2, 6, {delay:10, alpha:.1});
		}
		
		private function endHomeStartLife():void
		{
			TweenMax.to(flare, 3, {alpha:0});
			TweenMax.to(flareRot, 3, {alpha:0});
			TweenMax.to(flareRot2, 3, {alpha:0});
			TweenMax.to(band1, 6, {delay:10, alpha:1});
			TweenMax.to(band2, 6, {delay:10, alpha:1});
			TweenMax.to(band3, 6, {delay:10, alpha:1});
			TweenMax.to(band4, 6, {delay:10, alpha:1});
			TweenMax.to(band5, 6, {delay:10, alpha:1});
			TweenMax.to(band6, 6, {delay:10, alpha:1});
			TweenMax.to(band7, 6, {delay:10, alpha:1});
			TweenMax.to(band8, 6, {delay:10, alpha:1});
			TweenMax.to(band9, 6, {delay:10, alpha:1});
			TweenMax.to(band10, 6, {delay:10, alpha:1});
		}
		
		private function endLifeStartPlay():void
		{
			
			TweenMax.to(band1, 3, {alpha:0});
			TweenMax.to(band2, 3, {alpha:0});
			TweenMax.to(band3, 3, {alpha:0});
			TweenMax.to(band4, 3, {alpha:0});
			TweenMax.to(band5, 3, {alpha:0});
			TweenMax.to(band6, 3, {alpha:0});
			TweenMax.to(band7, 3, {alpha:0});
			TweenMax.to(band8, 3, {alpha:0});
			TweenMax.to(band9, 3, {alpha:0});
			TweenMax.to(band10, 3, {alpha:0});
			TweenMax.to(wave1, 6, {delay:10, alpha:1});
			TweenMax.to(wave2, 6, {delay:10, alpha:1});
			TweenMax.to(wave3, 6, {delay:10, alpha:1});
			TweenMax.to(wave4, 6, {delay:10, alpha:1});
			TweenMax.to(wave5, 6, {delay:10, alpha:1});
			TweenMax.to(bubble1, 6, {delay:10, alpha:1});
			TweenMax.to(bubble2, 6, {delay:10, alpha:1});
			TweenMax.to(bubble3, 6, {delay:10, alpha:1});
			TweenMax.to(bubble4, 6, {delay:10, alpha:1});
			TweenMax.to(bubble5, 6, {delay:10, alpha:1});
			TweenMax.to(bubble6, 6, {delay:10, alpha:1});
			TweenMax.to(bubble7, 6, {delay:10, alpha:1});
			TweenMax.to(bubble8, 6, {delay:10, alpha:1});
			TweenMax.to(bubble9, 6, {delay:10, alpha:1});
			TweenMax.to(bubble10, 6, {delay:10, alpha:1});
		}
		
		private function endPlayStartBusiness():void
		{
			TweenMax.to(wave1, 3, {alpha:0});
			TweenMax.to(wave2, 3, {alpha:0});
			TweenMax.to(wave3, 3, {alpha:0});
			TweenMax.to(wave4, 3, {alpha:0});
			TweenMax.to(wave5, 3, {alpha:0});
			TweenMax.to(bubble1, 3, {alpha:0});
			TweenMax.to(bubble2, 3, {alpha:0});
			TweenMax.to(bubble3, 3, {alpha:0});
			TweenMax.to(bubble4, 3, {alpha:0});
			TweenMax.to(bubble5, 3, {alpha:0});
			TweenMax.to(bubble6, 3, {alpha:0});
			TweenMax.to(bubble7, 3, {alpha:0});
			TweenMax.to(bubble8, 3, {alpha:0});
			TweenMax.to(bubble9, 3, {alpha:0});
			TweenMax.to(bubble10, 3, {alpha:0});
			TweenMax.to(speck1, 6, {delay:10, alpha:1});
			TweenMax.to(speck2, 6, {delay:10, alpha:1});
			TweenMax.to(speck3, 6, {delay:10, alpha:1});
			TweenMax.to(speck4, 6, {delay:10, alpha:1});
			TweenMax.to(speck5, 6, {delay:10, alpha:1});
			TweenMax.to(speck6, 6, {delay:10, alpha:1});
			TweenMax.to(speck7, 6, {delay:10, alpha:1});
			TweenMax.to(speck8, 6, {delay:10, alpha:1});
			TweenMax.to(speck9, 6, {delay:10, alpha:1});
			TweenMax.to(speck10, 6, {delay:10, alpha:1});
		}
	}
}
