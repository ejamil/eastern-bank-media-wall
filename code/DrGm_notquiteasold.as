﻿package code 
{
	import flash.display.MovieClip;
	
	import org.tuio.*;
	import org.tuio.adapters.*;
	import org.tuio.gestures.*;
	import org.tuio.debug.TuioDebug;
	import org.tuio.TuioManager;
	import org.tuio.connectors.*;
	
	import flash.events.*;
	import flash.utils.*;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	
	public class DrGm extends MovieClip 
	{
		public var bubbleArray:Array = new Array();
		public var dollaArray:Array = new Array();
		private var fadeCounter:Number = 0;
		private var dollaValue:Number = 0;
		public var isBeginning:Boolean = true;

		public var owner:LoGm;
		public var b1:DrGmBubble;
		public var b2:DrGmBubble;
		public var b3:DrGmBubble;
		public var b4:DrGmBubble;
		public var b5:DrGmBubble;
		public var b6:DrGmBubble;
		public var b7:DrGmBubble;
				
		public function DrGm(myOwner:LoGm)//myOwner:LoGm
		{
			owner = myOwner;

			b1 = new DrGmBubble(this, 330, 1025, 1, .9, 2);
			b2 = new DrGmBubble(this, 800, 1290, 2, .35, 1);
			b3 = new DrGmBubble(this, 850, 1425, 3, .35, 4);
			b4 = new DrGmBubble(this, 655, 1540, 4, .35, 2);
			b5 = new DrGmBubble(this, 730, 1690, 5, .35, 4);
			b6 = new DrGmBubble(this, 800, 800, 6, .9, 1);
			b7 = new DrGmBubble(this, 350, 1370, 7, .35, 2);
			addChild(b1);
			addChild(b2);
			addChild(b3);
			addChild(b4);
			addChild(b5);
			addChild(b6);
			addChild(b7);
			setChildIndex(dreamNext, numChildren-1);
			
			bubbleArray.push(b1, b2, b3, b4, b5, b6, b7);
			dollaArray.push(dreamNext.d1, dreamNext.d2, dreamNext.d3, dreamNext.d4, dreamNext.d5, dreamNext.d6, dreamNext.d7, dreamNext.d8, 
							dreamNext.d9, dreamNext.d10, dreamNext.d11, dreamNext.d12, dreamNext.d13, dreamNext.d14, dreamNext.d15, dreamNext.d16);

			TweenMax.to(dreamFace, 1, {y:1920, ease:Strong.easeOut, onComplete:startBubbles});
		}
		
		public function startBubbles()
		{
			TweenMax.to(bc1, .5, {frame:21, ease:Strong.easeOut});
			TweenMax.to(bc2, 1, {frame:40, ease:Strong.easeOut});
			TweenMax.to(b1, .5, {delay:.25, alpha:1, ease:Linear.easeNone, onComplete:addEvent, onCompleteParams:["car"]});
			TweenMax.to(b6, .5, {delay:.5, alpha:1, ease:Linear.easeNone, onComplete:addEvent, onCompleteParams:["vacation"]});
		}
		
		public function addEvent(type:String)
		{
			if(type == "car")
			{
				b1.addClick();
			}
			if(type == "vacation")
			{
				b6.addClick();
			}
			if(type == "yes")
			{
				if(!bigB.yesButt.hasEventListener(TuioTouchEvent.TOUCH_DOWN))
				{
					//bigB.yesButt.addEventListener(MouseEvent.CLICK, heckYeah);
					bigB.yesButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
				}
			}
			if(type == "no")
			{
				if(!bigB.noButt.hasEventListener(TuioTouchEvent.TOUCH_DOWN))
				{
					//bigB.noButt.addEventListener(MouseEvent.CLICK, heckNah);
					bigB.noButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
				}
			}
			if(type == "done")
			{
				if(!dreamNext.hasEventListener(TuioTouchEvent.TOUCH_DOWN))
				{
					dreamNext.addEventListener(TuioTouchEvent.TOUCH_DOWN, onward);
					//dreamNext.addEventListener(MouseEvent.CLICK, onward);
				}
			}
			if(type == "all")
			{
				if(!dreamNext.hasEventListener(TuioTouchEvent.TOUCH_DOWN))
				{
					dreamNext.addEventListener(TuioTouchEvent.TOUCH_DOWN, onward);
					//dreamNext.addEventListener(MouseEvent.CLICK, onward);
				}
				
				if(!bigB.yesButt.hasEventListener(TuioTouchEvent.TOUCH_DOWN))
				{
					//bigB.yesButt.addEventListener(MouseEvent.CLICK, heckYeah);
					bigB.yesButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
				}
				
				if(!bigB.noButt.hasEventListener(TuioTouchEvent.TOUCH_DOWN))
				{
					//bigB.noButt.addEventListener(MouseEvent.CLICK, heckNah);
					bigB.noButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
				}
			}
		}
		public function removeEvent(type:String, bubble:DrGmBubble = null, index:Number = 0)
		{
			if(type == "yes")
			{
				//bigB.yesButt.removeEventListener(MouseEvent.CLICK, heckYeah);
				bigB.yesButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
			}
			if(type == "no")
			{
				//bigB.noButt.removeEventListener(MouseEvent.CLICK, heckNah);
				bigB.noButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
			}
			if(type == "done")
			{
				dreamNext.removeEventListener(TuioTouchEvent.TOUCH_DOWN, onward);
				//dreamNext.removeEventListener(MouseEvent.CLICK, onward);
			}
			if(type == "all")
			{
				dreamNext.removeEventListener(TuioTouchEvent.TOUCH_DOWN, onward);
				//dreamNext.removeEventListener(MouseEvent.CLICK, onward);
				
				//bigB.yesButt.removeEventListener(MouseEvent.CLICK, heckYeah);
				bigB.yesButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
				
				//bigB.noButt.removeEventListener(MouseEvent.CLICK, heckNah);
				bigB.noButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
			}
			if(type == "object")
			{
				if(bubble != null)
				{
					trace("removing your dreams...awww");
					removeChild(bubble);
					bubbleArray.splice(index, 1);
					
					if(bubbleArray.length <= 0)
					{
						onward();
					}
				}
			}
		}
		
		public function playBubbleNoise():void
		{
			owner.playSound(3);
		}
		
		public function fader(type:String, bubble:DrGmBubble = null, nDelay:Number = 7)
		{			
			if(type == "in")
			{
				//////Have bubbles fade in. Wait 5 seconds then start fading them out//////
				for (var i:Number=0; i < bubbleArray.length; i++)
				{
					if (bubbleArray[i].isOpen != true && bubbleArray[i].isHiding == true)
					{
						trace("FADING IN");
						bubbleArray[i].isHiding = false;
						TweenMax.to(bubbleArray[i], 1, {delay:4*i, alpha:1, ease:Linear.easeNone, onStart:playBubbleNoise, onComplete:fader, onCompleteParams:["out", bubbleArray[i]]});
					}
				}
			}
			if(type == "out")
			{
				bubble.addClick();
				TweenMax.to(bubble, 1, {delay:nDelay, alpha:0, ease:Linear.easeNone, onComplete:checkHide, onCompleteParams:[bubble]});
				TweenMax.delayedCall(nDelay, fader, ["in"]);
			}
		}
		
		public function select(e:TuioTouchEvent) //e:TuioTouchEvent
		{
			//////When selected, remove listener and move to center//////
			e.target.removeClick();
			TweenMax.killTweensOf(e.target);
			gotoEnd(e.target);
			e.target.alpha = 1;
			e.target.isHiding = false;
			
			trace("DrGm select currentTarget = " + e.currentTarget.name);
			trace("DrGm select target = " + e.target.name);
			
			switch (e.target)
			{
				case b1:
					trace("is 1");
					owner.playSound(2);
				break;
				case b2:
					trace("is 2");
					owner.playSound(12);
				break;
				case b3:
					trace("is 3");
					owner.playSound(7);
				break;
				case b4:
					trace("is 4");
					owner.playSound(13);
				break;
				case b5:
					trace("is 5");
					owner.playSound(9);
				break;
				case b6:
					trace("is 6");
					owner.playSound(11);
				break;
				case b7:
					trace("is 7");
					owner.playSound(4);
				break;
			}
			
			if(isBeginning == true)
			{
				isBeginning = false;
				TweenMax.to(bc1, .3, {alpha:0, ease:Linear.easeNone});
				TweenMax.to(bc2, .3, {alpha:0, ease:Linear.easeNone});				
				
				if(e.target.pos == 1)
				{
					b6.isOpen = false;
					b6.isHiding = false;
					b1.gotoAndStop(8);
					b6.gotoAndStop(9);
					fader("out", b6, 5);
				}
				if(e.target.pos == 6)
				{
					b1.isOpen = false;
					b1.isHiding = false;
					b1.gotoAndStop(8);
					b6.gotoAndStop(9);
					fader("out", b1, 5);
				}
				
				fader("in");
			}
		}
		
		public function gotoEnd(targ:Object)
		{
			//////Set up screen for large bubble. Move bubble to front//////
			TweenMax.to(dreamNext, .5, {x:0, ease:Strong.easeOut});
			TweenMax.to(dreamFace, .5, {scaleX:.65, scaleY:.65, x:300, y:1734, ease:Strong.easeOut});
			TweenMax.to(bigB, .5, {alpha:1, ease:Linear.easeNone, onComplete:addEvent, onCompleteParams:["all"]});
			TweenMax.to(dreamText, .5, {alpha:0, ease:Linear.easeNone});
			bigB.gotoAndStop(targ.pos);
			
			//////Set selected as open, move it to center and reset all other bubbles//////			
			targ.isOpen = true;
			for (var i:Number=0; i < bubbleArray.length; i++)
			{
				if (bubbleArray[i].isOpen == true)
				{
					TweenMax.to(bubbleArray[i], .5, {scaleX:.65, scaleY:.65, x:358, y:1146, ease:Strong.easeOut});
					targ.isOpen = false;
				}
				else
				{
					bubbleArray[i].resetDream("start");
				}
			}
		}
		
		public function checkHide(bubble:DrGmBubble)
		{
			bubble.removeClick();
			bubble.isHiding = true;
		}
		
		public function heckYeah(e:TuioTouchEvent)//e:TuioTouchEvent
		{
			trace("heckYeah");
			removeEvent("yes");
			owner.playSound(1);
			owner.playSound(8);
			TweenMax.to(bigB, .5, {alpha:0, ease:Linear.easeNone});
			TweenMax.to(dreamText, .5, {alpha:1, ease:Linear.easeNone});
			
			for (var i:Number=0; i < bubbleArray.length; i++)
			{
				if (bubbleArray[i].scaleX > .5)
				{
					TweenMax.to(bubbleArray[i], .5, {alpha:0, ease:Linear.easeNone, onComplete:removeEvent, onCompleteParams:["object", bubbleArray[i], i]});
					switch (bubbleArray[i])
					{
						case b1:
							owner.iconArray[owner.whichi] = 1;
						break;
						case b2:
							owner.iconArray[owner.whichi] = 2;
						break;
						case b3:
							owner.iconArray[owner.whichi] = 3;
						break;
						case b4:
							owner.iconArray[owner.whichi] = 4;
						break;
						case b5:
							owner.iconArray[owner.whichi] = 5;
						break;
						case b6:
							owner.iconArray[owner.whichi] = 6;
						break;
						case b7:
							owner.iconArray[owner.whichi] = 7;
						break;
					}
					owner.whichi++;
					
					dollaValue += bubbleArray[i].price;
					
					for (var d:Number=0; d < dollaValue; d++)
					{
						TweenMax.to(dollaArray[d], 1, {frame:30, ease:Strong.easeOut});
					}
					owner.howManyDollars = dollaValue;
				}
				else
				{
					TweenMax.killTweensOf(bubbleArray[i]);
					bubbleArray[i].alpha = 0;
					bubbleArray[i].isHiding = true;
					fader("in");
					bubbleArray[i].resetDream("end");
				}
			}
		}
		
		public function heckNah(e:TuioTouchEvent)//e:TuioTouchEvent
		{
			trace("heckNah");
			removeEvent("no");
			TweenMax.to(bigB, .5, {alpha:0, ease:Linear.easeNone});
			TweenMax.to(dreamText, .5, {alpha:1, ease:Linear.easeNone});
			
			for (var i:Number=0; i < bubbleArray.length; i++)
			{
				if (bubbleArray[i].scaleX > .5)
				{
					TweenMax.to(bubbleArray[i], .5, {alpha:0, ease:Linear.easeNone, onComplete:removeEvent, onCompleteParams:["object", bubbleArray[i], i]});
				}
				else
				{
					TweenMax.killTweensOf(bubbleArray[i]);
					bubbleArray[i].alpha = 0;
					bubbleArray[i].isHiding = true;
					fader("in");
					bubbleArray[i].resetDream("end");
				}
			}
		}
		
		public function onward(e:TuioTouchEvent = null)//e:TuioTouchEvent
		{
			trace("something obviously awesome is going to happen here. Possibly even life changing.");
			removeEvent("all");
			//TweenMax.killAll();
			TweenMax.killTweensOf(b1);
			TweenMax.killTweensOf(b2);
			TweenMax.killTweensOf(b3);
			TweenMax.killTweensOf(b4);
			TweenMax.killTweensOf(b5);
			TweenMax.killTweensOf(b6);
			TweenMax.killTweensOf(b7);
			TweenMax.killTweensOf(bc1);
			TweenMax.killTweensOf(bc2);
			TweenMax.killTweensOf(yesButt);
			TweenMax.killTweensOf(bigB);
			TweenMax.killTweensOf(dreamText);
			TweenMax.killTweensOf(dreamNext);
			TweenMax.killTweensOf(dreamFace);
			for (var i:Number=0; i < bubbleArray.length; i++)
				{
					TweenMax.killTweensOf(bubbleArray[i]);
				}
			owner.playSound(1);
			owner.countdown();
		}
	}
}
