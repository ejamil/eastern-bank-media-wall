﻿package code
{
	import code.drawingApp;
	import code.Icons;
	import code.LoadXML;
	import code.Popup;
	import code.StageVideoPlayerExample;
	import code.twoScreens;
	
	import com.swingpants.effect.GenieBmd;
	
	import com.bostonproductions.events.CMSEvent;
	import com.bostonproductions.data.commandConstants;
	import com.bostonproductions.net.cmsAPI;
	import com.bostonproductions.text.textFieldBaseFunctions;
	
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.display.StageDisplayState;
	
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.NetStatusEvent;
	import flash.events.StageVideoAvailabilityEvent;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import flash.media.Sound;
	import flash.media.SoundChannel;
    import flash.media.SoundTransform;
	import flash.media.StageVideo;
	import flash.media.StageVideoAvailability;
	import flash.media.Video;
	
	import flash.net.NetConnection;
	import flash.net.NetStream;
    import flash.net.NetStreamAppendBytesAction;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import flash.text.TextField ;
	
	import flash.ui.Mouse;
	
    import flash.utils.ByteArray;
	
	import org.tuio.*;
	import org.tuio.adapters.*;
	import org.tuio.gestures.*;
	import org.tuio.debug.TuioDebug;
	import org.tuio.TuioManager;
	import org.tuio.connectors.*;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	import starling.core.Starling;
	import code.BackgroundContainer;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	public class Document extends MovieClip
	{
		public var video:Video = new Video();
		public var nc:NetConnection = new NetConnection();
		public var ns:NetStream;
		private var stageVideo:StageVideo;
		
		private const videoURL:String = "assets/video/EB_h264.mov";
		
		private var tf:TextField;
		
		private var anIcon:Icons;

		private var buffer:Number = 200;

		private var iconArray:Array = new Array();
		
		private var gb:GenieBmd;
		private var gbArray:Array = new Array();
		
		private var curX:Number;
		private var curY:Number;
		
		public var loadXML:LoadXML;
		
		public var popup:Popup;
		public var bmd:BitmapData;
		
		public var popupArray:Array = new Array();
		public var bmdArray:Array = new Array();
		
		public var tuio:TuioClient;
		public var tuioManager:TuioManager;
		public var tdbg:TuioDebug;
		public var mta:MouseTuioAdapter;
		public var gm:GestureManager;
		
		private var SFXChannel:SoundChannel = new SoundChannel();
		private var SFXSound:Sound = new Sound();
		private var SFXTransform:SoundTransform = new SoundTransform();
		
		private var playedOnce:Boolean = false; 
		
		private var touchX:Number;
		private var touchY:Number;
		
		private var drawApp:drawingApp;
		
		public var ninjaGame1:BillNinja;
		public var ninjaGame2:BillNinja;
		public var bigNinja:BillNinja;
		
		public var pongGame1:Pong;
		public var pongGame2:Pong;
		
		public var platformGame1:PlatformGame;
		public var platformGame2:PlatformGame;
		
		public var brickGame1:BrickGame;
		public var brickGame2:BrickGame;
		
		public var ninjaOn1:Boolean = false;
		public var ninjaOn2:Boolean = false;
		public var ninjaOn3:Boolean = false;
		public var ninjaOn4:Boolean = false;
		
		public var pongOn1:Boolean = false;
		public var pongOn2:Boolean = false;
		public var pongOn3:Boolean = false;
		public var pongOn4:Boolean = false;
		
		public var platformOn1:Boolean = false;
		public var platformOn2:Boolean = false;
		public var platformOn3:Boolean = false;
		public var platformOn4:Boolean = false;
		
		public var brickOn1:Boolean = false;
		public var brickOn2:Boolean = false;
		public var brickOn3:Boolean = false;
		public var brickOn4:Boolean = false;
		
		public var  bigOn1:Boolean = false;
		public var  bigOn2:Boolean = false;
		public var  bigOn3:Boolean = false;
		public var  bigOn4:Boolean = false;
		
		public var _api:cmsAPI;
		
		public var xmlChanged:Array;
		
		private var _starling:Starling;
		//private var businessText:Loader;
		//private var bturl:URLRequest = new URLRequest("assets/background/businessText.swf");
		//private var btMC:MovieClip;
		
		public var businessAnim:BusinessAnim;
		public var dreamAnim:DreamAnim;
		public var homeAnim:HomeAnim;
		public var lifeAnim:LifeAnim;
		public var playAnim:PlayAnim;
		
		private var businessAnimTimeline:TimelineMax;
		private var dreamAnimTimeline:TimelineMax;
		private var homeAnimTimeline:TimelineMax;
		private var lifeAnimTimeline:TimelineMax;
		private var playAnimTimeline:TimelineMax;
		
		private var bgTimeline:TimelineMax;
		private var trans:Number = 10;
		private var del:Number = 80;
		public var timeoutTimer:Timer;
		public var backupXML:XML;
		private var backupLoader:URLLoader;
		public var backupGameXML:XML;
		private var backupGameLoader:URLLoader;
		
		public var bGames:BigGames;
		public var dGames:BigGames;
		public var hGames:BigGames;
		public var lGames:BigGames;
		public var pGames:BigGames;
		public var tempGame:BigGames;
		
		public var bigWordsTimeline:TimelineMax;
		
		private var dg:SavingsGame;
		private var lg:CreditGame;
		
		public var lifeLang:String = "english";
		public var dreamLang:String = "english";
		public var businessLang:String = "english";
		public var homeLang:String = "english";
		public var playLang:String = "english";
		
		public var whichScreenStarted:Number = 0;
		
		public var bVid:BigVideo;
		public var hVid:BigVideo;
		
		public var bigIsShowing:Boolean = false;
		public var bigNumberShowing:Number = 1;
		
		public var somethingOpen:Boolean = false;
		public var someGameOpen:Boolean = false;

		public function Document()
		{
			//create the TextField for logging
			tf = new TextField ( );
			tf.height = stage.stageHeight ;
			tf.width = stage.stageWidth ;
			tf.mouseWheelEnabled = true ; 
			tf.multiline = true ; 
			tf.textColor = 0x000000
			tf.wordWrap = true ;
			tf.selectable = false;
			tf.mouseEnabled = false;
 
			//add the TextField to the display list
			addChild (tf) ;
			tf.visible = false;
			
			stage.addEventListener(KeyboardEvent.KEY_DOWN, checkKey);
			
			loadXML = new LoadXML(this);
			createCMSAPI();
			
		}
		
		private function createCMSAPI():void 
		{
			addText("createCMSAPI");
			if (_api == null) {
				_api = new cmsAPI();
				_api.addEventListener(cmsAPI.INITCOMPLETE, handleCMSReady, false, 0, true);
				_api.addEventListener(cmsAPI.CMS_DATA, handleCMSData, false, 0, true);
				//_api.addEventListener(cmsAPI.NEW_CMS_DATA, handleUnsolicitedNewCMSData, false, 0, true);
				_api.init();
				
				timeoutTimer = new Timer(300000, 1);
				timeoutTimer.stop();
				timeoutTimer.start();
				timeoutTimer.addEventListener(TimerEvent.TIMER_COMPLETE, timeout);
			} else {
				destroyCMSAPI( true );
			}
		}
		
		private function timeout(e:TimerEvent):void
		{
			_api.removeEventListener(cmsAPI.INITCOMPLETE, handleCMSReady);
			_api.removeEventListener(cmsAPI.CMS_DATA, handleCMSData);
			timeoutTimer.stop();
			timeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
			
			backupLoader = new URLLoader();
			backupLoader.load(new URLRequest("C:\EBCache\xmlCache\media_wall_data_export.xml"));
			backupLoader.addEventListener(Event.COMPLETE, processBackupXML);
			
		}
		private function processBackupXML(e:Event):void
		{
			
			backupXML = new XML(e.target.data);
			backupGameLoader = new URLLoader();
			backupGameLoader.load(new URLRequest("C:\EBCache\xmlCache\media_wall_game_data_export.xml"));
			backupGameLoader.addEventListener(Event.COMPLETE, combineXML);
		}
		
		private function combineXML(e:Event):void
		{
			
			backupGameXML = new XML(e.target.data);
			backupXML.appendChild(backupGameXML.data);
			addText("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& " + backupXML);
			loadXML.CMSData(backupXML);
		}
		
		private function destroyCMSAPI(optionalCallback:Boolean = false):void 
		{
			if (_api != null) {
				_api.removeEventListener(cmsAPI.INITCOMPLETE, handleCMSReady);
				_api.removeEventListener(cmsAPI.CMS_DATA, handleCMSData);
				//_api.removeEventListener(cmsAPI.NEW_CMS_DATA, handleUnsolicitedNewCMSData);
				_api.destroyInternals();
				_api = null;
				if (optionalCallback == true) createCMSAPI();
			}
		}
		
		private function handleCMSReady(e:Event):void 
		{
			timeoutTimer.stop();
			timeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
			_api.removeEventListener(cmsAPI.INITCOMPLETE, handleCMSReady);
			_api.get_media_wall_all();
			// We are now able to request data from the CMS
			// If the application was waiting for startup initialization
			// this would be a good time to do it.
			
			// This event means the cms application has cached all content 
			// available for the configured exhibit. On a fresh install combined
			// with a CMS with a lot of content for the target exhibit this
			// may take several minutes for this event to fire. You can watch
			// caching progress on the UI of the caching application.
			//btnPromotionsText.enabled = true;
			//btnPromotionsVideos.enabled = true;
			//btnPromotionsPhotos.enabled = true;
			//btnPromotionsAll.enabled = true;
			//btnMedGames.enabled = true;
			//btnMedLife.enabled = true;
			//btnMedBusiness.enabled = true;
			//btnMedDreams.enabled = true;
			//btnMedHome.enabled = true;
			//btnMedAll.enabled = true;	
			/*
			btnVestAll.enabled = true;
			btnVestPhotos.enabled = true;
			btnVestText.enabled = true;
			btnComAll.enabled = true;
			btnComPhotos.enabled = true;
			btnComVideos.enabled = true;
			btnComtText.enabled = true;
			*/
			
			/*btnPromotionsText.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnPromotionsVideos.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnPromotionsPhotos.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnPromotionsAll.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnMedGames.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnMedLife.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnMedBusiness.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnMedDreams.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnMedHome.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnMedAll.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);*/
			/*
			btnVestAll.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnVestPhotos.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnVestText.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnComAll.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnComPhotos.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnComVideos.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnComtText.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			*/
			//txtData.text = 'Apps Connected..Press the buttons to see example api interaction..';
		}
		
		/**
		 * When you make method calls to the cmsAPI, responses
		 * will come back through this event handler.
		 * 
		 * You would want to parse xml and take action on the data here.
		 * XML will be in the e.Data.resultData property.
		 * @param	e
		 */
		private function handleCMSData(e:CMSEvent):void 
		{
			addText("handleCMSData");
			switch(e.Data.command) 
			{
				
				case commandConstants.MEDIA_WALL_ALL:
					//addText("commandConstants.MEDIA_WALL_ALL");
					addText(e.Data.resultData);
					//parse xml here
					////trace(e.Data.resultData);
					loadXML.CMSData(e.Data.resultData);
					break;
				case commandConstants.MEDIA_WALL_BUSINESS:
					checkUpdated(e.Data.resultData, 1);
					break;
				case commandConstants.MEDIA_WALL_DREAMS:
				checkUpdated(e.Data.resultData, 2);
					break;
				case commandConstants.MEDIA_WALL_HOME:
					checkUpdated(e.Data.resultData, 3);
					break;
				case commandConstants.MEDIA_WALL_LIFE:
				checkUpdated(e.Data.resultData, 4);
					break;
				case commandConstants.MEDIA_WALL_GAMES:
					checkUpdated(e.Data.resultData, 5);
					break;
			}
			
			//txtData.text = "command:" + e.Data.command + "\n" + String( e.Data.resultData );
		}
		
		/**
		 * Be very careful here. Events will happen on this event handler unsolicited
		 * whenever new data is detected and cached from the CMS.
		 * 
		 * If you want to have a near real time synchronized display, you should program
		 * around handling these events.  The event comes with the ALL version of data
		 * from each configured display...so all the old and new xml data for each type of
		 * asset combined.
		 * 
		 * If you don't want to deal with parsing all the individual data types out
		 * of an ALL data response here, its perfectly acceptable to respond to an event
		 * like this with an api call to just get photo, video...etc data.
		 * @param	e
		 */
		/*private function handleUnsolicitedNewCMSData(e:CMSEvent):void {
			switch(e.Data.command) {
				case commandConstants.MEDIA_WALL_EVENT:
					//showAlertText('Unsolicited MEDIA WALL data from CMS! - (see function handleUnsolicitedNewCMSData)');
				break;
				case commandConstants.MEDIA_WALL_GAMES_EVENT:
					//showAlertText('Unsolicited MEDIA WALL GAMES data from CMS! - (see function handleUnsolicitedNewCMSData)');
				break;
			}
			
			//txtData.text = " handleUnsolicitedNewCMSData command:" + e.Data.command + "\n" + String( e.Data.resultData );
		}*/
		
		public function testing123(e:TuioTouchEvent):void
		{
			addText("testing123 = " + e.toString());
		}
		
		public function init():void
		{
			addText("init");
			tuio = new TuioClient(new UDPConnector());
			 
			// This is the TuioClient listener, doesn't do TouchEvent events and uses a different type of listeners
			// http://bubblebird.at/tuioflash/guides/using-the-tuioclient/
			//this.tuio.addListener(this);
			 
			//This activates listening to Blobs for TouchEvents.
			tuioManager = TuioManager.init(stage);
			tuioManager.triggerTouchOnBlob = true;
			tuio.addListener(tuioManager);
			gm = GestureManager.init(stage);
			GestureManager.addGesture(new DragGesture());
			GestureManager.addGesture(new ZoomGesture(TwoFingerMoveGesture.TRIGGER_MODE_MOVE));
			//GestureManager.addGesture(new RotateGesture());
			
			if (loadXML.useTuio == false)
			{
				//addText("use mouse clicks");
				mta = new MouseTuioAdapter(stage);
				mta.addListener(tuioManager);
			}
			//tuioManager.dispatchMouseEvents = true
			 
			//Debugging data, just delete to remove
			//tdbg = TuioDebug.init(stage);
			//this.tuio.addListener(tdbg);

			//stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
			
			/*addEventListener(TuioTouchEvent.TOUCH_DOWN, testing123);
			addEventListener(TuioTouchEvent.TOUCH_UP, testing123);
			addEventListener(TuioTouchEvent.TOUCH_MOVE, testing123);
			addEventListener(TuioTouchEvent.TOUCH_OUT, testing123);
			addEventListener(TuioTouchEvent.TOUCH_OVER, testing123);
			addEventListener(TuioTouchEvent.ROLL_OUT, testing123);
			addEventListener(TuioTouchEvent.ROLL_OVER, testing123);
			addEventListener(TuioTouchEvent.TAP, testing123);
			addEventListener(TuioTouchEvent.DOUBLE_TAP, testing123);
			addEventListener(TuioTouchEvent.HOLD, testing123);*/
			
			//startVideo();
			startUp();
			
			
			if (loadXML.hideMouse == true)
			{
				Mouse.hide();
			}
			
			//if (loadXML.showDrawApp == true)
			//{
				/*drawApp = new drawingApp(this);
				addChild(drawApp);
				drawApp.x = 0;
				drawApp.y = 0;*/
			//}
			
			addIcons();
			
			SFXSound.load(new URLRequest(loadXML.waterSound));
			
			addBigGames();
		}
		
		private function addBigGames():void
		{
			bGames = new BigGames(this);
			bGames.setFrame(1);
			dGames = new BigGames(this);
			dGames.setFrame(2);
			hGames = new BigGames(this);
			hGames.setFrame(3);
			lGames = new BigGames(this);
			lGames.setFrame(4);
			pGames = new BigGames(this);
			pGames.setFrame(5);
			
			addChild(bGames);
			addChild(dGames);
			addChild(hGames);
			addChild(lGames);
			addChild(pGames);
			
			bGames.alpha = 0;
			dGames.alpha = 0;
			hGames.alpha = 0;
			lGames.alpha = 0;
			pGames.alpha = 0;
			
			bGames.x = dGames.x = hGames.x = lGames.x = pGames.x = 4320;
			
			//////////// V1 = whole thing //////////////
			if (loadXML.whichMode == 1)
			{
				bigWordsTimeline = new TimelineMax();
				bigWordsTimeline.repeat = -1;
				
				bigWordsTimeline.append(new TweenMax(bGames, 1, {x:0, onComplete:playBusiness}));
				bigWordsTimeline.append(new TweenMax(bGames, 1, {alpha:1})); //, onStart:isShowing, onStartParams:[true, 1]
				bigWordsTimeline.append(new TweenMax(bGames, 10, {x:0}));
				bigWordsTimeline.append(new TweenMax(bGames, 1, {alpha:0, onComplete:isShowing, onCompleteParams:[false, 1]}));
				bigWordsTimeline.append(new TweenMax(bGames, 69, {x:-4320}));
				
				bigWordsTimeline.append(new TweenMax(dGames, 1, {delay:10, x:0, onComplete:playDreams}));
				bigWordsTimeline.append(new TweenMax(dGames, 1, {alpha:1})); //, onStart:isShowing, onStartParams:[true, 2]
				bigWordsTimeline.append(new TweenMax(dGames, 10, {x:0}));
				bigWordsTimeline.append(new TweenMax(dGames, 1, {alpha:0, onComplete:isShowing, onCompleteParams:[false, 2]}));
				bigWordsTimeline.append(new TweenMax(dGames, 69, {x:-4320}));
				
				bigWordsTimeline.append(new TweenMax(hGames, 1, {delay:10, x:0, onComplete:playHome}));
				bigWordsTimeline.append(new TweenMax(hGames, 1, {alpha:1})); //, onStart:isShowing, onStartParams:[true, 3]
				bigWordsTimeline.append(new TweenMax(hGames, 10, {x:0}));
				bigWordsTimeline.append(new TweenMax(hGames, 1, {alpha:0, onComplete:isShowing, onCompleteParams:[false, 3]}));
				bigWordsTimeline.append(new TweenMax(hGames, 69, {x:-4320}));
				
				bigWordsTimeline.append(new TweenMax(lGames, 1, {delay:10, x:0, onComplete:playLife}));
				bigWordsTimeline.append(new TweenMax(lGames, 1, {alpha:1})); //, onStart:isShowing, onStartParams:[true, 4]
				bigWordsTimeline.append(new TweenMax(lGames, 10, {x:0}));
				bigWordsTimeline.append(new TweenMax(lGames, 1, {alpha:0, onComplete:isShowing, onCompleteParams:[false, 4]}));
				bigWordsTimeline.append(new TweenMax(lGames, 69, {x:-4320}));
				
				bigWordsTimeline.append(new TweenMax(pGames, 1, {delay:10, x:0, onComplete:playPlay}));
				bigWordsTimeline.append(new TweenMax(pGames, 1, {alpha:1})); //, onStart:isShowing, onStartParams:[true, 5]
				bigWordsTimeline.append(new TweenMax(pGames, 28, {x:0}));
				bigWordsTimeline.append(new TweenMax(pGames, 10, {alpha:0, onComplete:isShowing, onCompleteParams:[false, 5]}));
				bigWordsTimeline.append(new TweenMax(pGames, 43, {x:-4320}));
				
				bigWordsTimeline.append(new TweenMax(bGames, 10, {x:-4320}));
			}
			//////////// V1 = whole thing //////////////
			
			
			//////////// V2 = Business Game //////////////
			if (loadXML.whichMode == 2)
			{
				TweenMax.to(bGames, 2, {x:0, alpha:1, onComplete:playBusiness});
				bigIsShowing = true;
				bigNumberShowing = 1;
			}
			//////////// V2 = Business Game //////////////
			
			
			//////////// V3 = Dreams Game //////////////
			if (loadXML.whichMode == 3)
			{
				TweenMax.to(dGames, 2, {x:0, alpha:1, onComplete:playDreams});
				bigIsShowing = true;
				bigNumberShowing = 2;
			}
			//////////// V3 = Dreams Game //////////////
			
			
			//////////// V4 = Home Game //////////////
			if (loadXML.whichMode == 4)
			{
				TweenMax.to(hGames, 2, {x:0, alpha:1, onComplete:playHome});
				bigIsShowing = true;
				bigNumberShowing = 3;
			}
			//////////// V4 = Home Game //////////////
			
			
			//////////// V5 = Life Game //////////////
			if (loadXML.whichMode == 5)
			{
				TweenMax.to(lGames, 2, {x:0, alpha:1, onComplete:playLife});
				bigIsShowing = true;
				bigNumberShowing = 4;
			}
			//////////// V5 = Life Game //////////////
			
			
			//////////// V6 = Play Game //////////////
			if (loadXML.whichMode == 6)
			{
				TweenMax.to(pGames, 2, {x:0, alpha:1, onComplete:playPlay});
				bigIsShowing = true;
				bigNumberShowing = 5;
			}
			//////////// V6 = Play Game //////////////
			
			//trace("((((((((((((((((((((((((((((( whichMode = " + loadXML.whichMode);
		}
		
		public function isShowing(b:Boolean, n:Number):void
		{
			trace("isShowing b, n = " + b + ", " + n);
			bigIsShowing = b;
			bigNumberShowing = n;
			trace("isShowing somethingOpen = " + somethingOpen);
			if (bigIsShowing == true && somethingOpen == false)
			{
				trace("this should fade the next one in");
				fadeGamesIn();
				somethingOpen = true;
			}
			if (bigIsShowing == false)
			{
				trace("this should remove listeners");
				bGames.stopGames();
				dGames.stopGames();
				hGames.stopGames();
				lGames.stopGames();
				pGames.stopGames();
				if (tempGame != null)
				{
					tempGame.stopGames();
					removeChild(tempGame);
					tempGame = null;
				}
				somethingOpen = false;
				
				bGames.visible = false;
				dGames.visible = false;
				hGames.visible = false;
				lGames.visible = false;
				pGames.visible = false;
			}
		}
		
		public function addTempGame(n:Number):void
		{
			trace("addTempGame = " + n);
			trace("somethingOpen = " + somethingOpen);
			if (tempGame == null && somethingOpen == false)
			{
				//removeChild(tempGame);
				//tempGame = null;
				somethingOpen = true;
				tempGame = new BigGames(this);
				addChild(tempGame);
				
				switch (n)
				{
					case 1:
						tempGame.setFrame(2);
						TweenMax.to(tempGame, 1, {delay:10, alpha:0, onComplete:isShowing, onCompleteParams:[false, 2]});
					break;
					
					case 2:
						tempGame.setFrame(4);
						TweenMax.to(tempGame, 1, {delay:10, alpha:0, onComplete:isShowing, onCompleteParams:[false, 4]});
					break;
				}
				
				tempGame.big1.gotoAndStop(1);
				tempGame.big2.gotoAndStop(1);
				tempGame.big3.gotoAndStop(1);
				tempGame.big4.gotoAndStop(1);
				
				tempGame.big1.play();
				tempGame.big2.play();
				tempGame.big3.play();
				tempGame.big4.play();
			}
			
		}
		
		public function playBusiness():void
		{
			bGames.big1.gotoAndStop(1);
			bGames.big2.gotoAndStop(1);
			bGames.big3.gotoAndStop(1);
			bGames.big4.gotoAndStop(1);
			
			dGames.big1.gotoAndStop(1);
			dGames.big2.gotoAndStop(1);
			dGames.big3.gotoAndStop(1);
			dGames.big4.gotoAndStop(1);
			
			hGames.big1.gotoAndStop(1);
			hGames.big2.gotoAndStop(1);
			hGames.big3.gotoAndStop(1);
			hGames.big4.gotoAndStop(1);
			
			lGames.big1.gotoAndStop(1);
			lGames.big2.gotoAndStop(1);
			lGames.big3.gotoAndStop(1);
			lGames.big4.gotoAndStop(1);
			
			pGames.big1.gotoAndStop(1);
			pGames.big2.gotoAndStop(1);
			pGames.big3.gotoAndStop(1);
			pGames.big4.gotoAndStop(1);
			
			bGames.big1.play();
			bGames.big2.play();
			bGames.big3.play();
			bGames.big4.play();
			bGames.visible = true;
			
			isShowing(true, 1);
		}
		
		public function playDreams():void
		{
			bGames.big1.gotoAndStop(1);
			bGames.big2.gotoAndStop(1);
			bGames.big3.gotoAndStop(1);
			bGames.big4.gotoAndStop(1);
			
			dGames.big1.gotoAndStop(1);
			dGames.big2.gotoAndStop(1);
			dGames.big3.gotoAndStop(1);
			dGames.big4.gotoAndStop(1);
			
			hGames.big1.gotoAndStop(1);
			hGames.big2.gotoAndStop(1);
			hGames.big3.gotoAndStop(1);
			hGames.big4.gotoAndStop(1);
			
			lGames.big1.gotoAndStop(1);
			lGames.big2.gotoAndStop(1);
			lGames.big3.gotoAndStop(1);
			lGames.big4.gotoAndStop(1);
			
			pGames.big1.gotoAndStop(1);
			pGames.big2.gotoAndStop(1);
			pGames.big3.gotoAndStop(1);
			pGames.big4.gotoAndStop(1);
			
			dGames.big1.play();
			dGames.big2.play();
			dGames.big3.play();
			dGames.big4.play();
			dGames.visible = true;
			
			isShowing(true, 2);
		}
		
		public function playHome():void
		{
			bGames.big1.gotoAndStop(1);
			bGames.big2.gotoAndStop(1);
			bGames.big3.gotoAndStop(1);
			bGames.big4.gotoAndStop(1);
			
			dGames.big1.gotoAndStop(1);
			dGames.big2.gotoAndStop(1);
			dGames.big3.gotoAndStop(1);
			dGames.big4.gotoAndStop(1);
			
			hGames.big1.gotoAndStop(1);
			hGames.big2.gotoAndStop(1);
			hGames.big3.gotoAndStop(1);
			hGames.big4.gotoAndStop(1);
			
			lGames.big1.gotoAndStop(1);
			lGames.big2.gotoAndStop(1);
			lGames.big3.gotoAndStop(1);
			lGames.big4.gotoAndStop(1);
			
			pGames.big1.gotoAndStop(1);
			pGames.big2.gotoAndStop(1);
			pGames.big3.gotoAndStop(1);
			pGames.big4.gotoAndStop(1);
			
			hGames.big1.play();
			hGames.big2.play();
			hGames.big3.play();
			hGames.big4.play();
			hGames.visible = true;
			
			isShowing(true, 3);
		}
		
		public function playLife():void
		{
			bGames.big1.gotoAndStop(1);
			bGames.big2.gotoAndStop(1);
			bGames.big3.gotoAndStop(1);
			bGames.big4.gotoAndStop(1);
			
			dGames.big1.gotoAndStop(1);
			dGames.big2.gotoAndStop(1);
			dGames.big3.gotoAndStop(1);
			dGames.big4.gotoAndStop(1);
			
			hGames.big1.gotoAndStop(1);
			hGames.big2.gotoAndStop(1);
			hGames.big3.gotoAndStop(1);
			hGames.big4.gotoAndStop(1);
			
			lGames.big1.gotoAndStop(1);
			lGames.big2.gotoAndStop(1);
			lGames.big3.gotoAndStop(1);
			lGames.big4.gotoAndStop(1);
			
			pGames.big1.gotoAndStop(1);
			pGames.big2.gotoAndStop(1);
			pGames.big3.gotoAndStop(1);
			pGames.big4.gotoAndStop(1);
			
			lGames.big1.play();
			lGames.big2.play();
			lGames.big3.play();
			lGames.big4.play();
			lGames.visible = true;
			
			isShowing(true, 4);
		}
		
		public function playPlay():void
		{
			bGames.big1.gotoAndStop(1);
			bGames.big2.gotoAndStop(1);
			bGames.big3.gotoAndStop(1);
			bGames.big4.gotoAndStop(1);
			
			dGames.big1.gotoAndStop(1);
			dGames.big2.gotoAndStop(1);
			dGames.big3.gotoAndStop(1);
			dGames.big4.gotoAndStop(1);
			
			hGames.big1.gotoAndStop(1);
			hGames.big2.gotoAndStop(1);
			hGames.big3.gotoAndStop(1);
			hGames.big4.gotoAndStop(1);
			
			lGames.big1.gotoAndStop(1);
			lGames.big2.gotoAndStop(1);
			lGames.big3.gotoAndStop(1);
			lGames.big4.gotoAndStop(1);
			
			pGames.big1.gotoAndStop(1);
			pGames.big2.gotoAndStop(1);
			pGames.big3.gotoAndStop(1);
			pGames.big4.gotoAndStop(1);
			
			pGames.big1.play();
			pGames.big2.play();
			pGames.big3.play();
			pGames.big4.play();
			pGames.visible = true;
			
			isShowing(true, 5);
		}
		
		public function fadeGamesOut():void
		{
			trace("fadeGamesOut");
			TweenMax.to(bGames, 1, {alpha:0});
			TweenMax.to(dGames, 1, {alpha:0});
			TweenMax.to(hGames, 1, {alpha:0});
			TweenMax.to(lGames, 1, {alpha:0});
			TweenMax.to(pGames, 1, {alpha:0});
			bGames.stopGames();
			dGames.stopGames();
			hGames.stopGames();
			lGames.stopGames();
			pGames.stopGames();
			bGames.visible = false;
			dGames.visible = false;
			hGames.visible = false;
			lGames.visible = false;
			pGames.visible = false;
			
			if (tempGame != null)
			{
				somethingOpen = false;
				tempGame.stopGames();
				tempGame.visible = false;
				removeChild(tempGame);
				tempGame = null;
			}
		}
		
		public function fadeGamesIn():void
		{
			trace("fadeGameIn bigNumberShowing = " + bigNumberShowing);
			if (bigIsShowing == true && somethingOpen == false)
			{
				//trace("fadeGamesIn bigIsShowing = " + bigNumberShowing);
				switch (bigNumberShowing)
				{
					case 1:
						trace("fadeGamesIn case 1");
						TweenMax.to(bGames, 1, {alpha:1});
						bGames.goGames();
						bGames.visible = true;
					break;
					case 2:
						trace("fadeGamesIn case 2");
						TweenMax.to(dGames, 1, {alpha:1});
						dGames.goGames();
						dGames.visible = true;
					break;
					case 3:
						trace("fadeGamesIn case 3");
						TweenMax.to(hGames, 1, {alpha:1});
						hGames.goGames();
						hGames.visible = true;
					break;
					case 4:
						trace("fadeGamesIn case 4");
						TweenMax.to(lGames, 1, {alpha:1});
						lGames.goGames();
						lGames.visible = true;
					break;
					case 5:
						trace("fadeGamesIn case 5");
						TweenMax.to(pGames, 1, {alpha:1});
						pGames.goGames();
						pGames.visible = true;
					break;
					default:
						trace("wrong number = " + bigNumberShowing);
					break;
				}
				
			}
		}
		
		public function removeThis(m:MovieClip):void
		{
			trace("removeThis");
			somethingOpen = false;
			someGameOpen = false;
			//trace("removeThis");
			if (this.contains(m))
			{
				removeChild(m);
				m = null;
			}
			//fadeGamesIn();
		}
		
		public function addBusinessGame(theX:Number = 0):void
		{
			if (someGameOpen == false)
			{
				someGameOpen = true;
				var vidPath:String = "";
				if (businessLang == "english")
				{
					vidPath = loadXML.businessVidEnglish;
				}
				else
				{
					vidPath = loadXML.businessVidSpanish;
				}
				//trace("document vidPath = " + vidPath);
				bVid = new BigVideo(this, vidPath);
				addChild(bVid);
				bVid.y = (1920/2) - (607/2);
				bVid.x = (whichScreenStarted - 1) * 1080;
				fadeGamesOut();
			}
		}
		
		public function addHomeGame():void
		{
			if (someGameOpen == false)
			{
				someGameOpen = true;
				var vidPath:String = "";
				if (homeLang == "english")
				{
					vidPath = loadXML.homeVidEnglish;
				}
				else
				{
					vidPath = loadXML.homeVidSpanish;
				}
				hVid = new BigVideo(this, vidPath);
				addChild(hVid);
				hVid.y = (1920/2) - (607/2);
				hVid.x = (whichScreenStarted - 1) * 1080;
				fadeGamesOut();
			}
		}
		
		public function addPlayGame():void
		{
			if (someGameOpen == false)
			{
				someGameOpen = true;
				var xPos:Number = (whichScreenStarted - 1) * 1080;
				var yPos:Number = 0;
				var whichScreen:Number = 1;
				var okToAdd:Boolean = false;
				bigNinja = new BillNinja(xPos, yPos, this, 0, whichScreenStarted);//xPos, yPos, this, whichPopup
				bigNinja.isSolo = true;
				addChild(bigNinja);
				bigNinja.width = loadXML.screenWidth/loadXML.numberOfScreens;
				
				switch (whichScreenStarted)
				{
					case 1:
						ninjaOn1 = true;
					break;
					case 1:
						ninjaOn2 = true;
					break;
					case 1:
						ninjaOn3 = true;
					break;
					case 1:
						ninjaOn4 = true;
					break;
				}
				fadeGamesOut();
			}
		}
		
		public function addSavingsGame():void
		{
			if (someGameOpen == false)
			{
				someGameOpen = true;
				dg = new SavingsGame(this, dreamLang);
				addChild(dg);
				
				switch(whichScreenStarted)
				{
					case 1:
						dg.lo1.startGame();
					break;
					
					case 2:
						dg.lo2.startGame();
					break;
					
					case 3:
						dg.lo3.startGame();
					break;
					
					case 4:
						dg.lo4.startGame();
					break;
				}
				fadeGamesOut();
			}
		}
		
		public function removeSavingsGame():void
		{
			someGameOpen = false;
			if (dg != null)
			{
				removeChild(dg);
				dg = null;
			}
		}
		
		public function addLifeGame():void
		{
			trace("addLifeGame someGameOpen = " + someGameOpen);
			if (someGameOpen == false)
			{
				trace("trying to add life game");
				someGameOpen = true;
				//trace("************ start Life Game");
						/*t.visible = false;
						st1.visible = false;
						st2.visible = false;
						st3.visible = false;
						st4.visible = false;
						st1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, startGame);
						st2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, startGame);
						st3.removeEventListener(TuioTouchEvent.TOUCH_DOWN, startGame);
						st4.removeEventListener(TuioTouchEvent.TOUCH_DOWN, startGame);*/
						//if (lg == null)
						//{
							lg = new CreditGame(this, lifeLang);
							addChild(lg);
						//}
				lg.startCountdown();
				fadeGamesOut();
			}
		}
		
		public function removeLifeGame():void
		{
			someGameOpen = false;
			if (lg != null)
			{
				removeChild(lg);
				lg = null;
			}
		}
		
		public function startUp():void 
		{
			addText("startUp");
			Starling.handleLostContext = true;		
			_starling = new Starling(BackgroundContainer, stage);
			//_starling.stage.y = -200;
        	_starling.start();
			
			/*businessAnim = new BusinessAnim();
			addChild(businessAnim);
			businessAnim.width = 4380;
			businessAnim.x = -20;
			businessAnim.height = 1920;
			businessAnim.alpha = 0;
			businessAnim.stop();
			
			dreamAnim = new DreamAnim();
			addChild(dreamAnim);
			dreamAnim.width = 4380;
			dreamAnim.x = -20;
			dreamAnim.height = 1920;
			dreamAnim.alpha = 0;
			dreamAnim.stop();
			
			homeAnim = new HomeAnim();
			addChild(homeAnim);
			homeAnim.width = 4380;
			homeAnim.x = -20;
			homeAnim.height = 1920;
			homeAnim.alpha = 0;
			homeAnim.stop();
			
			lifeAnim = new LifeAnim();
			addChild(lifeAnim);
			lifeAnim.width = 4380;
			lifeAnim.x = -20;
			lifeAnim.height = 1920;
			lifeAnim.alpha = 0;
			lifeAnim.stop();
			
			playAnim = new PlayAnim();
			addChild(playAnim);
			playAnim.width = 4380;
			playAnim.x = -20;
			playAnim.height = 1920;
			playAnim.alpha = 0;
			playAnim.stop();
			
			businessAnimTimeline = new TimelineMax();
			businessAnimTimeline.append(new TweenMax(businessAnim, trans/2, {alpha:.5}));
			businessAnimTimeline.append(new TweenMax(businessAnim, del, {frame:businessAnim.totalFrames, ease:Linear.easeNone}));
			businessAnimTimeline.append(new TweenMax(businessAnim, trans/2, {alpha:0}));
			
			dreamAnimTimeline = new TimelineMax();
			dreamAnimTimeline.append(new TweenMax(dreamAnim, trans/2, {alpha:.5}));
			dreamAnimTimeline.append(new TweenMax(dreamAnim, del, {frame:dreamAnim.totalFrames, ease:Linear.easeNone}));
			dreamAnimTimeline.append(new TweenMax(dreamAnim, trans/2, {alpha:0}));
			
			homeAnimTimeline = new TimelineMax();
			homeAnimTimeline.append(new TweenMax(homeAnim, trans/2, {alpha:.5}));
			homeAnimTimeline.append(new TweenMax(homeAnim, del, {frame:homeAnim.totalFrames, ease:Linear.easeNone}));
			homeAnimTimeline.append(new TweenMax(homeAnim, trans/2, {alpha:0}));
			
			lifeAnimTimeline = new TimelineMax();
			lifeAnimTimeline.append(new TweenMax(lifeAnim, trans/2, {alpha:.5}));
			lifeAnimTimeline.append(new TweenMax(lifeAnim, del, {frame:lifeAnim.totalFrames, ease:Linear.easeNone}));
			lifeAnimTimeline.append(new TweenMax(lifeAnim, trans/2, {alpha:0}));
			
			playAnimTimeline = new TimelineMax();
			playAnimTimeline.append(new TweenMax(playAnim, trans/2, {alpha:.5}));
			playAnimTimeline.append(new TweenMax(playAnim, del, {frame:playAnim.totalFrames, ease:Linear.easeNone}));
			playAnimTimeline.append(new TweenMax(playAnim, trans/2, {alpha:0}));
			
			bgTimeline = new TimelineMax();
			bgTimeline.repeat = -1;
			bgTimeline.append(businessAnimTimeline);
			bgTimeline.append(dreamAnimTimeline);
			bgTimeline.append(homeAnimTimeline);
			bgTimeline.append(lifeAnimTimeline);
			bgTimeline.append(playAnimTimeline);*/
			
			//businessText = new Loader();
			//businessText.load(bturl);
			//addChild(businessText);
			//businessText.addEventListener(Event.COMPLETE, btComplete);
		}
		private function btComplete(e:Event):void
		{
			//trace("btComplete");
			//businessText.width = 4320;
			//businessText.width = 1920;
			//btMC = businessText.content as MovieClip;
			//btMC.width = 4320;
			//btMC.height = 1920;
			//btMC.gotoAndStop(btMC.totalFrames/2);
			//addChild(btMC);
			//TweenMax.to(btMC, 15, {frame:btMC.totalFrames});
		}
		
		private function startVideo()
		{
			
			//addText("***************************** Video Added");
			
			////trace("adding video");
			//Add the video to the stage
			nc.connect(null);
			ns = new NetStream(nc);
			ns.client = this;
			ns.addEventListener(NetStatusEvent.NET_STATUS, ns_onPlayStatus);
			if (loadXML.useAcceleration)
			{
				//addText("using hardware acceleration");
				ns.play(videoURL);
				stage.addEventListener(StageVideoAvailabilityEvent.STAGE_VIDEO_AVAILABILITY, _onStageVideoAvailability);
			}
			else
			{
				addChild(video);
				
				video.attachNetStream(ns);
				ns.play(videoURL);
				
				video.smoothing = true;
				video.width = loadXML.screenWidth;
				video.height = loadXML.screenHeight;
				video.x = 0;
				video.y = 0;
			}
			
			//addEventListener(Event.ENTER_FRAME, loopVid);
		}
		
		//called when we wish to start StageVideo playback
		private function _enableStageVideo():void 
		{
			
			//addText("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ Enable Stage Video");
			//if we do not have a StageVideo object reference 
			//already stored in _stageVideo
			if (stageVideo == null) 
			{
				//set _stageVideo to reference the first of our
				//available StageVideo objects (up to 8 available on desktops)
				stageVideo = stage.stageVideos[0];
				//size and position our _stageVideo with a new Rectangle
				stageVideo.viewPort = new Rectangle(0, 0, loadXML.screenWidth, loadXML.screenHeight);
			}
			//if the _video object is in the display list
			if (video.parent)
			{
				//remove _video from the display list
				removeChild(video) ;
			}
			//attach our NetStream instance to our StageVideo instance
			stageVideo.attachNetStream(ns);
			
			//setChildIndex(bg, 0);
			//setChildIndex(_stageVideo, 1);
		}
 
		//called when StageVideo becomes unavailable
		//and we need to use a Video object as back-up
		private function _disableStageVideo() : void 
		{
			//addText("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ Disable Stage Video");
			video.x = 0;
			video.y = 0;
			video.width = loadXML.screenWidth;
			video.height = loadXML.screenHeight;
			video.smoothing = true;
			//attach our NetStream to our Video object
			video.attachNetStream(ns);
			//add the Video object to the display list
			//(so that it can be seen)           
			addChildAt(video, 0);
			//add the _tf TextField back to the display list
			//so that it is on top of the _video object
			
		}
		//our callback function for the
		//StageVideoAvailabilityEvent.STAGE_VIDEO_AVAILABILITY event
		private function _onStageVideoAvailability(e:StageVideoAvailabilityEvent):void 
		{
			//log the availability of StageVideo
			//addText("Stage Video: " + e.availability);
			//if StageVideo is available
			if  (e.availability == "available")
			{
				//call _enableStageVideo
				//addText("enabling stage video");
				_enableStageVideo();
			} 
			else 
			{ 
				//otherwise call _disableStageVideo
				_disableStageVideo();
			}
		}
 
 
		//--------------------------------------
		//  NetStream Event Handlers
		//--------------------------------------
 
		//the NetStream will look for these two methods
		//on whatever object is identified as the NetStream's client
		//if they do not exist, you will receive a run time error
		//when the NetStream encounters metadata or cuepoints in the video
		public function onMetaData(info:Object):void
		{ 
		
		}
		
		public function onCuePoint(info:Object):void
		{
			
		}
		
		private function ns_onMetaData(_data:Object)
		{
			
		}	
		
		private function ns_onCuePoint(_data:Object)
		{
			
		}
		
		private function onPlayStatus(info:Object)
		{
			 
		}
		
		private function ns_onPlayStatus(e:NetStatusEvent)
		{
			var tempString:String = e.info.code;
			if (e.info.code == "NetStream.Buffer.Empty") 
			{
				//addText(tempString);
				ns.seek(0);
			} 
		}
		
		private function checkKey(e:KeyboardEvent):void
		{
			if (e.keyCode == 83)
			{
				video.smoothing = ! video.smoothing;
			}
			
			if (e.keyCode == 81)
			{
				tf.visible = ! tf.visible;
			}
		}

		//Function to randomise a new action point. Put the sprites in place and fire the genie effect
		private function fireNewGenie(gb:GenieBmd, i:Icons, b:BitmapData, t:Number = 3):void
		{
			//addText("fireNewGenie");
			var pt:Point;
			if (i.whichWay == 1)
			{
				pt=new Point(touchX, touchY); //pt=new Point(touchX - i.x - (gb.IMG_W * 1/4), touchY - i.y - (gb.IMG_W * 1/4)); pt=new Point(mouseX - i.x - (gb.IMG_W*(3/4)), mouseY - i.y - (gb.IMG_H*(3/4)));
			}
			else
			{
				pt=new Point(touchX, touchY); //pt=new Point(touchX - i.x + (gb.IMG_W*(3/4)), touchY - i.y + (gb.IMG_H*(3/4))); pt=new Point(mouseX - i.x - (gb.IMG_W*(3/4)), mouseY - i.y - (gb.IMG_H*(3/4)));
			}
			//addText(pt.toString());
			
			
			
			gb.fireAtPoint(pt.x,pt.y,t);
			
			//gb.sprite.visible = true;
			//gb.visible = true;
			//Fire the effect  (x, y, time in secs);

		}

		private function addIcons():void
		{
			var tempX:Number = loadXML.screenWidth; //stage.stageWidth;
			var tempY:Number = loadXML.screenHeight; //stage.stageHeight;
			var s1:Number = loadXML.speedArray[0];
			var s2:Number = loadXML.speedArray[1];
			var s3:Number = loadXML.speedArray[2];
			var orderArray:Array = new Array (2, 5, 1, 4, 3, 5, 4, 3, 2, 1);
			xmlChanged = new Array();
			
			for (var i:int = 0; i < loadXML.iconsArray.length; i++)
			{
				var isChanged:Boolean = false;
				xmlChanged.push(isChanged);
				
				popup = new Popup(this, loadXML.iconsArray[i][1]);
				popup.gotoAndStop(loadXML.iconsArray[i][1]);
				popup.whichPopup = i;
				addChild(popup);
				popup.visible = false;
				popup.alpha = 0;
				popup.setFilters(false);
				//addText(popup.name.toString());
				
				bmd = new BitmapData(popup.width, popup.height, true, 0x0);
				bmd.draw(popup);
				//addText(bmd.width.toString());
				
				popupArray.push(popup);
				popup.name = "popup" + i;
				bmdArray.push(bmd);
				
				//addText(popup.whichPopup.toString());
				var tempWhichWay:Number;
				if (i < 5)
				{
					tempWhichWay = 1;
				}
				else
				{
					tempWhichWay = -1
				}
				anIcon = new Icons(tempX, tempY, s1, s2, s3, this, tempWhichWay, orderArray[i]);
				addChild(anIcon);
				anIcon.whichOne = loadXML.iconsArray[i][0];
				anIcon.gotoAndStop(loadXML.iconsArray[i][1]);
				anIcon.whichIcon = i;
				anIcon.addEventListener(TuioTouchEvent.TOUCH_DOWN, tuioPauseIcon);
				
				iconArray.push(anIcon);
				//addText(anIcon.whichIcon.toString());
				//gb = new GenieBmd();
				gbArray.push(gb);
			}
			
			
		}
		
		private function tuioPauseIcon(e:TuioTouchEvent):void
		{
			try
			{
				SFXChannel = SFXSound.play();
			}
			catch (error:Error)
			{
				
			}
			addText("pauseIcon");
			e.target.removeEventListener(TuioTouchEvent.TOUCH_DOWN, tuioPauseIcon);
			
			e.target.fadeThis();
			//e.target.addEventListener(TuioTouchEvent.TOUCH_DOWN, tuioStartIcon);
			//addText(e.target.whichOne);
			touchX = e.stageX;
			touchY = e.stageY;
			
			var which:Number = e.target.whichIcon;
			
			iconArray[which].removeEventListener(TuioTouchEvent.TOUCH_DOWN, tuioPauseIcon);
			
			////trace("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& " + loadXML.popupArray[which][0][1]);
			popupArray[which].setFilters(false);
			popupArray[which].visible = false;
			popupArray[which].resetPopup();
			//popupArray[which].resetAll();
			
			//gbArray[which] = null;
			
			
			popupArray[which].startTimeout();
			popupArray[which].visible = true;
			popupArray[which].setScaleBack();
			
			if (e.target.whichWay == 1)
			{
				popupArray[which].x = touchX;
				popupArray[which].y = touchY;
			}
			else
			{
				popupArray[which].x = touchX - popupArray[which].width;
				popupArray[which].y = touchY - popupArray[which].height;
			}
			
			
			TweenMax.to(popupArray[which], 1, {alpha:1, onComplete:startListeners, onCompleteParams:[which]});
			
			popupArray[which].setFilters(true);
			setChildIndex(popupArray[which], numChildren - 1);
			if (popupArray[which].x > loadXML.screenWidth - popupArray[which].width)
			{
				TweenMax.to(popupArray[which], .5, {x:loadXML.screenWidth - popupArray[which].width - 10});
			}
			if (popupArray[which].x < 0)
			{
				TweenMax.to(popupArray[which], .5, {x:200});
			}
			if (popupArray[which].x > 1080 - popupArray[which].width && popupArray[which].x < 1080)
			{
				if (popupArray[which].x < 1080 - popupArray[which].width/2)
				{
					TweenMax.to(popupArray[which], .5, {x:1080 - popupArray[which].width - 10});
				}
				else
				{
					TweenMax.to(popupArray[which], .5, {x:1090});
				}
			}
			if (popupArray[which].x > 2160 - popupArray[which].width && popupArray[which].x < 2160)
			{
				//if (popupArray[which].x < 2160 - popupArray[which].width/2)
				//{
					TweenMax.to(popupArray[which], .5, {x:2160 - popupArray[which].width - 10});
				//}
				//else
				//{
					//TweenMax.to(popupArray[which], .5, {x:2170});
				//}
				
				//uncomment for 4 screens
			}
			if (popupArray[which].x > 3240 - popupArray[which].width && popupArray[which].x < 3240)
			{
				if (popupArray[which].x < 3240 - popupArray[which].width/2)
				{
					TweenMax.to(popupArray[which], .5, {x:3240 - popupArray[which].width - 10});
				}
				else
				{
					TweenMax.to(popupArray[which], .5, {x:3250});
				}
			}
			if (popupArray[which].y > loadXML.screenHeight - 678)
			{
				TweenMax.to(popupArray[which], .5, {y:loadXML.screenHeight - 668});
			}
			if (popupArray[which].y < 0)
			{
				TweenMax.to(popupArray[which], .5, {y:200});
			}
			//addText("width = " + popupArray[w].width + ", height = " + popupArray[w].height);
			
			
			//addText("width = " + popupArray[which].width + ", height = " + popupArray[which].height);
			
			
			
			/*if (!this.contains(gbArray[which]))
			{
				//addText("adding gbArray[" + which + "]");
				addChild(gbArray[which]);
			}*/
			//gbArray[which].startGenie(bmdArray[which]);
			
			//gbArray[which].addEventListener(Event.COMPLETE, addArguments(genieCompleteGB, [which]));
			//fireNewGenie(gbArray[which], iconArray[which], bmdArray[which], 3);
			//gbArray[which].visible = true;
			//setChildIndex(MovieClip(e.target), numChildren - 1);
		}
		
		private function startListeners(w:Number):void
		{
			popupArray[w].addListeners();
			popupArray[w].close_btn.addEventListener(TuioTouchEvent.TOUCH_DOWN, tuioStartIcon);
		}
		
		private function tuioStartIcon(e:TuioTouchEvent):void
		{
			//addText("TUIO Event Received!")
			var which:Number = e.currentTarget.parent.whichPopup;
			////trace("whichPopup = " + which);
			popupArray[which].close_btn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, tuioStartIcon);
			
			touchX = e.stageX;
			touchY = e.stageY;
			SFXChannel = SFXSound.play();
			//addText("startIcon");
			//addText(e.target.whichOne);
			//e.target.addEventListener(TuioTouchEvent.TOUCH_DOWN, tuioPauseIcon);
			
			//addText("whichPopup = " + which);
			iconArray[which].showThis();
			//popupArray[which].visible = false;
			TweenMax.to(popupArray[which], 1, {alpha:0, onComplete:hidePopup, onCompleteParams:[which]});
			
			//popupArray[which].setFilters(false);
			popupArray[which].stopTimeout();
			
			popupArray[which].removeListeners();
			
			if (loadXML.checkCMS == true)
			{
				compareXML(which);
			}
			
			//iconArray[which].addEventListener(TuioTouchEvent.TOUCH_DOWN, tuioPauseIcon);
			/*gbArray[which].sprite.x = popupArray[which].x;
			gbArray[which].sprite.y = popupArray[which].y;
			gbArray[which].visible = true;
			gbArray[which].addEventListener(Event.COMPLETE, addArguments(genieCompleteHandlerGB, [which]));
			gbArray[which].startGenie();
			fireNewGenie(gbArray[which], iconArray[which], bmdArray[which]);*/
			//e.target.playThis();
		}
		
		public function closeMe(n:Popup):void
		{
			trace("popup closeMe");
			n.close_btn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, tuioStartIcon);
			
			//touchX = e.stageX;
			//touchY = e.stageY;
			//addText("startIcon");
			//addText(e.target.whichOne);
			//e.target.addEventListener(TuioTouchEvent.TOUCH_DOWN, tuioPauseIcon);
			
			//addText("whichPopup = " + which);
			iconArray[popupArray.indexOf(n)].showThis();
			//n.visible = false;
			TweenMax.to(n, 1, {alpha:0, onComplete:hidePopup, onCompleteParams:[popupArray.indexOf(n)]});
			
			//n.setFilters(false);
			n.stopTimeout();
			
			n.removeListeners();
		}
		
		private function compareXML(n:Number):void
		{
			switch(n)
			{
				case 0:
				_api.get_media_wall_business();
				break;
				case 1:
				_api.get_media_wall_dreams();
				break;
				case 2:
				_api.get_media_wall_home();
				break;
				case 3:
				_api.get_media_wall_life();
				break;
				case 4:
				_api.get_media_wall_games();
				break;
				case 5:
				_api.get_media_wall_business();
				break;
				case 6:
				_api.get_media_wall_dreams();
				break;
				case 7:
				_api.get_media_wall_home();
				break;
				case 8:
				_api.get_media_wall_life();
				break;
				case 9:
				_api.get_media_wall_games();
				break;
				
			}
		}
		
		private function checkUpdated(d:XML, n:Number):void
		{
			//trace("checkUpdated");
			////trace(d);
			
			for each (var i:Boolean in xmlChanged)
			{
				i = false;
			}
			
			switch (n)
			{
				case 1:
				//trace("case 1");
				if (d.data.title != loadXML.businessTitle)
				{
					loadXML.businessTitle = d.data.title;
					xmlChanged[0] = true;
					xmlChanged[5] = true;
				}
				if (d.data.file != loadXML.businessFile)
				{
					loadXML.businessFile = d.data.file;
					xmlChanged[0] = true;
					xmlChanged[5] = true;
				}
				if (d.data.file_size != loadXML.businessFileSize)
				{
					loadXML.businessFileSize = d.data.file_size;
					xmlChanged[0] = true;
					xmlChanged[5] = true;
				}
				for (var j:int = 0; j < d.data.tab.length(); j++)
				{
					if (d.data.tab[j].title != loadXML.businessArray[j][0])
					{
						loadXML.businessArray[j][0] = d.data.tab[j].title;
						xmlChanged[0] = true;
						xmlChanged[5] = true;
					}
					
					if (d.data.tab[j].button_text != loadXML.businessArray[j][1])
					{
						loadXML.businessArray[j][1] = d.data.tab[j].button_text;
						xmlChanged[0] = true;
						xmlChanged[5] = true;
					}
					if (d.data.tab[j].body != loadXML.businessArray[j][2])
					{
						loadXML.businessArray[j][2] = d.data.tab[j].body;
						xmlChanged[0] = true;
						xmlChanged[5] = true;
					}
					if (d.data.tab[j].photo.file != loadXML.businessArray[j][3])
					{
						loadXML.businessArray[j][3] = d.data.tab[j].photo.file;
						xmlChanged[0] = true;
						xmlChanged[5] = true;
					}
					if (d.data.tab[j].photo.file_size != loadXML.businessArray[j][4])
					{
						loadXML.businessArray[j][4] = d.data.tab[j].photo.file_size;
						xmlChanged[0] = true;
						xmlChanged[5] = true;
					}
					if (d.data.tab[j].video.file != loadXML.businessArray[j][5])
					{
						loadXML.businessArray[j][5] = d.data.tab[j].video.file;
						xmlChanged[0] = true;
						xmlChanged[5] = true;
					}
					if (d.data.tab[j].video.file_size != loadXML.businessArray[j][6])
					{
						loadXML.businessArray[j][6] = d.data.tab[j].video.file_size;
						xmlChanged[0] = true;
						xmlChanged[5] = true;
					}
					/*if (d.data.tab[j].disable != loadXML.businessArray[j][7])
					{
						loadXML.businessArray[j][7] = d.data.tab[j].disable;
						xmlChanged[0] = true;
						xmlChanged[5] = true;
					}*/
				}
				break;
				
				case 2:
				//trace("case 2");
				////trace("*******************************************************");
				////trace("******************************************************* " + d.data);
				////trace("******************************************************* " + d.data.title);
				////trace("******************************************************* " + loadXML.dreamsTitle);
				if (d.data.title != loadXML.dreamsTitle)
				{
					loadXML.dreamsTitle = d.data.title;
					xmlChanged[1] = true;
					xmlChanged[6] = true;
				}
				if (d.data.file != loadXML.dreamsFile)
				{
					loadXML.dreamsFile = d.data.file;
					xmlChanged[1] = true;
					xmlChanged[6] = true;
				}
				if (d.data.file_size != loadXML.dreamsFileSize)
				{
					loadXML.dreamsFileSize = d.data.file_size;
					xmlChanged[1] = true;
					xmlChanged[6] = true;
				}
				for (var j2:int = 0; j2 < d.data.tab.length(); j2++)
				{
					if (d.data.tab[j2].title != loadXML.dreamsArray[j2][0])
					{
						loadXML.dreamsArray[j2][0] = d.data.tab[j2].title;
						xmlChanged[1] = true;
						xmlChanged[6] = true;
					}
					
					if (d.data.tab[j2].button_text != loadXML.dreamsArray[j2][1])
					{
						loadXML.dreamsArray[j2][1] = d.data.tab[j2].button_text;
						xmlChanged[1] = true;
						xmlChanged[6] = true;
					}
					if (d.data.tab[j2].body != loadXML.dreamsArray[j2][2])
					{
						loadXML.dreamsArray[j2][2] = d.data.tab[j2].body;
						xmlChanged[1] = true;
						xmlChanged[6] = true;
					}
					if (d.data.tab[j2].photo.file != loadXML.dreamsArray[j2][3])
					{
						loadXML.dreamsArray[j2][3] = d.data.tab[j2].photo.file;
						xmlChanged[1] = true;
						xmlChanged[6] = true;
					}
					if (d.data.tab[j2].photo.file_size != loadXML.dreamsArray[j2][4])
					{
						loadXML.dreamsArray[j2][4] = d.data.tab[j2].photo.file_size;
						xmlChanged[1] = true;
						xmlChanged[6] = true;
					}
					if (d.data.tab[j2].video.file != loadXML.dreamsArray[j2][5])
					{
						loadXML.dreamsArray[j2][5] = d.data.tab[j2].video.file;
						xmlChanged[1] = true;
						xmlChanged[6] = true;
					}
					if (d.data.tab[j2].video.file_size != loadXML.dreamsArray[j2][6])
					{
						loadXML.dreamsArray[j2][6] = d.data.tab[j2].video.file_size;
						xmlChanged[1] = true;
						xmlChanged[6] = true;
					}
					/*if (d.data.tab[j2].disable != loadXML.dreamsArray[j2][7])
					{
						loadXML.dreamsArray[j2][7] = d.data.tab[j2].disable;
						xmlChanged[1] = true;
						xmlChanged[6] = true;
					}*/
				}
				break;
				
				case 3:
				//trace("case 3");
				//trace(":::::::::::::::::::::::::::::::::::::::::::::::::::");
				var num:Number = 0;
				//trace(num);
				num++;
				if (d.data.title != loadXML.homeTitle)
				{
					
					//trace("d.data.title = " + d.data.title);
					//trace("loadXML.homeTitle = " + loadXML.homeTitle);
					loadXML.homeTitle = d.data.title;
					xmlChanged[2] = true;
					xmlChanged[7] = true;
				}
				//trace(num);
				num++;
				if (d.data.file != loadXML.homeFile)
				{
					//trace(d.data.file);
					//trace(loadXML.homeFile);
					loadXML.homeFile = d.data.file;
					xmlChanged[2] = true;
					xmlChanged[7] = true;
				}
				//trace(num);
				num++;
				if (d.data.file_size != loadXML.homeFileSize)
				{
					//trace(d.data.file_size);
					//trace(loadXML.homeFileSize);
					loadXML.homeFileSize = d.data.file_size;
					xmlChanged[2] = true;
					xmlChanged[7] = true;
				}
				for (var j3:int = 0; j3 < d.data.tab.length(); j3++)
				{
				//trace(num);
				num++;
					if (d.data.tab[j3].title != loadXML.homeArray[j3][0])
					{
						//trace("d.data.tab[j3].title = " + d.data.tab[j3].title);
						//trace("loadXML.homeArray[j3][0] = " + loadXML.homeArray[j3][0]);
						loadXML.homeArray[j3][0] = d.data.tab[j3].title;
						xmlChanged[2] = true;
						xmlChanged[7] = true;
					}
					
				//trace(num);
				num++;
					if (d.data.tab[j3].button_text != loadXML.homeArray[j3][1])
					{
						//trace(d.data.tab[j3].button_text);
						//trace(loadXML.homeArray[j3][1]);
						loadXML.homeArray[j3][1] = d.data.tab[j3].button_text;
						xmlChanged[2] = true;
						xmlChanged[7] = true;
					}
				//trace(num);
				num++;
					if (d.data.tab[j3].body != loadXML.homeArray[j3][2])
					{
						//trace(d.data.tab[j3].body);
						//trace(loadXML.homeArray[j3][2]);
						loadXML.homeArray[j3][2] = d.data.tab[j3].body;
						xmlChanged[2] = true;
						xmlChanged[7] = true;
					}
				//trace(num);
				num++;
					if (d.data.tab[j3].photo.file != loadXML.homeArray[j3][3])
					{
						//trace(d.data.tab[j3].photo.file);
						//trace(loadXML.homeArray[j3][3]);
						loadXML.homeArray[j3][3] = d.data.tab[j3].photo.file;
						xmlChanged[2] = true;
						xmlChanged[7] = true;
					}
				//trace(num);
				num++;
					if (d.data.tab[j3].photo.file_size != loadXML.homeArray[j3][4])
					{
						//trace(d.data.tab[j3].photo.file_size);
						//trace(loadXML.homeArray[j3][4]);
						loadXML.homeArray[j3][4] = d.data.tab[j3].photo.file_size;
						xmlChanged[2] = true;
						xmlChanged[7] = true;
					}
				//trace(num);
				num++;
					if (d.data.tab[j3].video.file != loadXML.homeArray[j3][5])
					{
						//trace(d.data.tab[j3].video.file);
						//trace(loadXML.homeArray[j3][5]);
						loadXML.homeArray[j3][5] = d.data.tab[j3].video.file;
						xmlChanged[2] = true;
						xmlChanged[7] = true;
					}
				//trace(num);
				num++;
					if (d.data.tab[j3].video.file_size != loadXML.homeArray[j3][6])
					{
						//trace(d.data.tab[j3].video.file_size);
						//trace(loadXML.homeArray[j3][6]);
						loadXML.homeArray[j3][6] = d.data.tab[j3].video.file_size;
						xmlChanged[2] = true;
						xmlChanged[7] = true;
					}
				//trace(num);
				num++;
					/*if (d.data.tab[j3].disable != loadXML.homeArray[j3][7])
					{
						//trace("d.data.tab[j3].disable = " + d.data.tab[j3].disable);
						//trace("loadXML.homeArray[j3][7] = " + loadXML.homeArray[j3][7]);
						loadXML.homeArray[j3][7] = d.data.tab[j3].disable;
						xmlChanged[2] = true;
						xmlChanged[7] = true;
					}*/
				}
				break;
				
				case 4:
				//trace("case 4");
				if (d.data.title != loadXML.lifeTitle)
				{
					loadXML.lifeTitle = d.data.title;
					xmlChanged[3] = true;
					xmlChanged[8] = true;
				}
				if (d.data.file != loadXML.lifeFile)
				{
					loadXML.lifeFile = d.data.file;
					xmlChanged[3] = true;
					xmlChanged[8] = true;
				}
				if (d.data.file_size != loadXML.lifeFileSize)
				{
					loadXML.lifeFileSize = d.data.file_size;
					xmlChanged[3] = true;
					xmlChanged[8] = true;
				}
				for (var j4:int = 0; j4 < d.data.tab.length(); j4++)
				{
					if (d.data.tab[j4].title != loadXML.lifeArray[j4][0])
					{
						loadXML.lifeArray[j4][0] = d.data.tab[j4].title;
						xmlChanged[3] = true;
						xmlChanged[8] = true;
					}
					
					if (d.data.tab[j4].button_text != loadXML.lifeArray[j4][1])
					{
						loadXML.lifeArray[j4][1] = d.data.tab[j4].button_text;
						xmlChanged[3] = true;
						xmlChanged[8] = true;
					}
					if (d.data.tab[j4].body != loadXML.lifeArray[j4][2])
					{
						loadXML.lifeArray[j4][2] = d.data.tab[j4].body;
						xmlChanged[3] = true;
						xmlChanged[8] = true;
					}
					if (d.data.tab[j4].photo.file != loadXML.lifeArray[j4][3])
					{
						loadXML.lifeArray[j4][3] = d.data.tab[j4].photo.file;
						xmlChanged[3] = true;
						xmlChanged[8] = true;
					}
					if (d.data.tab[j4].photo.file_size != loadXML.lifeArray[j4][4])
					{
						loadXML.lifeArray[j4][4] = d.data.tab[j4].photo.file_size;
						xmlChanged[3] = true;
						xmlChanged[8] = true;
					}
					if (d.data.tab[j4].video.file != loadXML.lifeArray[j4][5])
					{
						loadXML.lifeArray[j4][5] = d.data.tab[j4].video.file;
						xmlChanged[3] = true;
						xmlChanged[8] = true;
					}
					if (d.data.tab[j4].video.file_size != loadXML.lifeArray[j4][6])
					{
						loadXML.lifeArray[j4][6] = d.data.tab[j4].video.file_size;
						xmlChanged[3] = true;
						xmlChanged[8] = true;
					}
					/*if (d.data.tab[j4].disable != loadXML.lifeArray[j4][7])
					{
						loadXML.lifeArray[j4][7] = d.data.tab[j4].disable;
						xmlChanged[3] = true;
						xmlChanged[8] = true;
					}*/
				}
				break;
				
				case 5:
				
				//trace("case 5");
				//trace(d.data);
				//trace(loadXML.playTitle + ", " + loadXML.playFile + ", " + loadXML.playFileSize + ", " + loadXML.playImage + ", " + loadXML.playText);
			
				if (d.data.title != loadXML.playTitle)
				{
					loadXML.playTitle = d.data.title;
					xmlChanged[4] = true;
					xmlChanged[9] = true;
				}
				if (d.data.file != loadXML.playFile)
				{
					loadXML.playFile = d.data.file;
					xmlChanged[4] = true;
					xmlChanged[9] = true;
				}
				if (d.data.file_size != loadXML.playFileSize)
				{
					loadXML.playFileSize = d.data.file_size;
					xmlChanged[4] = true;
					xmlChanged[9] = true;
				}
				if (d.data.tab.photo.file != loadXML.playImage)
				{
					loadXML.playImage = d.data.tab.photo.file;
					xmlChanged[4] = true;
					xmlChanged[9] = true;
				}
				if (d.data.tab.body != loadXML.playText)
				{
					loadXML.playText = d.data.tab.body;
					xmlChanged[4] = true;
					xmlChanged[9] = true;
				}
				
				//trace(loadXML.playTitle + ", " + loadXML.playFile + ", " + loadXML.playFileSize + ", " + loadXML.playImage + ", " + loadXML.playText);
				
				break;
			}
			//trace("++++++++++++++++++++++++++++++");
			//trace(xmlChanged);
		}
		
		public function hidePopup(w:Number):void
		{
			iconArray[w].addEventListener(TuioTouchEvent.TOUCH_DOWN, tuioPauseIcon);
			//for each(var w:Popup in popupArray)
			//{
				//if (w.alpha == 0)
				//{
					popupArray[w].setFilters(false);
					popupArray[w].visible = false;
					popupArray[w].resetPopup();
					//popupArray[w].resetAll();
					if (iconArray[w].alpha == 0)
					{
						iconArray[w].showThis();
						
					}
					
					if (w > 4)
					{
						if (xmlChanged[w] == true || xmlChanged[w - 5] == true)
						{
							xmlChanged[w] = false;
							popupArray[w].unloadAll();
						}
					}
					if (w <= 4)
					{
						if (xmlChanged[w] == true || xmlChanged[w + 5] == true)
						{
							xmlChanged[w] = false;
							popupArray[w].unloadAll();
						}
					}
				//}
			//}
			//addText("hidePopup");
		}
		
		public function closePopup(w:Number = 0):void
		{
			//touchX = popupArray[w].x;
			//touchY = popupArray[w].y;
			iconArray[w].showThis();
			TweenMax.to(popupArray[w], 1, {alpha:0, onComplete:hidePopup, onCompleteParams:[w]});
			//popupArray[w].visible = false;
			//popupArray[w].setFilters(false);
			//popupArray[w].resetPopup();
			iconArray[w].addEventListener(TuioTouchEvent.TOUCH_DOWN, tuioPauseIcon);
			/*gbArray[w].sprite.x = popupArray[w].x;
			gbArray[w].sprite.y = popupArray[w].y;
			gbArray[w].visible = true;
			gbArray[w].addEventListener(Event.COMPLETE, addArguments(genieCompleteHandlerGB, [w]));
			gbArray[w].startGenie();
			fireNewGenie(gbArray[w], iconArray[w], bmdArray[w]);*/
		}
		
		private function addArguments(method:Function, additionalArguments:Array):Function 
		{
			return function(event:Event):void
			{
				method.apply(null, [event].concat(additionalArguments));
				event.currentTarget.removeEventListener(event.type, arguments.callee);
			}
		}

  		private function genieCompleteGB(e:Event, w:Number):void
		{
			//addText("genieCompleteGB " + w);
			gbArray[w].visible = false;
			popupArray[w].x = gbArray[w].sprite.x;
			popupArray[w].y = gbArray[w].sprite.y;
			popupArray[w].visible = true;
			popupArray[w].setFilters(true);
			setChildIndex(popupArray[w], numChildren - 1);
			if (popupArray[w].x > loadXML.screenWidth - popupArray[w].width)
			{
				TweenMax.to(popupArray[w], .5, {x:loadXML.screenWidth - popupArray[w].width - 10});
			}
			if (popupArray[w].x < 0)
			{
				TweenMax.to(popupArray[w], .5, {x:200});
			}
			if (popupArray[w].y > loadXML.screenHeight - popupArray[w].height)
			{
				TweenMax.to(popupArray[w], .5, {y:loadXML.screenHeight - popupArray[w].height - 10});
			}
			if (popupArray[w].y < 0)
			{
				TweenMax.to(popupArray[w], .5, {y:200});
			}
			//addText("width = " + popupArray[w].width + ", height = " + popupArray[w].height);
			popupArray[w].close_btn.addEventListener(TuioTouchEvent.TOUCH_DOWN, tuioStartIcon);
			gbArray[w].stopGenie();
		}
		
		private function genieCompleteHandlerGB(e:Event, w:Number):void
		{
			//addText("genieCompleteHandlerGB " + w);
			iconArray[w].addEventListener(TuioTouchEvent.TOUCH_DOWN, tuioPauseIcon);
			gbArray[w].stopGenie();
			gbArray[w].visible = false;
		}
		
		public function addNewPong(w:Number):void
		{
			var xPos:Number = 0;
			var yPos:Number = 0;
			var whichScreen:Number = 1;
			var okToAdd:Boolean = false;
			if (popupArray[w].x <= loadXML.screenWidth/loadXML.numberOfScreens)
			{
				xPos = 0;
				whichScreen = 1;
				if (pongOn1 || ninjaOn1 || platformOn1 || brickOn1 || bigOn1)
				{
					xPos = loadXML.screenWidth/loadXML.numberOfScreens;
					whichScreen = 2;
					pongOn2 = true;
				}
				else
				{
					pongOn1 = true;
				}
			}
			if (popupArray[w].x > loadXML.screenWidth/loadXML.numberOfScreens && popupArray[w].x <= loadXML.screenWidth/loadXML.numberOfScreens * 2)
			{
				xPos = loadXML.screenWidth/loadXML.numberOfScreens;
				whichScreen = 2;
				if (pongOn2 || ninjaOn2 || platformOn2 || brickOn2 || bigOn2)
				{
					xPos = 0;
					whichScreen = 1;
					pongOn1 = true;
				}
				else
				{
					pongOn2 = true;
				}
			}
			if (popupArray[w].x > loadXML.screenWidth/loadXML.numberOfScreens * 2 && popupArray[w].x <= loadXML.screenWidth/loadXML.numberOfScreens * 3)
			{
				xPos = loadXML.screenWidth/loadXML.numberOfScreens * 2;
				whichScreen = 3;
				
				if (pongOn3 || ninjaOn3 || platformOn3 || brickOn3 || bigOn3)
				{
					xPos = loadXML.screenWidth/loadXML.numberOfScreens * 3;
					whichScreen = 4;
					pongOn4 = true;
				}
				else
				{
					pongOn3 = true;
				}
			}
			if (popupArray[w].x > loadXML.screenWidth/loadXML.numberOfScreens * 3)
			{
				xPos = loadXML.screenWidth/loadXML.numberOfScreens * 3;
				whichScreen = 4;
				
				if (pongOn4 || ninjaOn4 || platformOn4 || brickOn4 || bigOn4)
				{
					xPos = loadXML.screenWidth/loadXML.numberOfScreens * 2;
					whichScreen = 3;
					pongOn4 = true;
				}
				else
				{
					pongOn4 = true;
				}
			}
			if (pongGame1 == null)
			{
				pongGame1 = new Pong(xPos, yPos, this, w, whichScreen);//xPos, yPos, this, whichPopup
				addChild(pongGame1);
				pongGame1.width = loadXML.screenWidth/loadXML.numberOfScreens;
				pongGame1.height = loadXML.screenHeight;
				//trace("pong1 width = " + pongGame1.width + ", height = " + pongGame1.height + ", x = " + pongGame1.x + ", y = " + pongGame1.y);
				
				popupArray[w].visible = false;
			}
			else if (pongGame2 == null)
			{
				pongGame2 = new Pong(xPos, yPos, this, w, whichScreen);//xPos, yPos, this, whichPopup
				addChild(pongGame2);
				pongGame2.width = loadXML.screenWidth/loadXML.numberOfScreens;
				pongGame2.height = loadXML.screenHeight;
				//trace("pong2 width = " + pongGame2.width + ", height = " + pongGame2.height + ", x = " + pongGame2.x + ", y = " + pongGame2.y);
				
				popupArray[w].visible = false;
			}
			
			
		}
		
		public function addNewPlatform(w:Number):void
		{
			var xPos:Number = 0;
			var yPos:Number = 0;
			var whichScreen:Number = 1;
			var okToAdd:Boolean = false;
			if (popupArray[w].x <= loadXML.screenWidth/loadXML.numberOfScreens)
			{
				xPos = 0;
				whichScreen = 1;
				if (pongOn1 || ninjaOn1 || platformOn1 || brickOn1 || bigOn1)
				{
					xPos = loadXML.screenWidth/loadXML.numberOfScreens;
					whichScreen = 2;
					platformOn2 = true;
				}
				else
				{
					platformOn1 = true;
				}
			}
			if (popupArray[w].x > loadXML.screenWidth/loadXML.numberOfScreens && popupArray[w].x <= loadXML.screenWidth/loadXML.numberOfScreens * 2)
			{
				xPos = loadXML.screenWidth/loadXML.numberOfScreens;
				whichScreen = 2;
				if (pongOn2 || ninjaOn2 || platformOn2 || brickOn2 || bigOn2)
				{
					xPos = 0;
					whichScreen = 1;
					platformOn1 = true;
				}
				else
				{
					platformOn2 = true;
				}
			}
			if (popupArray[w].x > loadXML.screenWidth/loadXML.numberOfScreens * 2 && popupArray[w].x <= loadXML.screenWidth/loadXML.numberOfScreens * 3)
			{
				xPos = loadXML.screenWidth/loadXML.numberOfScreens * 2;
				whichScreen = 3;
				
				if (pongOn3 || ninjaOn3 || platformOn3 || brickOn3 || bigOn3)
				{
					xPos = loadXML.screenWidth/loadXML.numberOfScreens * 3;
					whichScreen = 4;
					platformOn4 = true;
				}
				else
				{
					platformOn3 = true;
				}
			}
			if (popupArray[w].x > loadXML.screenWidth/loadXML.numberOfScreens * 3)
			{
				xPos = loadXML.screenWidth/loadXML.numberOfScreens * 3;
				whichScreen = 4;
				
				if (pongOn4 || ninjaOn4 || platformOn4 || brickOn4 || bigOn4)
				{
					xPos = loadXML.screenWidth/loadXML.numberOfScreens * 2;
					whichScreen = 3;
					platformOn4 = true;
				}
				else
				{
					platformOn3 = true;
				}
			}
			if (platformGame1 == null)
			{
				platformGame1 = new PlatformGame(xPos, yPos, this, w, whichScreen);//xPos, yPos, this, whichPopup
				addChild(platformGame1);
				//platformGame1.width = loadXML.screenWidth/loadXML.numberOfScreens;
				//platformGame1.height = loadXML.screenHeight;
				////trace("pong1 width = " + platformGame1.width + ", height = " + platformGame1.height + ", x = " + platformGame1.x + ", y = " + platformGame1.y);
				platformGame1.addEventListener(TuioTouchEvent.TOUCH_DOWN, focusJohnny1);
				popupArray[w].visible = false;
			}
			else if (platformGame2 == null)
			{
				platformGame2 = new PlatformGame(xPos, yPos, this, w, whichScreen);//xPos, yPos, this, whichPopup
				addChild(platformGame2);
				platformGame2.addEventListener(TuioTouchEvent.TOUCH_DOWN, focusJohnny2);
				//pongGame2.width = loadXML.screenWidth/loadXML.numberOfScreens;
				//pongGame2.height = loadXML.screenHeight;
				////trace("pong2 width = " + pongGame2.width + ", height = " + pongGame2.height + ", x = " + pongGame2.x + ", y = " + pongGame2.y);
				
				popupArray[w].visible = false;
			}
			
			
		}
		
		private function focusJohnny1(e:TuioTouchEvent):void
		{
			setChildIndex(platformGame1, numChildren - 1);
		}
		
		private function focusJohnny2(e:TuioTouchEvent):void
		{
			setChildIndex(platformGame2, numChildren - 1);
		}
		
		public function addNewBreaker(w:Number):void
		{
			var xPos:Number = 0;
			var yPos:Number = 0;
			var whichScreen:Number = 1;
			var okToAdd:Boolean = false;
			if (popupArray[w].x <= loadXML.screenWidth/loadXML.numberOfScreens)
			{
				xPos = 0;
				whichScreen = 1;
				if (pongOn1 || ninjaOn1 || platformOn1 || brickOn1 || bigOn1)
				{
					xPos = loadXML.screenWidth/loadXML.numberOfScreens;
					whichScreen = 2;
					brickOn2 = true;
				}
				else
				{
					brickOn1 = true;
				}
			}
			if (popupArray[w].x > loadXML.screenWidth/loadXML.numberOfScreens && popupArray[w].x <= loadXML.screenWidth/loadXML.numberOfScreens * 2)
			{
				xPos = loadXML.screenWidth/loadXML.numberOfScreens;
				whichScreen = 2;
				if (pongOn2 || ninjaOn2 || platformOn2 || brickOn2 || bigOn2)
				{
					xPos = 0;
					whichScreen = 1;
					brickOn1 = true;
				}
				else
				{
					brickOn2 = true;
				}
			}
			if (popupArray[w].x > loadXML.screenWidth/loadXML.numberOfScreens * 2 && popupArray[w].x <= loadXML.screenWidth/loadXML.numberOfScreens * 3)
			{
				xPos = loadXML.screenWidth/loadXML.numberOfScreens * 2;
				whichScreen = 3;
				
				if (pongOn3 || ninjaOn3 || platformOn3 || brickOn3 || bigOn3)
				{
					xPos = loadXML.screenWidth/loadXML.numberOfScreens * 3;
					whichScreen = 4;
					brickOn4 = true;
				}
				else
				{
					brickOn3 = true;
				}
			}
			if (popupArray[w].x > loadXML.screenWidth/loadXML.numberOfScreens * 3)
			{
				xPos = loadXML.screenWidth/loadXML.numberOfScreens * 3;
				whichScreen = 4;
				
				if (pongOn4 || ninjaOn4 || platformOn4 || brickOn4 || bigOn4)
				{
					xPos = loadXML.screenWidth/loadXML.numberOfScreens * 2;
					whichScreen = 3;
					brickOn4 = true;
				}
				else
				{
					brickOn3 = true;
				}
			}
			if (brickGame1 == null)
			{
				brickGame1 = new BrickGame(xPos, yPos, this, w, whichScreen);//xPos, yPos, this, whichPopup
				addChild(brickGame1);
				brickGame1.width = loadXML.screenWidth/loadXML.numberOfScreens;
				brickGame1.height = loadXML.screenHeight;
				////trace("pong1 width = " + platformGame1.width + ", height = " + platformGame1.height + ", x = " + platformGame1.x + ", y = " + platformGame1.y);
				
				popupArray[w].visible = false;
			}
			else if (brickGame2 == null)
			{
				brickGame2 = new BrickGame(xPos, yPos, this, w, whichScreen);//xPos, yPos, this, whichPopup
				addChild(brickGame2);
				brickGame2.width = loadXML.screenWidth/loadXML.numberOfScreens;
				brickGame2.height = loadXML.screenHeight;
				////trace("pong2 width = " + pongGame2.width + ", height = " + pongGame2.height + ", x = " + pongGame2.x + ", y = " + pongGame2.y);
				
				popupArray[w].visible = false;
			}
		}
		
		public function addNewBillNinja(w:Number):void
		{
			var xPos:Number = 0;
			var yPos:Number = 0;
			var whichScreen:Number = 1;
			var okToAdd:Boolean = false;
			if (popupArray[w].x <= loadXML.screenWidth/loadXML.numberOfScreens)
			{
				xPos = 0;
				whichScreen = 1;
				if (ninjaOn1 || pongOn1 || platformOn1 || brickOn1 || bigOn1)
				{
					xPos = loadXML.screenWidth/loadXML.numberOfScreens;
					whichScreen = 2;
					ninjaOn2 = true;
				}
				else
				{
					ninjaOn1 = true;
				}
			}
			if (popupArray[w].x > loadXML.screenWidth/loadXML.numberOfScreens && popupArray[w].x <= loadXML.screenWidth/loadXML.numberOfScreens * 2)
			{
				xPos = loadXML.screenWidth/loadXML.numberOfScreens;
				whichScreen = 2;
				if (ninjaOn2 || pongOn2 || platformOn2 || brickOn2 || bigOn2)
				{
					xPos = 0;
					whichScreen = 1;
					ninjaOn1 = true;
				}
				else
				{
					ninjaOn2 = true;
				}
			}
			if (popupArray[w].x > loadXML.screenWidth/loadXML.numberOfScreens * 2 && popupArray[w].x <= loadXML.screenWidth/loadXML.numberOfScreens * 3)
			{
				xPos = loadXML.screenWidth/loadXML.numberOfScreens * 2;
				whichScreen = 3;
				
				if (ninjaOn3 || pongOn3 || platformOn3 || brickOn3 || bigOn3)
				{
					xPos = loadXML.screenWidth/loadXML.numberOfScreens * 3;
					whichScreen = 4;
					ninjaOn4 = true;
				}
				else
				{
					ninjaOn3 = true;
				}
			}
			if (popupArray[w].x > loadXML.screenWidth/loadXML.numberOfScreens * 3)
			{
				xPos = loadXML.screenWidth/loadXML.numberOfScreens * 3;
				whichScreen = 4;
				
				if (ninjaOn4 || pongOn4 || platformOn4 || brickOn4 || bigOn4)
				{
					xPos = loadXML.screenWidth/loadXML.numberOfScreens * 2;
					whichScreen = 3;
					ninjaOn4 = true;
				}
				else
				{
					ninjaOn4 = true;
				}
			}
			if (ninjaGame1 == null)
			{
				ninjaGame1 = new BillNinja(xPos, yPos, this, w, whichScreen);//xPos, yPos, this, whichPopup
				addChild(ninjaGame1);
				ninjaGame1.width = loadXML.screenWidth/loadXML.numberOfScreens;
				//ninjaGame1.height = loadXML.screenHeight;
				//trace("ninja1 width = " + ninjaGame1.width + ", height = " + ninjaGame1.height + ", x = " + ninjaGame1.x + ", y = " + ninjaGame1.y);
				
				popupArray[w].visible = false;
			}
			else if (ninjaGame2 == null)
			{
				ninjaGame2 = new BillNinja(xPos, yPos, this, w, whichScreen);//xPos, yPos, this, whichPopup
				addChild(ninjaGame2);
				ninjaGame2.width = loadXML.screenWidth/loadXML.numberOfScreens;
				//ninjaGame2.height = loadXML.screenHeight;
				//trace("ninja2 width = " + ninjaGame2.width + ", height = " + ninjaGame2.height + ", x = " + ninjaGame2.x + ", y = " + ninjaGame2.y);
				
				popupArray[w].visible = false;
			}
			
		}
		
		public function removeNewBillNinja(w:Number, s:Number, b:Boolean = false):void
		{
			addText("w = " + w + ", s = " + s + ", b = " + b);
			if (b == false)
			{
				if (ninjaGame1 != null)
				{
					//addText("ninjaGame1 != null");
					//addText("ninjaGame1.whichNinja = " + ninjaGame1.whichNinja);
					if (ninjaGame1.whichNinja == w)
					{
						//addText("ninjaGame1.whichNinja == w");
						popupArray[w].startTimeout();
						removeChild(ninjaGame1);
						ninjaGame1 = null;
						popupArray[w].visible = true;
						popupArray[w].resetPopup();
					}
					else
					{
						if (ninjaGame2 != null)
						{
							if (ninjaGame2.whichNinja == w)
							{
								//addText("ninjaGame2.whichNinja == w");
								popupArray[w].startTimeout();
								removeChild(ninjaGame2);
								ninjaGame2 = null;
								popupArray[w].visible = true;
								popupArray[w].resetPopup();
							}
						}
					}
				}
				else if (ninjaGame2 != null)
				{
					//addText("ninjaGame2 != null");
					//addText("ninjaGame2.whichNinja = " + ninjaGame2.whichNinja);
					if (ninjaGame2.whichNinja == w)
					{
						//addText("ninjaGame2.whichNinja == w");
						popupArray[w].startTimeout();
						removeChild(ninjaGame2);
						ninjaGame2 = null;
						popupArray[w].visible = true;
						popupArray[w].resetPopup();
					}
				}
				
				
			}
			else
			{
				removeChild(bigNinja);
				bigNinja = null;
				
				someGameOpen = false;
				fadeGamesIn();
			}
			switch (s)
				{
					case 1:
					ninjaOn1 = false;
					break;
					case 2:
					ninjaOn2 = false;
					break;
					case 3:
					ninjaOn3 = false;
					break;
					case 4:
					ninjaOn4 = false;
					break;
				}
			
		}
		
		public function removeNewPong(w:Number, s:Number):void
		{
			//addText("w = " + w + ", s = " + s);
			if (pongGame1 != null)
			{
				//addText("ninjaGame1 != null");
				//addText("ninjaGame1.whichNinja = " + ninjaGame1.whichNinja);
				if (pongGame1.whichPong == w)
				{
					//addText("ninjaGame1.whichNinja == w");
					popupArray[w].startTimeout();
					removeChild(pongGame1);
					pongGame1 = null;
					popupArray[w].visible = true;
					popupArray[w].resetPopup();
				}
				else
				{
					if (pongGame2 != null)
					{
						if (pongGame2.whichPong == w)
						{
							//addText("ninjaGame2.whichNinja == w");
							popupArray[w].startTimeout();
							removeChild(pongGame2);
							pongGame2 = null;
							popupArray[w].visible = true;
							popupArray[w].resetPopup();
						}
					}
				}
			}
			else if (pongGame2 != null)
			{
				//addText("ninjaGame2 != null");
				//addText("ninjaGame2.whichNinja = " + ninjaGame2.whichNinja);
				if (pongGame2.whichPong == w)
				{
					//addText("ninjaGame2.whichNinja == w");
					popupArray[w].startTimeout();
					removeChild(pongGame2);
					pongGame2 = null;
					popupArray[w].visible = true;
					popupArray[w].resetPopup();
				}
			}
			
			switch (s)
			{
				case 1:
				pongOn1 = false;
				break;
				case 2:
				pongOn2 = false;
				break;
				case 3:
				pongOn3 = false;
				break;
				case 4:
				pongOn4 = false;
				break;
			}
			
		}
		
		public function removeNewPlatform(w:Number, s:Number):void
		{
			//addText("w = " + w + ", s = " + s);
			if (platformGame1 != null)
			{
				//addText("ninjaGame1 != null");
				//addText("ninjaGame1.whichNinja = " + ninjaGame1.whichNinja);
				if (platformGame1.whichPong == w)
				{
					//addText("ninjaGame1.whichNinja == w");
					popupArray[w].startTimeout();
					platformGame1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, focusJohnny1);
					removeChild(platformGame1);
					platformGame1 = null;
					popupArray[w].visible = true;
					popupArray[w].resetPopup();
				}
				else
				{
					if (platformGame2 != null)
					{
						if (platformGame2.whichPong == w)
						{
							//addText("ninjaGame2.whichNinja == w");
							popupArray[w].startTimeout();
							platformGame2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, focusJohnny2);
							removeChild(platformGame2);
							platformGame2 = null;
							popupArray[w].visible = true;
							popupArray[w].resetPopup();
						}
					}
				}
			}
			else if (platformGame2 != null)
			{
				//addText("ninjaGame2 != null");
				//addText("ninjaGame2.whichNinja = " + ninjaGame2.whichNinja);
				if (platformGame2.whichPong == w)
				{
					//addText("ninjaGame2.whichNinja == w");
					popupArray[w].startTimeout();
					platformGame2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, focusJohnny2);
					removeChild(platformGame2);
					platformGame2 = null;
					popupArray[w].visible = true;
					popupArray[w].resetPopup();
				}
			}
			
			switch (s)
			{
				case 1:
				platformOn1 = false;
				break;
				case 2:
				platformOn2 = false;
				break;
				case 3:
				platformOn3 = false;
				break;
				case 4:
				platformOn4 = false;
				break;
			}
			
		}
		
		public function removeNewBrick(w:Number, s:Number):void
		{
			//addText("w = " + w + ", s = " + s);
			if (brickGame1 != null)
			{
				//addText("ninjaGame1 != null");
				//addText("ninjaGame1.whichNinja = " + ninjaGame1.whichNinja);
				if (brickGame1.whichPong == w)
				{
					//addText("ninjaGame1.whichNinja == w");
					popupArray[w].startTimeout();
					removeChild(brickGame1);
					brickGame1 = null;
					popupArray[w].visible = true;
					popupArray[w].resetPopup();
				}
				else
				{
					if (brickGame2 != null)
					{
						if (brickGame2.whichPong == w)
						{
							//addText("ninjaGame2.whichNinja == w");
							popupArray[w].startTimeout();
							removeChild(brickGame2);
							brickGame2 = null;
							popupArray[w].visible = true;
							popupArray[w].resetPopup();
						}
					}
				}
			}
			else if (brickGame2 != null)
			{
				//addText("ninjaGame2 != null");
				//addText("ninjaGame2.whichNinja = " + ninjaGame2.whichNinja);
				if (brickGame2.whichPong == w)
				{
					//addText("ninjaGame2.whichNinja == w");
					popupArray[w].startTimeout();
					removeChild(brickGame2);
					brickGame2 = null;
					popupArray[w].visible = true;
					popupArray[w].resetPopup();
				}
			}
			
			switch (s)
			{
				case 1:
				brickOn1 = false;
				break;
				case 2:
				brickOn2 = false;
				break;
				case 3:
				brickOn3 = false;
				break;
				case 4:
				brickOn4 = false;
				break;
			}
			
		}
		
		public function setPopupDepth(w:Number):void
		{
			setChildIndex(popupArray[w], numChildren - 1);
		}
		
		public function addText(s:String):void
		{
			//trace(s);
			tf.text = s + " \n" + "********************" + " \n" + tf.text;
			//setChildIndex(text1, numChildren - 1);
		}
	}

}