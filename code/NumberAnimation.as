﻿package code
{
	/// This imports two number mc's; they alternate transitioning in to look like a spinner
	/// Choose a transition time(transTime)
	/// May need to fiddle with the mask location & size


	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import flash.filters.*;
	import flash.utils.Timer;
	import flash.geom.Point;

	public class NumberAnimation extends Sprite
	{

		public var number0:SpinNumberRow = new SpinNumberRow();
		public var number1:SpinNumberRow = new SpinNumberRow();
		
		public var numMask:NumberMask = new NumberMask();
		
		public var transTime:Number;
		public var slowTimer:Timer;
		public var stoppingFlag:Boolean;
		public var finalNumber:Number;
		public var hideFinalNumber:Boolean;
		
		private var numRandom:Number;
		public var whichDigit:Number;

		private var myBlur:BlurFilter = new BlurFilter();
		private var amountToMove:Number;

		public var startY:Number;
		public var endY:Number;

		private var finalYLocAry:Array;
		
		public static const ANIM_DONE:String = "animdone";

		public function NumberAnimation()
		{
			amountToMove = 15;
			myBlur.quality = 5;
			myBlur.blurX = 0;
			myBlur.blurY = 10;
			
			this.mask = numMask;
			if(!this.contains(numMask)){
				addChild(numMask);
			}
			this.mask.x = -20;
			this.mask.y = -14;
			
			// these are the y locs for showing numbers
			finalYLocAry = [-546,-485,-424,-363,-302,-241,-180,-120,-59,2];// where to put mc so the numbers appear (0,1,2,3,4,5,6,7,8,9);
			stoppingFlag = false;
		}

		public function init():void
		{					
					removeListeners();

					this.alpha = 0;
					number0.alpha = 1;
					number1.alpha = 1;
					
					endY = number0.height - 20;
					startY = -(number0.height);
					var ndx:Number =  (Math.floor(Math.random() * (7 - 0 + 1)) + 0);
					
					var randomLoc:Number = finalYLocAry[ndx];
					number0.y = 0 + randomLoc;
					number1.y = startY + randomLoc;
					
					this.alpha = 1;
					number0.alpha = 1;
					number1.alpha = 1;
					if(!this.contains(number0)){
						addChild(number0);
						addChild(number1);
					}
					
					number0.name = "number0";
					number1.name = "number1";
					number0.filters = [myBlur];
					number1.filters = [myBlur];
					
					if(!this.hasEventListener(Event.ENTER_FRAME)){
						this.addEventListener(Event.ENTER_FRAME,moveNumbers,false,0,true);
					}
		}

		public function moveNumbers(e:Event):void
		{
			number0.y +=  amountToMove;
			number1.y +=  amountToMove;

			checkForOnBottom();
		}


		public function stopAtNumber():void
		{
			stoppingFlag = true;
			
			if(slowTimer != null){
				slowTimer = null;
				}
				
			slowTimer = new Timer(20,10);
			slowTimer.addEventListener(TimerEvent.TIMER, decAmtToMove,false,0,true);
			slowTimer.addEventListener(TimerEvent.TIMER_COMPLETE,finalStop,false,0,true);
			slowTimer.start();
		}

		private function decAmtToMove(e:TimerEvent):void
		{
			if (amountToMove > 8)
			{
				amountToMove -=  2;
			}
		}

		private function finalStop(e:TimerEvent=null):void
		{
			slowTimer.removeEventListener(TimerEvent.TIMER, decAmtToMove);
			slowTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,finalStop);
			this.removeEventListener(Event.ENTER_FRAME,moveNumbers);
			this.addEventListener(Event.ENTER_FRAME,checkLocForEnd,false,0,true);
			slowTimer.stop();
			slowTimer = null;
			
		}

		public function checkLocForEnd(e:Event):void
		{
			if(hideFinalNumber == true && this.alpha > 0)
			{
				
				if(this.alpha > 0.11){
					this.alpha -= 0.15;
				}
				else
				{
					this.alpha = 0;
				}
			}
				
			var finalNumberIndex:Number = 9 - finalNumber;
			var numberHeight:Number = finalYLocAry[finalNumber];
			var num1Y:Number = number1.y;
			
			number0.filters = [];
			number1.filters = [];
			

			if (number0.y < numberHeight)
			{
				var deltaY:Number = num1Y - numberHeight;
				
				switch (true) {
					case deltaY > 500:
						amountToMove = 15;
						break;
					case deltaY > 400:
						amountToMove = 13;
						break;
					case deltaY > 300:
						amountToMove = 13;
						break;
					case deltaY > 200:
						amountToMove = 10;
						break;
					case deltaY > 100:
						amountToMove =10;
						break;
					default:
						amountToMove = 9;
				}
			}

			number0.y +=  amountToMove;
			number1.y +=  amountToMove;
			
			checkForOnBottom();
			
			if(number0.y >= numberHeight){
				if (number0.y > (numberHeight + 80))
				{
					var changeAmt:Number = number0.y; // remember this y
					number0.y = numberHeight - 250; // so it will move down slowly
					changeAmt = changeAmt - number0.y;

					number1.y = number0.y +  number0.height;
					}
				else
				{
					this.removeEventListener(Event.ENTER_FRAME,checkLocForEnd);
					if (finalNumber == 0)
					{
						number0.y = numberHeight;
						if(hideFinalNumber == true && this.alpha > 0){
							this.alpha = 0;
							}
					}
					else
					{
						number0.y = numberHeight;
					}
					dispatchEvent(new Event(ANIM_DONE, true));
					stoppingFlag = false;
				}
			}
		}
		
		private function checkForOnBottom():void
		{
			if (number0.y >= endY)
			{
				number0.y = startY;
			}
			if (number1.y >= endY)
			{
				number1.y = startY;
			}
		}

		public function get final_Number():Number
		{
			return finalNumber;
		}

		public function set final_Number(num:Number):void
		{
			finalNumber = num;
		}
		
		public function removeListeners():void{
			this.removeEventListener(Event.ENTER_FRAME,moveNumbers);
			this.removeEventListener(Event.ENTER_FRAME,checkLocForEnd);
			
			if(slowTimer != null){
				slowTimer.removeEventListener(TimerEvent.TIMER, decAmtToMove);
				slowTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,finalStop);
			}
			
			
		}


	}


}