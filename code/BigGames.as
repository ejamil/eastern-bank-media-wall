﻿package  code
{
	import flash.display.MovieClip;
	
	import org.tuio.*;
	import org.tuio.adapters.*;
	import org.tuio.gestures.*;
	import org.tuio.debug.TuioDebug;
	import org.tuio.TuioManager;
	import org.tuio.connectors.*;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	public class BigGames extends MovieClip
	{
		private var player1:Boolean = false;
		private var player2:Boolean = false;
		private var player3:Boolean = false;
		private var player4:Boolean = false;
		
		public var whichGame:Number;
		
		//private var dg:SavingsGame;
		//private var lg:CreditGame;
		public var countdownTimer:Timer;
		public var time:Number = 10;
		
		public var owner:Document;

		public function BigGames(myOwner:Document) 
		{
			owner = myOwner;
		}
		
		public function setFrame(n:Number):void
		{
			this.gotoAndStop(n);
			whichGame = n;
			//trace("BigGames whichGame = " + whichGame);
			switch (whichGame)
			{
				case 1:
					if (owner.loadXML.showBEVid == false)
					{
						big1.ste.visible = false;
						big2.ste.visible = false;
						big3.ste.visible = false;
						big4.ste.visible = false;
					}
					if (owner.loadXML.showBSVid == false)
					{
						big1.sts.visible = false;
						big2.sts.visible = false;
						big3.sts.visible = false;
						big4.sts.visible = false;
					}
				break;
				case 3:
					big1.sts.visible = false;
					big2.sts.visible = false;
					big3.sts.visible = false;
					big4.sts.visible = false;
					//trace("case 3");
					if (owner.loadXML.showHEVid == false)
					{
						//trace("owner.loadXML.showHEVid == false");
						big1.ste.visible = false;
						big2.ste.visible = false;
						big3.ste.visible = false;
						big4.ste.visible = false;
					}
					if (owner.loadXML.showHSVid == true)
					{
						//trace("owner.loadXML.showHSVid == false");
						big1.sts.visible = true;
						big2.sts.visible = true;
						big3.sts.visible = true;
						big4.sts.visible = true;
						//trace(big1.sts.visible);
					}
				break;
			}
			goGames();
			
		}
		
		private function startGameSpanish(e:TuioTouchEvent):void
		{
			switch (whichGame)
			{
				case 1:
					
				break;
				
				case 2:
					owner.dreamLang = "spanish";
				break;
				
				case 3:
					
				break;
				
				case 4:
					owner.lifeLang = "spanish";
				break;
				
				case 5:
					
				break;
			}
			startGame();
		}
		
		private function startGameEnglish(e:TuioTouchEvent):void
		{
			if (e.currentTarget.name == "st1e" || e.currentTarget.name == "st1s")
			{
				owner.whichScreenStarted = 1;
			}
			if (e.currentTarget.name == "st2e" || e.currentTarget.name == "st2s")
			{
				owner.whichScreenStarted = 2;
			}
			if (e.currentTarget.name == "st3e" || e.currentTarget.name == "st3s")
			{
				owner.whichScreenStarted = 3;
			}
			if (e.currentTarget.name == "st4e" || e.currentTarget.name == "st4s")
			{
				owner.whichScreenStarted = 4;
			}
			//trace("whichScreenStarted = " + owner.whichScreenStarted);
			switch (whichGame)
			{
				case 1:
					owner.businessLang = "english";
				break;
				
				case 2:
					owner.dreamLang = "english";
				break;
				
				case 3:
					owner.homeLang = "english";
				break;
				
				case 4:
					owner.lifeLang = "english";
				break;
				
				case 5:
					owner.playLang = "english";
				break;
			}
			startGame();
		}
		
		
		
		private function startGame1(e:TuioTouchEvent):void
		{
			owner.whichScreenStarted = 1;
			trace("startGame1 whichGame = " + whichGame);
			trace("whichScreenStarted = " + owner.whichScreenStarted);
			var langString:String = "";
			switch (e.currentTarget.name)
			{
				case "ste":
					langString = "english";
				break;
				case "sts":
					langString = "spanish";
				break;
			}
			switch (whichGame)
			{
				case 1:
					owner.businessLang = langString;
				break;
				
				case 2:
					owner.dreamLang = langString;
				break;
				
				case 3:
					owner.homeLang = langString;
				break;
				
				case 4:
					owner.lifeLang = langString;
				break;
				
				case 5:
					owner.playLang = langString;
				break;
			}
			startGame();
		}
		
		private function startGame2(e:TuioTouchEvent):void
		{
			owner.whichScreenStarted = 2;
			
			//trace("whichScreenStarted = " + owner.whichScreenStarted);
			var langString:String = "";
			switch (e.currentTarget.name)
			{
				case "ste":
					langString = "english";
				break;
				case "sts":
					langString = "spanish";
				break;
			}
			switch (whichGame)
			{
				case 1:
					owner.businessLang = langString;
				break;
				
				case 2:
					owner.dreamLang = langString;
				break;
				
				case 3:
					owner.homeLang = langString;
				break;
				
				case 4:
					owner.lifeLang = langString;
				break;
				
				case 5:
					owner.playLang = langString;
				break;
			}
			startGame();
		}
		
		private function startGame3(e:TuioTouchEvent):void
		{
			owner.whichScreenStarted = 3;
			
			//trace("whichScreenStarted = " + owner.whichScreenStarted);
			var langString:String = "";
			switch (e.currentTarget.name)
			{
				case "ste":
					langString = "english";
				break;
				case "sts":
					langString = "spanish";
				break;
			}
			//trace("langString = " + langString);
			switch (whichGame)
			{
				case 1:
					owner.businessLang = langString;
				break;
				
				case 2:
					owner.dreamLang = langString;
				break;
				
				case 3:
					owner.homeLang = langString;
				break;
				
				case 4:
					owner.lifeLang = langString;
				break;
				
				case 5:
					owner.playLang = langString;
				break;
			}
			startGame();
		}
		
		private function startGame4(e:TuioTouchEvent):void
		{
			owner.whichScreenStarted = 4;
			
			//trace("whichScreenStarted = " + owner.whichScreenStarted);
			var langString:String = "";
			switch (e.currentTarget.name)
			{
				case "ste":
					langString = "english";
				break;
				case "sts":
					langString = "spanish";
				break;
			}
			switch (whichGame)
			{
				case 1:
					owner.businessLang = langString;
				break;
				
				case 2:
					owner.dreamLang = langString;
				break;
				
				case 3:
					owner.homeLang = langString;
				break;
				
				case 4:
					owner.lifeLang = langString;
				break;
				
				case 5:
					owner.playLang = langString;
				break;
			}
			startGame();
		}
		
		private function startGame(e:TuioTouchEvent = null):void
		{
			trace("startGame " + whichGame);
			switch (whichGame)
			{
				case 1:
					owner.addBusinessGame();
				break;
				
				case 2:
					owner.addSavingsGame();
				break;
				
				case 3:
					owner.addHomeGame();
				break;
				
				case 4:
					owner.addLifeGame();
				break;
				
				case 5:
					owner.addPlayGame();
				break;
				
				default:
					//owner.addSavingsGame();
					/*//trace("************ start Big Game");
					t.visible = false;
					st1.visible = false;
					st2.visible = false;
					st3.visible = false;
					st4.visible = false;
					st1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, startGame);
					st2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, startGame);
					st3.removeEventListener(TuioTouchEvent.TOUCH_DOWN, startGame);
					st4.removeEventListener(TuioTouchEvent.TOUCH_DOWN, startGame);
					if (dg == null)
					{
						dg = new SavingsGame(this);
						addChild(dg);
					}
					dg.lo1.jg.addEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
					dg.lo2.jg.addEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
					dg.lo3.jg.addEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
					dg.lo4.jg.addEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
					countdownTimer = new Timer(1000);
					countdownTimer.stop();
					countdownTimer.start();
					countdownTimer.addEventListener(TimerEvent.TIMER, countdown2);*/
				break;
			}
			owner.fadeGamesOut();
		}
		
		/*private function countdown2(e:TimerEvent):void
		{
			time--;
			dg.lo1.t.text = time.toString();
			dg.lo2.t.text = time.toString();
			dg.lo3.t.text = time.toString();
			dg.lo4.t.text = time.toString();
			if (time == 0)
			{
				countdownTimer.stop();
				countdownTimer.removeEventListener(TimerEvent.TIMER, countdown2);
				dg.lo1.nextFrame();
				dg.lo2.nextFrame();
				dg.lo3.nextFrame();
				dg.lo4.nextFrame();
				if (player1 == false)
				{
					dg.lo1.visible = false;
				}
				if (player2 == false)
				{
					dg.lo2.visible = false;
				}
				if (player3 == false)
				{
					dg.lo3.visible = false;
				}
				if (player4 == false)
				{
					dg.lo4.visible = false;
				}
				if (player1 == true || player2 == true || player3 == true || player4 == true)
				{
					dg.startGame();
				}
			}
		}*/
		
		
		
		public function goGames():void
		{
			//trace("whichGame = " + whichGame);
			/*if (whichGame != 4 && whichGame != 2)
			{
				if (st1 != null)
				{
					st1.addEventListener(TuioTouchEvent.TOUCH_DOWN, startGame);
					st2.addEventListener(TuioTouchEvent.TOUCH_DOWN, startGame);
					st3.addEventListener(TuioTouchEvent.TOUCH_DOWN, startGame);
					st4.addEventListener(TuioTouchEvent.TOUCH_DOWN, startGame);
				}
			}
			else
			{
				if (st1e != null)
				{*/
					big1.ste.addEventListener(TuioTouchEvent.TOUCH_DOWN, startGame1);
					big2.ste.addEventListener(TuioTouchEvent.TOUCH_DOWN, startGame2);
					big3.ste.addEventListener(TuioTouchEvent.TOUCH_DOWN, startGame3);
					big4.ste.addEventListener(TuioTouchEvent.TOUCH_DOWN, startGame4);
					if (big1.sts != null && big1.sts.visible == true)
					{
						big1.sts.addEventListener(TuioTouchEvent.TOUCH_DOWN, startGame1);
						big2.sts.addEventListener(TuioTouchEvent.TOUCH_DOWN, startGame2);
						big3.sts.addEventListener(TuioTouchEvent.TOUCH_DOWN, startGame3);
						big4.sts.addEventListener(TuioTouchEvent.TOUCH_DOWN, startGame4);
					}
				//}
			//}
		}
		
		public function stopGames():void
		{
			/*if (whichGame != 4 && whichGame != 2)
			{
				if (st1 != null)
				{
					st1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, startGame);
					st2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, startGame);
					st3.removeEventListener(TuioTouchEvent.TOUCH_DOWN, startGame);
					st4.removeEventListener(TuioTouchEvent.TOUCH_DOWN, startGame);
				}
			}
			else
			{
				if (st1e != null)
				{*/
					big1.ste.removeEventListener(TuioTouchEvent.TOUCH_DOWN, startGame1);
					big2.ste.removeEventListener(TuioTouchEvent.TOUCH_DOWN, startGame2);
					big3.ste.removeEventListener(TuioTouchEvent.TOUCH_DOWN, startGame3);
					big4.ste.removeEventListener(TuioTouchEvent.TOUCH_DOWN, startGame4);
					if (big1.sts != null)
					{
						big1.sts.removeEventListener(TuioTouchEvent.TOUCH_DOWN, startGame1);
						big2.sts.removeEventListener(TuioTouchEvent.TOUCH_DOWN, startGame2);
						big3.sts.removeEventListener(TuioTouchEvent.TOUCH_DOWN, startGame3);
						big4.sts.removeEventListener(TuioTouchEvent.TOUCH_DOWN, startGame4);
					}
				//}
			//}
		}
		
		/*private function countPlayers(e:TuioTouchEvent):void
		{
			//trace("countPlayers");
			//trace(e.currentTarget.name);
			switch (e.currentTarget.parent.name)
			{
				case "lo1":
					player1 = true;
				break;
				
				case "lo2":
					player2 = true;
				break;
				
				case "lo3":
					player3 = true;
				break;
				
				case "lo4":
					player4 = true;
				break;
				
				
				case "cr1":
					player1 = true;
				break;
				
				case "cr2":
					player2 = true;
				break;
				
				case "cr3":
					player3 = true;
				break;
				
				case "cr4":
					player4 = true;
				break;
				
				default:
					//trace("none");
				break;
			}
		}*/
		
		private function goHome():void
		{
			
		}
		
		private function goLife():void
		{
			
		}
		
		private function goPlay():void
		{
			
		}

	}
	
}
