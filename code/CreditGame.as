﻿package  code
{
	import flash.display.MovieClip;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import flash.events.Event;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	
	import org.tuio.*;
	import org.tuio.adapters.*;
	import org.tuio.gestures.*;
	import org.tuio.debug.TuioDebug;
	import org.tuio.TuioManager;
	import org.tuio.connectors.*;
	
	//import flash.media.Sound;
	//import flash.media.SoundChannel;
  	//import flash.media.SoundTransform;
	//import flash.net.URLLoader;
	//import flash.net.URLRequest;
	
	import com.treefortress.sound.SoundAS;
	import com.treefortress.sound.SoundInstance;
	import com.treefortress.sound.SoundManager;
	
	
	public class CreditGame extends MovieClip
	{
		public var noPlayer2:Boolean = false;
		public static var VO2:String = "vo2";
		public static var VO3:String = "vo3";
		public static var VO4:String = "vo4";
		public static var VO6:String = "vo6";
		public static var VO6A:String = "vo6a";
		public static var VO6B:String = "vo6b";
		public static var VO7:String = "vo7";
		public static var VO9:String = "vo9";
		public static var VO9A:String = "vo9a";
		public static var VO9B:String = "vo9b";
		public static var VO10:String = "vo10";
		public static var VO12:String = "vo12";
		public static var VO12A:String = "vo12a";
		public static var VO12B:String = "vo12b";
		public static var VO13:String = "vo13";
		public static var VO15:String = "vo15";
		public static var VO15A:String = "vo15a";
		public static var VO15B:String = "vo15b";
		public static var VO16:String = "vo16";
		public static var VO18:String = "vo18";
		public static var VO18A:String = "vo18a";
		public static var VO18B:String = "vo18b";
		public static var VO19:String = "vo19";
		public static var VO20:String = "vo20";
		public static var VO21:String = "vo21";
		public static var VO22:String = "vo22";
		public static var VO24A:String = "vo24a";
		public static var VO24B:String = "vo24b";
		public static var VO25:String = "vo25";
		public static var SING1:String = "sing1";
		public static var SING2:String = "sing2";
		public static var SING3:String = "sing3";
		public static var SING4:String = "sing4";
		public static var SING5:String = "sing5";
		
		public static var CAPI:String = "capi";
		public static var CCR1:String = "ccr1";
		public static var CCR2:String = "ccr2";
		public static var CFSR1:String = "cfsr1";
		public static var CIR1:String = "cir1";
		public static var CIR2:String = "cir2";
		public static var CIQMR2:String = "ciqmr2";
		public static var CIMSG:String = "cimsg";
		public static var CM:String = "cm";
		public static var CND:String = "cnd";
		public static var CPAR1:String = "cpar1";
		public static var CQMR1:String = "cqmr1";
		public static var CQSI:String = "cqsi";
		public static var CTRO:String = "ctro";
		public static var CW:String = "cw";
		
		public var owner:Document;
		public var gameTimer:Timer;
		private var ct:Number = 0;
		
		private var s1:String = "";
		private var s2:String = "";
		private var s3:String = "";
		private var s4:String = "";
		
		private var player1:Boolean = false;
		private var player2:Boolean = false;
		private var player3:Boolean = false;
		private var player4:Boolean = false;
		
		public var countdownTimer:Timer;
		public var time:Number = 10;
		
		
		
		public var moved:Boolean = false;
		
		public var whatLang:String;
		
		public var numPlayers:Number = 0;
		
		public var cr1:CrGm;
		public var cr2:CrGm;
		public var cr3:CrGm;
		public var cr4:CrGm;
		
		public var player1Score:Number = 0;
		public var player2Score:Number = 0;
		
		public var qTime:Number = 15;
		
		public var currentPlayer1:CrGm;
		public var currentPlayer2:CrGm;
		
		public var player1ScoreFrame:Number;
		public var player2ScoreFrame:Number;
		
		public var winnerScoreFrame:Number;
		public var loserScoreFrame:Number;
		
		public var l:Number = 0;
		
		public var sp:String = "";
		
		
		public function CreditGame(myOwner:Document, lang:String) 
		{
			owner = myOwner;
			whatLang = lang;
			
			if (whatLang == "english")
			{
				l = 0;
			}
			else
			{
				l = 1;
			}
			
			cr1 = new CrGm(this, "cr1");
			addChild(cr1);
			cr1.x = 0;
			cr2 = new CrGm(this, "cr2");
			addChild(cr2);
			cr2.x = cr1.x + 1080;
			cr3 = new CrGm(this, "cr3");
			addChild(cr3);
			cr3.x = cr2.x + 1080;
			cr4 = new CrGm(this, "cr4");
			addChild(cr4);
			cr4.x = cr3.x + 1080;
			
			//SoundAS.loadSound(sp + owner.loadXML.sing1Sound, SING1);
			
			//trace(owner.loadXML.VO2Sound);
			
			if (whatLang == "spanish")
			{
				sp = "assetsSpanish/";
			}
			SoundAS.loadSound(sp + owner.loadXML.VO2Sound, VO2);
			SoundAS.loadSound(sp + owner.loadXML.VO3Sound, VO3);
			SoundAS.loadSound(sp + owner.loadXML.VO4Sound, VO4);
			SoundAS.loadSound(sp + owner.loadXML.sing1Sound, SING1);
			
			SoundAS.loadSound(sp + owner.loadXML.creditAnswersPopIn, CAPI);
			SoundAS.loadSound(sp + owner.loadXML.creditCorrectRound1, CCR1);
			SoundAS.loadSound(sp + owner.loadXML.creditCorrectRound2, CCR2);
			SoundAS.loadSound(sp + owner.loadXML.creditFeedbackScreensRound1, CFSR1);
			SoundAS.loadSound(sp + owner.loadXML.creditIncorrectRound1, CIR1);
			SoundAS.loadSound(sp + owner.loadXML.creditIncorrectRound2, CIR2);
			SoundAS.loadSound(sp + owner.loadXML.creditIntro_and_QuestionMusicRound2, CIQMR2);
			SoundAS.loadSound(sp + owner.loadXML.creditIntroMusicStartGame, CIMSG);
			SoundAS.loadSound(sp + owner.loadXML.creditMeter, CM);
			SoundAS.loadSound(sp + owner.loadXML.creditNosedive, CND);
			SoundAS.loadSound(sp + owner.loadXML.creditPressAnswerRound1, CPAR1);
			SoundAS.loadSound(sp + owner.loadXML.creditQuestionMusicRound1, CQMR1);
			SoundAS.loadSound(sp + owner.loadXML.creditQuestionSlideIn, CQSI);
			SoundAS.loadSound(sp + owner.loadXML.creditTimeRanOut, CTRO);
			SoundAS.loadSound(sp + owner.loadXML.creditWinner, CW);
			
			var v2:SoundInstance = SoundAS.play(VO2);
			cr1.creditTitle.gotoAndStop(l + 1);
			cr2.creditTitle.gotoAndStop(l + 1);
			cr3.creditTitle.gotoAndStop(l + 1);
			cr4.creditTitle.gotoAndStop(l + 1);
		}
		
		public function startGame():void
		{
			trace("startGame");
			SoundAS.stopAll();
					trace("stop all sounds startGame");
			//SoundAS.removeAll();
			ct = -1;
			
			var v3:SoundInstance = SoundAS.play(VO3);
			v3.soundCompleted.add(VOAllDone);
			var cimsg:SoundInstance = SoundAS.play(CIMSG);
			cr1.r1Anim.anim.play();
			cr1.r1Anim.anim.an.play();
			cr2.r1Anim.anim.play();
			cr2.r1Anim.anim.an.play();
			cr3.r1Anim.anim.play();
			cr3.r1Anim.anim.an.play();
			cr4.r1Anim.anim.play();
			cr4.r1Anim.anim.an.play();
			
			cr1.r1Anim.anim.an.t.text = owner.loadXML.lifeRound[l];
			cr2.r1Anim.anim.an.t.text = owner.loadXML.lifeRound[l];
			cr3.r1Anim.anim.an.t.text = owner.loadXML.lifeRound[l];
			cr4.r1Anim.anim.an.t.text = owner.loadXML.lifeRound[l];
			
			/*exitBtn = new CloseBlue();
			addChild(exitBtn);
			setChildIndex(exitBtn, numChildren - 1);
			exitBtn.x = 1080 - exitBtn.width;
			exitBtn.addEventListener(TuioTouchEvent.TOUCH_DOWN, exitGame);*/
			cr1.addExitTop();
			cr2.addExitTop();
			cr3.addExitTop();
			cr4.addExitTop();
		}
		
		public function playClick():void
		{
			var cpar1:SoundInstance = SoundAS.play(CPAR1);
		}
		
		public function startCountdown():void
		{
			cr1.gotoAndStop(1);
			cr2.gotoAndStop(1);
			cr3.gotoAndStop(1);
			cr4.gotoAndStop(1);
			cr1.jg.addEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
			cr2.jg.addEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
			cr3.jg.addEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
			cr4.jg.addEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
			cr1.jg.t.text = owner.loadXML.lifeJoinGame[l];
			cr2.jg.t.text = owner.loadXML.lifeJoinGame[l];
			cr3.jg.t.text = owner.loadXML.lifeJoinGame[l];
			cr4.jg.t.text = owner.loadXML.lifeJoinGame[l];
			cr1.jg.wel.text = owner.loadXML.lifeWelcome[l];
			cr2.jg.wel.text = owner.loadXML.lifeWelcome[l];
			cr3.jg.wel.text = owner.loadXML.lifeWelcome[l];
			cr4.jg.wel.text = owner.loadXML.lifeWelcome[l];
			cr1.jg.t.mouseChildren = false;
			cr1.jg.t.mouseEnabled = false;
			cr2.jg.t.mouseChildren = false;
			cr2.jg.t.mouseEnabled = false;
			cr3.jg.t.mouseChildren = false;
			cr3.jg.t.mouseEnabled = false;
			cr4.jg.t.mouseChildren = false;
			cr4.jg.t.mouseEnabled = false;
			countdownTimer = new Timer(1000);
			countdownTimer.stop();
			countdownTimer.start();
			countdownTimer.addEventListener(TimerEvent.TIMER, countdown4);
		}
		
		private function countPlayers(e:TuioTouchEvent):void
		{
			trace("countPlayers");
			//trace(e.currentTarget.parent.whichOne);
			//trace("countPlayers player1 = " + player1);
			switch (e.currentTarget.parent.whichOne)
			{
				case "cr1":
					//trace("cr1");
					cr1.jg.removeEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
					player1 = true;
					numPlayers++;
					if (numPlayers == 1)
					{
						currentPlayer1 = cr1;
						cr1.jg.gotoAndStop(2);
					}
					if (numPlayers == 2)
					{
						currentPlayer2 = cr1;
					}
					cr1.whichPlayer = numPlayers;
					//trace("case cr1 player1 = " + player1);
				break;
				
				case "cr2":
					cr2.jg.removeEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
					player2 = true;
					numPlayers++;
					if (numPlayers == 1)
					{
						currentPlayer1 = cr2;
						cr2.jg.gotoAndStop(2);
					}
					if (numPlayers == 2)
					{
						currentPlayer2 = cr2;
					}
					cr2.whichPlayer = numPlayers;
				break;
				
				case "cr3":
					cr3.jg.removeEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
					player3 = true;
					numPlayers++;
					if (numPlayers == 1)
					{
						currentPlayer1 = cr3;
						cr3.jg.gotoAndStop(2);
					}
					if (numPlayers == 2)
					{
						currentPlayer2 = cr3;
					}
					cr3.whichPlayer = numPlayers;
				break;
				
				case "cr4":
					cr4.jg.removeEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
					player4 = true;
					numPlayers++;
					if (numPlayers == 1)
					{
						currentPlayer1 = cr4;
						cr4.jg.gotoAndStop(2);
					}
					if (numPlayers == 2)
					{
						currentPlayer2 = cr4;
					}
					cr4.whichPlayer = numPlayers;
				break;
				
				default:
					//trace("none");
				break;
			}
			if (numPlayers == 2)
			{
				//trace("numPlayers = 2 player1 = " + player1);
				countdownTimer.stop();
				countdownTimer.removeEventListener(TimerEvent.TIMER, countdown4);
				cr1.nextFrame();
				cr2.nextFrame();
				cr3.nextFrame();
				cr4.nextFrame();
				if (player1 == false)
				{
					cr1.visible = false;
				}
				if (player2 == false)
				{
					cr2.visible = false;
				}
				if (player3 == false)
				{
					cr3.visible = false;
				}
				if (player4 == false)
				{
					cr4.visible = false;
				}
				if (player1 == true || player2 == true || player3 == true || player4 == true)
				{
					trace("countPlayers try to start game");
					startGame();
				}
				else
				{
					trace("countPlayers no one joined");
					SoundAS.stopAll();
					trace("stop all sounds countPlayers");
					SoundAS.removeAll();
					SoundAS.stopAll();
					owner.removeThis(this);
				}
			}
		}
		
		private function countdown4(e:TimerEvent = null):void
		{
			time--;
			cr1.t.text = time.toString();
			cr2.t.text = time.toString();
			cr3.t.text = time.toString();
			cr4.t.text = time.toString();
			//trace("countdown4 player1 = " + player1);
			if (time == 0 || numPlayers == 2)
			{
				//trace("countdown over");
				//trace("countdownover player1 = " + player1);
				countdownTimer.stop();
				countdownTimer.removeEventListener(TimerEvent.TIMER, countdown4);
				cr1.nextFrame();
				cr2.nextFrame();
				cr3.nextFrame();
				cr4.nextFrame();
				if (player1 == false)
				{
					cr1.visible = false;
				}
				if (player2 == false)
				{
					cr2.visible = false;
				}
				if (player3 == false)
				{
					cr3.visible = false;
				}
				if (player4 == false)
				{
					cr4.visible = false;
				}
				
				if (numPlayers == 1)
				{
					noPlayer2 = true;
					if (player1 == false)
					{
						cr1.placeHolder = true;
						
						cr1.didNotAnswer = false;
						
						currentPlayer2 = cr1;
						
						player1 = true;
					}
					else if (player2 == false)
					{
						cr2.placeHolder = true;
						
						cr2.didNotAnswer = false;
						currentPlayer2 = cr2;
						player2 = true;
					}
					else if (player3 == false)
					{
						cr3.placeHolder = true;
						
						cr3.didNotAnswer = false;
						currentPlayer2 = cr3;
						player3 = true;
					}
					else if (player4 == false)
					{
						cr4.placeHolder = true;
						
						cr4.didNotAnswer = false;
						currentPlayer2 = cr4;
						player4 = true;
					}
					/*cr1.hideMeters(false);
					cr2.hideMeters(false);
					cr3.hideMeters(false);
					cr4.hideMeters(false);*/
				}
				trace("player1 = " + player1);
				trace("player2 = " + player2);
				trace("player3 = " + player3);
				trace("player4 = " + player4);
				if (player1 == true || player2 == true || player3 == true || player4 == true)
				{
					trace("countdown4 try to start game");
					startGame();
				}
				else
				{
					trace("countdown4 no one joined");
					SoundAS.stopAll();
					trace("stop all sounds countdown4");
					SoundAS.removeAll();
					SoundAS.stopAll();
					owner.removeThis(this);
				}
				
			}
		}
		
		public function stopVO():void
		{
			//VOChannel.stop();
		}
		
		public function bothHaveAnswered(w:Number = 0):void
		{
			//trace("bothHaveAnswered ct = " + ct);
			if (currentPlayer2.placeHolder == true)
			{
				currentPlayer2.didNotAnswer = false;
			}
			if (currentPlayer1.didNotAnswer == false && currentPlayer2.didNotAnswer == false)
			{
				
				if (currentPlayer2.placeHolder == true)
				{
					currentPlayer2.didNotAnswer = true;
				}
				if (gameTimer != null)
				{
					gameTimer.removeEventListener(TimerEvent.TIMER, countdown);
					gameTimer.removeEventListener(TimerEvent.TIMER, questTimer);
					gameTimer.stop();
				}
				SoundAS.stopAll();
					trace("stop all sounds bothHaveAnswered");
					playResultNoise(w);
				//VOAllDone();
				
				qTime = 15;
				
				
				cr1.removeQuestListeners();
				cr2.removeQuestListeners();
				cr3.removeQuestListeners();
				cr4.removeQuestListeners();
				
				switch (ct)
				{
					case 3:
						trace("bothHaveAnswered case 4");
						//remove q listeners
						if (cr1.didNotAnswer == true)
						{
							cr1.setScore(cr1.currentFrame);
						}
						if (cr2.didNotAnswer == true)
						{
							cr2.setScore(cr2.currentFrame);
						}
						if (cr3.didNotAnswer == true)
						{
							cr3.setScore(cr3.currentFrame);
						}
						if (cr4.didNotAnswer == true)
						{
							cr4.setScore(cr4.currentFrame);
						}
						trace("cr1.didNotAnswer = " + cr1.didNotAnswer);
						trace("cr2.didNotAnswer = " + cr2.didNotAnswer);
						trace("cr3.didNotAnswer = " + cr3.didNotAnswer);
						trace("cr4.didNotAnswer = " + cr4.didNotAnswer);
						if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
						{
							ct--;
							trace("bothHaveAnswered no answer");
							var v6a:SoundInstance = SoundAS.play(VO6A);
							v6a.soundCompleted.add(VOAllDone);
						}
						else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
						{
							ct--;
							trace("bothHaveAnswered wrong answer");
							var v6b:SoundInstance = SoundAS.play(VO6B);
							v6b.soundCompleted.add(VOAllDone);
						}
						else
						{
							trace("bothHaveAnswered right answer");
							//ct++;
							
							cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr1.hideQ();
							cr2.hideQ();
							cr3.hideQ();
							cr4.hideQ();
							cr1.nextFrame();
							cr2.nextFrame();
							cr3.nextFrame();
							cr4.nextFrame();
							
					
							cr1.setQText(6);
							cr2.setQText(6);
							cr3.setQText(6);
							cr4.setQText(6);
							var v7:SoundInstance = SoundAS.play(VO7);
							v7.soundCompleted.add(VOAllDone);
						}
					break;
					
					case 6:
						trace("questTimer case 8");
						
						if (cr1.didNotAnswer == true)
						{
							cr1.setScore(cr1.currentFrame);
						}
						if (cr2.didNotAnswer == true)
						{
							cr2.setScore(cr2.currentFrame);
						}
						if (cr3.didNotAnswer == true)
						{
							cr3.setScore(cr3.currentFrame);
						}
						if (cr4.didNotAnswer == true)
						{
							cr4.setScore(cr4.currentFrame);
						}
						if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
						{
							trace("onVODone no one answered");
							ct--;
							var v9a:SoundInstance = SoundAS.play(VO9A);
							v9a.soundCompleted.add(VOAllDone);
						}
						else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
						{
							trace("onVODone no one got it right");
							ct--;
							var v9b:SoundInstance = SoundAS.play(VO9B);
							v9b.soundCompleted.add(VOAllDone);
						}
						else
						{
							cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr1.hideQ();
							cr2.hideQ();
							cr3.hideQ();
							cr4.hideQ();
							cr1.nextFrame();
							cr2.nextFrame();
							cr3.nextFrame();
							cr4.nextFrame();
							
					
							cr1.setQText(7);
							cr2.setQText(7);
							cr3.setQText(7);
							cr4.setQText(7);
							var v10:SoundInstance = SoundAS.play(VO10);
							v10.soundCompleted.add(VOAllDone);
						}
					break;
					case 9:
						//trace("questTimer case 12");
					
						if (cr1.didNotAnswer == true)
						{
							cr1.setScore(cr1.currentFrame);
						}
						if (cr2.didNotAnswer == true)
						{
							cr2.setScore(cr2.currentFrame);
						}
						if (cr3.didNotAnswer == true)
						{
							cr3.setScore(cr3.currentFrame);
						}
						if (cr4.didNotAnswer == true)
						{
							cr4.setScore(cr4.currentFrame);
						}
						if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
						{
							trace("onVODone no one answered");
							ct--;
							var v12a:SoundInstance = SoundAS.play(VO12A);
							v12a.soundCompleted.add(VOAllDone);
						}
						else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
						{
							trace("onVODone no one got it right");
							ct--;
							var v12b:SoundInstance = SoundAS.play(VO12B);
							v12b.soundCompleted.add(VOAllDone);
						}
						else
						{
							cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr1.hideQ();
							cr2.hideQ();
							cr3.hideQ();
							cr4.hideQ();
							cr1.nextFrame();
							cr2.nextFrame();
							cr3.nextFrame();
							cr4.nextFrame();
					
							cr1.setQText(8);
							cr2.setQText(8);
							cr3.setQText(8);
							cr4.setQText(8);
							var v13:SoundInstance = SoundAS.play(VO13);
							v13.soundCompleted.add(VOAllDone);
						}
					break;
					case 12:
						//trace("questTimer case 16");
					
						if (cr1.didNotAnswer == true)
						{
							cr1.setScore(cr1.currentFrame);
						}
						if (cr2.didNotAnswer == true)
						{
							cr2.setScore(cr2.currentFrame);
						}
						if (cr3.didNotAnswer == true)
						{
							cr3.setScore(cr3.currentFrame);
						}
						if (cr4.didNotAnswer == true)
						{
							cr4.setScore(cr4.currentFrame);
						}
						if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
						{
							trace("onVODone no one answered");
							ct--;
							var v15a:SoundInstance = SoundAS.play(VO15A);
							v15a.soundCompleted.add(VOAllDone);
						}
						else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
						{
							trace("onVODone no one got it right");
							ct--;
							var v15b:SoundInstance = SoundAS.play(VO15B);
							v15b.soundCompleted.add(VOAllDone);
						}
						else
						{
							cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr1.hideQ();
							cr2.hideQ();
							cr3.hideQ();
							cr4.hideQ();
							cr1.nextFrame();
							cr2.nextFrame();
							cr3.nextFrame();
							cr4.nextFrame();
					
							cr1.setQText(9);
							cr2.setQText(9);
							cr3.setQText(9);
							cr4.setQText(9);
							var v16:SoundInstance = SoundAS.play(VO16);
							v16.soundCompleted.add(VOAllDone);
						}
					break;
					case 15:
						//trace("questTimer case 20");
					
						if (cr1.didNotAnswer == true)
						{
							cr1.setScore(cr1.currentFrame);
						}
						if (cr2.didNotAnswer == true)
						{
							cr2.setScore(cr2.currentFrame);
						}
						if (cr3.didNotAnswer == true)
						{
							cr3.setScore(cr3.currentFrame);
						}
						if (cr4.didNotAnswer == true)
						{
							cr4.setScore(cr4.currentFrame);
						}
						if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
						{
							ct--;
							var v18a:SoundInstance = SoundAS.play(VO18A);
							v18a.soundCompleted.add(VOAllDone);
						}
						else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
						{
							ct--;
							var v18b:SoundInstance = SoundAS.play(VO18B);
							v18b.soundCompleted.add(VOAllDone);
						}
						else
						{
							cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr1.hideQ();
							cr2.hideQ();
							cr3.hideQ();
							cr4.hideQ();
							cr1.nextFrame();
							cr2.nextFrame();
							cr3.nextFrame();
							cr4.nextFrame();
					
							cr1.setQText(10);
							cr2.setQText(10);
							cr3.setQText(10);
							cr4.setQText(10);
							var v19:SoundInstance = SoundAS.play(VO19);
							v19.soundCompleted.add(VOAllDone);
						}
					break;
				}
			}
			
				
			if (currentPlayer2.placeHolder == true)
			{
				currentPlayer2.didNotAnswer = true;
			}
		}
		
		public function VOAllDone(si:SoundInstance = null):void
		{
			//trace("OMG THIS ACTUALLY WORKED!");
			ct++;
			//trace("countdown ct = " + ct);
			
			switch (ct)
			{
				case 0: //example
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					cr1.creditQ1Ex.gotoAndStop(l + 1);
					cr2.creditQ1Ex.gotoAndStop(l + 1);
					cr3.creditQ1Ex.gotoAndStop(l + 1);
					cr4.creditQ1Ex.gotoAndStop(l + 1);
					cr1.creditQ1Ex.anim.play();
					cr2.creditQ1Ex.anim.play();
					cr3.creditQ1Ex.anim.play();
					cr4.creditQ1Ex.anim.play();
					//VOChannel = VO4Sound.play();
					var v4:SoundInstance = SoundAS.play(VO4);
					v4.soundCompleted.add(VOAllDone);
					//v2.destroy();
					//v3.destroy();
					SoundAS.removeSound(VO2);
					SoundAS.removeSound(VO3);
					SoundAS.loadSound(sp + owner.loadXML.VO6Sound, VO6);
					SoundAS.loadSound(sp + owner.loadXML.VO6aSound, VO6A);
					SoundAS.loadSound(sp + owner.loadXML.VO6bSound, VO6B);
					//VOAllDone(v4);
					//trace(ct + " frame = " + cr1.currentFrame);
					
					//timerGo(2);
					
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
				break;
				case 1:
					//trace("onVODone ************************* case 1");
					//trace("sing 1");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					//VOChannel = sing1Sound.play();
					var s1:SoundInstance = SoundAS.play(SING1);
					s1.soundCompleted.add(VOAllDone);
					cr1.q1Anim.gotoAndStop(l + 1);
					cr2.q1Anim.gotoAndStop(l + 1);
					cr3.q1Anim.gotoAndStop(l + 1);
					cr4.q1Anim.gotoAndStop(l + 1);
					cr1.q1Anim.anim.play();
					cr2.q1Anim.anim.play();
					cr3.q1Anim.anim.play();
					cr4.q1Anim.anim.play();
					//s1.soundCompleted.removeAll();
					//v4.destroy();
					SoundAS.removeSound(VO4);
					SoundAS.loadSound(sp + owner.loadXML.VO7Sound, VO7);
					SoundAS.loadSound(sp + owner.loadXML.sing2Sound, SING2);
					//VOAllDone(s1);
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					
					//timerGo(ct); // ct = 1
					//trace("case 1 ct = " + ct);
					//trace("singing 1");
				break;
				case 2:
					//trace("VOAllDone ************************* case 2");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					
					cr1.setQText(1);
					cr2.setQText(1);
					cr3.setQText(1);
					cr4.setQText(1);
					
					cr1.initMeters();
					cr2.initMeters();
					cr3.initMeters();
					cr4.initMeters();
					cr1.showMeters();
					cr2.showMeters();
					cr3.showMeters();
					cr4.showMeters();
					if (numPlayers < 2)
					{
						cr1.hideMeters(false);
						cr2.hideMeters(false);
						cr3.hideMeters(false);
						cr4.hideMeters(false);
					}
					//VOChannel.removeEventListener(Event.SOUND_COMPLETE, onVODone);
					//VOChannel.stop();
					//VOChannel = null;
					//VOChannel = new SoundChannel();
					//VOChannel = VO6Sound.play();
					var v6:SoundInstance = SoundAS.play(VO6);
					//v6.soundCompleted.add(VOAllDone);
					SoundAS.removeSound(SING1);
					SoundAS.loadSound(sp + owner.loadXML.VO9Sound, VO9);
					SoundAS.loadSound(sp + owner.loadXML.VO9aSound, VO9A);
					SoundAS.loadSound(sp + owner.loadXML.VO9bSound, VO9B);
			
					//VOAllDone(v6);
					
					cr1.resetQuestions();
					cr2.resetQuestions();
					cr3.resetQuestions();
					cr4.resetQuestions();
					
					cr1.addQuestListeners();
					cr2.addQuestListeners();
					cr3.addQuestListeners();
					cr4.addQuestListeners();
					
					//trace(ct + " frame = " + cr1.currentFrame);
					cr1.showQ();
					cr2.showQ();
					cr3.showQ();
					cr4.showQ();
					ct++;
					timerGo(ct); // ct = 4
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
				break;
				
				//case 3:
					//trace("onVODone ************************* case 3");
					
				//break;
				
				
				/*case 3:
					if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
					{
						VOChannel = VO6aSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
					{
						VOChannel = VO6bSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else
					{
						ct++;
						cr1.setScore(cr1.currentFrame);
						cr2.setScore(cr2.currentFrame);
						cr3.setScore(cr3.currentFrame);
						cr4.setScore(cr4.currentFrame);
						cr1.hideQ();
						cr2.hideQ();
						cr3.hideQ();
						cr4.hideQ();
						cr1.nextFrame();
						cr2.nextFrame();
						cr3.nextFrame();
						cr4.nextFrame();
						VOChannel = VO7Sound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
				break;*/
				
				case 3:
					//trace("onVODone ************************* case 4");
					/*cr1.setScore(cr1.currentFrame); // 5
					cr2.setScore(cr2.currentFrame);
					cr3.setScore(cr3.currentFrame);
					cr4.setScore(cr4.currentFrame);*/
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr1.hideQ();
					cr2.hideQ();
					cr3.hideQ();
					cr4.hideQ();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					
					cr1.setQText(6);
					cr2.setQText(6);
					cr3.setQText(6);
					cr4.setQText(6);
					//trace("case 4 cr1.currentFrame = " + cr1.currentFrame);
					//VOChannel = VO7Sound.play();
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					//SFXChannel.stop();
					//SFXChannel = null;
					//SFXChannel = new SoundChannel();
					//SFXChannel = creditFeedbackScreensRound1.play();
					var v7:SoundInstance = SoundAS.play(VO7);
					v7.soundCompleted.add(VOAllDone);
					SoundAS.removeSound(VO6);
			
					var cfsr1:SoundInstance = SoundAS.play(CFSR1);
				break;
				case 4:
					//SFXChannel.stop();
					//trace("onVODone ************************* case 5");
					cr1.hideQ();
					cr2.hideQ();
					cr3.hideQ();
					cr4.hideQ();
					cr1.hideMeters();
					cr2.hideMeters();
					cr3.hideMeters();
					cr4.hideMeters();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					//VOChannel = sing2Sound.play();
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					var s2:SoundInstance = SoundAS.play(SING2);
					s2.soundCompleted.add(VOAllDone);
					cr1.q2Anim.gotoAndStop(l + 1);
					cr2.q2Anim.gotoAndStop(l + 1);
					cr3.q2Anim.gotoAndStop(l + 1);
					cr4.q2Anim.gotoAndStop(l + 1);
					cr1.q2Anim.anim.play();
					cr2.q2Anim.anim.play();
					cr3.q2Anim.anim.play();
					cr4.q2Anim.anim.play();
					SoundAS.removeSound(VO7);
					SoundAS.loadSound(sp + owner.loadXML.VO10Sound, VO10);
			
				break;
				
				case 5: //question 2
					//trace("onVODone ************************* case 6");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					
					cr1.setQText(2);
					cr2.setQText(2);
					cr3.setQText(2);
					cr4.setQText(2);
					cr1.showMeters();
					cr2.showMeters();
					cr3.showMeters();
					cr4.showMeters();
					if (numPlayers < 2)
					{
						cr1.hideMeters(false);
						cr2.hideMeters(false);
						cr3.hideMeters(false);
						cr4.hideMeters(false);
					}
					
					//VOChannel = VO9Sound.play();
					var v9:SoundInstance = SoundAS.play(VO9);
					//v9.soundCompleted.add(VOAllDone);
					SoundAS.removeSound(SING2);
					
					SoundAS.loadSound(sp + owner.loadXML.sing3Sound, SING3);
					SoundAS.loadSound(sp + owner.loadXML.VO12Sound, VO12);
					SoundAS.loadSound(sp + owner.loadXML.VO12aSound, VO12A);
					SoundAS.loadSound(sp + owner.loadXML.VO12bSound, VO12B);
			
					
					cr1.resetQuestions();
					cr2.resetQuestions();
					cr3.resetQuestions();
					cr4.resetQuestions();
					
					
					cr1.addQuestListeners();
					cr2.addQuestListeners();
					cr3.addQuestListeners();
					cr4.addQuestListeners();
					
					//trace(ct + " frame = " + cr1.currentFrame);
					cr1.showQ();
					cr2.showQ();
					cr3.showQ();
					cr4.showQ();
					
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					ct++;
					timerGo(ct); // ct = 8
				break;
				/*case 7:
					trace("************************* case 7");
					if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
					{
						VOChannel = VO9aSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
					{
						VOChannel = VO9bSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else
					{
						ct++;
						cr1.setScore(cr1.currentFrame);
						cr2.setScore(cr2.currentFrame);
						cr3.setScore(cr3.currentFrame);
						cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
						cr1.hideQ();
						cr2.hideQ();
						cr3.hideQ();
						cr4.hideQ();
						cr1.nextFrame();
						cr2.nextFrame();
						cr3.nextFrame();
						cr4.nextFrame();
						VOChannel = VO10Sound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
				break;*/
				
				
				//case 7:
					//trace("onVODone ************************* case 7");
					//ct++;
					//timerGo(ct); // ct = 8
				//break;
				case 6:
					//trace("onVODone ************************* case 8");
					/*cr1.setScore(cr1.currentFrame);
					cr2.setScore(cr2.currentFrame);
					cr3.setScore(cr3.currentFrame);
					cr4.setScore(cr4.currentFrame);*/
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr1.hideQ();
					cr2.hideQ();
					cr3.hideQ();
					cr4.hideQ();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					
					cr1.setQText(7);
					cr2.setQText(7);
					cr3.setQText(7);
					cr4.setQText(7);
					//VOChannel = VO10Sound.play();
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					//SFXChannel.stop();
					//SFXChannel = null;
					//SFXChannel = new SoundChannel();
					//SFXChannel = creditFeedbackScreensRound1.play();
					var v10:SoundInstance = SoundAS.play(VO10);
					v10.soundCompleted.add(VOAllDone);
					SoundAS.removeSound(VO9);
					SoundAS.removeSound(VO9A);
					SoundAS.removeSound(VO9B);
			
					var cfsr1a:SoundInstance = SoundAS.play(CFSR1);
				break;
				case 7:
					//trace("onVODone ************************* case 9");
					//SFXChannel.stop();
					cr1.hideMeters();
					cr2.hideMeters();
					cr3.hideMeters();
					cr4.hideMeters();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					//VOChannel = sing3Sound.play();
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					var s3:SoundInstance = SoundAS.play(SING3);
					s3.soundCompleted.add(VOAllDone);
					cr1.q3Anim.gotoAndStop(l + 1);
					cr2.q3Anim.gotoAndStop(l + 1);
					cr3.q3Anim.gotoAndStop(l + 1);
					cr4.q3Anim.gotoAndStop(l + 1);
					cr1.q3Anim.anim.play();
					cr2.q3Anim.anim.play();
					cr3.q3Anim.anim.play();
					cr4.q3Anim.anim.play();
					SoundAS.removeSound(VO10);
					SoundAS.loadSound(sp + owner.loadXML.VO13Sound, VO13);
			
				break;
				
				case 8: //question 3
					//trace("onVODone ************************* case 10");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					
					cr1.setQText(3);
					cr2.setQText(3);
					cr3.setQText(3);
					cr4.setQText(3);
					cr1.showMeters();
					cr2.showMeters();
					cr3.showMeters();
					cr4.showMeters();
					if (numPlayers < 2)
					{
						cr1.hideMeters(false);
						cr2.hideMeters(false);
						cr3.hideMeters(false);
						cr4.hideMeters(false);
					}
					
					//VOChannel = VO12Sound.play();
					var v12:SoundInstance = SoundAS.play(VO12);
					//v12.soundCompleted.add(VOAllDone);
					SoundAS.removeSound(SING3);
					SoundAS.loadSound(sp + owner.loadXML.sing4Sound, SING4);
					SoundAS.loadSound(sp + owner.loadXML.VO15Sound, VO15);
					SoundAS.loadSound(sp + owner.loadXML.VO15aSound, VO15A);
					SoundAS.loadSound(sp + owner.loadXML.VO15bSound, VO15B);
			
					
					cr1.resetQuestions();
					cr2.resetQuestions();
					cr3.resetQuestions();
					cr4.resetQuestions();
					
					
					
					cr1.addQuestListeners();
					cr2.addQuestListeners();
					cr3.addQuestListeners();
					cr4.addQuestListeners();
					
					//trace(ct + " frame = " + cr1.currentFrame);
					cr1.showQ();
					cr2.showQ();
					cr3.showQ();
					cr4.showQ();
					
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					ct++;
					timerGo(ct); // ct = 12
				break;
				/*case 11:
					if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
					{
						VOChannel = VO12aSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
					{
						VOChannel = VO12bSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else
					{
						ct++;
						cr1.setScore(cr1.currentFrame);
						cr2.setScore(cr2.currentFrame);
						cr3.setScore(cr3.currentFrame);
						cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
						cr1.hideQ();
						cr2.hideQ();
						cr3.hideQ();
						cr4.hideQ();
						cr1.nextFrame();
						cr2.nextFrame();
						cr3.nextFrame();
						cr4.nextFrame();
						VOChannel = VO13Sound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
				break;*/
				
				
				//case 11:
					//trace("onVODone ************************* case 11");
					//ct++;
					//timerGo(ct); // ct = 12
				//break;
				case 9:
					//trace("onVODone ************************* case 12");
					/*cr1.setScore(cr1.currentFrame);
					cr2.setScore(cr2.currentFrame);
					cr3.setScore(cr3.currentFrame);
					cr4.setScore(cr4.currentFrame);*/
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr1.hideQ();
					cr2.hideQ();
					cr3.hideQ();
					cr4.hideQ();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					
					cr1.setQText(8);
					cr2.setQText(8);
					cr3.setQText(8);
					cr4.setQText(8);
					//VOChannel = VO13Sound.play();
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					//SFXChannel.stop();
					//SFXChannel = null;
					//SFXChannel = new SoundChannel();
					//SFXChannel = creditFeedbackScreensRound1.play();
					var v13:SoundInstance = SoundAS.play(VO13);
					v13.soundCompleted.add(VOAllDone);
					SoundAS.removeSound(VO12);
					SoundAS.removeSound(VO12A);
					SoundAS.removeSound(VO12B);
			
					var cfsr1b:SoundInstance = SoundAS.play(CFSR1);
				break;
				
				case 10:
					//trace("onVODone ************************* case 13");
					//SFXChannel.stop();
					cr1.hideMeters();
					cr2.hideMeters();
					cr3.hideMeters();
					cr4.hideMeters();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					//VOChannel = sing4Sound.play();
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					var s4:SoundInstance = SoundAS.play(SING4);
					s4.soundCompleted.add(VOAllDone);
					cr1.q4Anim.gotoAndStop(l + 1);
					cr2.q4Anim.gotoAndStop(l + 1);
					cr3.q4Anim.gotoAndStop(l + 1);
					cr4.q4Anim.gotoAndStop(l + 1);
					cr1.q4Anim.anim.play();
					cr2.q4Anim.anim.play();
					cr3.q4Anim.anim.play();
					cr4.q4Anim.anim.play();
					SoundAS.removeSound(VO13);
					SoundAS.loadSound(sp + owner.loadXML.VO16Sound, VO16);
			
				break;
				
				case 11: //question 4
					//trace("onVODone ************************* case 14");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					
					cr1.setQText(4);
					cr2.setQText(4);
					cr3.setQText(4);
					cr4.setQText(4);
					cr1.showMeters();
					cr2.showMeters();
					cr3.showMeters();
					cr4.showMeters();
					if (numPlayers < 2)
					{
						cr1.hideMeters(false);
						cr2.hideMeters(false);
						cr3.hideMeters(false);
						cr4.hideMeters(false);
					}
					
					//VOChannel = VO15Sound.play();
					var v15:SoundInstance = SoundAS.play(VO15);
					//v15.soundCompleted.add(VOAllDone);
					SoundAS.removeSound(SING4);
					SoundAS.loadSound(sp + owner.loadXML.sing5Sound, SING5);
					SoundAS.loadSound(sp + owner.loadXML.VO18Sound, VO18);
					SoundAS.loadSound(sp + owner.loadXML.VO18aSound, VO18A);
					SoundAS.loadSound(sp + owner.loadXML.VO18bSound, VO18B);
			
					cr1.resetQuestions();
					cr2.resetQuestions();
					cr3.resetQuestions();
					cr4.resetQuestions();
					
					cr1.addQuestListeners();
					cr2.addQuestListeners();
					cr3.addQuestListeners();
					cr4.addQuestListeners();
					
					//trace(ct + " frame = " + cr1.currentFrame);
					cr1.showQ();
					cr2.showQ();
					cr3.showQ();
					cr4.showQ();
					
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					ct++;
					timerGo(ct); // ct = 16
				break;
				/*case 15:
					if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
					{
						VOChannel = VO15aSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
					{
						VOChannel = VO15bSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else
					{
						ct++;
						cr1.setScore(cr1.currentFrame);
						cr2.setScore(cr2.currentFrame);
						cr3.setScore(cr3.currentFrame);
						cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
						cr1.hideQ();
						cr2.hideQ();
						cr3.hideQ();
						cr4.hideQ();
						cr1.nextFrame();
						cr2.nextFrame();
						cr3.nextFrame();
						cr4.nextFrame();
						VOChannel = VO16Sound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
				break;*/
				
				
				//case 15:
					//trace("onVODone ************************* case 15");
					//ct++;
					//timerGo(ct); // ct = 16
				//break;
				case 12:
					//trace("onVODone ************************* case 16");
					/*cr1.setScore(cr1.currentFrame);
					cr2.setScore(cr2.currentFrame);
					cr3.setScore(cr3.currentFrame);
					cr4.setScore(cr4.currentFrame);*/
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr1.hideQ();
					cr2.hideQ();
					cr3.hideQ();
					cr4.hideQ();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					
					cr1.setQText(9);
					cr2.setQText(9);
					cr3.setQText(9);
					cr4.setQText(9);
					//VOChannel = VO16Sound.play();
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					//SFXChannel.stop();
					//SFXChannel = null;
					//SFXChannel = new SoundChannel();
					//SFXChannel = creditFeedbackScreensRound1.play();
					var v16:SoundInstance = SoundAS.play(VO16);
					v16.soundCompleted.add(VOAllDone);
					SoundAS.removeSound(VO15);
					SoundAS.removeSound(VO15A);
					SoundAS.removeSound(VO15B);
			
					var cfsr1c:SoundInstance = SoundAS.play(CFSR1);
				break;
				
				case 13:
					//trace("onVODone ************************* case 17");
					//SFXChannel.stop();
					cr1.hideMeters();
					cr2.hideMeters();
					cr3.hideMeters();
					cr4.hideMeters();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					//VOChannel = sing5Sound.play();
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					var s5:SoundInstance = SoundAS.play(SING5);
					s5.soundCompleted.add(VOAllDone);
					cr1.q5Anim.gotoAndStop(l + 1);
					cr2.q5Anim.gotoAndStop(l + 1);
					cr3.q5Anim.gotoAndStop(l + 1);
					cr4.q5Anim.gotoAndStop(l + 1);
					cr1.q5Anim.anim.play();
					cr2.q5Anim.anim.play();
					cr3.q5Anim.anim.play();
					cr4.q5Anim.anim.play();
					SoundAS.removeSound(VO16);
					SoundAS.loadSound(sp + owner.loadXML.VO19Sound, VO19);
			
				break;
				
				case 14: //question 5
					//trace("onVODone ************************* case 18");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					
					cr1.setQText(5);
					cr2.setQText(5);
					cr3.setQText(5);
					cr4.setQText(5);
					cr1.showMeters();
					cr2.showMeters();
					cr3.showMeters();
					cr4.showMeters();
					if (numPlayers < 2)
					{
						cr1.hideMeters(false);
						cr2.hideMeters(false);
						cr3.hideMeters(false);
						cr4.hideMeters(false);
					}
					
					//VOChannel = VO18Sound.play();
					var v18:SoundInstance = SoundAS.play(VO18);
					//v18.soundCompleted.add(VOAllDone);
					SoundAS.removeSound(SING5);
					SoundAS.loadSound(sp + owner.loadXML.VO20Sound, VO20);
					SoundAS.loadSound(sp + owner.loadXML.VO21Sound, VO21);
			
					cr1.resetQuestions();
					cr2.resetQuestions();
					cr3.resetQuestions();
					cr4.resetQuestions();
					
					cr1.addQuestListeners();
					cr2.addQuestListeners();
					cr3.addQuestListeners();
					cr4.addQuestListeners();
					
					//trace(ct + " frame = " + cr1.currentFrame);
					cr1.showQ();
					cr2.showQ();
					cr3.showQ();
					cr4.showQ();
					
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					ct++;
					timerGo(ct); // ct = 20
				break;
				/*case 16:
					if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
					{
						VOChannel = VO18aSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
					{
						VOChannel = VO18bSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else
					{
						ct++;
						cr1.setScore(cr1.currentFrame);
						cr2.setScore(cr2.currentFrame);
						cr3.setScore(cr3.currentFrame);
						cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
						cr1.hideQ();
						cr2.hideQ();
						cr3.hideQ();
						cr4.hideQ();
						cr1.nextFrame();
						cr2.nextFrame();
						cr3.nextFrame();
						cr4.nextFrame();
						VOChannel = VO19Sound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
				break;*/
				
				
				//case 19:
					//trace("onVODone ************************* case 19");
					//ct++;
					//timerGo(ct); // ct = 20
				//break;
				case 15:
					//trace("onVODone ************************* case 20");
					/*cr1.setScore(cr1.currentFrame);
					cr2.setScore(cr2.currentFrame);
					cr3.setScore(cr3.currentFrame);
					cr4.setScore(cr4.currentFrame);*/
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr1.hideQ();
					cr2.hideQ();
					cr3.hideQ();
					cr4.hideQ();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					
					cr1.setQText(10);
					cr2.setQText(10);
					cr3.setQText(10);
					cr4.setQText(10);
					//VOChannel = VO19Sound.play();
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					//SFXChannel.stop();
					//SFXChannel = null;
					//SFXChannel = new SoundChannel();
					//SFXChannel = creditFeedbackScreensRound1.play();
					var v19:SoundInstance = SoundAS.play(VO19);
					v19.soundCompleted.add(VOAllDone);
					SoundAS.removeSound(VO18);
					SoundAS.removeSound(VO18A);
					SoundAS.removeSound(VO18B);
			
					var cfsr1d:SoundInstance = SoundAS.play(CFSR1);
				break;
				
				case 16:
					//trace("onVODone ************************* case 21");
					//SFXChannel.stop();
					cr1.removeQuestListeners();
					cr2.removeQuestListeners();
					cr3.removeQuestListeners();
					cr4.removeQuestListeners();
					
					cr1.gotoAndStop(19);
					cr2.gotoAndStop(19);
					cr3.gotoAndStop(19);
					cr4.gotoAndStop(19);
					
					cr1.setQText(11);
					cr2.setQText(11);
					cr3.setQText(11);
					cr4.setQText(11);
					
					cr1.rW1.rw.visible = false;
					cr1.rW2.rw.visible = false;
					cr1.rW1.met.x = 218;
					cr1.rW1.met.y = 382;
					cr1.rW2.met.x = 218;
					cr1.rW2.met.y = 382;
					
					cr2.rW1.rw.visible = false;
					cr2.rW2.rw.visible = false;
					cr2.rW1.met.x = 218;
					cr2.rW1.met.y = 382;
					cr2.rW2.met.x = 218;
					cr2.rW2.met.y = 382;
					
					cr3.rW1.rw.visible = false;
					cr3.rW2.rw.visible = false;
					cr3.rW1.met.x = 218;
					cr3.rW1.met.y = 382;
					cr3.rW2.met.x = 218;
					cr3.rW2.met.y = 382;
					
					cr4.rW1.rw.visible = false;
					cr4.rW2.rw.visible = false;
					cr4.rW1.met.x = 218;
					cr4.rW1.met.y = 382;
					cr4.rW2.met.x = 218;
					cr4.rW2.met.y = 382;
					
					if (player1ScoreFrame < player2ScoreFrame)
					{
						cr1.rW1.y -= 200;
						cr1.rW2.y += 200;
						cr2.rW1.y -= 200;
						cr2.rW2.y += 200;
						cr3.rW1.y -= 200;
						cr3.rW2.y += 200;
						cr4.rW1.y -= 200;
						cr4.rW2.y += 200;
					}
					else if (player2ScoreFrame < player1ScoreFrame)
					{
						cr1.rW1.y += 200;
						cr1.rW2.y -= 200;
						cr2.rW1.y += 200;
						cr2.rW2.y -= 200;
						cr3.rW1.y += 200;
						cr3.rW2.y -= 200;
						cr4.rW1.y += 200;
						cr4.rW2.y -= 200;
					}
					//VOChannel = VO20Sound.play();
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					var v20:SoundInstance = SoundAS.play(VO20);
					v20.soundCompleted.add(VOAllDone);
					SoundAS.removeSound(VO19);
					SoundAS.loadSound(sp + owner.loadXML.VO22Sound, VO22);
			
					//trace(ct + " frame = " + cr1.currentFrame);
				break;
				
				case 17:
					//trace("onVODone ************************* case 22");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					//VOChannel = VO21Sound.play();
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					var v21:SoundInstance = SoundAS.play(VO21);
					v21.soundCompleted.add(VOAllDone);
					
					var ciaqmr2:SoundInstance = SoundAS.play(CIQMR2);
					cr1.r2Anim.anim.play();
					cr1.r2Anim.anim.an.play();
					cr2.r2Anim.anim.play();
					cr2.r2Anim.anim.an.play();
					cr3.r2Anim.anim.play();
					cr3.r2Anim.anim.an.play();
					cr4.r2Anim.anim.play();
					cr4.r2Anim.anim.an.play();
					cr1.r2Anim.anim.an.t.text = owner.loadXML.lifeRound[l];
					cr2.r2Anim.anim.an.t.text = owner.loadXML.lifeRound[l];
					cr3.r2Anim.anim.an.t.text = owner.loadXML.lifeRound[l];
					cr4.r2Anim.anim.an.t.text = owner.loadXML.lifeRound[l];
					
					SoundAS.removeSound(VO20);
					SoundAS.loadSound(sp + owner.loadXML.VO24aSound, VO24A);
					SoundAS.loadSound(sp + owner.loadXML.VO24bSound, VO24B);
					SoundAS.loadSound(sp + owner.loadXML.VO25Sound, VO25);
					//trace(ct + " frame = " + cr1.currentFrame);
				break;
				
				case 18: //sample cloud
					//trace("onVODone ************************* case 23");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					cr1.creditQ2Ex.gotoAndStop(l + 1);
					cr2.creditQ2Ex.gotoAndStop(l + 1);
					cr3.creditQ2Ex.gotoAndStop(l + 1);
					cr4.creditQ2Ex.gotoAndStop(l + 1);
					cr1.creditQ2Ex.anim.play();
					cr2.creditQ2Ex.anim.play();
					cr3.creditQ2Ex.anim.play();
					cr4.creditQ2Ex.anim.play();
					//VOChannel = VO22Sound.play();
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					var v22:SoundInstance = SoundAS.play(VO22);
					v22.soundCompleted.add(VOAllDone);
					SoundAS.removeSound(VO21);
					//trace(ct + " frame = " + cr1.currentFrame);
				break;
				
				case 19: //cloud
					//trace("onVODone ************************* case 24");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					//trace(ct + " frame = " + cr1.currentFrame);
					
					cr1.addGListeners();
					cr2.addGListeners();
					cr3.addGListeners();
					cr4.addGListeners();
					
					//timerGo(24);
				break;
				
				case 20:
					//trace("VOAllDone ************************* case 25");
					/*cr1.setScore(cr1.currentFrame);
					cr2.setScore(cr2.currentFrame);
					cr3.setScore(cr3.currentFrame);
					cr4.setScore(cr4.currentFrame);*/
					
					cr1.removeGListeners();
					cr2.removeGListeners();
					cr3.removeGListeners();
					cr4.removeGListeners();
					
					cr1.resetGs();
					cr2.resetGs();
					cr3.resetGs();
					cr4.resetGs();
					
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					//VOChannel.stop();
					//VOChannel = null;
					//VOChannel = new SoundChannel();
					//VOChannel = VO24bSound.play();
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					var v24b:SoundInstance = SoundAS.play(VO24B);
					v24b.soundCompleted.add(VOAllDone);
					SoundAS.removeSound(VO22);
					//trace(ct + " frame = " + cr1.currentFrame);
					
					cr1.showScore();
					cr2.showScore();
					cr3.showScore();
					cr4.showScore();
				break;
				
				case 21:
					//trace("onVODone ************************* case 26");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					cr1.creditStairs.play();
					cr2.creditStairs.play();
					cr3.creditStairs.play();
					cr4.creditStairs.play();
					
					
					cr1.setQText(13);
					cr2.setQText(13);
					cr3.setQText(13);
					cr4.setQText(13);
					//VOChannel = VO25Sound.play();
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					var v25:SoundInstance = SoundAS.play(VO25);
					v25.soundCompleted.add(VOAllDone);
					//trace(ct + " frame = " + cr1.currentFrame);
				break;
				
				case 22:
					//trace("onVODone ************************* case 27");
					numPlayers = 0;
					SoundAS.stopAll();
					SoundAS.removeAll();
					owner.removeThis(this);
				break;
			}
			
			cr1.setExitTop();
			cr2.setExitTop();
			cr3.setExitTop();
			cr4.setExitTop();
		}
		
		public function playResultNoise(n:Number):void
		{
			switch (n)
			{
				case 1:
					SoundAS.playFx(CCR1);
					trace("playResultNoise 1");
				break;
				
				case 2:
					SoundAS.playFx(CIR1);
					trace("playResultNoise 2");
				break;
				
				case 3:
					SoundAS.playFx(CCR2);
					trace("playResultNoise 3");
				break;
				
				case 4:
					SoundAS.playFx(CIR2);
					trace("playResultNoise 4");
				break;
				
				default:
				
				break;
			}
		}
		
		
		public function moveOnGame():void
		{
			if (moved == false)
			{
				moved = true;
				//trace("moveOnGame");
				cr1.removeGListeners();
				cr2.removeGListeners();
				cr3.removeGListeners();
				cr4.removeGListeners();
				
				cr1.resetGs();
				cr2.resetGs();
				cr3.resetGs();
				cr4.resetGs();
				
				cr1.nextFrame();
				cr2.nextFrame();
				cr3.nextFrame();
				cr4.nextFrame();
				
				cr1.setMeters(currentPlayer1.score, currentPlayer2.score, false, false, true, true, true);
				cr2.setMeters(currentPlayer1.score, currentPlayer2.score, false, false, true, true, true);
				cr3.setMeters(currentPlayer1.score, currentPlayer2.score, false, false, true, true, true);
				cr4.setMeters(currentPlayer1.score, currentPlayer2.score, false, false, true, true, true);
				
				
				if (cr1.whichPlayer == 1)
				{
					player1Score = cr1.score;
				}
				if (cr1.whichPlayer == 2)
				{
					player2Score = cr1.score;
				}
				if (cr2.whichPlayer == 1)
				{
					player1Score = cr2.score;
				}
				if (cr2.whichPlayer == 2)
				{
					player2Score = cr2.score;
				}
				if (cr3.whichPlayer == 1)
				{
					player1Score = cr3.score;
				}
				if (cr3.whichPlayer == 2)
				{
					player2Score = cr3.score;
				}
				if (cr4.whichPlayer == 1)
				{
					player1Score = cr4.score;
				}
				if (cr4.whichPlayer == 2)
				{
					player2Score = cr4.score;
				}
				
				
				SoundAS.stopAll();
					trace("stop all sounds moveOnGame");
					cr1.setQText(12);
					cr2.setQText(12);
					cr3.setQText(12);
					cr4.setQText(12);
				if (player1Score > player2Score)
				{
					//trace("player 1 won");
					//VOChannel = VO24aSound.play();
					var v24a:SoundInstance = SoundAS.play(VO24A);
					v24a.soundCompleted.add(VOAllDone);
					
					SoundAS.play(CW);
					cr1.creditEnd.winner.n.text = "1";
					cr1.creditEnd.loser.n.text = "2";
					cr2.creditEnd.winner.n.text = "1";
					cr2.creditEnd.loser.n.text = "2";
					cr3.creditEnd.winner.n.text = "1";
					cr3.creditEnd.loser.n.text = "2";
					cr4.creditEnd.winner.n.text = "1";
					cr4.creditEnd.loser.n.text = "2";
					winnerScoreFrame = player1ScoreFrame;
					loserScoreFrame = player2ScoreFrame;
					cr1.creditEnd.visible = true;
					cr2.creditEnd.visible = true;
					cr3.creditEnd.visible = true;
					cr4.creditEnd.visible = true;
					cr1.creditEndTie.visible = false;
					cr2.creditEndTie.visible = false;
					cr3.creditEndTie.visible = false;
					cr4.creditEndTie.visible = false;
				}
				else if (player2Score > player1Score)
				{
					SoundAS.play(CW);
					cr1.creditEnd.visible = true;
					cr2.creditEnd.visible = true;
					cr3.creditEnd.visible = true;
					cr4.creditEnd.visible = true;
					cr1.creditEndTie.visible = false;
					cr2.creditEndTie.visible = false;
					cr3.creditEndTie.visible = false;
					cr4.creditEndTie.visible = false;
					//trace("player 2 won");
					//VOChannel = VO24bSound.play();
					var v24b:SoundInstance = SoundAS.play(VO24B);
					v24b.soundCompleted.add(VOAllDone);
					cr1.creditEnd.winner.n.text = "2";
					cr1.creditEnd.loser.n.text = "1";
					cr2.creditEnd.winner.n.text = "2";
					cr2.creditEnd.loser.n.text = "1";
					cr3.creditEnd.winner.n.text = "2";
					cr3.creditEnd.loser.n.text = "1";
					cr4.creditEnd.winner.n.text = "2";
					cr4.creditEnd.loser.n.text = "1";
					winnerScoreFrame = player2ScoreFrame;
					loserScoreFrame = player1ScoreFrame;
				}
				else
				{
					//SoundAS.play(CW);
					var cw:SoundInstance = SoundAS.play(CW);
					cw.soundCompleted.add(VOAllDone);
					cr1.creditEnd.visible = false;
					cr2.creditEnd.visible = false;
					cr3.creditEnd.visible = false;
					cr4.creditEnd.visible = false;
					cr1.creditEndTie.visible = true;
					cr2.creditEndTie.visible = true;
					cr3.creditEndTie.visible = true;
					cr4.creditEndTie.visible = true;
					winnerScoreFrame = player2ScoreFrame;
					loserScoreFrame = player1ScoreFrame;
				}
				
				if (noPlayer2 == true)
				{
					//cr1.creditEnd.x -= 110;
					//cr1.creditEnd.wins.x += 110;
					cr1.creditEnd.loser.visible = false;
					cr1.creditEnd.l1.visible = false;
					cr1.creditEnd.l2.visible = false;
					cr1.creditEnd.l3.visible = false;
					cr1.creditEnd.l4.visible = false;
					cr1.creditEnd.l5.visible = false;
					
					//cr2.creditEnd.x -= 110;
					//cr2.creditEnd.wins.x += 110;
					cr2.creditEnd.loser.visible = false;
					cr2.creditEnd.l1.visible = false;
					cr2.creditEnd.l2.visible = false;
					cr2.creditEnd.l3.visible = false;
					cr2.creditEnd.l4.visible = false;
					cr2.creditEnd.l5.visible = false;
					
					//cr3.creditEnd.x -= 110;
					//cr3.creditEnd.wins.x += 110;
					cr3.creditEnd.loser.visible = false;
					cr3.creditEnd.l1.visible = false;
					cr3.creditEnd.l2.visible = false;
					cr3.creditEnd.l3.visible = false;
					cr3.creditEnd.l4.visible = false;
					cr3.creditEnd.l5.visible = false;
					
					//cr4.creditEnd.x -= 110;
					//cr4.creditEnd.wins.x += 110;
					cr4.creditEnd.loser.visible = false;
					cr4.creditEnd.l1.visible = false;
					cr4.creditEnd.l2.visible = false;
					cr4.creditEnd.l3.visible = false;
					cr4.creditEnd.l4.visible = false;
					cr4.creditEnd.l5.visible = false;
				}
				
				switch (winnerScoreFrame)
				{
					case 1:
						
					break;
					
					case 2:
						cr1.creditEnd.w1.visible = false;
						cr2.creditEnd.w1.visible = false;
						cr3.creditEnd.w1.visible = false;
						cr4.creditEnd.w1.visible = false;
						cr1.creditEndTie.w1.visible = false;
						cr2.creditEndTie.w1.visible = false;
						cr3.creditEndTie.w1.visible = false;
						cr4.creditEndTie.w1.visible = false;
					break;
					
					case 3:
						cr1.creditEnd.w1.visible = false;
						cr1.creditEnd.w2.visible = false;
						cr2.creditEnd.w1.visible = false;
						cr2.creditEnd.w2.visible = false;
						cr3.creditEnd.w1.visible = false;
						cr3.creditEnd.w2.visible = false;
						cr4.creditEnd.w1.visible = false;
						cr4.creditEnd.w2.visible = false;
						cr1.creditEndTie.w1.visible = false;
						cr1.creditEndTie.w2.visible = false;
						cr2.creditEndTie.w1.visible = false;
						cr2.creditEndTie.w2.visible = false;
						cr3.creditEndTie.w1.visible = false;
						cr3.creditEndTie.w2.visible = false;
						cr4.creditEndTie.w1.visible = false;
						cr4.creditEndTie.w2.visible = false;
					break;
					
					case 4:
						cr1.creditEnd.w1.visible = false;
						cr1.creditEnd.w2.visible = false;
						cr1.creditEnd.w3.visible = false;
						cr2.creditEnd.w1.visible = false;
						cr2.creditEnd.w2.visible = false;
						cr2.creditEnd.w3.visible = false;
						cr3.creditEnd.w1.visible = false;
						cr3.creditEnd.w2.visible = false;
						cr3.creditEnd.w3.visible = false;
						cr4.creditEnd.w1.visible = false;
						cr4.creditEnd.w2.visible = false;
						cr4.creditEnd.w3.visible = false;
						cr1.creditEndTie.w1.visible = false;
						cr1.creditEndTie.w2.visible = false;
						cr1.creditEndTie.w3.visible = false;
						cr2.creditEndTie.w1.visible = false;
						cr2.creditEndTie.w2.visible = false;
						cr2.creditEndTie.w3.visible = false;
						cr3.creditEndTie.w1.visible = false;
						cr3.creditEndTie.w2.visible = false;
						cr3.creditEndTie.w3.visible = false;
						cr4.creditEndTie.w1.visible = false;
						cr4.creditEndTie.w2.visible = false;
						cr4.creditEndTie.w3.visible = false;
					break;
					
					case 5:
						cr1.creditEnd.w1.visible = false;
						cr1.creditEnd.w2.visible = false;
						cr1.creditEnd.w3.visible = false;
						cr1.creditEnd.w4.visible = false;
						cr2.creditEnd.w1.visible = false;
						cr2.creditEnd.w2.visible = false;
						cr2.creditEnd.w3.visible = false;
						cr2.creditEnd.w4.visible = false;
						cr3.creditEnd.w1.visible = false;
						cr3.creditEnd.w2.visible = false;
						cr3.creditEnd.w3.visible = false;
						cr3.creditEnd.w4.visible = false;
						cr4.creditEnd.w1.visible = false;
						cr4.creditEnd.w2.visible = false;
						cr4.creditEnd.w3.visible = false;
						cr4.creditEnd.w4.visible = false;
						cr1.creditEndTie.w1.visible = false;
						cr1.creditEndTie.w2.visible = false;
						cr1.creditEndTie.w3.visible = false;
						cr1.creditEndTie.w4.visible = false;
						cr2.creditEndTie.w1.visible = false;
						cr2.creditEndTie.w2.visible = false;
						cr2.creditEndTie.w3.visible = false;
						cr2.creditEndTie.w4.visible = false;
						cr3.creditEndTie.w1.visible = false;
						cr3.creditEndTie.w2.visible = false;
						cr3.creditEndTie.w3.visible = false;
						cr3.creditEndTie.w4.visible = false;
						cr4.creditEndTie.w1.visible = false;
						cr4.creditEndTie.w2.visible = false;
						cr4.creditEndTie.w3.visible = false;
						cr4.creditEndTie.w4.visible = false;					
						break;
				}
				
				switch (loserScoreFrame)
				{
					case 1:
						
					break;
					
					case 2:
						cr1.creditEnd.l1.visible = false;
						cr2.creditEnd.l1.visible = false;
						cr3.creditEnd.l1.visible = false;
						cr4.creditEnd.l1.visible = false;
						cr1.creditEndTie.l1.visible = false;
						cr2.creditEndTie.l1.visible = false;
						cr3.creditEndTie.l1.visible = false;
						cr4.creditEndTie.l1.visible = false;
					break;
					
					case 3:
						cr1.creditEnd.l1.visible = false;
						cr1.creditEnd.l2.visible = false;
						cr2.creditEnd.l1.visible = false;
						cr2.creditEnd.l2.visible = false;
						cr3.creditEnd.l1.visible = false;
						cr3.creditEnd.l2.visible = false;
						cr4.creditEnd.l1.visible = false;
						cr4.creditEnd.l2.visible = false;
						cr1.creditEndTie.l1.visible = false;
						cr1.creditEndTie.l2.visible = false;
						cr2.creditEndTie.l1.visible = false;
						cr2.creditEndTie.l2.visible = false;
						cr3.creditEndTie.l1.visible = false;
						cr3.creditEndTie.l2.visible = false;
						cr4.creditEndTie.l1.visible = false;
						cr4.creditEndTie.l2.visible = false;
					break;
					
					case 4:
						cr1.creditEnd.l1.visible = false;
						cr1.creditEnd.l2.visible = false;
						cr1.creditEnd.l3.visible = false;
						cr2.creditEnd.l1.visible = false;
						cr2.creditEnd.l2.visible = false;
						cr2.creditEnd.l3.visible = false;
						cr3.creditEnd.l1.visible = false;
						cr3.creditEnd.l2.visible = false;
						cr3.creditEnd.l3.visible = false;
						cr4.creditEnd.l1.visible = false;
						cr4.creditEnd.l2.visible = false;
						cr4.creditEnd.l3.visible = false;
						cr1.creditEndTie.l1.visible = false;
						cr1.creditEndTie.l2.visible = false;
						cr1.creditEndTie.l3.visible = false;
						cr2.creditEndTie.l1.visible = false;
						cr2.creditEndTie.l2.visible = false;
						cr2.creditEndTie.l3.visible = false;
						cr3.creditEndTie.l1.visible = false;
						cr3.creditEndTie.l2.visible = false;
						cr3.creditEndTie.l3.visible = false;
						cr4.creditEndTie.l1.visible = false;
						cr4.creditEndTie.l2.visible = false;
						cr4.creditEndTie.l3.visible = false;
					break;
					
					case 5:
						cr1.creditEnd.l1.visible = false;
						cr1.creditEnd.l2.visible = false;
						cr1.creditEnd.l3.visible = false;
						cr1.creditEnd.l4.visible = false;
						cr2.creditEnd.l1.visible = false;
						cr2.creditEnd.l2.visible = false;
						cr2.creditEnd.l3.visible = false;
						cr2.creditEnd.l4.visible = false;
						cr3.creditEnd.l1.visible = false;
						cr3.creditEnd.l2.visible = false;
						cr3.creditEnd.l3.visible = false;
						cr3.creditEnd.l4.visible = false;
						cr4.creditEnd.l1.visible = false;
						cr4.creditEnd.l2.visible = false;
						cr4.creditEnd.l3.visible = false;
						cr4.creditEnd.l4.visible = false;
						cr1.creditEndTie.l1.visible = false;
						cr1.creditEndTie.l2.visible = false;
						cr1.creditEndTie.l3.visible = false;
						cr1.creditEndTie.l4.visible = false;
						cr2.creditEndTie.l1.visible = false;
						cr2.creditEndTie.l2.visible = false;
						cr2.creditEndTie.l3.visible = false;
						cr2.creditEndTie.l4.visible = false;
						cr3.creditEndTie.l1.visible = false;
						cr3.creditEndTie.l2.visible = false;
						cr3.creditEndTie.l3.visible = false;
						cr3.creditEndTie.l4.visible = false;
						cr4.creditEndTie.l1.visible = false;
						cr4.creditEndTie.l2.visible = false;
						cr4.creditEndTie.l3.visible = false;
						cr4.creditEndTie.l4.visible = false;
					break;
				}
				
				
					cr1.creditEnd.play();
					cr2.creditEnd.play();
					cr3.creditEnd.play();
					cr4.creditEnd.play();
					cr1.creditEndTie.play();
					cr2.creditEndTie.play();
					cr3.creditEndTie.play();
					cr4.creditEndTie.play();
				
				//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
				//trace(ct + " frame = " + cr1.currentFrame);
				
				cr1.showScore();
				cr2.showScore();
				cr3.showScore();
				cr4.showScore();
			
				ct++;
			}
		}
		
		private function countdown(e:TimerEvent):void
		{
			ct++;
			//trace("countdown ct = " + ct);
			gameTimer.stop();
			gameTimer.removeEventListener(TimerEvent.TIMER, countdown);
			switch (ct)
			{
				case 2:
					//trace("countdown ************************* case 2");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					
					cr1.initMeters();
					cr2.initMeters();
					cr3.initMeters();
					cr4.initMeters();
					cr1.showMeters();
					cr2.showMeters();
					cr3.showMeters();
					cr4.showMeters();
					if (numPlayers < 2)
					{
						cr1.hideMeters(false);
						cr2.hideMeters(false);
						cr3.hideMeters(false);
						cr4.hideMeters(false);
					}
					//VOChannel.removeEventListener(Event.SOUND_COMPLETE, onVODone);
					//VOChannel.stop();
					//VOChannel = null;
					//VOChannel = new SoundChannel();
					//VOChannel = VO6Sound.play();
					var v6:SoundInstance = SoundAS.play(VO6);
					v6.soundCompleted.add(VOAllDone);
					
					cr1.resetQuestions();
					cr2.resetQuestions();
					cr3.resetQuestions();
					cr4.resetQuestions();
					
					cr1.addQuestListeners();
					cr2.addQuestListeners();
					cr3.addQuestListeners();
					cr4.addQuestListeners();
					
					//trace(ct + " frame = " + cr1.currentFrame);
					cr1.showQ();
					cr2.showQ();
					cr3.showQ();
					cr4.showQ();
					
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
				break;
				
				case 21:
					//trace("countdown ************************* case 21");
					/*cr1.setScore(cr1.currentFrame);
					cr2.setScore(cr2.currentFrame);
					cr3.setScore(cr3.currentFrame);
					cr4.setScore(cr4.currentFrame);*/
					
					cr1.removeGListeners();
					cr2.removeGListeners();
					cr3.removeGListeners();
					cr4.removeGListeners();
					
					cr1.resetGs();
					cr2.resetGs();
					cr3.resetGs();
					cr4.resetGs();
					
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					//VOChannel.stop();
					//VOChannel = null;
					//VOChannel = new SoundChannel();
					//VOChannel = VO24bSound.play();
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					var v24b:SoundInstance = SoundAS.play(VO24B);
					v24b.soundCompleted.add(VOAllDone);
					//trace(ct + " frame = " + cr1.currentFrame);
					
					cr1.showScore();
					cr2.showScore();
					cr3.showScore();
					cr4.showScore();
				break;
				
				/*case 17:
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel.stop();
					VOChannel = new SoundChannel();
					VOChannel = VO25Sound.play();
					trace(ct + " frame = " + cr1.currentFrame);
					timerGo(18);
				break;
				
				case 18:
					trace(ct + " frame = " + cr1.currentFrame);
					trace("----------------------------------------- done");
					numPlayers = 0;
					owner.removeThis(this);
				break;*/
			}
		}
		
		/*private function addAmount1(e:TuioTouchEvent):void
		{
			s1 += e.currentTarget.t.text;
			trace(Number(s1)/100);
			cr1.savings.text = "$" + String((Number(s1)/100).toFixed(2));
		}
		
		private function addAmount2(e:TuioTouchEvent):void
		{
			s2 += e.currentTarget.t.text;
			trace(Number(s2)/100);
			cr2.savings.text = "$" + String((Number(s2)/100).toFixed(2));
		}
		
		private function addAmount3(e:TuioTouchEvent):void
		{
			s3 += e.currentTarget.t.text;
			trace(Number(s3)/100);
			cr3.savings.text = "$" + String((Number(s3)/100).toFixed(2));
		}
		
		private function addAmount4(e:TuioTouchEvent):void
		{
			s4 += e.currentTarget.t.text;
			trace(Number(s4)/100);
			cr4.savings.text = "$" + String((Number(s4)/100).toFixed(2));
		}*/
		
		private function goOk(e:TuioTouchEvent):void
		{
			//cr1.ok.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goOk);
			//cr2.ok.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goOk);
			//cr3.ok.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goOk);
			//cr4.ok.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goOk);
			//cr1.removeCalLiseners();
			//cr2.removeCalLiseners();
			//cr3.removeCalLiseners();
			//cr4.removeCalLiseners();
			cr1.gotoAndStop(16);
			cr2.gotoAndStop(16);
			cr3.gotoAndStop(16);
			cr4.gotoAndStop(16);
			cr1.resetGs();
			cr2.resetGs();
			cr3.resetGs();
			cr4.resetGs();
			cr1.addGListeners();
			cr2.addGListeners();
			cr3.addGListeners();
			cr4.addGListeners();
			//cr1.ok.addEventListener(TuioTouchEvent.TOUCH_DOWN, allDone);
			//cr2.ok.addEventListener(TuioTouchEvent.TOUCH_DOWN, allDone);
			//cr3.ok.addEventListener(TuioTouchEvent.TOUCH_DOWN, allDone);
			//cr4.ok.addEventListener(TuioTouchEvent.TOUCH_DOWN, allDone);
		}
		
		private function allDone(e:TuioTouchEvent):void
		{
			cr1.resetGs();
			cr2.resetGs();
			cr3.resetGs();
			cr4.resetGs();
			cr1.removeGListeners();
			cr2.removeGListeners();
			cr3.removeGListeners();
			cr4.removeGListeners();
			
			//cr1.ok.removeEventListener(TuioTouchEvent.TOUCH_DOWN, allDone);
			//cr2.ok.removeEventListener(TuioTouchEvent.TOUCH_DOWN, allDone);
			//cr3.ok.removeEventListener(TuioTouchEvent.TOUCH_DOWN, allDone);
			//cr4.ok.removeEventListener(TuioTouchEvent.TOUCH_DOWN, allDone);
			
			cr1.gotoAndStop(17);
			cr2.gotoAndStop(17);
			cr3.gotoAndStop(17);
			cr4.gotoAndStop(17);
			
			cr1.calc();
			cr2.calc();
			cr3.calc();
			cr4.calc();
			
			timerGo(17);
		}
		
		/*private function setG1(e:TuioTouchEvent):void
		{
			e.currentTarget.gotoAndStop(2);
			cr1.cCalc.visible = true;
			cr1.tCalc.visible = true;
		}
		
		private function setG2(e:TuioTouchEvent):void
		{
			e.currentTarget.gotoAndStop(2);
		}
		
		private function setG3(e:TuioTouchEvent):void
		{
			e.currentTarget.gotoAndStop(2);
		}
		
		private function setG4(e:TuioTouchEvent):void
		{
			e.currentTarget.gotoAndStop(2);
		}*/
		
		private function moveOn(e:TuioTouchEvent):void
		{
			//cr1.moveOn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, moveOn);
			//cr2.moveOn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, moveOn);
			//cr3.moveOn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, moveOn);
			//cr4.moveOn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, moveOn);
			cr1.gotoAndStop(14);
			cr2.gotoAndStop(14);
			cr3.gotoAndStop(14);
			cr4.gotoAndStop(14);
			timerGo(8);
		}
		
		private function questTimer(e:TimerEvent):void
		{
			qTime--;
			if (cr1.countTime != null)
			{
				cr1.countTime.text = qTime.toString();
			}
			if (cr2.countTime != null)
			{
				cr2.countTime.text = qTime.toString();
			}
			if (cr3.countTime != null)
			{
				cr3.countTime.text = qTime.toString();
			}
			if (cr4.countTime != null)
			{
				cr4.countTime.text = qTime.toString();
			}
			if (qTime <= 0)
			{
				gameTimer.removeEventListener(TimerEvent.TIMER, questTimer);
				qTime = 15;
				
				//var ctro:SoundInstance = SoundAS.play(CTRO);
				
				cr1.removeQuestListeners();
				cr2.removeQuestListeners();
				cr3.removeQuestListeners();
				cr4.removeQuestListeners();
						
				switch (ct)
				{
					case 3:
						trace("questTimer case 4");
						//remove q listeners
						if (cr1.didNotAnswer == true)
						{
							cr1.setScore(cr1.currentFrame);
						}
						if (cr2.didNotAnswer == true)
						{
							cr2.setScore(cr2.currentFrame);
						}
						if (cr3.didNotAnswer == true)
						{
							cr3.setScore(cr3.currentFrame);
						}
						if (cr4.didNotAnswer == true)
						{
							cr4.setScore(cr4.currentFrame);
						}
						trace(cr1.didNotAnswer);
						trace(cr2.didNotAnswer);
						trace(cr3.didNotAnswer);
						trace(cr4.didNotAnswer);
						if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
						{
							ct--;
							trace("questTimer no answer");
							//VOChannel.removeEventListener(Event.SOUND_COMPLETE, onVODone);
							//VOChannel.stop();
							//VOChannel = null;
							//VOChannel = new SoundChannel();
							//VOChannel = VO6aSound.play();
							//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
							var v6a:SoundInstance = SoundAS.play(VO6A);
							v6a.soundCompleted.add(VOAllDone);
						}
						else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
						{
							ct--;
							trace("questTimer wrong answer");
							//VOChannel.removeEventListener(Event.SOUND_COMPLETE, onVODone);
							//VOChannel.stop();
							//VOChannel = null;
							//VOChannel = new SoundChannel();
							//VOChannel = VO6bSound.play();
							//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
							var v6b:SoundInstance = SoundAS.play(VO6B);
							v6b.soundCompleted.add(VOAllDone);
						}
						else
						{
							//trace("questTimer right answer");
							//ct++;
							
							cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr1.hideQ();
							cr2.hideQ();
							cr3.hideQ();
							cr4.hideQ();
							cr1.nextFrame();
							cr2.nextFrame();
							cr3.nextFrame();
							cr4.nextFrame();
							
					
							cr1.setQText(6);
							cr2.setQText(6);
							cr3.setQText(6);
							cr4.setQText(6);
							//VOChannel.removeEventListener(Event.SOUND_COMPLETE, onVODone);
							//VOChannel.stop();
							//VOChannel = null;
							//VOChannel = new SoundChannel();
							//VOChannel = VO7Sound.play();
							//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
							var v7:SoundInstance = SoundAS.play(VO7);
							v7.soundCompleted.add(VOAllDone);
						}
					break;
					
					case 6:
						//trace("questTimer case 8");
						
						if (cr1.didNotAnswer == true)
						{
							cr1.setScore(cr1.currentFrame);
						}
						if (cr2.didNotAnswer == true)
						{
							cr2.setScore(cr2.currentFrame);
						}
						if (cr3.didNotAnswer == true)
						{
							cr3.setScore(cr3.currentFrame);
						}
						if (cr4.didNotAnswer == true)
						{
							cr4.setScore(cr4.currentFrame);
						}
						if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
						{
							ct--;
							//VOChannel = VO9aSound.play();
							//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
							var v9a:SoundInstance = SoundAS.play(VO9A);
							v9a.soundCompleted.add(VOAllDone);
						}
						else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
						{
							ct--;
							//VOChannel = VO9bSound.play();
							//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
							var v9b:SoundInstance = SoundAS.play(VO9B);
							v9b.soundCompleted.add(VOAllDone);
						}
						else
						{
							//cr1.setScore(cr1.currentFrame);
							//cr2.setScore(cr2.currentFrame);
							//cr3.setScore(cr3.currentFrame);
							//cr4.setScore(cr4.currentFrame);
							cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr1.hideQ();
							cr2.hideQ();
							cr3.hideQ();
							cr4.hideQ();
							cr1.nextFrame();
							cr2.nextFrame();
							cr3.nextFrame();
							cr4.nextFrame();
					
							cr1.setQText(7);
							cr2.setQText(7);
							cr3.setQText(7);
							cr4.setQText(7);
							//VOChannel = VO10Sound.play();
							//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
							var v10:SoundInstance = SoundAS.play(VO10);
							v10.soundCompleted.add(VOAllDone);
						}
					break;
					case 9:
					//trace("questTimer case 12");
					
						if (cr1.didNotAnswer == true)
						{
							cr1.setScore(cr1.currentFrame);
						}
						if (cr2.didNotAnswer == true)
						{
							cr2.setScore(cr2.currentFrame);
						}
						if (cr3.didNotAnswer == true)
						{
							cr3.setScore(cr3.currentFrame);
						}
						if (cr4.didNotAnswer == true)
						{
							cr4.setScore(cr4.currentFrame);
						}
						if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
						{
							ct--;
							//VOChannel = VO12aSound.play();
							//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
							var v12a:SoundInstance = SoundAS.play(VO12A);
							v12a.soundCompleted.add(VOAllDone);
						}
						else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
						{
							ct--;
							//VOChannel = VO12bSound.play();
							//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
							var v12b:SoundInstance = SoundAS.play(VO12B);
							v12b.soundCompleted.add(VOAllDone);
						}
						else
						{
							//cr1.setScore(cr1.currentFrame);
							//cr2.setScore(cr2.currentFrame);
							//cr3.setScore(cr3.currentFrame);
							//cr4.setScore(cr4.currentFrame);
							cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr1.hideQ();
							cr2.hideQ();
							cr3.hideQ();
							cr4.hideQ();
							cr1.nextFrame();
							cr2.nextFrame();
							cr3.nextFrame();
							cr4.nextFrame();
					
							cr1.setQText(8);
							cr2.setQText(8);
							cr3.setQText(8);
							cr4.setQText(8);
							//VOChannel = VO13Sound.play();
							//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
							var v13:SoundInstance = SoundAS.play(VO13);
							v13.soundCompleted.add(VOAllDone);
						}
					break;
					case 12:
					//trace("questTimer case 16");
					
						if (cr1.didNotAnswer == true)
						{
							cr1.setScore(cr1.currentFrame);
						}
						if (cr2.didNotAnswer == true)
						{
							cr2.setScore(cr2.currentFrame);
						}
						if (cr3.didNotAnswer == true)
						{
							cr3.setScore(cr3.currentFrame);
						}
						if (cr4.didNotAnswer == true)
						{
							cr4.setScore(cr4.currentFrame);
						}
						if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
						{
							ct--;
							//VOChannel = VO15aSound.play();
							//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
							var v15a:SoundInstance = SoundAS.play(VO15A);
							v15a.soundCompleted.add(VOAllDone);
						}
						else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
						{
							ct--;
							//VOChannel = VO15bSound.play();
							//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
							var v15b:SoundInstance = SoundAS.play(VO15B);
							v15b.soundCompleted.add(VOAllDone);
						}
						else
						{
							//cr1.setScore(cr1.currentFrame);
							//cr2.setScore(cr2.currentFrame);
							//cr3.setScore(cr3.currentFrame);
							//cr4.setScore(cr4.currentFrame);
							cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr1.hideQ();
							cr2.hideQ();
							cr3.hideQ();
							cr4.hideQ();
							cr1.nextFrame();
							cr2.nextFrame();
							cr3.nextFrame();
							cr4.nextFrame();
					
							cr1.setQText(9);
							cr2.setQText(9);
							cr3.setQText(9);
							cr4.setQText(9);
							//VOChannel = VO16Sound.play();
							//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
							var v16:SoundInstance = SoundAS.play(VO16);
							v16.soundCompleted.add(VOAllDone);
						}
					break;
					case 15:
					//trace("questTimer case 20");
					
						if (cr1.didNotAnswer == true)
						{
							cr1.setScore(cr1.currentFrame);
						}
						if (cr2.didNotAnswer == true)
						{
							cr2.setScore(cr2.currentFrame);
						}
						if (cr3.didNotAnswer == true)
						{
							cr3.setScore(cr3.currentFrame);
						}
						if (cr4.didNotAnswer == true)
						{
							cr4.setScore(cr4.currentFrame);
						}
						if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
						{
							ct--;
							//VOChannel = VO15aSound.play();
							//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
							var v18a:SoundInstance = SoundAS.play(VO18A);
							v18a.soundCompleted.add(VOAllDone);
						}
						else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
						{
							ct--;
							//VOChannel = VO15bSound.play();
							//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
							var v18b:SoundInstance = SoundAS.play(VO18B);
							v18b.soundCompleted.add(VOAllDone);
						}
						else
						{
							//cr1.setScore(cr1.currentFrame);
							//cr2.setScore(cr2.currentFrame);
							//cr3.setScore(cr3.currentFrame);
							//cr4.setScore(cr4.currentFrame);
							cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr1.hideQ();
							cr2.hideQ();
							cr3.hideQ();
							cr4.hideQ();
							cr1.nextFrame();
							cr2.nextFrame();
							cr3.nextFrame();
							cr4.nextFrame();
							
					
							cr1.setQText(10);
							cr2.setQText(10);
							cr3.setQText(10);
							cr4.setQText(10);
							//VOChannel = VO16Sound.play();
							//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
							var v19:SoundInstance = SoundAS.play(VO19);
							v19.soundCompleted.add(VOAllDone);
						}
					break;
				}
			}
		}
		
		private function timerGo(n:Number):void
		{
			//trace("timerGo ct before = " + ct);
			ct = n;
			//trace("timerGo ct after = " + ct);
			switch (ct)
			{
				case 1:
					//trace("timerGo ************************* case 1");
					gameTimer = new Timer(6000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				/*case 2:
					
					gameTimer = new Timer(20000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 3:
					gameTimer = new Timer(20000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;*/
				case 3:
					//trace("timerGo ************************* case 4");
					gameTimer = new Timer(1000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, questTimer);
					//SFXChannel.stop();
					//SFXChannel = null;
					//SFXChannel = new SoundChannel();
					//SFXChannel = creditQuestionMusicRound1.play();
					var cqmr1:SoundInstance = SoundAS.play(CQMR1);
				break;
				/*case 5:
					gameTimer = new Timer(20000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 6:
					gameTimer = new Timer(20000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;*/
				case 6:
					//trace("timerGo ************************* case 7");
					gameTimer = new Timer(1000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, questTimer);
					//SFXChannel.stop();
					//SFXChannel = null;
					//SFXChannel = new SoundChannel();
					//SFXChannel = creditQuestionMusicRound1.play();
					var cqmr1a:SoundInstance = SoundAS.play(CQMR1);
				break;
				/*case 8:
					gameTimer = new Timer(20000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 9:
					gameTimer = new Timer(20000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;*/
				case 9:
					//trace("timerGo ************************* case 10");
					gameTimer = new Timer(1000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, questTimer);
					//SFXChannel.stop();
					//SFXChannel = null;
					//SFXChannel = new SoundChannel();
					//SFXChannel = creditQuestionMusicRound1.play();
					var cqmr1b:SoundInstance = SoundAS.play(CQMR1);
				break;
				/*case 11:
					gameTimer = new Timer(20000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 12:
					gameTimer = new Timer(20000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;*/
				case 12:
					//trace("timerGo ************************* case 13");
					gameTimer = new Timer(1000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, questTimer);
					//SFXChannel.stop();
					//SFXChannel = null;
					//SFXChannel = new SoundChannel();
					//SFXChannel = creditQuestionMusicRound1.play();
					var cqmr1c:SoundInstance = SoundAS.play(CQMR1);
				break;
				/*case 14:
					gameTimer = new Timer(20000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 15:
					gameTimer = new Timer(15000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;*/
				case 15:
					//trace("timerGo ************************* case 16");
					gameTimer = new Timer(1000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, questTimer);
					//SFXChannel.stop();
					//SFXChannel = null;
					//SFXChannel = new SoundChannel();
					//SFXChannel = creditQuestionMusicRound1.play();
					var cqmr1d:SoundInstance = SoundAS.play(CQMR1);
				break;
				case 24:
					//trace("timerGo ************************* case 24");
					gameTimer = new Timer(30000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 17:
					//trace("timerGo ************************* case 17");
					gameTimer = new Timer(10000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 18:
					//trace("timerGo ************************* case 18");
					gameTimer = new Timer(10000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 19:
					//trace("timerGo ************************* case 19");
					gameTimer = new Timer(15000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
			}
		}
		
		private function cr1Chosen(e:TuioTouchEvent):void
		{
			cr1.q1.gotoAndStop(1);
			cr1.q2.gotoAndStop(1);
			cr1.q3.gotoAndStop(1);
			e.currentTarget.gotoAndStop(2);
		}
		
		private function cr2Chosen(e:TuioTouchEvent):void
		{
			cr2.q1.gotoAndStop(1);
			cr2.q2.gotoAndStop(1);
			cr2.q3.gotoAndStop(1);
			e.currentTarget.gotoAndStop(2);
		}
		
		private function cr3Chosen(e:TuioTouchEvent):void
		{
			cr3.q1.gotoAndStop(1);
			cr3.q2.gotoAndStop(1);
			cr3.q3.gotoAndStop(1);
			e.currentTarget.gotoAndStop(2);
		}
		
		private function cr4Chosen(e:TuioTouchEvent):void
		{
			cr4.q1.gotoAndStop(1);
			cr4.q2.gotoAndStop(1);
			cr4.q3.gotoAndStop(1);
			e.currentTarget.gotoAndStop(2);
		}
		
		public function tryToRemoveThis(c:CrGm):void
		{
			if (cr1.visible == false && cr2.visible == false && cr3.visible == false && cr4.visible == false)
			{
				SoundAS.stopAll();
					trace("stop all sounds tryToRemoveThis");
				SoundAS.removeAll();
				cr1.stopTweens();
				cr2.stopTweens();
				cr3.stopTweens();
				cr4.stopTweens();
				cr1.removeQuestListeners();
				cr2.removeQuestListeners();
				cr3.removeQuestListeners();
				cr4.removeQuestListeners();
				cr1.removeGListeners();
				cr2.removeGListeners();
				cr3.removeGListeners();
				cr4.removeGListeners();
				if (countdownTimer != null)
				{
					countdownTimer.stop();
					countdownTimer.addEventListener(TimerEvent.TIMER, countdown4);
				}
				if (gameTimer != null)
				{
					gameTimer.stop();
					gameTimer.removeEventListener(TimerEvent.TIMER, countdown);
					gameTimer.removeEventListener(TimerEvent.TIMER, questTimer);
				}
				owner.removeThis(this);
			}
		}

	}
	
}
