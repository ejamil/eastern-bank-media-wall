﻿package code {
	
	import flash.display.MovieClip;
	import flash.events.TransformGestureEvent;
	
	
	public class Paddle extends MovieClip 
	{
		private var pX:Number;
		private var pY:Number;
		private var pW:Number;
		private var pS:Number;
		
		public function Paddle(theX:Number, theY:Number, theWidth:Number, theScreen:Number = 1) 
		{
			pX = 0;
			pY = 0;
			pW = theWidth;
			pS = theScreen;
			this.addEventListener(TransformGestureEvent.GESTURE_PAN, handleDrag);
		}
		
		private function handleDrag(e:TransformGestureEvent):void 
		{
			if (this.x >= pX && this.x <= pX + pW - this.width)
			{
				this.x += e.offsetX;
				//this.y += e.offsetY;
			}
			if (this.x < pX)
			{
				this.x = pX;
			}
			if (this.x > pX + pW - this.width)
			{
				this.x = pX + pW - this.width;
			}
		}
	}
	
	
	
}
