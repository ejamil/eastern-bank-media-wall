﻿package  code {
	
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.display.Image;
	import starling.display.Stage;
	import starling.utils.deg2rad;
	import starling.events.Event;
	import starling.core.Starling;
	//import flash.display.Bitmap;
	//import flash.utils.setTimeout;
	import starling.textures.Texture;
	import starling.animation.Juggler;
	//import caurina.transitions.Tweener;
	//import flash.display.Loader;
	//import flash.net.URLRequest;
	//import flash.events.Event;
	//import starling.events.KeyboardEvent;
	import starling.animation.Tween;
	import starling.animation.Transitions;
	import flash.display.MovieClip;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	import com.greensock.motionPaths.*;
	
	import flash.display.Loader;
	
	public class Background extends MovieClip
	{
		public function Background() 
		{
			d_bg.alpha = 0;
			h_bg.alpha = 0;
			l_bg.alpha = 0;
			p_bg.alpha = 0;
			d_smile.alpha = 0;
			d_boys.alpha = 0;
			d_swing.alpha = 0;
			d_team.alpha = 0;
			d_baby.alpha = 0;
			d_chicks.alpha = 0;
			b_clipboardLady.alpha = .2;
			b_bike.alpha = .2;
			b_register.alpha = .2;
			
			b_clipboardLady.x = 940;
			b_clipboardLady.y = 490;
			
			b_bike.x = 940;
			b_bike.y = 490;
			
			b_register.x = 3320;
			b_register.y = 1160;
			
		}

	}
	
}
