﻿package code {
 
    import flash.display.MovieClip;
	import com.flashandmath.dg.GUI.GradientSwatch;
	import com.flashandmath.dg.bitmapUtilities.BitmapSaver;
	import flash.events.MouseEvent;
	import flash.events.KeyboardEvent;
	import flash.events.Event;
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Matrix;
	import code.Document;
	
	import org.tuio.*;
	import org.tuio.adapters.*;
	import org.tuio.debug.TuioDebug;
	import org.tuio.TuioManager;
	import org.tuio.connectors.*;
    import flash.geom.*; // needed for curPt;
 
    public class drawingApp extends MovieClip {
 
        private var color:uint = 0xff0000; // color of lines
        private var thickness:Number = 30; // thickness of lines
 
        private var blobs:Array = new Array(); // blobs we are currently interacting with
        private var lines:Array = new Array(); // array to hold the lines
 
        public function drawingApp():void {
 
            //--------connect to TUIO-----------------
           /* TUIO.init(this,'localhost',3000,'',true);
            trace("MyTouchApp Initialized");*/
            //---------------------------------------- 
 
            addEventListener(TuioTouchEvent.TOUCH_DOWN, downEvent, false, 0, true); // run when finger down
            addEventListener(TuioTouchEvent.TOUCH_UP, upEvent, false, 0, true); // run when finger up
            addEventListener(Event.ENTER_FRAME, updater, false, 0, true); // keep running
        }
 
        // updates/draws the drawn lines by:
        // cycling through all existing blobs
        // getting the id of each blob
        // use that id to get the line, as the lines are named after their blobs (eg line1280 )
        // tuioobj is the blob - see how it is called by request using the blob id?
        // if it is not there remove it from the blobs array.
        // draw the next segment of the line using lineTo and current loc (tuioobj.x, tuioobj.y)
        function updater(e:Event):void {
            for (var i:int = 0; i < blobs.length; i++) { 
                var tuioobj:TUIOObject=TUIO.getObjectById(blobs[i].id);
 
                // if not found, then it must have died..
                if (! tuioobj) {
                    removeBlob(blobs[i].id);
                } else if (parent != null) {
 
                    lines["line"+blobs[i].id].graphics.lineTo(tuioobj.x, tuioobj.y);
                }
            }
        }
 
        // when touched, this calls addblob with the blob ID and the X/Y of where it started
        public function downEvent(e:TuioTouchEvent):void {
            var curPt:Point=parent.globalToLocal(new Point(e.stageX,e.stageY));
            addBlob(e.ID, curPt.x, curPt.y);
            e.stopPropagation();
        }
 
        // When a finger stops touching, call removeBlob
        public function upEvent(e:TuioTouchEvent):void {
            removeBlob(e.ID);
            e.stopPropagation();
        }
 
        // This takes the sent ID, x,y and puts it all in the blobs array
        // It then creats a new sprite, and sets the starting loc of the line
        // The line is put in an array called lines, so it can be called over and over by name
        // if the lines did not have unique names you could only draw one line at a time
        // so each line is named in the array after the id of the blob (EG line1280)
        function addBlob(id:Number, origX:Number, origY:Number):void {
            blobs.push( {id: id, origX: origX, origY: origY, myOrigX: x, myOrigY:y} );
 
            var line:Sprite = new Sprite();
            lines["line"+id] = line;
            lines["line"+id].graphics.lineStyle(thickness, color); //<-- config above
            lines["line"+id].graphics.moveTo(origX, origY); //starting point of line to be drawn
            addChild(lines["line"+id]); // add line to stage
        }
 
        // when called, this function removes the blobs from the array
        // cycles through the blobs array until the blob is found by its ID, then it is removed.
        function removeBlob(id:Number):void {
            for (var i=0; i < blobs.length; i++) {
                if (blobs[i].id==id) {
                    blobs.splice(i, 1);
                    return;
                }
            }
        }
 
    }
}