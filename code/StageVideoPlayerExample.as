﻿/* AS3
	Copyright 2011 R Blank
*/
package code
{
 
	/**
	 *	This class illustrates a simple StageVideo implementation.
	 *
	 *	@langversion ActionScript 3.0
	 *	@playerversion Flash 10.2
	 *
	 *	@author R Blank
	 *	@since  09.09.2011
	 */
 
	//import the MovieClip, because this class is a MovieClip
	import flash.display.MovieClip ;
	//import the Rectangle, so that we can size and position the StageVideo instance
	import flash.geom.Rectangle ;
	//import StageVideoAvailabilityEvent so we can hear 
	//when StageVideo availability changes
	import flash.events.StageVideoAvailabilityEvent;
	//import StageVideo so we can use StageVideo
	import flash.media.StageVideo;
	//import StageVideoAvailability to get the values for the
	//StageVideoAvailabilityEvent.available property
	import flash.media.StageVideoAvailability;
	//import Video as a backup for when StageVideo is not available
	import flash.media.Video;
	//import the NetConnection and NetStream to play our video file
	import flash.net.NetConnection;
	import flash.net.NetStream;
	//import the TextField so we can log values visibly 
	import flash.text.TextField ;
 
	public class StageVideoPlayerExample extends MovieClip {
 
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
 
		//the location of our video file
		private const _videoURL : String = "EB_2880.mp4" ;
 
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		public function StageVideoPlayerExample ( ) {
			//call the _init function to startup
			_init () ;
		}
 
		//--------------------------------------
		//  PRIVATE VARIABLES
		//--------------------------------------
 
		//the Video object to play video when StageVideo is not available
		private var _video : Video ;
		//the StageVideo object to play fully accelerated video
		private var _stageVideo : StageVideo ;
		//the NetStream and NetConnection objects to play video
		private var _ns : NetStream ;
		private var _nc : NetConnection ;
		//the TextField object to log informationvisibly in the browser
		private var _tf : TextField ;
 
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------
 
		//called by the constructor to startup the player
		private function _init ( ) : void
		{
			//the NetConnection object over which video is delivered
			_nc = new NetConnection ( ) ;
			//connect _nc to null because we are playing video progressively
			_nc.connect ( null ) ;
			//create the NetStream to deliver video over the NetConnection
			_ns = new NetStream ( _nc ) ;
			//define the NetStream client as 'this'
			//where _ns will look for the onMetaData and onCuePoint methods
			_ns.client = this ;
			//tell the NetStream to play our video file
			_ns.play ( _videoURL ) ;
			//create the video object as back up, in case StageVideo is not available
			_video = new Video ( ) ;
			//listen for the StageVideoAvailabilityEvent
			//which is fired as soon as this listener is added
			//and when StageVideo becomes available or unavailable 
			//throughout SWF playback
			stage.addEventListener(StageVideoAvailabilityEvent.STAGE_VIDEO_AVAILABILITY, _onStageVideoAvailability);
			//create the TextField for logging
			_tf = new TextField ( );
			_tf.height = stage.stageHeight ;
			_tf.width = stage.stageWidth ;
			_tf.mouseWheelEnabled = true ; 
			_tf.multiline = true ; 
			_tf.textColor = 0x000000
			_tf.wordWrap = true ; 
 
			//add the TextField to the display list
			addChild ( _tf ) ;
 
		}
 
		//called when we wish to start StageVideo playback
		private function _enableStageVideo() : void {
			//if we do not have a StageVideo object reference 
			//already stored in _stageVideo
			if ( _stageVideo == null ) 
			{
				//set _stageVideo to reference the first of our
				//available StageVideo objects (up to 8 available on desktops)
				_stageVideo = stage.stageVideos [ 0 ] ;
				//size and position our _stageVideo with a new Rectangle
				_stageVideo.viewPort = new Rectangle ( 0 , 0 , 4320 , 1080 ) ;
			}
			//if the _video object is in the display list
			if ( _video.parent )
				//remove _video from the display list
				removeChild ( _video ) ;
			//attach our NetStream instance to our StageVideo instance
			_stageVideo.attachNetStream ( _ns ) ;
		}
 
		//called when StageVideo becomes unavailable
		//and we need to use a Video object as back-up
		private function _disableStageVideo() : void {
			//attach our NetStream to our Video object
			_video.attachNetStream ( _ns ) ;
			//add the Video object to the display list
			//(so that it can be seen)
			addChild ( _video ) ;
			//add the _tf TextField back to the display list
			//so that it is on top of the _video object
			addChild ( _tf ) ;
		}
 
 
 
		//--------------------------------------
		//  StageVideo Event Handlers
		//--------------------------------------
 
		//our callback function for the
		//StageVideoAvailabilityEvent.STAGE_VIDEO_AVAILABILITY event
		private function _onStageVideoAvailability ( evt : StageVideoAvailabilityEvent ) : void {
			//log the availability of StageVideo
			_tf.text = "Stage Video: " + evt.availability ;
			//if StageVideo is available
			if ( evt.availability )
				//call _enableStageVideo
				_enableStageVideo ( ) ;
			else 
				//otherwise call _disableStageVideo
				_disableStageVideo ( ) ;
		}
 
 
		//--------------------------------------
		//  NetStream Event Handlers
		//--------------------------------------
 
		//the NetStream will look for these two methods
		//on whatever object is identified as the NetStream's client
		//if they do not exist, you will receive a run time error
		//when the NetStream encounters metadata or cuepoints in the video
		public function onMetaData( info:Object ) : void { }
		public function onCuePoint( info:Object ) : void { }
 
	}
 
}