﻿package code
{
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	import flash.display.Bitmap;
	import starling.display.Image;

	public class MaskSprite extends Sprite
	{
		[Embed(source="../assets/background/mask.png")]
		private var AnimTexture:Class;		
		
		public function MaskSprite()
		{
			super();

			
			
			var flowerBitmap:Bitmap = new AnimTexture();
			var heroTexture:Texture = Texture.fromBitmap(flowerBitmap);
			var flower = new Image(heroTexture);
			addChild(flower);
			//Starling.juggler.add(flower);
			
			
		}
	}
}