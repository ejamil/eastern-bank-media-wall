﻿package code 
{
	
	import flash.display.MovieClip;
	
	import org.tuio.*;
	import org.tuio.adapters.*;
	import org.tuio.gestures.*;
	import org.tuio.debug.TuioDebug;
	import org.tuio.TuioManager;
	import org.tuio.connectors.*;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	
	import flash.net.FileReference;
	import flash.filesystem.*
	
	
	public class SaGm extends MovieClip 
	{
		
		public var playerNumber:Number;
		
		private var s:String = "";
		private var sa:String = "";
		private var sy:String = "";
		private var sm:String = "";
		
		public var score:Number = 0;
		public var monthlySavings:Number = 0;
		public var gArray:Array = new Array();
		public var gAYMArray:Array;
		
		public var whichG:Number = 0;
		public var whichField:Number = 0;
		
		public var playerGo:Boolean = false;
		
		public var ct:Number = 0;
		
		private var s1:String = "";
		private var s2:String = "";
		private var s3:String = "";
		private var s4:String = "";
		
		public var countdownTimer:Timer;
		public var time:Number = 10;
		public var gameTimer:Timer;
		
		public var owner:LoGm;
		
		public var whichQuest:Number = 0;
		
		public var icArray:Array;
		
		public var endTimer:Timer;
		
		public var thisQNum:Number;
		
		public var percTort:Number;
		public var percGo:Number;
		public var percGamb:Number;
		public var l:Number = 0;
		public var numberSpinner:TenMillionSpinner;
		
		public function SaGm(myOwner:LoGm) 
		{
			owner = myOwner;
			if (owner.owner.whatLang == "english")
			{
				l = 0;
			}
			else
			{
				l = 1;
			}
			/*for (var i:int = 0; i < 12; i++)
			{
				gAYMArray = new Array(0, 0, 0);
				gArray.push(gAYMArray);
			}*/
			
			q1.x = 1080;
			q2.x = 1080;
			q3.x = 1080;
			
			TweenMax.to(q1, .5, {x:0});
			TweenMax.to(q2, .5, {delay:.125, x:0});
			TweenMax.to(q3, .5, {delay:.25, x:0, onComplete:loadInitText});
			
			
			thisQNum = zeroOrOne();
			if (thisQNum == 1)
			{
				q1.ans.text = owner.owner.owner.loadXML.dreamSHQ1AnswerA[l];
				q2.ans.text = owner.owner.owner.loadXML.dreamSHQ1AnswerB[l];
				q3.ans.text = owner.owner.owner.loadXML.dreamSHQ1AnswerC[l];
				q.text = owner.owner.owner.loadXML.dreamSHQuestion1[l];
				//q3.ans.text = "THIS IS A TEST 1";
				//this.q.text = "THIS IS A TEST 1";
				whichQuest = 1;
			}
			else
			{
				q1.ans.text = owner.owner.owner.loadXML.dreamSHQ2AnswerA[l];
				q2.ans.text = owner.owner.owner.loadXML.dreamSHQ2AnswerB[l];
				q3.ans.text = owner.owner.owner.loadXML.dreamSHQ2AnswerC[l];
				//q3.ans.text = "THIS IS A TEST 2";
				q.text = owner.owner.owner.loadXML.dreamSHQuestion2[l];
				//this.q.text = "THIS IS A TEST 2";
				whichQuest = 2;
			}
			
			ins.x += 1;
			quest.x += 1;
			q.x += 1;
			
			ins.text = owner.owner.owner.loadXML.dreamTouchTheChoice[l];
			quest.text = owner.owner.owner.loadXML.lifeR1Question[l];
			
			icArray = new Array(ic1, ic2, ic3, ic4, ic5, ic6, ic7);
			
			
			for (var i:int = 0; i < icArray.length; i++)
			{
				if (owner.iconArray[i] == 0)
				{
					icArray[i].visible = false;
				}
				else
				{
					icArray[i].gotoAndStop(owner.iconArray[i]);
				}
			}
			
		}
		
		public function loadInitText():void
		{
			if (thisQNum == 1)
			{
				q.text = owner.owner.owner.loadXML.dreamSHQuestion1[l];
			}
			else
			{
				q.text = owner.owner.owner.loadXML.dreamSHQuestion2[l];
			}
			
			ins.text = owner.owner.owner.loadXML.dreamTouchTheChoice[l];
			quest.text = owner.owner.owner.loadXML.lifeR1Question[l];
			q1.addEventListener(TuioTouchEvent.TOUCH_DOWN, goNext);
			q2.addEventListener(TuioTouchEvent.TOUCH_DOWN, goNext);
			q3.addEventListener(TuioTouchEvent.TOUCH_DOWN, goNext);
		}
		
		public function tweenUs(c:Number):void
		{
			TweenMax.to(q1, .5, {x:-1080, onComplete:tweenAgain, onCompleteParams:[q1, c]});
			TweenMax.to(q2, .5, {delay:.125, x:-1080, onComplete:tweenAgain, onCompleteParams:[q2, c]});
			TweenMax.to(q3, .5, {delay:.25, x:-1080, onComplete:tweenAgain, onCompleteParams:[q3, c]});
			
		}
		
		public function tweenAgain(s:MovieClip, c:Number):void
		{
			//trace("$$$$$$$$$$$$$$$$$$$$$$$$$$$ tweenAgain");
			s.x = 1080;
			
			TweenMax.to(s, .5, {x:0});
			
			switch (s)
			{
				case q1:
					switch (c)
					{
						case 1:
							if (thisQNum == 1)
							{
								q1.ans.text = owner.owner.owner.loadXML.dreamSHQ3AnswerA[l];
							}
							else
							{
								q1.ans.text = owner.owner.owner.loadXML.dreamSHQ4AnswerA[l];
							}
						break;
						
						case 2:
							if (thisQNum == 1)
							{
								q1.ans.htmlText = owner.owner.owner.loadXML.dreamSHQ5AnswerA[l];
							}
							else
							{
								q1.ans.text = owner.owner.owner.loadXML.dreamSHQ6AnswerA[l];
							}
						break;
						
						case 3:
							if (thisQNum == 1)
							{
								q1.ans.text = owner.owner.owner.loadXML.dreamSHQ7AnswerA[l];
							}
							else
							{
								q1.ans.text = owner.owner.owner.loadXML.dreamSHQ8AnswerA[l];
							}
						break;
						
						case 4:
							q1.ans.text = owner.owner.owner.loadXML.dreamSHQ9AnswerA[l];
						break;
					}
					q1.addEventListener(TuioTouchEvent.TOUCH_DOWN, goNext);
					q1.gotoAndStop(1);
				break;
				
				case q2:
					switch (c)
					{
						case 1:
							if (thisQNum == 1)
							{
								q2.ans.text = owner.owner.owner.loadXML.dreamSHQ3AnswerB[l];
							}
							else
							{
								q2.ans.text = owner.owner.owner.loadXML.dreamSHQ4AnswerB[l];
							}
						break;
						
						case 2:
							if (thisQNum == 1)
							{
								q2.ans.htmlText = owner.owner.owner.loadXML.dreamSHQ5AnswerB[l];
							}
							else
							{
								q2.ans.text = owner.owner.owner.loadXML.dreamSHQ6AnswerB[l];
							}
						break;
						
						case 3:
							if (thisQNum == 1)
							{
								q2.ans.text = owner.owner.owner.loadXML.dreamSHQ7AnswerB[l];
							}
							else
							{
								q2.ans.text = owner.owner.owner.loadXML.dreamSHQ8AnswerB[l];
							}
						break;
						
						case 4:
							q2.ans.text = owner.owner.owner.loadXML.dreamSHQ9AnswerB[l];
						break;
					}
					q2.addEventListener(TuioTouchEvent.TOUCH_DOWN, goNext);
					q2.gotoAndStop(1);
				break;
				
				case q3:
					switch (c)
					{
						case 1:
							if (thisQNum == 1)
							{
								q3.ans.text = owner.owner.owner.loadXML.dreamSHQ3AnswerC[l];
							}
							else
							{
								q3.ans.text = owner.owner.owner.loadXML.dreamSHQ4AnswerC[l];
							}
						break;
						
						case 2:
							if (thisQNum == 1)
							{
								q3.ans.htmlText = owner.owner.owner.loadXML.dreamSHQ5AnswerC[l];
							}
							else
							{
								q3.ans.text = owner.owner.owner.loadXML.dreamSHQ6AnswerC[l];
							}
						break;
						
						case 3:
							if (thisQNum == 1)
							{
								q3.ans.text = owner.owner.owner.loadXML.dreamSHQ7AnswerC[l];
							}
							else
							{
								q3.ans.text = owner.owner.owner.loadXML.dreamSHQ8AnswerC[l];
							}
						break;
						
						case 4:
							q3.ans.htmlText = owner.owner.owner.loadXML.dreamSHQ9AnswerC[l];
						break;
					}
					q3.addEventListener(TuioTouchEvent.TOUCH_DOWN, goNext);
					q3.gotoAndStop(1);
				break;
			}
		}
		
		public function goNext(e:TuioTouchEvent = null):void
		{
			owner.playSound(1);
			if (q1 != null)
			{
				q1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goNext);
				q2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goNext);
				q3.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goNext);
				q1.gotoAndStop(1);
				q2.gotoAndStop(1);
				q3.gotoAndStop(1);
				e.currentTarget.gotoAndStop(2);
				setScore();
			}
			ct++;
			
			switch (ct)
			{
				case 1:
					n.text = "2";
					thisQNum = zeroOrOne();
					if (thisQNum == 1)
					{
						q.text = owner.owner.owner.loadXML.dreamSHQuestion3[l];
						whichQuest = 3;
					}
					else
					{
						q.text = owner.owner.owner.loadXML.dreamSHQuestion4[l];
						whichQuest = 4;
					}
					tweenUs(ct);
				break;
				case 2:
					n.text = "3";
					thisQNum = zeroOrOne();
					if (thisQNum == 1)
					{
						q.text = owner.owner.owner.loadXML.dreamSHQuestion5[l];
						whichQuest = 5;
						showDic();
					}
					else
					{
						q.text = owner.owner.owner.loadXML.dreamSHQuestion6[l];
						whichQuest = 6;
					}
					tweenUs(ct);
				break;
				case 3:
					n.text = "4";
					thisQNum = zeroOrOne();
					if (thisQNum == 1)
					{
						q.text = owner.owner.owner.loadXML.dreamSHQuestion7[l];
						whichQuest = 7;
					}
					else
					{
						q.text = owner.owner.owner.loadXML.dreamSHQuestion8[l];
						whichQuest = 8;
					}
					tweenUs(ct);
				break;
				case 4:
					n.text = "5";
					q.text = owner.owner.owner.loadXML.dreamSHQuestion9[l];
					whichQuest = 9;
					showDic();
					tweenUs(ct);
				break;
				case 5:
					this.nextFrame();
					owner.stopSound();
					owner.numberSpinner.visible = false;
					owner.scales.visible = false;
					ratings.t.visible = true;
					ratings.i.visible = true;
					//ratings.tortPerc.visible = false;
					ratings.tortGraph.visible = false;
					ratings.tortGraphS.visible = false;
					//ratings.tortT.visible = false;
					//ratings.goPerc.visible = false;
					ratings.goGraph.visible = false;
					ratings.goGraphS.visible = false;
					//ratings.goT.visible = false;
					//ratings.gambPerc.visible = false;
					ratings.gambGraph.visible = false;
					ratings.gambGraphS.visible = false;
					//ratings.gambT.visible = false;
					//trace("all done, score is: " + score);
					
					var file:File = new File();
					file.nativePath = "C:/MediaWall/stats.xml";
					var fileStream:FileStream = new FileStream();
					fileStream.open(file,FileMode.WRITE);
					var temp:XML = <Stat>0</Stat>;
					
					spst.text = owner.owner.owner.loadXML.dreamSpendStyle[l];
					sast.text = owner.owner.owner.loadXML.dreamSaveStyle[l];
					tipsBtn.t.text = owner.owner.owner.loadXML.dreamTips[l];
					t1.text = owner.owner.owner.loadXML.dreamWhatKindSaver1[l];
					t2.text = owner.owner.owner.loadXML.dreamWhatKindSaver2[l];
				
					saverIcon.sp.sp.gotoAndStop(l + 1);
					saverIcon.sa.gotoAndStop(l + 1);
						
					if (score <= 7)
					{
						temp = <Stat>tortoise</Stat>;
						owner.owner.owner.loadXML.numTort++;
						owner.playSound(10);
						savingsResult.gotoAndStop(1);
						saverIcon.ic.gotoAndStop(1);
						saverIcon.titleT.gotoAndStop(1);
						saverIcon.titleT.t.text = owner.owner.owner.loadXML.dreamTheTortoise[l];
						ratings.gotoAndStop(1);
						ratings.i.gotoAndStop(1);
						ratings.t.text = owner.owner.owner.loadXML.dreamTortoise1[l];
					}
					if (score > 7 && score <= 12)
					{
						temp = <Stat>gogetter</Stat>;
						owner.owner.owner.loadXML.numGo++;
						owner.playSound(6);
						savingsResult.gotoAndStop(2);
						saverIcon.ic.gotoAndStop(2);
						saverIcon.titleT.gotoAndStop(2);
						saverIcon.titleT.t.text = owner.owner.owner.loadXML.dreamTheGoGetter[l];
						ratings.gotoAndStop(2);
						ratings.i.gotoAndStop(2);
						ratings.t.text = owner.owner.owner.loadXML.dreamGoGetter1[l];
					}
					if (score > 12)
					{
						temp = <Stat>gambler</Stat>;
						owner.owner.owner.loadXML.numGamb++;
						owner.playSound(5);
						savingsResult.gotoAndStop(3);
						saverIcon.ic.gotoAndStop(3);
						saverIcon.titleT.gotoAndStop(3);
						saverIcon.titleT.t.text = owner.owner.owner.loadXML.dreamTheGambler[l];
						ratings.gotoAndStop(3);
						ratings.i.gotoAndStop(3);
						ratings.t.text = owner.owner.owner.loadXML.dreamGambler1[l];
					}
					savingsResult.t.gotoAndStop(l + 1);
					ratings.howEB.text = owner.owner.owner.loadXML.dreamCheckOutOtherEBVisitors[l];
						
					if (owner.howMuchMoney <= 400000)
					{
						spendingResult.gotoAndStop(1);
					}
					else
					{
						spendingResult.gotoAndStop(2);
					}
					
					spendingResult.t.gotoAndStop(l + 1);
					//ADD SPANISH LOGIC
					if (savingsResult.currentFrame == 1 && spendingResult.currentFrame == 1)
					{
						//match
						resultsMatch.gotoAndStop(1 + l);
						resultsP.text = owner.owner.owner.loadXML.dreamMatch1[l];
					}
					
					if (savingsResult.currentFrame == 1 && spendingResult.currentFrame == 2)
					{
						//mismatch
						resultsMatch.gotoAndStop(3 + l);
						resultsP.text = owner.owner.owner.loadXML.dreamMisMatch3[l];
					}
					
					if (savingsResult.currentFrame == 2 && spendingResult.currentFrame == 1)
					{
						//match
						resultsMatch.gotoAndStop(1 + l);
						resultsP.text = owner.owner.owner.loadXML.dreamMatch2[l];
					}
					
					if (savingsResult.currentFrame == 2 && spendingResult.currentFrame == 2)
					{
						//match
						resultsMatch.gotoAndStop(1 + l);
						resultsP.text = owner.owner.owner.loadXML.dreamMatch3[l];
					}
					
					if (savingsResult.currentFrame == 3 && spendingResult.currentFrame == 1)
					{
						//match
						resultsMatch.gotoAndStop(1 + l);
						resultsP.text = owner.owner.owner.loadXML.dreamMatch4[l];
					}
					
					if (savingsResult.currentFrame == 3 && spendingResult.currentFrame == 2)
					{
						//mismatch
						resultsMatch.gotoAndStop(3 + l);
						resultsP.text = owner.owner.owner.loadXML.dreamMisMatch4[l];
					}
					
					//trace("************************************** temp xml = " + temp);
					var oldXML:XML = owner.owner.owner.loadXML.oldXML;
					if (oldXML != null)
					{
						oldXML.appendChild(temp);
					}
					else
					{
						oldXML = temp;
					}
					
					var thingToExport:Object = oldXML;//{aProperty:"someValue"};
					var as3XMLMapper:String = XMLMaker.generateXML(thingToExport);
					fileStream.writeUTFBytes(as3XMLMapper);
					fileStream.close();
					
					var tot:Number = owner.owner.owner.loadXML.numTort + owner.owner.owner.loadXML.numGo + owner.owner.owner.loadXML.numGamb;
					//trace("tot = " + tot);
					percTort = Math.round(owner.owner.owner.loadXML.numTort/tot * 100);
					percGo = Math.round(owner.owner.owner.loadXML.numGo/tot * 100);
					percGamb = Math.round(owner.owner.owner.loadXML.numGamb/tot * 100);
					
					var total:Number = percTort + percGo + percGamb;
					while (total != 100)
					{
						//trace("while total = " + total);
						if (total > 100)
						{
							percGamb--;
						}
						if (total < 100)
						{
							percGamb++;
						}
						total = percTort + percGo + percGamb;
					}
					
					//trace("percTort = " + percTort + ", percGo = " + percGo + ", percGamb = " + percGamb);
					ratings.tortGraph.gotoAndStop(percTort);
					ratings.goGraph.gotoAndStop(percGo);
					ratings.gambGraph.gotoAndStop(percGamb);
					ratings.tortGraph.tortPerc.text = String(percTort) + "%";
					ratings.goGraph.goPerc.text = String(percGo) + "%";
					ratings.gambGraph.gambPerc.text = String(percGamb) + "%";
					
					ratings.tortGraphS.gotoAndStop(percTort);
					ratings.goGraphS.gotoAndStop(percGo);
					ratings.gambGraphS.gotoAndStop(percGamb);
					ratings.tortGraphS.tortPerc.text = String(percTort) + "%";
					ratings.goGraphS.goPerc.text = String(percGo) + "%";
					ratings.gambGraphS.gambPerc.text = String(percGamb) + "%";
					
					saverIcon.play();
					tipsBtn.addEventListener(TuioTouchEvent.TOUCH_DOWN, goNext);
					graphBox.addEventListener(TuioTouchEvent.TOUCH_DOWN, openRatings);
					tipsBtn.x = -tipsBtn.width;
					TweenMax.to(tipsBtn, 1, {x:650.15});
					
					numberSpinner = new TenMillionSpinner();
					//numberSpinner.x = 1080 - numberSpinner.width;
					//numberSpinner.y = 1920 - numberSpinner.height;
					addChild(numberSpinner);
					numberSpinner.x = 354;
					numberSpinner.y = 1285;
					numberSpinner.init(owner.howMuchMoney);
					numberSpinner.stopAnimation();
					
					owner.playSound(15);
					TweenMax.delayedCall(1.2, owner.playSound, [16]);
				break;
				case 6:
					this.nextFrame();
					exitBtn.x = -exitBtn.width;
					TweenMax.to(exitBtn, 1, {x:650.15});
					TweenMax.to(tipsBtn, 1, {x:1080});
					tipsBtn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goNext);
					exitBtn.addEventListener(TuioTouchEvent.TOUCH_DOWN, goNext);
					exitBtn.t.text = owner.owner.owner.loadXML.dreamNext[l];
					
					if (score <= 7)
					{
						//infoBox.gotoAndStop(1);
						infoBox.inst.text = owner.owner.owner.loadXML.dreamTortoise2[l];
						infoBox.t.text = "";
						infoBox.ic1.tw.text = owner.owner.owner.loadXML.dream401K[l];
						infoBox.ic1.tb.text = owner.owner.owner.loadXML.dream401K[l];
						infoBox.ic2.tw.text = owner.owner.owner.loadXML.dreamRothIRA[l];
						infoBox.ic2.tb.text = owner.owner.owner.loadXML.dreamRothIRA[l];
						infoBox.ic1.tb.visible = false;
						infoBox.ic2.tb.visible = false;
						infoBox.ic1.addEventListener(TuioTouchEvent.TOUCH_DOWN, goK401);
						infoBox.ic2.addEventListener(TuioTouchEvent.TOUCH_DOWN, goRoth);
					}
					if (score > 7 && score <= 12)
					{
						
						infoBox.inst.text = owner.owner.owner.loadXML.dreamGoGetter2[l];
						infoBox.t.text = "";
						infoBox.ic1.tw.text = owner.owner.owner.loadXML.dreamBuyAHouse[l];
						infoBox.ic1.tb.text = owner.owner.owner.loadXML.dreamBuyAHouse[l];
						infoBox.ic2.tw.text = owner.owner.owner.loadXML.dreamBuildCredit[l];
						infoBox.ic2.tb.text = owner.owner.owner.loadXML.dreamBuildCredit[l];
						infoBox.ic1.tb.visible = false;
						infoBox.ic2.tb.visible = false;
						infoBox.ic1.addEventListener(TuioTouchEvent.TOUCH_DOWN, goBuyh);
						infoBox.ic2.addEventListener(TuioTouchEvent.TOUCH_DOWN, goBuildc);
					}
					if (score > 12)
					{
						
						infoBox.inst.text = owner.owner.owner.loadXML.dreamGambler2[l];
						infoBox.t.text = "";
						infoBox.ic1.tw.text = owner.owner.owner.loadXML.dreamAutomaticSavingsPlan[l];
						infoBox.ic1.tb.text = owner.owner.owner.loadXML.dreamAutomaticSavingsPlan[l];
						infoBox.ic2.tw.text = owner.owner.owner.loadXML.dreamStocks[l];
						infoBox.ic2.tb.text = owner.owner.owner.loadXML.dreamStocks[l];
						infoBox.ic1.tb.visible = false;
						infoBox.ic2.tb.visible = false;
						infoBox.ic1.addEventListener(TuioTouchEvent.TOUCH_DOWN, goSavp);
						infoBox.ic2.addEventListener(TuioTouchEvent.TOUCH_DOWN, goStocks);
					}
					
				break;
				case 7:
					owner.countdown();
				break;

			}
		}
		
		public function openRatings(e:TuioTouchEvent):void
		{
			graphBox.removeEventListener(TuioTouchEvent.TOUCH_DOWN, openRatings);
			graphBox.addEventListener(TuioTouchEvent.TOUCH_DOWN, closeRatings);
			//ratings.gotoAndStop(2);
			ratings.t.visible = false;
			ratings.i.visible = false;
			if (l == 0)
			{
				ratings.tortGraph.visible = true;
				ratings.goGraph.visible = true;
				ratings.gambGraph.visible = true;
			}
			else
			{
				ratings.tortGraphS.visible = true;
				ratings.goGraphS.visible = true;
				ratings.gambGraphS.visible = true;
			}
		}
		
		public function closeRatings(e:TuioTouchEvent):void
		{
			graphBox.removeEventListener(TuioTouchEvent.TOUCH_DOWN, closeRatings);
			graphBox.addEventListener(TuioTouchEvent.TOUCH_DOWN, openRatings);
			//ratings.gotoAndStop(1);
			ratings.t.visible = true;
			ratings.i.visible = true;
			//ratings.tortPerc.visible = false;
			ratings.tortGraph.visible = false;
			ratings.tortGraphS.visible = false;
			//ratings.tortT.visible = false;
			//ratings.goPerc.visible = false;
			ratings.goGraph.visible = false;
			ratings.goGraphS.visible = false;
			//ratings.goT.visible = false;
			//ratings.gambPerc.visible = false;
			ratings.gambGraph.visible = false;
			ratings.gambGraphS.visible = false;
			//ratings.gambT.visible = false;
		}
		
		public function resetEndTimer(e:TuioTouchEvent = null):void
		{
			/*endTimer.stop();
			endTimer.removeEventListener(TimerEvent.TIMER, allDone);
			endTimer = new Timer(20000);
			endTimer.stop();
			endTimer.start();
			endTimer.addEventListener(TimerEvent.TIMER, allDone);*/
		}
		
		public function allDone(e:TimerEvent):void
		{
			endTimer.stop();
			endTimer.removeEventListener(TimerEvent.TIMER, allDone);
			removeEventListener(TuioTouchEvent.TOUCH_DOWN, resetEndTimer);
			owner.allDone();
		}
		
		public function goK401(e:TuioTouchEvent):void
		{
			owner.playSound(1);
			//resetEndTimer();
			if (infoBox != null)
			{
				infoBox.ic1.gotoAndStop(2);
				infoBox.ic2.gotoAndStop(1);
				infoBox.ic1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goK401);
				infoBox.ic2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goRoth);
				infoBox.ic2.addEventListener(TuioTouchEvent.TOUCH_DOWN, goRoth);
				infoBox.gotoAndStop(2);
				infoBox.ic1.tb.visible = true;
				infoBox.ic2.tb.visible = false;
				infoBox.ic1.tw.visible = false;
				infoBox.ic2.tw.visible = true;
				infoBox.t.text = owner.owner.owner.loadXML.dreamTortoise3[l] + "\n" + owner.owner.owner.loadXML.dreamTortoise4[l];
			}
		}
		
		public function goRoth(e:TuioTouchEvent):void
		{
			owner.playSound(1);
			//resetEndTimer();
			if (infoBox != null)
			{
				infoBox.ic1.gotoAndStop(1);
				infoBox.ic2.gotoAndStop(2);
				infoBox.ic2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goRoth);
				infoBox.ic1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goK401);
				infoBox.ic1.addEventListener(TuioTouchEvent.TOUCH_DOWN, goK401);
				infoBox.gotoAndStop(3);
				infoBox.ic1.tb.visible = false;
				infoBox.ic2.tb.visible = true;
				infoBox.ic1.tw.visible = true;
				infoBox.ic2.tw.visible = false;
				infoBox.t.text = owner.owner.owner.loadXML.dreamTortoise5[l] + "\n" + owner.owner.owner.loadXML.dreamTortoise6[l];
			}
		}
		
		public function goBuyh(e:TuioTouchEvent):void
		{
			owner.playSound(1);
			//resetEndTimer();
			if (infoBox != null)
			{
				infoBox.ic1.gotoAndStop(2);
				infoBox.ic2.gotoAndStop(1);
				infoBox.ic1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goBuyh);
				infoBox.ic2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goBuildc);
				infoBox.ic2.addEventListener(TuioTouchEvent.TOUCH_DOWN, goBuildc);
				infoBox.gotoAndStop(5);
				infoBox.ic1.tb.visible = true;
				infoBox.ic2.tb.visible = false;
				infoBox.ic1.tw.visible = false;
				infoBox.ic2.tw.visible = true;
				infoBox.t.text = owner.owner.owner.loadXML.dreamGoGetter3[l] + "\n" + owner.owner.owner.loadXML.dreamGoGetter4[l];
			}
		}
		
		public function goBuildc(e:TuioTouchEvent):void
		{
			owner.playSound(1);
			//resetEndTimer();
			if (infoBox != null)
			{
				infoBox.ic1.gotoAndStop(1);
				infoBox.ic2.gotoAndStop(2);
				infoBox.ic2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goBuildc);
				infoBox.ic1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goBuyh);
				infoBox.ic1.addEventListener(TuioTouchEvent.TOUCH_DOWN, goBuyh);
				infoBox.gotoAndStop(6);
				infoBox.ic1.tb.visible = false;
				infoBox.ic2.tb.visible = true;
				infoBox.ic1.tw.visible = true;
				infoBox.ic2.tw.visible = false;
				infoBox.t.text = owner.owner.owner.loadXML.dreamGoGetter5[l];
			}
		}
		
		public function goSavp(e:TuioTouchEvent):void
		{
			owner.playSound(1);
			//resetEndTimer();
			if (infoBox != null)
			{
				infoBox.ic1.gotoAndStop(2);
				infoBox.ic2.gotoAndStop(1);
				infoBox.ic1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goSavp);
				infoBox.ic2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goStocks);
				infoBox.ic2.addEventListener(TuioTouchEvent.TOUCH_DOWN, goStocks);
				infoBox.gotoAndStop(8);
				infoBox.ic1.tb.visible = true;
				infoBox.ic2.tb.visible = false;
				infoBox.ic1.tw.visible = false;
				infoBox.ic2.tw.visible = true;
				infoBox.t.text = owner.owner.owner.loadXML.dreamGambler3[l];
			}
		}
		
		public function goStocks(e:TuioTouchEvent):void
		{
			owner.playSound(1);
			//resetEndTimer();
			if (infoBox != null)
			{
				infoBox.ic1.gotoAndStop(1);
				infoBox.ic2.gotoAndStop(2);
				infoBox.ic2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goStocks);
				infoBox.ic1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goSavp);
				infoBox.ic1.addEventListener(TuioTouchEvent.TOUCH_DOWN, goSavp);
				infoBox.gotoAndStop(9);
				infoBox.ic1.tb.visible = false;
				infoBox.ic2.tb.visible = true;
				infoBox.ic1.tw.visible = true;
				infoBox.ic2.tw.visible = false;
				infoBox.t.text = owner.owner.owner.loadXML.dreamGambler4[l];
			}
		}
		
		public function showDic():void
		{
			switch (whichQuest)
			{
				case 5:
				
				break;
				
				case 9:
				
				break;
				
				default:
					//trace("no dictionary");
				break;
			}
		}
		
		public function setScore():void
		{
			switch (whichQuest)
			{
				case 1:
					if (q1.currentFrame == 2)
					{
						score += 1;
					}
					if (q2.currentFrame == 2)
					{
						score += 2;
					}
					if (q3.currentFrame == 2)
					{
						score += 3;
					}
				break;
				case 2:
					if (q1.currentFrame == 2)
					{
						score += 3;
					}
					if (q2.currentFrame == 2)
					{
						score += 2;
					}
					if (q3.currentFrame == 2)
					{
						score += 1;
					}
				break;
				case 3:
					if (q1.currentFrame == 2)
					{
						score += 3;
					}
					if (q2.currentFrame == 2)
					{
						score += 1;
					}
					if (q3.currentFrame == 2)
					{
						score += 2;
					}
				break;
				case 4:
					if (q1.currentFrame == 2)
					{
						score += 1;
					}
					if (q2.currentFrame == 2)
					{
						score += 2;
					}
					if (q3.currentFrame == 2)
					{
						score += 3;
					}
				break;
				case 5:
					if (q1.currentFrame == 2)
					{
						score += 1;
					}
					if (q2.currentFrame == 2)
					{
						score += 3;
					}
					if (q3.currentFrame == 2)
					{
						score += 2;
					}
				break;
				case 6:
					if (q1.currentFrame == 2)
					{
						score += 2;
					}
					if (q2.currentFrame == 2)
					{
						score += 1;
					}
					if (q3.currentFrame == 2)
					{
						score += 3;
					}
				break;
				case 7:
					if (q1.currentFrame == 2)
					{
						score += 2;
					}
					if (q2.currentFrame == 2)
					{
						score += 1;
					}
					if (q3.currentFrame == 2)
					{
						score += 3;
					}
				break;
				case 8:
					if (q1.currentFrame == 2)
					{
						score += 2;
					}
					if (q2.currentFrame == 2)
					{
						score += 1;
					}
					if (q3.currentFrame == 2)
					{
						score += 3;
					}
				break;
				case 9:
					if (q1.currentFrame == 2)
					{
						score += 3;
					}
					if (q2.currentFrame == 2)
					{
						score += 1;
					}
					if (q3.currentFrame == 2)
					{
						score += 2;
					}
				break;
			}
			//trace("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% score = " + score);
			//resetQuestions();
		}
		
		public function zeroOrOne():Number
		{
			var num:Number = Math.floor(Math.random() * 2);
			return num;
		}
		
	}
	
}
