﻿package code
{
    import flash.utils.describeType;

    import mx.collections.IList;

    public class XMLMaker
    {

        public function XMLMaker()
        {
        }

        public static function generateXML(objectToMap:Object, basePropertyName:String="root"):String
        {
			//trace("generateXML.objectToMap = " + objectToMap);
            var describeXML:XML = describeType(objectToMap);

           // var xmlOutput:String = "<"+basePropertyName+">\n";
			var xmlOutput:String = "";
            if(describeXML.@isDynamic=="true")
            {
                for(var property:String in objectToMap)
                {
                    xmlOutput += "<"+property+">";
                    xmlOutput += objectToMap[property];
                    xmlOutput += "</"+property+">";
                }
            }
            else if(objectToMap is XML)
            {
                xmlOutput+=(objectToMap as XML).toString();
            }
            else if(objectToMap is XMLList)
            {
                xmlOutput+=(objectToMap as XMLList).toString();
            }
            else
            {
                for each(var accessor:XML in describeXML..accessor)
                {
                    xmlOutput+="\t"+exportProperty(objectToMap, accessor,true);
                }
                for each(var variable:XML in describeXML..variable)
                {
                    xmlOutput+="\t"+exportProperty(objectToMap, variable, false);
                }
            }

           // xmlOutput += "\n</"+basePropertyName+">";
           // trace(xmlOutput);
            return xmlOutput;
        }

        private static function exportProperty(objectToMap:Object, xmlObj:XML, isAccessor:Boolean):String
        {
            var xmlOutput:String="";
            var propName:String = xmlObj.@name.toString();
            var objectValue:Object = objectToMap[propName];
            if(!objectValue)
            {
                xmlOutput += "<"+propName+">";
                xmlOutput += "</"+propName+">";
                return xmlOutput;
            }

            if(isAccessor && xmlObj.@access != "readwrite")
            {
                return "";
            }
            if(objectValue is Array)
            {
                return exportArray(objectValue as Array, xmlObj.@name);
            }
            else if(objectValue is IList)
            {
                return exportArray((objectValue as IList).toArray(), propName);
            }
            else if(objectValue is int || objectValue is Number || objectValue is String || objectValue is uint || objectValue is Boolean)
            {
                xmlOutput += "<"+propName+" type=\""+xmlObj.@type+"\">";

                xmlOutput += objectValue;
                xmlOutput += "</"+propName+">"; 
            }
            else
            {
                return generateXML(objectValue, propName);
            }
            return xmlOutput;
        }

        private static function exportArray(array:Array, arrayName:String):String
        {
            var xmlOutput:String = "<"+arrayName+">\n";

            for each(var element:Object in array)
            {
                xmlOutput+="\t"+generateXML(element,"arrayElement");
            }

            xmlOutput += "</"+arrayName+">\n";
            return xmlOutput;
        }
    }
}