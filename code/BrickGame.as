﻿package code
{	
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	import flash.utils.getTimer;
	import flash.ui.Mouse;
	import flash.geom.Point;
	
	import code.*;
	import org.tuio.*;
	import org.tuio.adapters.*;
	import org.tuio.debug.TuioDebug;
	import org.tuio.TuioManager;
	import org.tuio.connectors.*;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	public class BrickGame extends MovieClip
	{	
		private var currentLvl:int = 1;
		private var lvlArray:Array;
		public var ballXSpeed:Number = 30;
		public var ballYSpeed:Number = 30;
		public var brickAmt:int = 0;
		private var lives:int = 3;
		public var gameOver:Boolean = false;
		public var score:int = 0;
		private var owner:Document;
		public var mcBall:McBall;
		public var mcPaddle:Paddle;
		public var whichPong:Number;
		public var whichScreen:Number;
		private var closeLarge:CloseLarge;
		private var closeoutTimer:Timer;
		
		public function BrickGame(xPos:Number, yPos:Number, myOwner:Document, w:Number, s:Number) 
		{
			owner = myOwner;
			owner = myOwner;
			
			
			whichPong = w;
			whichScreen = s;
			this.x = xPos;
			this.y = yPos;
			
			//if the mouse clicks, then begin the game
			addEventListener(TuioTouchEvent.TOUCH_DOWN, beginCode);
			addEventListener(Event.ENTER_FRAME, updateTextFields);
			brickGame.txtStart.text = "Touch the paddle to Begin";
			
			//making the level
			buildLevels();
			makeLvl();
			
			
			
			closeLarge = new CloseLarge();
			addChild(closeLarge);
			closeLarge.x = 1080; // - closeLarge.width;
			closeLarge.y = 1920; // - closeLarge.height;
			closeLarge.rotation = 90;
			closeLarge.endTxt.text == " "
			closeLarge.addEventListener(TuioTouchEvent.TOUCH_DOWN, endClickX);
			
			closeoutTimer = new Timer(180000, 1);
			closeoutTimer.start();
			closeoutTimer.addEventListener(TimerEvent.TIMER_COMPLETE, timeout);
			addEventListener(TuioTouchEvent.TOUCH_DOWN, movePaddle);
		}
		
		

		private function timeout(e:TimerEvent):void
		{
			closeoutTimer.reset();
			closeoutTimer.stop();
			closeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
			owner.removeNewBrick(whichPong, whichScreen);
		
		}

		
		private function endClickX(e:TuioTouchEvent):void
		{
			
			
			//reset all assets and play again!
			//isEnding = false;
			
			if (closeoutTimer != null)
			{
				closeoutTimer.reset();
				closeoutTimer.stop();
				closeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
			}
				
			owner.removeNewBrick(whichPong, whichScreen);
		}
		
		private function buildLevels()
		{
			//The array code All of the later levels add one more row of bricks
			var lvl1Code:Array = new Array(1,1,1,1,1,1,1,1);
			var lvl2Code:Array = new Array(1,0,1,0,0,1,0,1,
										   1,1,0,1,1,0,1,1);
			var lvl3Code:Array = new Array(1,1,1,1,1,1,1,1,
										   1,1,0,0,0,0,1,1,
										   1,1,1,1,1,1,1,1);
			var lvl4Code:Array = new Array(0,1,1,1,1,1,1,0,
										   1,0,0,1,1,0,0,1,
										   1,0,0,1,1,0,0,1,
										   0,1,1,1,1,1,1,0);
			var lvl5Code:Array = new Array(0,1,1,1,0,1,1,0,
										   0,1,0,0,0,1,0,1,
										   0,1,1,0,0,1,1,0,
										   0,1,0,0,0,1,0,1,
										   0,1,1,1,0,1,1,0);
			//The array that contains all of the level codes
			lvlArray = new Array(lvl1Code, lvl2Code, lvl3Code, lvl4Code);//, lvl5Code
			
			mcBall = new McBall();
			mcBall.x = 526;
			mcBall.y = 1500;
			addChild(mcBall);
			
			mcPaddle = new Paddle(this.x, this.y, 1080);
			mcPaddle.x = 445;
			mcPaddle.y = 1600;
			addChild(mcPaddle);
		}
		
		function beginCode(event:TuioTouchEvent):void
		{
			closeoutTimer.reset();
			closeoutTimer.start();
			//removes the listener for a click
			owner.stage.removeEventListener(TuioTouchEvent.TOUCH_DOWN, beginCode);
			//Adds a listener to the paddle which runs a function every time a frame passes
			//mcPaddle.addEventListener(Event.ENTER_FRAME, movePaddle);
			//Adds a listener to the ball which runs a function every time a frame passes
			mcBall.addEventListener(Event.ENTER_FRAME, moveBall);
			
			//adding a listener to check if the level is done
			this.addEventListener(Event.ENTER_FRAME, checkLevel);
			
			//removing the "Click to Start" Text
			brickGame.txtStart.text = '';
		}
		
		function movePaddle(event:TuioTouchEvent):void
		{
			closeoutTimer.reset();
			closeoutTimer.start();
		}
		
		function moveBall(event:Event):void
		{
			//Code for moving ball goes here
			if (mcBall.x <= 0)
			{
				ballXSpeed *=  -1;
				mcBall.x = 1;
			}
			else if (mcBall.x >= owner.loadXML.screenWidth/4 - mcBall.width)
			{
				ballXSpeed *=  -1;
				mcBall.x = owner.loadXML.screenWidth/4 - mcBall.width - 1;
			}
			/*if (mcBall.hitTestObject(mcPaddle))
			{
				ballYSpeed *=  -1;
				//mcBall.ball.play();
				//mcPaddle.play();
				mcBall.y = mcPaddle.y - mcBall.height;
				checkHitLocation(mcPaddle);
			}*/
			var temp:Number = lives - 1;
			brickGame.txtStats.text = "Level: "+currentLvl+"     Lives: "+temp+"     Score: "+score;
			if (mcBall.y >= owner.loadXML.screenHeight - mcBall.height)
			{
				mcBall.removeEventListener(Event.ENTER_FRAME, moveBall);
				this.removeEventListener(Event.ENTER_FRAME, checkLevel);
				this.removeEventListener(Event.ENTER_FRAME, updateTextFields);
				addEventListener(TuioTouchEvent.TOUCH_DOWN, beginCode);
				
				lives --;
				
				
				mcBall.x = 526;
				mcBall.y = 1500;
				mcPaddle.x = 445;
				
				//if there aren't any lives left
				if(lives <= 0)
				{
					//the game is over now
					gameOver = true;
					//remove those silly listeners
					//mcPaddle.removeEventListener(Event.ENTER_FRAME, movePaddle);
					mcBall.removeEventListener(Event.ENTER_FRAME, moveBall);
					this.removeEventListener(Event.ENTER_FRAME, checkLevel);
					this.removeEventListener(Event.ENTER_FRAME, updateTextFields);
					
					mcBall.x = 526;
					mcBall.y = 1500;
					mcPaddle.x = 445;
				
					//go to a lose frame
					youLose();
				}
				else
				{
					brickGame.txtStart.text = "Touch the Paddle to Try Again";
				}
			}
			//Bouncing the ball off of the walls
			/*if(mcBall.x >= this.x + 1080 - mcBall.width){
				//if the ball hits the right side of the screen, then bounce off
				ballXSpeed *= -1;
			}
			if(mcBall.x <= this.x){
				//if the ball hits the left side of the screen, then bounce off
				ballXSpeed *= -1;
			}*/
			/*if(mcBall.y >= owner.stage.stageHeight-mcBall.height)
			{
				//mcPaddle.removeEventListener(Event.ENTER_FRAME, movePaddle);
				mcBall.removeEventListener(Event.ENTER_FRAME, moveBall);
				this.removeEventListener(Event.ENTER_FRAME, checkLevel);
				this.removeEventListener(Event.ENTER_FRAME, updateTextFields);
				owner.stage.addEventListener(TuioTouchEvent.TOUCH_DOWN, beginCode);
				
				lives --;
				brickGame.txtStats.text = "Level: "+currentLvl+"     Lives: "+lives+"     Score: "+score;
				
				mcBall.x = 526;
				mcBall.y = 1500;
				mcPaddle.x = 445;
				
				//if there aren't any lives left
				if(lives <= 0)
				{
					//the game is over now
					gameOver = true;
					//remove those silly listeners
					//mcPaddle.removeEventListener(Event.ENTER_FRAME, movePaddle);
					mcBall.removeEventListener(Event.ENTER_FRAME, moveBall);
					this.removeEventListener(Event.ENTER_FRAME, checkLevel);
					this.removeEventListener(Event.ENTER_FRAME, updateTextFields);
					
					mcBall.x = 526;
					mcBall.y = 1500;
					mcPaddle.x = 445;
				
					//go to a lose frame
					youLose();
				}
				else
				{
					brickGame.txtStart.text = "Touch the Paddle to Try Again";
				}
			}*/
			if(mcBall.y <= 0)
			{
				//if the ball hits the top then bounce down
				ballYSpeed *= -1;
			}
			//Hitting the paddle
			if(mcBall.hitTestObject(mcPaddle))
			{
				calcBallAngle();
			}
			mcBall.x += ballXSpeed; //Move the ball horizontally
			mcBall.y += ballYSpeed; //Move the ball vertically
		}
		
		function calcBallAngle():void
		{
			//ballPosition is the position of the ball is on the paddle
			var ballPosition:Number = mcBall.x - mcPaddle.x;
			//hitPercent converts ballPosition into a percent
			//All the way to the left is -.5
			//All the way to the right is .5
			//The center is 0
			var hitPercent:Number = (ballPosition / (mcPaddle.width - mcBall.width)) - .5;
			//Gets the hitPercent and makes it a larger number so the ball actually bounces
			ballXSpeed = hitPercent * 30;
			//Making the ball bounce back up
			ballYSpeed *= -1.03;
		}
		
		function makeLvl():void
		{
			//trace("makeLvl");
			//checking if indeed there are any more levels left
			if(currentLvl > lvlArray.length){
				//trace("if(currentLvl > lvlArray.length)");
				//the game is over now
				gameOver = true;
				//remove those silly listeners
				//mcPaddle.removeEventListener(Event.ENTER_FRAME, movePaddle);
				mcBall.removeEventListener(Event.ENTER_FRAME, moveBall);
				this.removeEventListener(Event.ENTER_FRAME, checkLevel);
				this.removeEventListener(Event.ENTER_FRAME, updateTextFields);
				//go to a lose frame
				youWin();
			}
			//finding the array length of the lvl code. The index has to be currentLvl-1 because: 
			//indexes start on 0 and our lvl starts at 1 our level will always be 1 higher than the actual index
			var arrayLength:int = lvlArray[currentLvl-1].length;
			
			//the current row of bricks we are creating
			var brickRow:int = 0;
			
			//Now, creating a loop which places the bricks onto the stage
			for(var i:int = 0;i<arrayLength;i++){
				//trace("for(var i:int = 0;i<arrayLength;i++)");
				//checking if it should place a brick there
				if(lvlArray[currentLvl-1][i] == 1){
				//trace("if(lvlArray[currentLvl-1][i] == 1)");
					//creating a variable which holds the brick instance
					var brick:Brick = new Brick(this);
					
					//checks if the current brick needs a new row
					for(var c:int = 1;c<=10;c++){
						if(i == c*8){
							//trace(c);
							brickRow ++;
						}
					}
					
					//setting the brick's coordinates via the i variable and brickRow
					brick.x = 10+(i-brickRow*8)*134;
					brick.y = 10+brickRow*50;
					
					if(currentLvl == 1){
						brick.gotoAndStop(1);
					}
					if(currentLvl == 2){
						if(brickRow == 0){
							brick.gotoAndStop(2);
						}
						if(brickRow == 1){
							brick.gotoAndStop(1);
						}
					}
					if(currentLvl == 3){
						if(brickRow == 0){
							brick.gotoAndStop(3);
						}
						if(brickRow == 1){
							brick.gotoAndStop(2);
						}
						if(brickRow == 2){
							brick.gotoAndStop(1);
						}
					}
					if(currentLvl == 4){
						if(brickRow == 0){
							brick.gotoAndStop(4);
						}
						if(brickRow == 1){
							brick.gotoAndStop(3);
						}
						if(brickRow == 2){
							brick.gotoAndStop(2);
						}
						if(brickRow == 3){
							brick.gotoAndStop(1);
						}
					}
					if(currentLvl == 5){
						if(brickRow == 0){
							brick.gotoAndStop(5);
						}
						if(brickRow == 1){
							brick.gotoAndStop(4);
						}
						if(brickRow == 2){
							brick.gotoAndStop(3);
						}
						if(brickRow == 3){
							brick.gotoAndStop(2);
						}
						if(brickRow == 4){
							brick.gotoAndStop(1);
						}
					}
				
					//finally, add the brick to stage
					addChild(brick);
				}
			}
		}
		
		function checkLevel(event:Event):void
		{
			//checking if the bricks are all gone
			if(brickAmt == 0){
				//reset the level by increasing the level
				currentLvl ++;
				ballXSpeed = 30;
				ballYSpeed = 30;
				//and re-running makeLvl
				//resetting the text's word
				brickGame.txtStart.text = "Touch the Paddle to Begin";
				makeLvl();
				//then resetting the ball's and paddle's position
				mcBall.x = 526;
				mcBall.y = 1500;
				mcPaddle.x = 445;
				//then removing all of the listeners
				//mcPaddle.removeEventListener(Event.ENTER_FRAME, movePaddle);
				mcBall.removeEventListener(Event.ENTER_FRAME, moveBall);
				removeEventListener(Event.ENTER_FRAME, checkLevel);
				//then listening for a mouse click to start the game again
				addEventListener(TuioTouchEvent.TOUCH_DOWN, beginCode);
				updateTextFields(null);
			}
		}
		
		public function updateTextFields(event:Event):void
		{
			var temp:Number = lives - 1;
			brickGame.txtStats.text = "Level: "+currentLvl+"     Lives: "+temp+"     Score: "+score;
		}
		
		function youLose():void
		{
			addEventListener(TuioTouchEvent.TOUCH_DOWN, endClickX);
			//closeLarge.addEventListener(TuioTouchEvent.TOUCH_DOWN, endClickX);
			//mcPaddle.removeEventListener(Event.ENTER_FRAME, movePaddle);
			mcBall.removeEventListener(Event.ENTER_FRAME, moveBall);
			removeEventListener(Event.ENTER_FRAME, checkLevel);
			
			brickGame.txtStart.text = "Touch to Close";
			brickGame.txtScore.text = "Final Score: "+score;
			ballXSpeed = 30;
			ballYSpeed = 30;

			mcPaddle.visible = false;
			mcBall.visible = false;
		}
		
		function youWin():void
		{
			addEventListener(TuioTouchEvent.TOUCH_DOWN, endClickX);
			//mcPaddle.removeEventListener(Event.ENTER_FRAME, movePaddle);
			mcBall.removeEventListener(Event.ENTER_FRAME, moveBall);
			removeEventListener(Event.ENTER_FRAME, checkLevel);
			
			brickGame.txtStart.text = "Touch to Close";
			brickGame.txtScore.text = "Final Score: "+score;
			ballXSpeed = 30;
			ballYSpeed = 30;

			mcPaddle.visible = false;
			mcBall.visible = false;
		}
		
		function resetGame(event:TuioTouchEvent):void
		{
			currentLvl = 1;
			lives = 3;
			score = 0;
			updateTextFields(null);
			buildLevels();
			makeLvl();
			closeoutTimer.reset();
			closeoutTimer.start();
			//removing this listener
			removeEventListener(TuioTouchEvent.TOUCH_DOWN, resetGame);
			addEventListener(TuioTouchEvent.TOUCH_DOWN, movePaddle);
			//mcPaddle.removeEventListener(Event.ENTER_FRAME, movePaddle);
			
			ballXSpeed = 30;
			ballYSpeed = 30;
			brickGame.txtScore.text = "";
			
			mcBall.x = 526;
			mcBall.y = 1500;
			mcPaddle.x = 445;
			//then removing all of the listeners
			//mcPaddle.removeEventListener(Event.ENTER_FRAME, movePaddle);
			mcBall.removeEventListener(Event.ENTER_FRAME, moveBall);
			removeEventListener(Event.ENTER_FRAME, checkLevel);
			//then listening for a mouse click to start the game again
			addEventListener(TuioTouchEvent.TOUCH_DOWN, beginCode);
			updateTextFields(null);
				
			brickGame.txtStart.text = '';
			mcPaddle.visible = true;
			mcBall.visible = true;
		}
	}
}
