﻿package code {
	
	import flash.display.MovieClip;
	
	import com.greensock.TweenMax;
	import com.greensock.TimelineMax;
	import com.greensock.easing.*
	import com.greensock.events.*;
	import flash.filters.DropShadowFilter;
	import flash.filters.BitmapFilterQuality;
	import code.Document;
	
	public class Icons extends MovieClip {
		
		private var playTimeline:TimelineMax;
		private var sWidth:Number;
		private var sHeight:Number;
		private var buffer:Number = 200;
		public var whichWay:Number;
		private var yOffset:Number;
		private var speed1:Number;
		private var speed2:Number;
		private var speed3:Number;
		
		private var whichOrder:Number;
		
		public var whichOne:String;
		public var whichIcon:Number;
		
		private var whatDelay:Number;
		
		private var shadowAlpha:Number = .5;
		private var shadowAngle:Number = 45;
		private var shadowBlurX:Number = 8;
		private var shadowBlurY:Number = 8;
		private var shadowColor:uint = 0x000000;
		private var shadowDistance:Number = 5;
		private var shadowHideObject:Boolean = false;
		private var shadowInner:Boolean = false;
		private var shadowKnockout:Boolean = false;
		private var shadowQuality:int = BitmapFilterQuality.MEDIUM;
		private var shadowStrength:Number = .5;
		private var sf:DropShadowFilter;
		
		private var owner:Document;
		
		public function Icons(w:Number, h:Number, s1:Number, s2:Number, s3:Number, myOwner:Document, ww:Number = 1, order:Number = 0)
		{
			owner = myOwner;
			
			sf = new DropShadowFilter(shadowDistance, shadowAngle, shadowColor, shadowAlpha, shadowBlurX, shadowBlurY, shadowStrength, shadowQuality, shadowInner, shadowKnockout, shadowHideObject);
			
			this.filters = [sf];
			
			speed1 = s1;
			speed2 = s2;
			speed3 = s3;
			
			whichOrder = order;
			
			whichWay = ww;
			//trace("*************************************************** " + whichWay);
			
			/*var temp:Number = Math.round(Math.random() * 100);
			if (temp < 50)
			{
				whichWay = 1;
			}
			else
			{
				whichWay  = -1;
			}*/
			sWidth = w;
			sHeight = h;
			if (whichWay == 1)
			{
				this.x = -this.width;
				this.y = owner.loadXML.screenHeight/3 - this.height; //400;
			}
			else
			{
				this.x = sWidth;
				this.y = sHeight - owner.loadXML.screenHeight/3;
			}
			
			whatDelay = (whichOrder - 1) * (speed1/5);
			//this.x = Math.random() * sWidth;
			setNumbers();
			playTimeline = new TimelineMax();
			playTimeline.repeat = -1;
			playTimeline.delay = whatDelay;
			if (whichWay == 1)
			{
				playTimeline.appendMultiple([new TweenMax(this, speed1, {x:sWidth, ease:Linear.easeNone, repeat:-1}),
									 new TweenMax(this, speed2, {y:this.y - yOffset, yoyo:true, repeat:-1, ease:Quad.easeInOut}),
									 new TweenMax(this, speed3, {scaleX:.75, scaleY:.75, yoyo:true, repeat:-1, ease:Quad.easeInOut})]);
			}
			else
			{
				playTimeline.appendMultiple([new TweenMax(this, speed1, {x:-this.width, ease:Linear.easeNone, repeat:-1}),
									 new TweenMax(this, speed2, {y:this.y + yOffset, yoyo:true, repeat:-1, ease:Quad.easeInOut}),
									 new TweenMax(this, speed3, {scaleX:.75, scaleY:.75, yoyo:true, repeat:-1, ease:Quad.easeInOut})]);
			}
			
			//playTimeline.insertMultiple([(new TweenMax(this, 1, {y:this.y + 30, ease:Quad.easeInOut, yoyo:true, repeat:-1}))])
			
			
			playTimeline.addEventListener(TweenEvent.REPEAT, resetX);
		}
		
		private function setNumbers():void
		{
			yOffset = 300;
			
			/*this.y = Math.random() * sHeight;
			if (this.y > sHeight - buffer - yOffset)
			{
				this.y -= buffer;
			}
			if (this.y < buffer)
			{
				this.y += buffer;
			}*/
			
			/*speed1 = speed1 * Math.random() + speed1;
			speed2 = speed2 * Math.random() + speed2;
			speed3 = speed3 * Math.random() + speed3;*/
		}
		
		private function resetX(e:TweenEvent):void
		{
			//setNumbers();
		}
		
		public function pauseThis():void
		{
			playTimeline.pause();
		}
		
		public function fadeThis():void
		{
			TweenMax.to(this, 1, {alpha:0});
		}
		
		public function showThis():void
		{
			TweenMax.to(this, 1, {alpha:1});
		}
		
		public function playThis():void
		{
			playTimeline.play();
		}
	}
	
}
