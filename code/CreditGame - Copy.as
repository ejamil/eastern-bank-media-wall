﻿package  code
{
	import flash.display.MovieClip;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import flash.events.Event;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	
	import org.tuio.*;
	import org.tuio.adapters.*;
	import org.tuio.gestures.*;
	import org.tuio.debug.TuioDebug;
	import org.tuio.TuioManager;
	import org.tuio.connectors.*;
	
	import flash.media.Sound;
	import flash.media.SoundChannel;
    import flash.media.SoundTransform;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import treefortress.sound.SoundAS;
	import treefortress.sound.SoundInstance;
	import treefortress.sound.SoundManager;
	
	public class CreditGame extends MovieClip
	{
		
		public static var TEST:String = "test";
		
		public var owner:Document;
		public var gameTimer:Timer;
		private var ct:Number = 0;
		
		private var s1:String = "";
		private var s2:String = "";
		private var s3:String = "";
		private var s4:String = "";
		
		private var player1:Boolean = false;
		private var player2:Boolean = false;
		private var player3:Boolean = false;
		private var player4:Boolean = false;
		
		public var countdownTimer:Timer;
		public var time:Number = 10;
		
		private var VOChannel:SoundChannel = new SoundChannel();
		private var VOTransform:SoundTransform = new SoundTransform();
		
		private var SFXChannel:SoundChannel = new SoundChannel();
		private var SFXTransform:SoundTransform = new SoundTransform();
		
		private var pressChannel:SoundChannel = new SoundChannel();
		private var pressTransform:SoundTransform = new SoundTransform();
		
		private var VO2Sound:Sound = new Sound();
		private var VO3Sound:Sound = new Sound();
		private var VO4Sound:Sound = new Sound();
		private var VO6Sound:Sound = new Sound();
		private var VO6aSound:Sound = new Sound();
		private var VO6bSound:Sound = new Sound();
		private var VO7Sound:Sound = new Sound();
		private var VO9Sound:Sound = new Sound();
		private var VO9aSound:Sound = new Sound();
		private var VO9bSound:Sound = new Sound();
		private var VO10Sound:Sound = new Sound();
		private var VO12Sound:Sound = new Sound();
		private var VO12aSound:Sound = new Sound();
		private var VO12bSound:Sound = new Sound();
		private var VO13Sound:Sound = new Sound();
		private var VO15Sound:Sound = new Sound();
		private var VO15aSound:Sound = new Sound();
		private var VO15bSound:Sound = new Sound();
		private var VO16Sound:Sound = new Sound();
		private var VO18Sound:Sound = new Sound();
		private var VO18aSound:Sound = new Sound();
		private var VO18bSound:Sound = new Sound();
		private var VO19Sound:Sound = new Sound();
		private var VO20Sound:Sound = new Sound();
		private var VO21Sound:Sound = new Sound();
		private var VO22Sound:Sound = new Sound();
		private var VO24aSound:Sound = new Sound();
		private var VO24bSound:Sound = new Sound();
		private var VO25Sound:Sound = new Sound();
		private var sing1Sound:Sound = new Sound();
		private var sing2Sound:Sound = new Sound();
		private var sing3Sound:Sound = new Sound();
		private var sing4Sound:Sound = new Sound();
		private var sing5Sound:Sound = new Sound();
		
		public var creditAnswersPopIn:Sound = new Sound();
		public var creditCorrectRound1:Sound = new Sound();
		public var creditCorrectRound2:Sound = new Sound();
		public var creditFeedbackScreensRound1:Sound = new Sound();
		public var creditIncorrectRound1:Sound = new Sound();
		public var creditIncorrectRound2:Sound = new Sound();
		public var creditIntro_and_QuestionMusicRound2:Sound = new Sound();
		public var creditIntroMusicStartGame:Sound = new Sound();
		public var creditMeter:Sound = new Sound();
		public var creditNosedive:Sound = new Sound();
		public var creditPressAnswerRound1:Sound = new Sound();
		public var creditQuestionMusicRound1:Sound = new Sound();
		public var creditQuestionSlideIn:Sound = new Sound();
		public var creditTimeRanOut:Sound = new Sound();
		public var creditWinner:Sound = new Sound();
		
		public var moved:Boolean = false;
		
		public var whatLang:String;
		
		public var numPlayers:Number = 0;
		
		public var cr1:CrGm;
		public var cr2:CrGm;
		public var cr3:CrGm;
		public var cr4:CrGm;
		
		public var player1Score:Number = 0;
		public var player2Score:Number = 0;
		
		public var qTime:Number = 15;
		
		public var currentPlayer1:CrGm;
		public var currentPlayer2:CrGm;
		
		
		public function CreditGame(myOwner:Document, lang:String) 
		{
			owner = myOwner;
			whatLang = lang;
			
			cr1 = new CrGm(this, "cr1");
			addChild(cr1);
			cr1.x = 0;
			cr2 = new CrGm(this, "cr2");
			addChild(cr2);
			cr2.x = cr1.x + 1080;
			cr3 = new CrGm(this, "cr3");
			addChild(cr3);
			cr3.x = cr2.x + 1080;
			cr4 = new CrGm(this, "cr4");
			addChild(cr4);
			cr4.x = cr3.x + 1080;
			/*cr1.y = 181.1;
			cr2.y = 181.1;
			cr3.y = 181.1;
			cr4.y = 181.1;*/
			
			
			SoundAS.loadSound(owner.loadXML.sing1Sound, TEST);
			
			trace(owner.loadXML.VO2Sound);
			VO2Sound.load(new URLRequest(owner.loadXML.VO2Sound));
			VO3Sound.load(new URLRequest(owner.loadXML.VO3Sound));
			VO4Sound.load(new URLRequest(owner.loadXML.VO4Sound));
			VO6Sound.load(new URLRequest(owner.loadXML.VO6Sound));
			VO6aSound.load(new URLRequest(owner.loadXML.VO6aSound));
			VO6bSound.load(new URLRequest(owner.loadXML.VO6bSound));
			VO7Sound.load(new URLRequest(owner.loadXML.VO7Sound));
			VO9Sound.load(new URLRequest(owner.loadXML.VO9Sound));
			VO9aSound.load(new URLRequest(owner.loadXML.VO9aSound));
			VO9bSound.load(new URLRequest(owner.loadXML.VO9bSound));
			VO10Sound.load(new URLRequest(owner.loadXML.VO10Sound));
			VO12Sound.load(new URLRequest(owner.loadXML.VO12Sound));
			VO12aSound.load(new URLRequest(owner.loadXML.VO12aSound));
			VO12bSound.load(new URLRequest(owner.loadXML.VO12bSound));
			VO13Sound.load(new URLRequest(owner.loadXML.VO13Sound));
			VO15Sound.load(new URLRequest(owner.loadXML.VO15Sound));
			VO15aSound.load(new URLRequest(owner.loadXML.VO15aSound));
			VO15bSound.load(new URLRequest(owner.loadXML.VO15bSound));
			VO16Sound.load(new URLRequest(owner.loadXML.VO16Sound));
			VO18Sound.load(new URLRequest(owner.loadXML.VO18Sound));
			VO18aSound.load(new URLRequest(owner.loadXML.VO18aSound));
			VO18bSound.load(new URLRequest(owner.loadXML.VO18bSound));
			VO19Sound.load(new URLRequest(owner.loadXML.VO19Sound));
			VO20Sound.load(new URLRequest(owner.loadXML.VO20Sound));
			VO21Sound.load(new URLRequest(owner.loadXML.VO21Sound));
			VO22Sound.load(new URLRequest(owner.loadXML.VO22Sound));
			VO24aSound.load(new URLRequest(owner.loadXML.VO24aSound));
			VO24bSound.load(new URLRequest(owner.loadXML.VO24bSound));
			VO25Sound.load(new URLRequest(owner.loadXML.VO25Sound));
			sing1Sound.load(new URLRequest(owner.loadXML.sing1Sound));
			sing2Sound.load(new URLRequest(owner.loadXML.sing2Sound));
			sing3Sound.load(new URLRequest(owner.loadXML.sing3Sound));
			sing4Sound.load(new URLRequest(owner.loadXML.sing4Sound));
			sing5Sound.load(new URLRequest(owner.loadXML.sing5Sound));
			
			creditAnswersPopIn.load(new URLRequest(owner.loadXML.creditAnswersPopIn));
			creditCorrectRound1.load(new URLRequest(owner.loadXML.creditCorrectRound1));
			creditCorrectRound2.load(new URLRequest(owner.loadXML.creditCorrectRound2));
			creditFeedbackScreensRound1.load(new URLRequest(owner.loadXML.creditFeedbackScreensRound1));
			creditIncorrectRound1.load(new URLRequest(owner.loadXML.creditIncorrectRound1));
			creditIncorrectRound2.load(new URLRequest(owner.loadXML.creditIncorrectRound2));
			creditIntro_and_QuestionMusicRound2.load(new URLRequest(owner.loadXML.creditIntro_and_QuestionMusicRound2));
			creditIntroMusicStartGame.load(new URLRequest(owner.loadXML.creditIntroMusicStartGame));
			creditMeter.load(new URLRequest(owner.loadXML.creditMeter));
			creditNosedive.load(new URLRequest(owner.loadXML.creditNosedive));
			creditPressAnswerRound1.load(new URLRequest(owner.loadXML.creditPressAnswerRound1));
			creditQuestionMusicRound1.load(new URLRequest(owner.loadXML.creditQuestionMusicRound1));
			creditQuestionSlideIn.load(new URLRequest(owner.loadXML.creditQuestionSlideIn));
			creditTimeRanOut.load(new URLRequest(owner.loadXML.creditTimeRanOut));
			creditWinner.load(new URLRequest(owner.loadXML.creditWinner));
			
			VOChannel = new SoundChannel();
			VOChannel = VO2Sound.play();
		}
		
		public function startGame():void
		{
			VOChannel.stop();
			VOChannel = VO3Sound.play();
			ct = -1;
			VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
			/*gameTimer = new Timer(5000);
			gameTimer.stop();
			gameTimer.start();
			gameTimer.addEventListener(TimerEvent.TIMER, countdown);*/
		}
		
		public function playClick():void
		{
			pressChannel.stop();
			pressChannel = new SoundChannel();
			pressChannel = creditPressAnswerRound1.play();
		}
		
		public function startCountdown():void
		{
			cr1.gotoAndStop(1);
			cr2.gotoAndStop(1);
			cr3.gotoAndStop(1);
			cr4.gotoAndStop(1);
			cr1.jg.addEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
			cr2.jg.addEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
			cr3.jg.addEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
			cr4.jg.addEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
			cr1.jg.t.mouseChildren = false;
			cr1.jg.t.mouseEnabled = false;
			cr2.jg.t.mouseChildren = false;
			cr2.jg.t.mouseEnabled = false;
			cr3.jg.t.mouseChildren = false;
			cr3.jg.t.mouseEnabled = false;
			cr4.jg.t.mouseChildren = false;
			cr4.jg.t.mouseEnabled = false;
			countdownTimer = new Timer(1000);
			countdownTimer.stop();
			countdownTimer.start();
			countdownTimer.addEventListener(TimerEvent.TIMER, countdown4);
		}
		
		private function countPlayers(e:TuioTouchEvent):void
		{
			trace("countPlayers");
			trace(e.currentTarget.parent.whichOne);
			trace("countPlayers player1 = " + player1);
			switch (e.currentTarget.parent.whichOne)
			{
				case "cr1":
					trace("cr1");
					cr1.jg.removeEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
					player1 = true;
					numPlayers++;
					if (numPlayers == 1)
					{
						currentPlayer1 = cr1;
						cr1.jg.gotoAndStop(2);
					}
					if (numPlayers == 2)
					{
						currentPlayer2 = cr1;
					}
					cr1.whichPlayer = numPlayers;
					trace("case cr1 player1 = " + player1);
				break;
				
				case "cr2":
					cr2.jg.removeEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
					player2 = true;
					numPlayers++;
					if (numPlayers == 1)
					{
						currentPlayer1 = cr2;
						cr2.jg.gotoAndStop(2);
					}
					if (numPlayers == 2)
					{
						currentPlayer2 = cr2;
					}
					cr2.whichPlayer = numPlayers;
				break;
				
				case "cr3":
					cr3.jg.removeEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
					player3 = true;
					numPlayers++;
					if (numPlayers == 1)
					{
						currentPlayer1 = cr3;
						cr3.jg.gotoAndStop(2);
					}
					if (numPlayers == 2)
					{
						currentPlayer2 = cr3;
					}
					cr3.whichPlayer = numPlayers;
				break;
				
				case "cr4":
					cr4.jg.removeEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
					player4 = true;
					numPlayers++;
					if (numPlayers == 1)
					{
						currentPlayer1 = cr4;
						cr4.jg.gotoAndStop(2);
					}
					if (numPlayers == 2)
					{
						currentPlayer2 = cr4;
					}
					cr4.whichPlayer = numPlayers;
				break;
				
				default:
					trace("none");
				break;
			}
			if (numPlayers == 2)
			{
					trace("numPlayers = 2 player1 = " + player1);
				countdownTimer.stop();
				countdownTimer.removeEventListener(TimerEvent.TIMER, countdown4);
				cr1.nextFrame();
				cr2.nextFrame();
				cr3.nextFrame();
				cr4.nextFrame();
				if (player1 == false)
				{
					cr1.visible = false;
				}
				if (player2 == false)
				{
					cr2.visible = false;
				}
				if (player3 == false)
				{
					cr3.visible = false;
				}
				if (player4 == false)
				{
					cr4.visible = false;
				}
				if (player1 == true || player2 == true || player3 == true || player4 == true)
				{
					startGame();
				}
				else
				{
					owner.removeThis(this);
				}
			}
		}
		
		private function countdown4(e:TimerEvent = null):void
		{
			time--;
			cr1.t.text = time.toString();
			cr2.t.text = time.toString();
			cr3.t.text = time.toString();
			cr4.t.text = time.toString();
					trace("countdown4 player1 = " + player1);
			if (time == 0 || numPlayers == 2)
			{
				trace("countdown over");
					trace("countdownover player1 = " + player1);
				countdownTimer.stop();
				countdownTimer.removeEventListener(TimerEvent.TIMER, countdown4);
				cr1.nextFrame();
				cr2.nextFrame();
				cr3.nextFrame();
				cr4.nextFrame();
				if (player1 == false)
				{
					cr1.visible = false;
				}
				if (player2 == false)
				{
					cr2.visible = false;
				}
				if (player3 == false)
				{
					cr3.visible = false;
				}
				if (player4 == false)
				{
					cr4.visible = false;
				}
				
				if (numPlayers < 2)
				{
					if (player1 == false)
					{
						currentPlayer2 = cr1;
						player1 = true;
					}
					else if (player2 == false)
					{
						currentPlayer2 = cr2;
						player2 = true;
					}
					else if (player3 == false)
					{
						currentPlayer2 = cr3;
						player3 = true;
					}
					else if (player4 == false)
					{
						currentPlayer2 = cr4;
						player4 = true;
					}
					/*cr1.hideMeters(false);
					cr2.hideMeters(false);
					cr3.hideMeters(false);
					cr4.hideMeters(false);*/
				}
				trace("player1 = " + player1);
				if (player1 == true || player2 == true || player3 == true || player4 == true)
				{
					trace("try to start game");
					startGame();
				}
				else
				{
					owner.removeThis(this);
				}
				
			}
		}
		
		public function stopVO():void
		{
			VOChannel.stop();
		}
		
		public function VOAllDone(si:SoundInstance):void
		{
			trace("OMG THIS ACTUALLY WORKED!");
			ct++;
			trace("countdown ct = " + ct);
			
			switch (ct)
			{
				case 0: //example
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = VO4Sound.play();
					trace(ct + " frame = " + cr1.currentFrame);
					
					//timerGo(2);
					
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
				break;
				case 1:
					trace("onVODone ************************* case 1");
					trace("sing 1");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					//VOChannel = sing1Sound.play();
					var solo:SoundInstance = SoundAS.play(TEST);
					solo.soundCompleted.add(VOAllDone);
					VOAllDone(solo);
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					
					//timerGo(ct); // ct = 1
					trace("case 1 ct = " + ct);
					trace("singing 1");
				break;
				case 2:
					trace("VOAllDone ************************* case 2");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					
					cr1.initMeters();
					cr2.initMeters();
					cr3.initMeters();
					cr4.initMeters();
					cr1.showMeters();
					cr2.showMeters();
					cr3.showMeters();
					cr4.showMeters();
					if (numPlayers < 2)
					{
						cr1.hideMeters(false);
						cr2.hideMeters(false);
						cr3.hideMeters(false);
						cr4.hideMeters(false);
					}
					VOChannel.removeEventListener(Event.SOUND_COMPLETE, onVODone);
					VOChannel.stop();
					VOChannel = null;
					VOChannel = new SoundChannel();
					VOChannel = VO6Sound.play();
					
					cr1.resetQuestions();
					cr2.resetQuestions();
					cr3.resetQuestions();
					cr4.resetQuestions();
					
					cr1.addQuestListeners();
					cr2.addQuestListeners();
					cr3.addQuestListeners();
					cr4.addQuestListeners();
					
					trace(ct + " frame = " + cr1.currentFrame);
					cr1.showQ();
					cr2.showQ();
					cr3.showQ();
					cr4.showQ();
					
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
				break;
				
				case 3:
					trace("onVODone ************************* case 3");
					ct++;
					timerGo(ct); // ct = 4
				break;
				
				
				/*case 3:
					if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
					{
						VOChannel = VO6aSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
					{
						VOChannel = VO6bSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else
					{
						ct++;
						cr1.setScore(cr1.currentFrame);
						cr2.setScore(cr2.currentFrame);
						cr3.setScore(cr3.currentFrame);
						cr4.setScore(cr4.currentFrame);
						cr1.hideQ();
						cr2.hideQ();
						cr3.hideQ();
						cr4.hideQ();
						cr1.nextFrame();
						cr2.nextFrame();
						cr3.nextFrame();
						cr4.nextFrame();
						VOChannel = VO7Sound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
				break;*/
				
				case 4:
					trace("onVODone ************************* case 4");
					cr1.setScore(cr1.currentFrame);
					cr2.setScore(cr2.currentFrame);
					cr3.setScore(cr3.currentFrame);
					cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr1.hideQ();
					cr2.hideQ();
					cr3.hideQ();
					cr4.hideQ();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					trace("case 4 cr1.currentFrame = " + cr1.currentFrame);
					VOChannel = VO7Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					SFXChannel.stop();
					SFXChannel = null;
					SFXChannel = new SoundChannel();
					SFXChannel = creditFeedbackScreensRound1.play();
				break;
				case 5:
					SFXChannel.stop();
					trace("onVODone ************************* case 5");
					cr1.hideQ();
					cr2.hideQ();
					cr3.hideQ();
					cr4.hideQ();
					cr1.hideMeters();
					cr2.hideMeters();
					cr3.hideMeters();
					cr4.hideMeters();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = sing2Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
				break;
				
				case 6: //question 2
					trace("onVODone ************************* case 6");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					cr1.showMeters();
					cr2.showMeters();
					cr3.showMeters();
					cr4.showMeters();
					if (numPlayers < 2)
					{
						cr1.hideMeters(false);
						cr2.hideMeters(false);
						cr3.hideMeters(false);
						cr4.hideMeters(false);
					}
					
					VOChannel = VO9Sound.play();
					
					cr1.resetQuestions();
					cr2.resetQuestions();
					cr3.resetQuestions();
					cr4.resetQuestions();
					trace(ct + " frame = " + cr1.currentFrame);
					cr1.showQ();
					cr2.showQ();
					cr3.showQ();
					cr4.showQ();
					
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					ct++;
					timerGo(ct); // ct = 7
				break;
				/*case 7:
					trace("************************* case 7");
					if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
					{
						VOChannel = VO9aSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
					{
						VOChannel = VO9bSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else
					{
						ct++;
						cr1.setScore(cr1.currentFrame);
						cr2.setScore(cr2.currentFrame);
						cr3.setScore(cr3.currentFrame);
						cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
						cr1.hideQ();
						cr2.hideQ();
						cr3.hideQ();
						cr4.hideQ();
						cr1.nextFrame();
						cr2.nextFrame();
						cr3.nextFrame();
						cr4.nextFrame();
						VOChannel = VO10Sound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
				break;*/
				
				case 7:
					trace("onVODone ************************* case 7");
					cr1.setScore(cr1.currentFrame);
					cr2.setScore(cr2.currentFrame);
					cr3.setScore(cr3.currentFrame);
					cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr1.hideQ();
					cr2.hideQ();
					cr3.hideQ();
					cr4.hideQ();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = VO10Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					SFXChannel.stop();
					SFXChannel = null;
					SFXChannel = new SoundChannel();
					SFXChannel = creditFeedbackScreensRound1.play();
				break;
				case 8:
					trace("onVODone ************************* case 8");
					SFXChannel.stop();
					cr1.hideMeters();
					cr2.hideMeters();
					cr3.hideMeters();
					cr4.hideMeters();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = sing3Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
				break;
				
				
				
				case 9: //question 3
					trace("onVODone ************************* case 9");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					cr1.showMeters();
					cr2.showMeters();
					cr3.showMeters();
					cr4.showMeters();
					if (numPlayers < 2)
					{
						cr1.hideMeters(false);
						cr2.hideMeters(false);
						cr3.hideMeters(false);
						cr4.hideMeters(false);
					}
					
					VOChannel = VO12Sound.play();
					
					cr1.resetQuestions();
					cr2.resetQuestions();
					cr3.resetQuestions();
					cr4.resetQuestions();
					trace(ct + " frame = " + cr1.currentFrame);
					cr1.showQ();
					cr2.showQ();
					cr3.showQ();
					cr4.showQ();
					
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					ct++;
					timerGo(ct); // ct = 10
				break;
				/*case 11:
					if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
					{
						VOChannel = VO12aSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
					{
						VOChannel = VO12bSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else
					{
						ct++;
						cr1.setScore(cr1.currentFrame);
						cr2.setScore(cr2.currentFrame);
						cr3.setScore(cr3.currentFrame);
						cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
						cr1.hideQ();
						cr2.hideQ();
						cr3.hideQ();
						cr4.hideQ();
						cr1.nextFrame();
						cr2.nextFrame();
						cr3.nextFrame();
						cr4.nextFrame();
						VOChannel = VO13Sound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
				break;*/
				
				case 10:
					trace("onVODone ************************* case 10");
					cr1.setScore(cr1.currentFrame);
					cr2.setScore(cr2.currentFrame);
					cr3.setScore(cr3.currentFrame);
					cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr1.hideQ();
					cr2.hideQ();
					cr3.hideQ();
					cr4.hideQ();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = VO13Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					SFXChannel.stop();
					SFXChannel = null;
					SFXChannel = new SoundChannel();
					SFXChannel = creditFeedbackScreensRound1.play();
				break;
				
				
				
				case 11:
					trace("onVODone ************************* case 11");
					SFXChannel.stop();
					cr1.hideMeters();
					cr2.hideMeters();
					cr3.hideMeters();
					cr4.hideMeters();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = sing4Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
				break;
				
				
				
				case 12: //question 3
					trace("onVODone ************************* case 12");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					cr1.showMeters();
					cr2.showMeters();
					cr3.showMeters();
					cr4.showMeters();
					if (numPlayers < 2)
					{
						cr1.hideMeters(false);
						cr2.hideMeters(false);
						cr3.hideMeters(false);
						cr4.hideMeters(false);
					}
					
					VOChannel = VO15Sound.play();
					
					cr1.resetQuestions();
					cr2.resetQuestions();
					cr3.resetQuestions();
					cr4.resetQuestions();
					trace(ct + " frame = " + cr1.currentFrame);
					cr1.showQ();
					cr2.showQ();
					cr3.showQ();
					cr4.showQ();
					
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					ct++;
					timerGo(ct); // ct = 10
				break;
				/*case 15:
					if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
					{
						VOChannel = VO15aSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
					{
						VOChannel = VO15bSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else
					{
						ct++;
						cr1.setScore(cr1.currentFrame);
						cr2.setScore(cr2.currentFrame);
						cr3.setScore(cr3.currentFrame);
						cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
						cr1.hideQ();
						cr2.hideQ();
						cr3.hideQ();
						cr4.hideQ();
						cr1.nextFrame();
						cr2.nextFrame();
						cr3.nextFrame();
						cr4.nextFrame();
						VOChannel = VO16Sound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
				break;*/
				
				case 13:
					trace("onVODone ************************* case 13");
					cr1.setScore(cr1.currentFrame);
					cr2.setScore(cr2.currentFrame);
					cr3.setScore(cr3.currentFrame);
					cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr1.hideQ();
					cr2.hideQ();
					cr3.hideQ();
					cr4.hideQ();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = VO16Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					SFXChannel.stop();
					SFXChannel = null;
					SFXChannel = new SoundChannel();
					SFXChannel = creditFeedbackScreensRound1.play();
				break;
				
				
				case 14:
					trace("onVODone ************************* case 14");
					SFXChannel.stop();
					cr1.hideMeters();
					cr2.hideMeters();
					cr3.hideMeters();
					cr4.hideMeters();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = sing5Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
				break;
				
				
				
				case 15: //question 3
					trace("onVODone ************************* case 15");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					cr1.showMeters();
					cr2.showMeters();
					cr3.showMeters();
					cr4.showMeters();
					if (numPlayers < 2)
					{
						cr1.hideMeters(false);
						cr2.hideMeters(false);
						cr3.hideMeters(false);
						cr4.hideMeters(false);
					}
					
					VOChannel = VO18Sound.play();
					
					cr1.resetQuestions();
					cr2.resetQuestions();
					cr3.resetQuestions();
					cr4.resetQuestions();
					trace(ct + " frame = " + cr1.currentFrame);
					cr1.showQ();
					cr2.showQ();
					cr3.showQ();
					cr4.showQ();
					
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					ct++;
					timerGo(ct);
				break;
				/*case 16:
					if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
					{
						VOChannel = VO18aSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
					{
						VOChannel = VO18bSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else
					{
						ct++;
						cr1.setScore(cr1.currentFrame);
						cr2.setScore(cr2.currentFrame);
						cr3.setScore(cr3.currentFrame);
						cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
						cr1.hideQ();
						cr2.hideQ();
						cr3.hideQ();
						cr4.hideQ();
						cr1.nextFrame();
						cr2.nextFrame();
						cr3.nextFrame();
						cr4.nextFrame();
						VOChannel = VO19Sound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
				break;*/
				
				case 16:
					trace("onVODone ************************* case 16");
					cr1.setScore(cr1.currentFrame);
					cr2.setScore(cr2.currentFrame);
					cr3.setScore(cr3.currentFrame);
					cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr1.hideQ();
					cr2.hideQ();
					cr3.hideQ();
					cr4.hideQ();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = VO19Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					SFXChannel.stop();
					SFXChannel = null;
					SFXChannel = new SoundChannel();
					SFXChannel = creditFeedbackScreensRound1.play();
				break;
				
				case 17:
					trace("onVODone ************************* case 17");
					SFXChannel.stop();
					cr1.removeQuestListeners();
					cr2.removeQuestListeners();
					cr3.removeQuestListeners();
					cr4.removeQuestListeners();
					
					cr1.gotoAndStop(19);
					cr2.gotoAndStop(19);
					cr3.gotoAndStop(19);
					cr4.gotoAndStop(19);
					VOChannel = VO20Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					trace(ct + " frame = " + cr1.currentFrame);
				break;
				
				case 18:
					trace("onVODone ************************* case 18");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = VO21Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					trace(ct + " frame = " + cr1.currentFrame);
				break;
				
				case 19: //sample cloud
					trace("onVODone ************************* case 19");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = VO22Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					trace(ct + " frame = " + cr1.currentFrame);
				break;
				
				case 20: //cloud
					trace("onVODone ************************* case 20");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					trace(ct + " frame = " + cr1.currentFrame);
					
					cr1.addGListeners();
					cr2.addGListeners();
					cr3.addGListeners();
					cr4.addGListeners();
					
					//timerGo(24);
				break;
				
				case 21:
					trace("VOAllDone ************************* case 21");
					/*cr1.setScore(cr1.currentFrame);
					cr2.setScore(cr2.currentFrame);
					cr3.setScore(cr3.currentFrame);
					cr4.setScore(cr4.currentFrame);*/
					
					cr1.removeGListeners();
					cr2.removeGListeners();
					cr3.removeGListeners();
					cr4.removeGListeners();
					
					cr1.resetGs();
					cr2.resetGs();
					cr3.resetGs();
					cr4.resetGs();
					
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel.stop();
					VOChannel = null;
					VOChannel = new SoundChannel();
					VOChannel = VO24bSound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					trace(ct + " frame = " + cr1.currentFrame);
					
					cr1.showScore();
					cr2.showScore();
					cr3.showScore();
					cr4.showScore();
				break;
				
				case 22:
					trace("onVODone ************************* case 22");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = VO25Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					trace(ct + " frame = " + cr1.currentFrame);
				break;
				
				case 23:
					trace("onVODone ************************* case 23");
					numPlayers = 0;
					owner.removeThis(this);
				break;
			}
		}
		
		private function onVODone(e:Event):void
		{
			VOChannel.removeEventListener(Event.SOUND_COMPLETE, onVODone);
			//VOChannel.stop();
			//VOChannel = null;
			//VOChannel = new SoundChannel();
			
			ct++;
			
			trace("onVODone ct = " + ct);
			
			switch (ct)
			{
				case 0: //example
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = VO4Sound.play();
					trace(ct + " frame = " + cr1.currentFrame);
					
					//timerGo(2);
					
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
				break;
				case 1:
					trace("onVODone ************************* case 1");
					trace("sing 1");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					//VOChannel = sing1Sound.play();
					var solo:SoundInstance = SoundAS.play(TEST);
					solo.soundCompleted.add(VOAllDone);
					VOAllDone(solo);
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					
					//timerGo(ct); // ct = 1
					trace("case 1 ct = " + ct);
					trace("singing 1");
				break;
				/*case 2: //question 1
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					
					cr1.initMeters();
					cr2.initMeters();
					cr3.initMeters();
					cr4.initMeters();
					
					VOChannel = VO6Sound.play();
					
					cr1.resetQuestions();
					cr2.resetQuestions();
					cr3.resetQuestions();
					cr4.resetQuestions();
					
					cr1.addQuestListeners();
					cr2.addQuestListeners();
					cr3.addQuestListeners();
					cr4.addQuestListeners();
					
					trace(ct + " frame = " + cr1.currentFrame);
					cr1.showQ();
					cr2.showQ();
					cr3.showQ();
					cr4.showQ();
					
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
				break;*/
				case 3:
					trace("onVODone ************************* case 3");
					ct++;
					timerGo(ct); // ct = 4
				break;
				
				
				/*case 3:
					if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
					{
						VOChannel = VO6aSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
					{
						VOChannel = VO6bSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else
					{
						ct++;
						cr1.setScore(cr1.currentFrame);
						cr2.setScore(cr2.currentFrame);
						cr3.setScore(cr3.currentFrame);
						cr4.setScore(cr4.currentFrame);
						cr1.hideQ();
						cr2.hideQ();
						cr3.hideQ();
						cr4.hideQ();
						cr1.nextFrame();
						cr2.nextFrame();
						cr3.nextFrame();
						cr4.nextFrame();
						VOChannel = VO7Sound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
				break;*/
				
				case 4:
					trace("onVODone ************************* case 4");
					cr1.setScore(cr1.currentFrame);
					cr2.setScore(cr2.currentFrame);
					cr3.setScore(cr3.currentFrame);
					cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr1.hideQ();
					cr2.hideQ();
					cr3.hideQ();
					cr4.hideQ();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					trace("case 4 cr1.currentFrame = " + cr1.currentFrame);
					VOChannel = VO7Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					SFXChannel.stop();
					SFXChannel = null;
					SFXChannel = new SoundChannel();
					SFXChannel = creditFeedbackScreensRound1.play();
				break;
				case 5:
					SFXChannel.stop();
					trace("onVODone ************************* case 5");
					cr1.hideQ();
					cr2.hideQ();
					cr3.hideQ();
					cr4.hideQ();
					cr1.hideMeters();
					cr2.hideMeters();
					cr3.hideMeters();
					cr4.hideMeters();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = sing2Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
				break;
				
				case 6: //question 2
					trace("onVODone ************************* case 6");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					cr1.showMeters();
					cr2.showMeters();
					cr3.showMeters();
					cr4.showMeters();
					if (numPlayers < 2)
					{
						cr1.hideMeters(false);
						cr2.hideMeters(false);
						cr3.hideMeters(false);
						cr4.hideMeters(false);
					}
					
					VOChannel = VO9Sound.play();
					
					cr1.resetQuestions();
					cr2.resetQuestions();
					cr3.resetQuestions();
					cr4.resetQuestions();
					trace(ct + " frame = " + cr1.currentFrame);
					cr1.showQ();
					cr2.showQ();
					cr3.showQ();
					cr4.showQ();
					
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					ct++;
					timerGo(ct); // ct = 7
				break;
				/*case 7:
					trace("************************* case 7");
					if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
					{
						VOChannel = VO9aSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
					{
						VOChannel = VO9bSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else
					{
						ct++;
						cr1.setScore(cr1.currentFrame);
						cr2.setScore(cr2.currentFrame);
						cr3.setScore(cr3.currentFrame);
						cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
						cr1.hideQ();
						cr2.hideQ();
						cr3.hideQ();
						cr4.hideQ();
						cr1.nextFrame();
						cr2.nextFrame();
						cr3.nextFrame();
						cr4.nextFrame();
						VOChannel = VO10Sound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
				break;*/
				
				case 7:
					trace("onVODone ************************* case 7");
					cr1.setScore(cr1.currentFrame);
					cr2.setScore(cr2.currentFrame);
					cr3.setScore(cr3.currentFrame);
					cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr1.hideQ();
					cr2.hideQ();
					cr3.hideQ();
					cr4.hideQ();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = VO10Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					SFXChannel.stop();
					SFXChannel = null;
					SFXChannel = new SoundChannel();
					SFXChannel = creditFeedbackScreensRound1.play();
				break;
				case 8:
					trace("onVODone ************************* case 8");
					SFXChannel.stop();
					cr1.hideMeters();
					cr2.hideMeters();
					cr3.hideMeters();
					cr4.hideMeters();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = sing3Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
				break;
				
				
				
				case 9: //question 3
					trace("onVODone ************************* case 9");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					cr1.showMeters();
					cr2.showMeters();
					cr3.showMeters();
					cr4.showMeters();
					if (numPlayers < 2)
					{
						cr1.hideMeters(false);
						cr2.hideMeters(false);
						cr3.hideMeters(false);
						cr4.hideMeters(false);
					}
					
					VOChannel = VO12Sound.play();
					
					cr1.resetQuestions();
					cr2.resetQuestions();
					cr3.resetQuestions();
					cr4.resetQuestions();
					trace(ct + " frame = " + cr1.currentFrame);
					cr1.showQ();
					cr2.showQ();
					cr3.showQ();
					cr4.showQ();
					
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					ct++;
					timerGo(ct); // ct = 10
				break;
				/*case 11:
					if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
					{
						VOChannel = VO12aSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
					{
						VOChannel = VO12bSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else
					{
						ct++;
						cr1.setScore(cr1.currentFrame);
						cr2.setScore(cr2.currentFrame);
						cr3.setScore(cr3.currentFrame);
						cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
						cr1.hideQ();
						cr2.hideQ();
						cr3.hideQ();
						cr4.hideQ();
						cr1.nextFrame();
						cr2.nextFrame();
						cr3.nextFrame();
						cr4.nextFrame();
						VOChannel = VO13Sound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
				break;*/
				
				case 10:
					trace("onVODone ************************* case 10");
					cr1.setScore(cr1.currentFrame);
					cr2.setScore(cr2.currentFrame);
					cr3.setScore(cr3.currentFrame);
					cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr1.hideQ();
					cr2.hideQ();
					cr3.hideQ();
					cr4.hideQ();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = VO13Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					SFXChannel.stop();
					SFXChannel = null;
					SFXChannel = new SoundChannel();
					SFXChannel = creditFeedbackScreensRound1.play();
				break;
				
				
				
				case 11:
					trace("onVODone ************************* case 11");
					SFXChannel.stop();
					cr1.hideMeters();
					cr2.hideMeters();
					cr3.hideMeters();
					cr4.hideMeters();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = sing4Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
				break;
				
				
				
				case 12: //question 3
					trace("onVODone ************************* case 12");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					cr1.showMeters();
					cr2.showMeters();
					cr3.showMeters();
					cr4.showMeters();
					if (numPlayers < 2)
					{
						cr1.hideMeters(false);
						cr2.hideMeters(false);
						cr3.hideMeters(false);
						cr4.hideMeters(false);
					}
					
					VOChannel = VO15Sound.play();
					
					cr1.resetQuestions();
					cr2.resetQuestions();
					cr3.resetQuestions();
					cr4.resetQuestions();
					trace(ct + " frame = " + cr1.currentFrame);
					cr1.showQ();
					cr2.showQ();
					cr3.showQ();
					cr4.showQ();
					
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					ct++;
					timerGo(ct); // ct = 10
				break;
				/*case 15:
					if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
					{
						VOChannel = VO15aSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
					{
						VOChannel = VO15bSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else
					{
						ct++;
						cr1.setScore(cr1.currentFrame);
						cr2.setScore(cr2.currentFrame);
						cr3.setScore(cr3.currentFrame);
						cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
						cr1.hideQ();
						cr2.hideQ();
						cr3.hideQ();
						cr4.hideQ();
						cr1.nextFrame();
						cr2.nextFrame();
						cr3.nextFrame();
						cr4.nextFrame();
						VOChannel = VO16Sound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
				break;*/
				
				case 13:
					trace("onVODone ************************* case 13");
					cr1.setScore(cr1.currentFrame);
					cr2.setScore(cr2.currentFrame);
					cr3.setScore(cr3.currentFrame);
					cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr1.hideQ();
					cr2.hideQ();
					cr3.hideQ();
					cr4.hideQ();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = VO16Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					SFXChannel.stop();
					SFXChannel = null;
					SFXChannel = new SoundChannel();
					SFXChannel = creditFeedbackScreensRound1.play();
				break;
				
				
				case 14:
					trace("onVODone ************************* case 14");
					SFXChannel.stop();
					cr1.hideMeters();
					cr2.hideMeters();
					cr3.hideMeters();
					cr4.hideMeters();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = sing5Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
				break;
				
				
				
				case 15: //question 3
					trace("onVODone ************************* case 15");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					cr1.showMeters();
					cr2.showMeters();
					cr3.showMeters();
					cr4.showMeters();
					if (numPlayers < 2)
					{
						cr1.hideMeters(false);
						cr2.hideMeters(false);
						cr3.hideMeters(false);
						cr4.hideMeters(false);
					}
					
					VOChannel = VO18Sound.play();
					
					cr1.resetQuestions();
					cr2.resetQuestions();
					cr3.resetQuestions();
					cr4.resetQuestions();
					trace(ct + " frame = " + cr1.currentFrame);
					cr1.showQ();
					cr2.showQ();
					cr3.showQ();
					cr4.showQ();
					
					//VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					ct++;
					timerGo(ct);
				break;
				/*case 16:
					if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
					{
						VOChannel = VO18aSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
					{
						VOChannel = VO18bSound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
					else
					{
						ct++;
						cr1.setScore(cr1.currentFrame);
						cr2.setScore(cr2.currentFrame);
						cr3.setScore(cr3.currentFrame);
						cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
						cr1.hideQ();
						cr2.hideQ();
						cr3.hideQ();
						cr4.hideQ();
						cr1.nextFrame();
						cr2.nextFrame();
						cr3.nextFrame();
						cr4.nextFrame();
						VOChannel = VO19Sound.play();
						VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					}
				break;*/
				
				case 16:
					trace("onVODone ************************* case 16");
					cr1.setScore(cr1.currentFrame);
					cr2.setScore(cr2.currentFrame);
					cr3.setScore(cr3.currentFrame);
					cr4.setScore(cr4.currentFrame);
					cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
					cr1.hideQ();
					cr2.hideQ();
					cr3.hideQ();
					cr4.hideQ();
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = VO19Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					SFXChannel.stop();
					SFXChannel = null;
					SFXChannel = new SoundChannel();
					SFXChannel = creditFeedbackScreensRound1.play();
				break;
				
				case 17:
					trace("onVODone ************************* case 17");
					SFXChannel.stop();
					cr1.removeQuestListeners();
					cr2.removeQuestListeners();
					cr3.removeQuestListeners();
					cr4.removeQuestListeners();
					
					cr1.gotoAndStop(19);
					cr2.gotoAndStop(19);
					cr3.gotoAndStop(19);
					cr4.gotoAndStop(19);
					VOChannel = VO20Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					trace(ct + " frame = " + cr1.currentFrame);
				break;
				
				case 18:
					trace("onVODone ************************* case 18");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = VO21Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					trace(ct + " frame = " + cr1.currentFrame);
				break;
				
				case 19: //sample cloud
					trace("onVODone ************************* case 19");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = VO22Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					trace(ct + " frame = " + cr1.currentFrame);
				break;
				
				case 20: //cloud
					trace("onVODone ************************* case 20");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					trace(ct + " frame = " + cr1.currentFrame);
					
					cr1.addGListeners();
					cr2.addGListeners();
					cr3.addGListeners();
					cr4.addGListeners();
					
					//timerGo(24);
				break;
				
				/*case 21:
					cr1.setScore(cr1.currentFrame);
					cr2.setScore(cr2.currentFrame);
					cr3.setScore(cr3.currentFrame);
					cr4.setScore(cr4.currentFrame);
					
					cr1.removeGListeners();
					cr2.removeGListeners();
					cr3.removeGListeners();
					cr4.removeGListeners();
					
					cr1.resetGs();
					cr2.resetGs();
					cr3.resetGs();
					cr4.resetGs();
					
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = VO24bSound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					trace(ct + " frame = " + cr1.currentFrame);
					
					cr1.showScore();
					cr2.showScore();
					cr3.showScore();
					cr4.showScore();
				break;*/
				
				case 22:
					trace("onVODone ************************* case 22");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel = VO25Sound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					trace(ct + " frame = " + cr1.currentFrame);
				break;
				
				case 23:
					trace("onVODone ************************* case 23");
					numPlayers = 0;
					owner.removeThis(this);
				break;
			}
		}
		
		public function moveOnGame():void
		{
			if (moved == false)
			{
				moved = true;
				trace("moveOnGame");
				cr1.removeGListeners();
				cr2.removeGListeners();
				cr3.removeGListeners();
				cr4.removeGListeners();
				
				cr1.resetGs();
				cr2.resetGs();
				cr3.resetGs();
				cr4.resetGs();
				
				cr1.nextFrame();
				cr2.nextFrame();
				cr3.nextFrame();
				cr4.nextFrame();
				VOChannel.stop();
			VOChannel = null;
				VOChannel = new SoundChannel();
				if (cr1.whichPlayer == 1)
				{
					player1Score = cr1.score;
				}
				if (cr1.whichPlayer == 2)
				{
					player2Score = cr1.score;
				}
				if (cr2.whichPlayer == 1)
				{
					player1Score = cr2.score;
				}
				if (cr2.whichPlayer == 2)
				{
					player2Score = cr2.score;
				}
				if (cr3.whichPlayer == 1)
				{
					player1Score = cr3.score;
				}
				if (cr3.whichPlayer == 2)
				{
					player2Score = cr3.score;
				}
				if (cr4.whichPlayer == 1)
				{
					player1Score = cr4.score;
				}
				if (cr4.whichPlayer == 2)
				{
					player2Score = cr4.score;
				}
				
				
				if (player1Score >= player2Score)
				{
					trace("player 1 won");
					VOChannel = VO24aSound.play();
				}
				else
				{
					trace("player 2 won");
					VOChannel = VO24bSound.play();
				}
				
				VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
				trace(ct + " frame = " + cr1.currentFrame);
				
				cr1.showScore();
				cr2.showScore();
				cr3.showScore();
				cr4.showScore();
			
				ct++;
			}
		}
		
		private function countdown(e:TimerEvent):void
		{
			ct++;
			trace("countdown ct = " + ct);
			gameTimer.stop();
			gameTimer.removeEventListener(TimerEvent.TIMER, countdown);
			switch (ct)
			{
				case 2:
					trace("countdown ************************* case 2");
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					
					cr1.initMeters();
					cr2.initMeters();
					cr3.initMeters();
					cr4.initMeters();
					cr1.showMeters();
					cr2.showMeters();
					cr3.showMeters();
					cr4.showMeters();
					if (numPlayers < 2)
					{
						cr1.hideMeters(false);
						cr2.hideMeters(false);
						cr3.hideMeters(false);
						cr4.hideMeters(false);
					}
					VOChannel.removeEventListener(Event.SOUND_COMPLETE, onVODone);
					VOChannel.stop();
					VOChannel = null;
					VOChannel = new SoundChannel();
					VOChannel = VO6Sound.play();
					
					cr1.resetQuestions();
					cr2.resetQuestions();
					cr3.resetQuestions();
					cr4.resetQuestions();
					
					cr1.addQuestListeners();
					cr2.addQuestListeners();
					cr3.addQuestListeners();
					cr4.addQuestListeners();
					
					trace(ct + " frame = " + cr1.currentFrame);
					cr1.showQ();
					cr2.showQ();
					cr3.showQ();
					cr4.showQ();
					
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
				break;
				
				case 21:
					trace("countdown ************************* case 21");
					/*cr1.setScore(cr1.currentFrame);
					cr2.setScore(cr2.currentFrame);
					cr3.setScore(cr3.currentFrame);
					cr4.setScore(cr4.currentFrame);*/
					
					cr1.removeGListeners();
					cr2.removeGListeners();
					cr3.removeGListeners();
					cr4.removeGListeners();
					
					cr1.resetGs();
					cr2.resetGs();
					cr3.resetGs();
					cr4.resetGs();
					
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel.stop();
			VOChannel = null;
					VOChannel = new SoundChannel();
					VOChannel = VO24bSound.play();
					VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
					trace(ct + " frame = " + cr1.currentFrame);
					
					cr1.showScore();
					cr2.showScore();
					cr3.showScore();
					cr4.showScore();
				break;
				
				/*case 17:
					cr1.nextFrame();
					cr2.nextFrame();
					cr3.nextFrame();
					cr4.nextFrame();
					VOChannel.stop();
					VOChannel = new SoundChannel();
					VOChannel = VO25Sound.play();
					trace(ct + " frame = " + cr1.currentFrame);
					timerGo(18);
				break;
				
				case 18:
					trace(ct + " frame = " + cr1.currentFrame);
					trace("----------------------------------------- done");
					numPlayers = 0;
					owner.removeThis(this);
				break;*/
			}
		}
		
		/*private function addAmount1(e:TuioTouchEvent):void
		{
			s1 += e.currentTarget.t.text;
			trace(Number(s1)/100);
			cr1.savings.text = "$" + String((Number(s1)/100).toFixed(2));
		}
		
		private function addAmount2(e:TuioTouchEvent):void
		{
			s2 += e.currentTarget.t.text;
			trace(Number(s2)/100);
			cr2.savings.text = "$" + String((Number(s2)/100).toFixed(2));
		}
		
		private function addAmount3(e:TuioTouchEvent):void
		{
			s3 += e.currentTarget.t.text;
			trace(Number(s3)/100);
			cr3.savings.text = "$" + String((Number(s3)/100).toFixed(2));
		}
		
		private function addAmount4(e:TuioTouchEvent):void
		{
			s4 += e.currentTarget.t.text;
			trace(Number(s4)/100);
			cr4.savings.text = "$" + String((Number(s4)/100).toFixed(2));
		}*/
		
		private function goOk(e:TuioTouchEvent):void
		{
			//cr1.ok.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goOk);
			//cr2.ok.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goOk);
			//cr3.ok.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goOk);
			//cr4.ok.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goOk);
			//cr1.removeCalLiseners();
			//cr2.removeCalLiseners();
			//cr3.removeCalLiseners();
			//cr4.removeCalLiseners();
			cr1.gotoAndStop(16);
			cr2.gotoAndStop(16);
			cr3.gotoAndStop(16);
			cr4.gotoAndStop(16);
			cr1.resetGs();
			cr2.resetGs();
			cr3.resetGs();
			cr4.resetGs();
			cr1.addGListeners();
			cr2.addGListeners();
			cr3.addGListeners();
			cr4.addGListeners();
			//cr1.ok.addEventListener(TuioTouchEvent.TOUCH_DOWN, allDone);
			//cr2.ok.addEventListener(TuioTouchEvent.TOUCH_DOWN, allDone);
			//cr3.ok.addEventListener(TuioTouchEvent.TOUCH_DOWN, allDone);
			//cr4.ok.addEventListener(TuioTouchEvent.TOUCH_DOWN, allDone);
		}
		
		private function allDone(e:TuioTouchEvent):void
		{
			cr1.resetGs();
			cr2.resetGs();
			cr3.resetGs();
			cr4.resetGs();
			cr1.removeGListeners();
			cr2.removeGListeners();
			cr3.removeGListeners();
			cr4.removeGListeners();
			
			//cr1.ok.removeEventListener(TuioTouchEvent.TOUCH_DOWN, allDone);
			//cr2.ok.removeEventListener(TuioTouchEvent.TOUCH_DOWN, allDone);
			//cr3.ok.removeEventListener(TuioTouchEvent.TOUCH_DOWN, allDone);
			//cr4.ok.removeEventListener(TuioTouchEvent.TOUCH_DOWN, allDone);
			
			cr1.gotoAndStop(17);
			cr2.gotoAndStop(17);
			cr3.gotoAndStop(17);
			cr4.gotoAndStop(17);
			
			cr1.calc();
			cr2.calc();
			cr3.calc();
			cr4.calc();
			
			timerGo(17);
		}
		
		/*private function setG1(e:TuioTouchEvent):void
		{
			e.currentTarget.gotoAndStop(2);
			cr1.cCalc.visible = true;
			cr1.tCalc.visible = true;
		}
		
		private function setG2(e:TuioTouchEvent):void
		{
			e.currentTarget.gotoAndStop(2);
		}
		
		private function setG3(e:TuioTouchEvent):void
		{
			e.currentTarget.gotoAndStop(2);
		}
		
		private function setG4(e:TuioTouchEvent):void
		{
			e.currentTarget.gotoAndStop(2);
		}*/
		
		private function moveOn(e:TuioTouchEvent):void
		{
			//cr1.moveOn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, moveOn);
			//cr2.moveOn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, moveOn);
			//cr3.moveOn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, moveOn);
			//cr4.moveOn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, moveOn);
			cr1.gotoAndStop(14);
			cr2.gotoAndStop(14);
			cr3.gotoAndStop(14);
			cr4.gotoAndStop(14);
			timerGo(8);
		}
		
		private function questTimer(e:TimerEvent):void
		{
			qTime--;
			if (cr1.countTime != null)
			{
				cr1.countTime.text = qTime.toString();
			}
			if (cr2.countTime != null)
			{
				cr2.countTime.text = qTime.toString();
			}
			if (cr3.countTime != null)
			{
				cr3.countTime.text = qTime.toString();
			}
			if (cr4.countTime != null)
			{
				cr4.countTime.text = qTime.toString();
			}
			if (qTime <= 0)
			{
				gameTimer.removeEventListener(TimerEvent.TIMER, questTimer);
				qTime = 15;
				
				SFXChannel.stop();
					SFXChannel = null;
				SFXChannel = new SoundChannel();
				SFXChannel = creditTimeRanOut.play();
				switch (ct)
				{
					case 4:
						trace("questTimer case 4");
						//remove q listeners
						if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
						{
							ct--;
							trace("questTimer no answer");
							VOChannel.removeEventListener(Event.SOUND_COMPLETE, onVODone);
							VOChannel.stop();
			VOChannel = null;
							VOChannel = new SoundChannel();
							VOChannel = VO6aSound.play();
							VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
						}
						else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
						{
							ct--;
							trace("questTimer wrong answer");
							VOChannel.removeEventListener(Event.SOUND_COMPLETE, onVODone);
							VOChannel.stop();
			VOChannel = null;
							VOChannel = new SoundChannel();
							VOChannel = VO6bSound.play();
							VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
						}
						else
						{
							trace("questTimer right answer");
							//ct++;
							cr1.setScore(cr1.currentFrame);
							cr2.setScore(cr2.currentFrame);
							cr3.setScore(cr3.currentFrame);
							cr4.setScore(cr4.currentFrame);
							cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr1.hideQ();
							cr2.hideQ();
							cr3.hideQ();
							cr4.hideQ();
							cr1.nextFrame();
							cr2.nextFrame();
							cr3.nextFrame();
							cr4.nextFrame();
							VOChannel.removeEventListener(Event.SOUND_COMPLETE, onVODone);
							VOChannel.stop();
			VOChannel = null;
							VOChannel = new SoundChannel();
							VOChannel = VO7Sound.play();
							VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
						}
					break;
					
					case 7:
						trace("questTimer case 7");
						if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
						{
							ct--;
							VOChannel = VO9aSound.play();
							VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
						}
						else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
						{
							ct--;
							VOChannel = VO9bSound.play();
							VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
						}
						else
						{
							cr1.setScore(cr1.currentFrame);
							cr2.setScore(cr2.currentFrame);
							cr3.setScore(cr3.currentFrame);
							cr4.setScore(cr4.currentFrame);
							cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr1.hideQ();
							cr2.hideQ();
							cr3.hideQ();
							cr4.hideQ();
							cr1.nextFrame();
							cr2.nextFrame();
							cr3.nextFrame();
							cr4.nextFrame();
							VOChannel = VO10Sound.play();
							VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
						}
					break;
					case 10:
					trace("questTimer case 10");
						if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
						{
							ct--;
							VOChannel = VO12aSound.play();
							VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
						}
						else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
						{
							ct--;
							VOChannel = VO12bSound.play();
							VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
						}
						else
						{
							cr1.setScore(cr1.currentFrame);
							cr2.setScore(cr2.currentFrame);
							cr3.setScore(cr3.currentFrame);
							cr4.setScore(cr4.currentFrame);
							cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr1.hideQ();
							cr2.hideQ();
							cr3.hideQ();
							cr4.hideQ();
							cr1.nextFrame();
							cr2.nextFrame();
							cr3.nextFrame();
							cr4.nextFrame();
							VOChannel = VO13Sound.play();
							VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
						}
					break;
					case 13:
					trace("questTimer case 13");
						if (cr1.didNotAnswer == true && cr2.didNotAnswer == true && cr3.didNotAnswer == true && cr4.didNotAnswer == true)
						{
							ct--;
							VOChannel = VO15aSound.play();
							VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
						}
						else if (cr1.thisCorrect == false && cr2.thisCorrect == false && cr3.thisCorrect == false && cr4.thisCorrect == false)
						{
							ct--;
							VOChannel = VO15bSound.play();
							VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
						}
						else
						{
							cr1.setScore(cr1.currentFrame);
							cr2.setScore(cr2.currentFrame);
							cr3.setScore(cr3.currentFrame);
							cr4.setScore(cr4.currentFrame);
							cr1.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr2.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr3.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr4.setMeters(currentPlayer1.score, currentPlayer2.score, currentPlayer1.thisCorrect, currentPlayer2.thisCorrect, currentPlayer1.didNotAnswer, currentPlayer2.didNotAnswer);
							cr1.hideQ();
							cr2.hideQ();
							cr3.hideQ();
							cr4.hideQ();
							cr1.nextFrame();
							cr2.nextFrame();
							cr3.nextFrame();
							cr4.nextFrame();
							VOChannel = VO16Sound.play();
							VOChannel.addEventListener(Event.SOUND_COMPLETE, onVODone);
						}
					break;
				}
			}
		}
		
		private function timerGo(n:Number):void
		{
			trace("timerGo ct before = " + ct);
			ct = n;
			trace("timerGo ct after = " + ct);
			switch (ct)
			{
				case 1:
					trace("timerGo ************************* case 1");
					gameTimer = new Timer(6000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				/*case 2:
					
					gameTimer = new Timer(20000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 3:
					gameTimer = new Timer(20000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;*/
				case 4:
					trace("timerGo ************************* case 4");
					gameTimer = new Timer(1000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, questTimer);
					SFXChannel.stop();
					SFXChannel = null;
					SFXChannel = new SoundChannel();
					SFXChannel = creditQuestionMusicRound1.play();
				break;
				/*case 5:
					gameTimer = new Timer(20000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 6:
					gameTimer = new Timer(20000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;*/
				case 7:
					trace("timerGo ************************* case 7");
					gameTimer = new Timer(1000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, questTimer);
					SFXChannel.stop();
					SFXChannel = null;
					SFXChannel = new SoundChannel();
					SFXChannel = creditQuestionMusicRound1.play();
				break;
				/*case 8:
					gameTimer = new Timer(20000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 9:
					gameTimer = new Timer(20000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;*/
				case 10:
					trace("timerGo ************************* case 10");
					gameTimer = new Timer(1000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, questTimer);
					SFXChannel.stop();
					SFXChannel = null;
					SFXChannel = new SoundChannel();
					SFXChannel = creditQuestionMusicRound1.play();
				break;
				/*case 11:
					gameTimer = new Timer(20000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 12:
					gameTimer = new Timer(20000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;*/
				case 13:
					trace("timerGo ************************* case 13");
					gameTimer = new Timer(1000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, questTimer);
					SFXChannel.stop();
					SFXChannel = null;
					SFXChannel = new SoundChannel();
					SFXChannel = creditQuestionMusicRound1.play();
				break;
				/*case 14:
					gameTimer = new Timer(20000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 15:
					gameTimer = new Timer(15000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;*/
				case 16:
					trace("timerGo ************************* case 16");
					gameTimer = new Timer(1000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, questTimer);
					SFXChannel.stop();
					SFXChannel = null;
					SFXChannel = new SoundChannel();
					SFXChannel = creditQuestionMusicRound1.play();
				break;
				case 24:
					trace("timerGo ************************* case 24");
					gameTimer = new Timer(30000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 17:
					trace("timerGo ************************* case 17");
					gameTimer = new Timer(10000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 18:
					trace("timerGo ************************* case 18");
					gameTimer = new Timer(10000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 19:
					trace("timerGo ************************* case 19");
					gameTimer = new Timer(15000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
			}
		}
		
		private function cr1Chosen(e:TuioTouchEvent):void
		{
			cr1.q1.gotoAndStop(1);
			cr1.q2.gotoAndStop(1);
			cr1.q3.gotoAndStop(1);
			e.currentTarget.gotoAndStop(2);
		}
		
		private function cr2Chosen(e:TuioTouchEvent):void
		{
			cr2.q1.gotoAndStop(1);
			cr2.q2.gotoAndStop(1);
			cr2.q3.gotoAndStop(1);
			e.currentTarget.gotoAndStop(2);
		}
		
		private function cr3Chosen(e:TuioTouchEvent):void
		{
			cr3.q1.gotoAndStop(1);
			cr3.q2.gotoAndStop(1);
			cr3.q3.gotoAndStop(1);
			e.currentTarget.gotoAndStop(2);
		}
		
		private function cr4Chosen(e:TuioTouchEvent):void
		{
			cr4.q1.gotoAndStop(1);
			cr4.q2.gotoAndStop(1);
			cr4.q3.gotoAndStop(1);
			e.currentTarget.gotoAndStop(2);
		}

	}
	
}
