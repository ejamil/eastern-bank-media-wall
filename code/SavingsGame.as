﻿package  code
{
	import flash.display.MovieClip;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	
	import org.tuio.*;
	import org.tuio.adapters.*;
	import org.tuio.gestures.*;
	import org.tuio.debug.TuioDebug;
	import org.tuio.TuioManager;
	import org.tuio.connectors.*;
	
	public class SavingsGame extends MovieClip
	{
		public var owner:Document;
		public var gameTimer:Timer;
		private var ct:Number = 0;
		
		private var s1:String = "";
		private var s2:String = "";
		private var s3:String = "";
		private var s4:String = "";
		
		public var countdownTimer:Timer;
		public var time:Number = 10;
		
		private var player1:Boolean = false;
		private var player2:Boolean = false;
		private var player3:Boolean = false;
		private var player4:Boolean = false;
		
		public var lo1:LoGm;
		public var lo2:LoGm;
		public var lo3:LoGm;
		public var lo4:LoGm;
		
		public var whatLang:String;
		
		
		public function SavingsGame(myOwner:Document, lang:String) 
		{
			owner = myOwner;
			whatLang = lang;
			
			lo1 = new LoGm(this);
			addChild(lo1);
			lo2 = new LoGm(this);
			addChild(lo2);
			lo3 = new LoGm(this);
			addChild(lo3);
			lo4 = new LoGm(this);
			addChild(lo4);
			//lo1.x = 159.2;
			lo2.x = 1080;// + 159.2;
			lo3.x = 2160;// + 159.2;
			lo4.x = 3240;// + 159.2;
			//lo1.y = lo2.y = lo3.y = lo4.y = 181.1;
		}
		
		public function startGame():void
		{
			/*ct = 1;
			gameTimer = new Timer(3000);
			gameTimer.stop();
			gameTimer.start();
			gameTimer.addEventListener(TimerEvent.TIMER, countdown);*/
			
			lo1.startGame();
			lo2.startGame();
			lo3.startGame();
			lo4.startGame();
		}
		
		public function isNowPlaying():void
		{
			if (lo1.playerGo == false)
			{
				lo1.stopCounting();
				lo1.visible = false;
				lo1.stopEverything();
			}
			if (lo2.playerGo == false)
			{
				lo2.stopCounting();
				lo2.visible = false;
				lo2.stopEverything();
			}
			if (lo3.playerGo == false)
			{
				lo3.stopCounting();
				lo3.visible = false;
				lo3.stopEverything();
			}
			if (lo4.playerGo == false)
			{
				lo4.stopCounting();
				lo4.visible = false;
				lo4.stopEverything();
			}
		}
		
		public function removeThis():void
		{
			lo1.stopCounting();
			lo2.stopCounting();
			lo3.stopCounting();
			lo4.stopCounting();
			owner.removeThis(this);
		}
		
		public function allDone():void
		{
			owner.removeThis(this);
		}
		
		

	}
	
}
