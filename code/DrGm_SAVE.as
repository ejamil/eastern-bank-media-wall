﻿package code 
{
	import flash.display.MovieClip;
	
	import org.tuio.*;
	import org.tuio.adapters.*;
	import org.tuio.gestures.*;
	import org.tuio.debug.TuioDebug;
	import org.tuio.TuioManager;
	import org.tuio.connectors.*;
	
	import flash.events.*;
	import flash.utils.*;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	
	public class DrGm extends MovieClip 
	{
		public var bubbleArray:Array = new Array();
		private var fadeCounter:Number = 0;
		public var isBeginning:Boolean = true;
		public var isEnd:Boolean = false;

		public var owner:LoGm;
		public var b1:DrGmBubble;
		public var b2:DrGmBubble;
		public var b3:DrGmBubble;
		public var b4:DrGmBubble;
		public var b5:DrGmBubble;
		public var b6:DrGmBubble;
		public var b7:DrGmBubble;
				
		public function DrGm(myOwner:LoGm)//myOwner:LoGm
		{
			owner = myOwner;

			b1 = new DrGmBubble(this, 330, 1025, 1, .9);
			b2 = new DrGmBubble(this, 800, 1290, 2, .35);
			b3 = new DrGmBubble(this, 850, 1425, 3, .35);
			b4 = new DrGmBubble(this, 655, 1540, 4, .35);
			b5 = new DrGmBubble(this, 730, 1690, 5, .35);
			b6 = new DrGmBubble(this, 800, 800, 6, .9);
			b7 = new DrGmBubble(this, 350, 1370, 7, .35);
			addChild(b1);
			addChild(b2);
			addChild(b3);
			addChild(b4);
			addChild(b5);
			addChild(b6);
			addChild(b7);
			setChildIndex(dreamNext, numChildren-1);
			bubbleArray.push(b1, b2, b3, b4, b5, b6, b7);

			TweenMax.to(dreamFace, 1, {y:1920, ease:Strong.easeOut, onComplete:startBubbles});
		}
		
		public function startBubbles()
		{
			TweenMax.to(bc1, .5, {frame:21, ease:Strong.easeOut});
			TweenMax.to(bc2, 1, {frame:40, ease:Strong.easeOut});
			TweenMax.to(b1, .5, {delay:.25, alpha:1, ease:Linear.easeNone, onComplete:addEvent, onCompleteParams:["car"]});
			TweenMax.to(b6, .5, {delay:.5, alpha:1, ease:Linear.easeNone, onComplete:addEvent, onCompleteParams:["vacation"]});
		}
		
		public function addEvent(type:String)
		{
			if(type == "car")
			{
				b1.addClick();
			}
			if(type == "vacation")
			{
				b6.addClick();
			}
			if(type == "yes")
			{
				//yesButt.addEventListener(MouseEvent.CLICK, heckYeah);
				yesButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
			}
			if(type == "done")
			{
				dreamNext.addEventListener(TuioTouchEvent.TOUCH_DOWN, onward);
				//dreamNext.addEventListener(MouseEvent.CLICK, onward);
			}
			if(type == "both")
			{
				dreamNext.addEventListener(TuioTouchEvent.TOUCH_DOWN, onward);
				//dreamNext.addEventListener(MouseEvent.CLICK, onward);
				
				//yesButt.addEventListener(MouseEvent.CLICK, heckYeah);
				yesButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
			}
		}
		public function removeEvent(type:String, bubble:DrGmBubble = null, index:Number = 0)
		{
			if(type == "yes")
			{
				//yesButt.removeEventListener(MouseEvent.CLICK, heckYeah);
				yesButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
			}
			if(type == "done")
			{
				dreamNext.removeEventListener(TuioTouchEvent.TOUCH_DOWN, onward);
				//dreamNext.removeEventListener(MouseEvent.CLICK, onward);
			}
			if(type == "both")
			{
				dreamNext.removeEventListener(TuioTouchEvent.TOUCH_DOWN, onward);
				//dreamNext.removeEventListener(MouseEvent.CLICK, onward);
				
				//yesButt.removeEventListener(MouseEvent.CLICK, heckYeah);
				yesButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
			}
			if(type == "object")
			{
				if(bubble != null)
				{
					trace("removing your dreams...awww");
					removeChild(bubble);
					bubbleArray.splice(index, 1);
				}
			}
		}
		
		public function playBubbleNoise():void
		{
			owner.playSound(3);
		}
		
		public function fader(type:String, bubble:DrGmBubble = null)
		{			
			if(type == "in")
			{
				//////Have bubbles fade in. Wait 5 seconds then start fading them out//////
				for (var i:Number=0; i < bubbleArray.length; i++)
				{
					if (bubbleArray[i].isOpen != true && bubbleArray[i].isHiding == true)
					{
						bubbleArray[i].isHiding = false;
						TweenMax.to(bubbleArray[i], 1, {delay:4*i, alpha:1, ease:Linear.easeNone, onComplete:fader, onStart:playBubbleNoise, onCompleteParams:["out", bubbleArray[i]]});
					}
				}
			}
			if(type == "out")
			{
				bubble.addClick();
				TweenMax.to(bubble, 1, {delay:7, alpha:0, ease:Linear.easeNone, onComplete:checkHide, onCompleteParams:[bubble]});
				TweenMax.delayedCall(7, fader, ["in"]);
			}
		}
		
		public function removeClick():void
		{
			
		}
		
		public function select(e:TuioTouchEvent) //e:TuioTouchEvent
		{
			//////When selected, remove listener and move to center//////
			e.target.removeClick();
			TweenMax.killTweensOf(e.target);
			gotoEnd(e.target);
			e.target.alpha = 1;
			e.target.isHiding = false;
			//TweenMax.to(yesButt, .2, {alpha:0, ease:Linear.easeNone, onComplete:checkYesPos, onCompleteParams:[e.target]});
			yesButt.alpha = 0;
			checkYesPos(e.target);
			trace("DrGm select currentTarget = " + e.currentTarget.name);
			trace("DrGm select target = " + e.target.name);
			
			switch (e.target)
			{
				case b1:
					trace("is 1");
					owner.playSound(2);
				break;
				case b2:
					trace("is 2");
					owner.playSound(12);
				break;
				case b3:
					trace("is 3");
					owner.playSound(7);
				break;
				case b4:
					trace("is 4");
					owner.playSound(13);
				break;
				case b5:
					trace("is 5");
					owner.playSound(9);
				break;
				case b6:
					trace("is 6");
					owner.playSound(11);
				break;
				case b7:
					trace("is 7");
					owner.playSound(4);
				break;
			}
			
			if(isBeginning == true)
			{
				isBeginning = false;
				TweenMax.to(bc1, .3, {alpha:0, ease:Linear.easeNone});
				TweenMax.to(bc2, .3, {alpha:0, ease:Linear.easeNone});				
				
				if(e.target.pos == 1)
				{
					b6.isOpen = false;
					b6.isHiding = false;
					b1.gotoAndStop(8);
					b6.gotoAndStop(9);
					fader("out", b6);
				}
				if(e.target.pos == 6)
				{
					b1.isOpen = false;
					b1.isHiding = false;
					b1.gotoAndStop(8);
					b6.gotoAndStop(9);
					fader("out", b1);
				}
				
				fader("in");
			}
			else
			{
				for (var i:Number=0; i < bubbleArray.length; i++)
				{
					if (bubbleArray[i].scaleX > .5)
					{
						trace("not what i want..." + i);
						fader("out", bubbleArray[i]);
					}
				}
			}
		}
		
		public function gotoEnd(targ:Object)
		{
			//////Set up screen for large bubble. Move bubble to front//////
			TweenMax.to(dreamNext, .5, {x:0, ease:Strong.easeOut, onComplete:addEvent, onCompleteParams:["both"]});
			TweenMax.to(dreamFace, .5, {scaleX:.65, scaleY:.65, x:300, y:1734, ease:Strong.easeOut});
			TweenMax.to(bigB, .5, {alpha:1, ease:Linear.easeNone});
			TweenMax.to(dreamText, .5, {alpha:0, ease:Linear.easeNone});
			bigB.gotoAndStop(targ.pos);
			//setChildIndex(targ, numChildren-2);
			
			//////Set selected as open, move it to center and reset all other bubbles//////			
			targ.isOpen = true;
			for (var i:Number=0; i < bubbleArray.length; i++)
			{
				if (bubbleArray[i].isOpen == true)
				{
					TweenMax.to(bubbleArray[i], .5, {scaleX:.65, scaleY:.65, x:358, y:1146, ease:Strong.easeOut});
					targ.isOpen = false;
				}
				else
				{
					bubbleArray[i].resetDream("start");
				}
			}
		}
		
		public function checkYesPos(bubble:Object)
		{
			//////Add back the event listener, and move the bubble to its reset start position//////
			setChildIndex(yesButt, numChildren-1);
			TweenMax.to(yesButt, .5, {delay:.3, alpha:1, ease:Linear.easeNone});
			
			if (bubble.pos == 1)
			{
				yesButt.x = 525;
				yesButt.y = 1170;
			}
			if (bubble.pos == 2)
			{
				yesButt.x = 400;
				yesButt.y = 1075;
			}
			if (bubble.pos == 3)
			{
				yesButt.x = 525;
				yesButt.y = 1180;
			}
			if (bubble.pos == 4)
			{
				yesButt.x = 525;
				yesButt.y = 1170;
			}
			if (bubble.pos == 5)
			{
				yesButt.x = 275;
				yesButt.y = 1210;
			}
			if (bubble.pos == 6)
			{
				yesButt.x = 430;
				yesButt.y = 1210;
			}
			if (bubble.pos == 7)
			{
				yesButt.x = 225;
				yesButt.y = 1210;
			}
		}
		
		public function checkHide(bubble:DrGmBubble)
		{
			bubble.removeClick();
			bubble.isHiding = true;
		}
		
		public function heckYeah(e:TuioTouchEvent)//e:TuioTouchEvent
		{
			removeEvent("yes");
			owner.playSound(1);
			owner.playSound(8);
			TweenMax.to(yesButt, .5, {alpha:0, ease:Linear.easeNone});
			TweenMax.to(bigB, .5, {alpha:0, ease:Linear.easeNone});
			TweenMax.to(dreamText, .5, {alpha:1, ease:Linear.easeNone});
			isEnd = true;
			
			for (var i:Number=0; i < bubbleArray.length; i++)
			{
				if (bubbleArray[i].scaleX > .5)
				{
					TweenMax.to(bubbleArray[i], .5, {alpha:0, ease:Linear.easeNone, onComplete:removeEvent, onCompleteParams:["object", bubbleArray[i], i]});
					owner.iconArray[owner.whichi] = i + 1;
					owner.whichi++;
				}
				else
				{
					bubbleArray[i].resetDream("end");
				}
			}
			
			trace("owner iconArray = " + owner.iconArray);
		}
		
		public function onward(e:TuioTouchEvent)//e:TuioTouchEvent
		{
			trace("something obviously awesome is going to happen here. Possibly even life changing.");
			TweenMax.killTweensOf(b1);
			TweenMax.killTweensOf(b2);
			TweenMax.killTweensOf(b3);
			TweenMax.killTweensOf(b4);
			TweenMax.killTweensOf(b5);
			TweenMax.killTweensOf(b6);
			TweenMax.killTweensOf(b7);
			TweenMax.killTweensOf(bc1);
			TweenMax.killTweensOf(bc2);
			TweenMax.killTweensOf(yesButt);
			TweenMax.killTweensOf(bigB);
			TweenMax.killTweensOf(dreamText);
			TweenMax.killTweensOf(dreamNext);
			TweenMax.killTweensOf(dreamFace);
			//TweenMax.killTweensOf(bubble);
			owner.playSound(1);
			owner.countdown();
			removeEvent("done");
		}
	}
}
