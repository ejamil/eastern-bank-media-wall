﻿package
{
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.utils.deg2rad;
	public class Game extends Sprite
	{
		private var q:Quad;
		private var r:Number = 0;
		private var g:Number = 0;
		private var b:Number = 0;
		private var rDest:Number;
		private var gDest:Number;
		private var bDest:Number;
		private var rot:Number = 360;
		public function Game()
		{
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		private function onAdded ( e:Event ):void
		{
			resetColors();
			q = new Quad(200, 200);
			q.x = stage.stageWidth - q.width >> 1;
			q.y = stage.stageHeight - q.height >> 1;
			addChild ( q );
			q.addEventListener(Event.ENTER_FRAME, onFrame);
		}
		private function onFrame (e:Event):void
		{
			r -= (r - rDest) * .01;
			g -= (g - gDest) * .01;
			b -= (b - bDest) * .01;
			var color:uint = r << 16 | g << 8 | b;
			q.color = color;
			// when reaching the color, pick another one
			if ( Math.abs( r - rDest) < 1 && Math.abs( g - gDest) < 1 &&
			Math.abs( b - bDest) ) resetColors();
			/*q.rotation += deg2rad(rot/360);
			trace("q.rotation = " + q.rotation);
			trace("deg2rad(360) = " + deg2rad(360));
			trace("deg2rad(rot/360) = " + deg2rad(rot/360));
			if (q.rotation >= deg2rad(360))
			{
				q.rotation = deg2rad(0);
				resetRotation();
			}*/
		}
		private function resetColors():void
		{
			rDest = Math.random()*255;
			gDest = Math.random()*255;
			bDest = Math.random()*255;
		}
		private function resetRotation():void
		{
			rot = Math.random()*360;
		}
	}
}