﻿package com.swingpants.effect
{	
	import flash.net.LocalConnection;
	import flash.display.*;
	import flash.events.*;
	import flash.geom.*;
	import flash.net.*;
	import flash.system.*;
	import org.libspark.betweenas3.BetweenAS3;
	import org.libspark.betweenas3.events.TweenEvent;
	import org.libspark.betweenas3.easing.*;
	import org.libspark.betweenas3.tweens.ITween;
    import org.libspark.betweenas3.tweens.IObjectTween;
	
	import code.Document;

	import flash.filters.DropShadowFilter;
	import flash.filters.BitmapFilterQuality;

	/*
	 * GenieBmd - Adaption of Ginny effect by Clockmaker: http://wonderfl.net/code/b8ec2e7155357ddc65d21eb8b1fa2e94c8363cfc
	 * 
	
	 */

	/*
	Class: com.swingpants.effect.GenieBmd
	
	Adaptor: Swingpants 16th Sept 2009
	
	Adapted from original: Ginny Effect by Clockmaker: http://wonderfl.net/code/b8ec2e7155357ddc65d21eb8b1fa2e94c8363cfc
	
	Version: 0.1
	
	Adaptions: Effect can be instantiated, fired by call, bitmapdata updated
	
	Description: Takes bitmapdata and applies the Genie(Ginny) effect to itself
	
	 * Usage: 
	 * //Construct
	 * import com.swingpants.effect.GenieBmd
	 * var gb:GenieBmd=new GenieBmd(400,300,20)// image width, height, number of segments
	 * gb.startGenie(bmd) // Bitmapdata
	 * gb.fireAtPoint(50,100,1) //x, y, speed (in secs)
	 * 
	
	
	*/

	public class GenieBmd extends Sprite
	{

		private const SCREENW:int = 425;
		private const SCREENH:int = 590;
		private const IMAGE_URL:String = "bug.jpg";
		public var IMG_W:int = 425;
		public var IMG_H:int = 590;
		private var SEGMENT:int = 20;
		private var loader:Loader;
		private var vertexs:Array;
		public var sprite:Sprite;
		private var isHide:Boolean = true;
		private var isShift:Boolean = false;
		private var shadowAlpha:Number = 1;
		private var shadowAngle:Number = 45;
		private var shadowBlurX:Number = 30;
		private var shadowBlurY:Number = 30;
		private var shadowColor:uint = 0x003366;
		private var shadowDistance:Number = 0;
		private var shadowHideObject:Boolean = false;
		private var shadowInner:Boolean = false;
		private var shadowKnockout:Boolean = false;
		private var shadowQuality:int = BitmapFilterQuality.MEDIUM;
		private var shadowStrength:Number = .75;
		private var sf:DropShadowFilter;


		private var _bmd:BitmapData;
		
		public var owner:Document;
		/*
		@function: GenieBmd - contructor
		@param: w - image width
		@param: h - image height
		@param: segment - segments to use
		*/
		public function GenieBmd(myOwner:Document, w:int=425,h:int=590,segment:int=20, sx:Number = 0, sy:Number = 0, offset:Number = 0):void
		{
			owner = myOwner;
			this.visible = false;
			sf = new DropShadowFilter(shadowDistance,shadowAngle,shadowColor,shadowAlpha,shadowBlurX,shadowBlurY,shadowStrength,shadowQuality,shadowInner,shadowKnockout,shadowHideObject);

			// init 
			IMG_W = w;
			IMG_H = h;
			SEGMENT = segment;
			init(sx, sy, offset);
		}
		
		public function gcHack():void {
			// unsupported technique that seems to force garbage collection
			try {
				new LocalConnection().connect('foo');
				new LocalConnection().connect('foo'); // not a duplication, it must be called twice!
			} catch (e:Error) {}
		}
		
		/*
		 @function init
		
		*/
		public function init(curX:Number, curY:Number, offset:Number):void
		{
			sprite = new Sprite();
			addChild(sprite);
			sprite.filters = [sf];
			sprite.x = curX + (IMG_W * offset);
			sprite.y = curY + (IMG_H * offset);
			//owner.addText("genie sprite width = " + sprite.width + ", height = " + sprite.height);
			vertexs = [];
			for (var xx:int = 0; xx < SEGMENT; xx++)
			{
				vertexs[xx] = [];
				for (var yy:int = 0; yy < SEGMENT; yy++)
				{
					vertexs[xx][yy] = new Point(xx * IMG_W / SEGMENT, yy * IMG_H/SEGMENT);
				}
			}
			
		}
		/*
		  @function applyNewImage - swap out the bitmaopdata
		 */
		public function applyNewImage(bmd:BitmapData):void
		{
			_bmd = bmd;
		}

		/*
		 @function startGenie - start the Genie efect
		 @param bmd - If passed then this  bitmapdata will overwrite the current
		 */
		public function startGenie(bmd:BitmapData=null):void
		{
			if (bmd)
			{
				applyNewImage(bmd);
			}
			draw();
			addEventListener(Event.ENTER_FRAME, draw);
		}
		/*
		 * function stopGenie - stop the Genie efect
		 */
		public function stopGenie():void
		{
			removeEventListener(Event.ENTER_FRAME, draw);
		}
		private function onComplete(e:TweenEvent):void 
        {
            this.visible = true;
        }
		
		/*
		 @function fireAtPoint - fire the Genie efect at the given point
		 @param posx - x position to fire
		 @param posy - y position to fire
		 @param speed - time to take to complete the transition (in seconds)
		 */
		public function fireAtPoint(posx:int,posy:int, speed:Number=1):void
		{
			var tweens:Array = [];
			var xx:int,yy:int,delay:Number;
			var px:Number = SEGMENT * ((posx - sprite.x) / IMG_W);
			var py:Number = SEGMENT * ((posy - sprite.y) / IMG_H);
			//sprite.visible = true;
			var objTween:IObjectTween = BetweenAS3.tween(this, {x:0}, null, .005);
            objTween.addEventListener(TweenEvent.COMPLETE, onComplete);
            objTween.play();
			
			if (! isHide)
			{
				for (xx = 0; xx < SEGMENT; xx++)
				{
					for (yy = 0; yy < SEGMENT; yy++)
					{
						vertexs[xx][yy] = new Point(xx * IMG_W / SEGMENT, yy * IMG_H/SEGMENT);
						delay = Math.sqrt((xx - px) * (xx - px) + (yy - py) * (yy - py)) / 40;

						tweens.push( 
						                              BetweenAS3.delay( 
						                                  BetweenAS3.tween(vertexs[xx][yy], { 
						                                      x : px * IMG_W / SEGMENT, 
						                                      y : py * IMG_H / SEGMENT 
						                                  },null, delay, Cubic.easeIn), 
						                                  delay / 2 
						                              ) 
						                          );
					}
				}
			}
			else
			{
				var max:Number = 0;
				for (xx = 0; xx < SEGMENT; xx++)
				{
					for (yy = 0; yy < SEGMENT; yy++)
					{
						vertexs[xx][yy] = new Point(px * IMG_W / SEGMENT, py * IMG_H / SEGMENT);
						delay = Math.sqrt((xx - px) * (xx - px) + (yy - py) * (yy - py)) ;
						max = Math.max(max,delay);
					}
				}
				for (xx = 0; xx < SEGMENT; xx++)
				{
					for (yy = 0; yy < SEGMENT; yy++)
					{
						delay = (max - Math.sqrt((xx - px) * (xx - px) + (yy - py) * (yy - py))) / 40;
						tweens.push( 
						                              BetweenAS3.delay( 
						                                  BetweenAS3.tween(vertexs[xx][yy], { 
						                                      x : xx * IMG_W / SEGMENT, 
						                                      y : yy * IMG_H / SEGMENT 
						                                  },null, delay + 0.05, Quad.easeOut), 
						                                  delay *0.5
						                              ) 
						                          );
					}
				}
			}

			var itw:ITween = BetweenAS3.parallelTweens(tweens);
			itw.onComplete = tweensComplete;

			itw = BetweenAS3.scale(itw,speed);
			itw.play();
			
			isHide = ! isHide;
			//this.visible = true;
			gcHack();
		}

		/* 
		   @function tweensComplete - COMPLETE event fired when tweens completed
		   */
		private function tweensComplete():void
		{
			dispatchEvent(new Event(Event.COMPLETE));
			gcHack();
		}

		/* 
		   @function draw - the triangles onto the stage
		   */
		private function draw(e:Event = null):void
		{
			//owner.addText("genie _bmd width = " + _bmd.width + ", height = " + _bmd.height);
			var vertices:Vector.<Number> = new Vector.<Number>();
			var indices:Vector.<int> = new Vector.<int>();
			var uvtData:Vector.<Number> = new Vector.<Number>();

			for (var xx:int = 0; xx < SEGMENT; xx++)
			{
				for (var yy:int = 0; yy < SEGMENT; yy++)
				{
					vertices[vertices.length] = vertexs[xx][yy].x;
					vertices[vertices.length] = vertexs[xx][yy].y;
					uvtData[uvtData.length] = xx / SEGMENT;
					uvtData[uvtData.length] = yy / SEGMENT;
				}
			}

			for (var i:int = 0; i < SEGMENT - 1; i++)
			{
				for (var j:int = 0; j < SEGMENT - 1; j++)
				{
					indices.push(i * SEGMENT + j, i * SEGMENT + j + 1, (i + 1) * SEGMENT + j);
					indices.push(i * SEGMENT + j + 1, (i + 1) * SEGMENT + 1 + j, (i + 1) * SEGMENT + j);
				}
			}

			//Bitmap fill onto triangles
			var g:Graphics = sprite.graphics;
			g.clear();
			g.beginBitmapFill(_bmd);
			g.drawTriangles(vertices, indices, uvtData);
			g.endFill();
			//owner.addText("genie g width = " + g.width + ", height = " + g.height);
			gcHack();
		}

	}
}