<?php
/**
* @file
* This module is for monitoring creation, editing, and deletion of
* specific content types. Then triggering xml generation on 
* specific views during cron. Also generates timestamp files
* that can be checked before ingesting an entire xml file.
*
* Built with the intention of high performance concurrent
* polling of the guide timestamp files in mind. Also
* minimizing operations during cron unless xml regen is
* explicitly needed.
* 
*/

/**
* Implements hook_help
*
* Displays help information for caching_xml_output
*
* @param path
*   Which path of the site we are using to display help
*
* @param arg
*   Array that holds the current path as returned from arg() function
*
*/
function caching_xml_output_help($path, $arg) {
   switch ($path) {
     case "admin/help#caching_xml_output":
	return '<p>' . t("Monitors specific content types for changes, queues views for xml output.") . 
               '<br/>' . t("If items are queued during cron, they are output as static xml files.") .
               '<br/>' . t("Ensure in your views_data_export settings that you export views to a ficticious directory appended by the machine name of the view. So for a data export view with a machine name of of export_view_a you should export to a ficticious path of :") . '<br/>' . t("caching_xml_output_gen_input/export_view_a") .
	       '<br/><br/><a href="http://www.transcendingdigital.com">' . t("Transcending Digital LLC") .
               '</a><br/>' . t("2013") . "<br/>" . '</p>';
     break;
   }
}

/**
* Implements hook_menu()
*
*
*/
function caching_xml_output_menu() {
  $items = array();
  $items['admin/config/content/caching_xml_output'] = array(
    'title' => 'Caching XML Output',
    'description' => 'Configuration for Caching XML Output Module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('caching_xml_output_form'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
*
* Page callback: Caching Xml Output Settings
*
* @see caching_xml_output_menu()
*/
function caching_xml_output_form($form, &$form_state) {
   # We need to provide a select list of all content
   # types here to check which we want to monitor.
   #$typesArr = node_type_get_types();
   $typesArr = node_type_get_names();
   $chkBxOptions = array();

   if( isset($typesArr) ) {
	$viewsArr = array();
        $viewsForForm = array();
        # If views is installed, list views to be regenerated
        if( module_exists('views') == TRUE ) {
           $viewsArr = views_get_all_views();
           foreach ($viewsArr as $singleView) {
	     if($singleView->disabled != TRUE) {
	       $viewsForForm[ $singleView->name ] = $singleView->human_name;
	     }
           }
        }

        # Generate a fieldset containing all views for each content type
        # allowing visitors to select which views should be regenerated
        # for each content type.
	$form['caching_xml_output_ctype_title'] = array (
	  '#type' => 'item',
	  '#title' => t('Select Views To Queue For Regeneration on Cron Run Under Each Content Type You Would Like To Monitor'),
	);

	foreach($typesArr as $typeKey => $typeValue) {
	       $form['caching_xml_output_ctype_categories_' . $typeKey] = array(
			'#tree' => TRUE,
			'#type' => 'fieldset',
			'#title' => $typeValue,
			'#description' => t('Please select zero or more views to regenerate on a change in this content type.'),
			'#collapsible' => TRUE,
			'#collapsed' => FALSE,
		);

		$baseStoredViewList = variable_get('caching_xml_output_ctype_categories_' . $typeKey, array());
		$defaultViewFormOptions = array();
		if( isset( $baseStoredViewList['views'] ) ) {
			$defaultViewFormOptions = $baseStoredViewList['views'];
		}
		$form['caching_xml_output_ctype_categories_' . $typeKey]['views'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Selected Views'),
		'#description' => t('Select the views that should be regenerated when this content type is changed.'),
		'#options' => $viewsForForm,
		'#default_value' => $defaultViewFormOptions,
		'#required' => FALSE,
		);
        }

/*
	$form['caching_xml_output_ctypes'] = array(
		'#type' => 'checkboxes',
		'#title' => t('What content types should we monitor for changes?'),
		'#description' => t('Please select one or more types to monitor'),
		'#default_value' => variable_get('caching_xml_output_ctypes', array()),
		'#options' => $typesArr,
	);
*/

   }


   # If views is installed, list views to be regenerated
/*
   if( module_exists('views') == TRUE ) {
     $viewsArr = views_get_all_views();
     if(isset($viewsArr)) {
       $viewsForForm = array();
	

       foreach ($viewsArr as $singleView) {
	 if( isset($singleView -> fields) ) {
           echo '<br/><br/>View Name: ' . $singleView->human_name . '<br/>Fields:<br/>';
           print_r($singleView -> fields);
         }
	 if($singleView->disabled != TRUE) {
	   $viewsForForm[ $singleView->name ] = $singleView->human_name;
	 }
       }

	$form['caching_xml_output_view_list'] = array(
		'#type' => 'checkboxes',
		'#title' => t('What views should be exported if changes are detected?'),
		'#description' => t('Please select one or more views to export when changes are detected on the selected content types.'),
		'#default_value' => variable_get('caching_xml_output_view_list', array()),
		'#options' => $viewsForForm,
	);

     }

//$enterprize = views_get_view('project_summaries');
//foreach($enterprize as $k => $v) {
//	echo '<br/>enterprize k: ' . $k;
//}
//echo '<br/><br/>';
//var_dump($enterprize);

//var_dump($viewsArr);

    $form['caching_xml_output_treetop'] = array(
     '#tree' => TRUE,
     '#type' => 'fieldset',
     '#title' => t('Use Nodes'),
     '#default_value' => FALSE,
     '#collapsible' => TRUE,
    );
    $form['caching_xml_output_treetop']['views'] = array(
     '#type' => 'checkboxes',
     '#title' => t('Selected Views'),
     '#description' => t('Select the views that should be regenerated when this content type is changed.'),
     '#options' => $viewsForForm,
     '#default_value' => $viewsForForm[0],
     '#required' => FALSE,
    );

   }
*/


   echo '<br/><br/>Saved form values: <br/><br/>';
   print_r(  variable_get('caching_xml_output_ctypes', array()) );

   echo '<br/><br/>Saved monitored ctype flags: <br/><br/>';
   print_r( variable_get('caching_xml_output_ctype_flags',array()) );

   echo '<br/><br/>Saved form structure: <br/><br/>';
   print_r( variable_get('caching_xml_output_ctype_categories',array()) );

  $viewsDataExportDir = 'public://caching_xml_output_gen_input/rohnny_johnny.xml';

   echo '<br/><br/>viewsDataExportDir: ' . file_create_url( file_build_uri($viewsDataExportDir) );
   echo '<br/><br/>viewsDataExportDir: ' . drupal_realpath( $viewsDataExportDir );

/*
   echo '<br/><br/>view execution: <br/><br/>';
   $myview = views_get_view('export_test_a');
   $myview -> set_display('views_data_export_1');
   $myview -> set_items_per_page(0);
   $myview -> pre_execute();
   $myview -> execute();
   echo $myview -> render();
   //var_dump($myview);
*/

   return system_settings_form($form);
}
/**
*
* implements hook_form_validate
*
* Validation is not the correct spot for this, but
* I could not get submit to work right.
*
* With more complex data structures than original
* we will now examine all the inputs and create a
* more simple stored data structre that the node
* monitoring hooks use.
*
* We examine all the content types in our input form and
* ensure they are in the structure used for node monitoring.
* the structure looks like this and includes every ctype:
*
* Array( [blog] => blog [video] => 0 [custom_c_type] = 0 )
*
* Point is, all ctypes should be present. Those unused are
* set to false. We dont care about previous values of this
* entry. It is always overwritten on each config form submit.
*
* @param form
*  I think this allows us to still mod the form
*
* @param form_state
*  The updated form values structure
*
*/
function caching_xml_output_form_validate($form, &$form_state) {
  $generatedMonitorArr = array();

  # Unfortunately we need all the content types again to iterate our data
    $typesArr = node_type_get_names();

  if( isset($typesArr) ) {
	foreach($typesArr as $typeKey => $typeValue) {
	  # First, set the default value
	  $generatedMonitorArr[$typeKey] = 0;

	  if( isset($form_state['values']['caching_xml_output_ctype_categories_' . $typeKey]) ) {
	    # Examine each form submission value to see if we need to monitor this content type
	    foreach( $form_state['values']['caching_xml_output_ctype_categories_' . $typeKey]['views'] as $viewKey => $viewValue) {
		if( $viewValue != FALSE ) {
			$generatedMonitorArr[$typeKey] = $typeKey;
			break;
		}
	    }
	  }
        }
  }
  # Update the stored value so our node monitoring hooks can reference it
    variable_set('caching_xml_output_ctypes', $generatedMonitorArr );
  # Also wipe out anything currently queued to be exported because the user may have just removed an item
  # for export anyway
    variable_set('caching_xml_output_ctype_flags', array() );
}
############################################################
########### NODE MONITORING OPERATIONS #####################
/**
*
* Implements hook_node_insert()
*
* Monitors node insertion to see if this is one of the content
* types we are monitoring for changes.
*
* @param node
*   The node data
*/
function caching_xml_output_node_insert($node) {
   $contentType = '';
   if( isset($node -> type) ) {
	$contentType = $node -> type;
   }

   if( $contentType != '' ) {
    caching_xml_output_check_ctype_cache( $contentType );
   }
}
/**
*
* Implements hook_node_update()
*
* @param node
*   The node data. Careful, may be old data before the update.
*   really does not matter for just monitoring content type.
*/
function caching_xml_output_node_update($node) {
   $contentType = '';
   if( isset($node -> type) ) {
	$contentType = $node -> type;
   }

   if( $contentType != '' ) {
    caching_xml_output_check_ctype_cache( $contentType );
   }
}
/**
* Implements hook_node_delete()
*
* @param node
*   The node data. We just need to check its content type.
*
*/
function caching_xml_output_node_delete($node) {
   $contentType = '';
   if( isset($node -> type) ) {
	$contentType = $node -> type;
   }

   if( $contentType != '' ) {
    caching_xml_output_check_ctype_cache( $contentType );
   }
}

############################################################
############# Cron and Queue Functions #####################
/**
*
* Implements hook_cron()
*
* We use this hook to schedule our xml output queue
* if it is not currently running and there are items
* that need processing.
*
*
* This kind of defies the queue concept because we only
* ever need one instance of our deferred process running.
* So we add a variable to prevent multiple runs concurrently.
*/
function caching_xml_output_cron() {

        $queueInstanceRunning = variable_get('caching_xml_output_queue_running', FALSE);

        if( $queueInstanceRunning == FALSE ) {
          # Write back the running state
	  variable_set('caching_xml_output_queue_running', TRUE);
	  $queue = DrupalQueue::get('caching_xml_output_queue_1');
          # Our queue item requires no data so we pass in an arbitrary param
          # so drupal wont throw errors
          $queue->createItem(FALSE);
	}
}
/**
*
* Implements hook_cron_queue_info()
*
* Here we schedule our awesome xml generation.
* In theory we could separate this and queue smaller
* operations as well if we run into performance issues.
*
* These are supposed to be able to run in parallel.
* Default time is 15 seconds if not specified. These
* will most likeley need more.
*
*/
function caching_xml_output_cron_queue_info() {
  $queues['caching_xml_output_queue_1'] = array(
    'worker callback' => 'caching_xml_output_queue_worker',
    'time' => 120,
  );

  return $queues;
}
/**
*
* This is a worker function that is executed 
* during cron. It handles processing of any 
* xml output that needs to be generated from views.
*
* Views entityreferences requre certain user roles to run.
* Cron runs anonymous and messes everything up so we impersonate
* admin when rendering the view.
*/
function caching_xml_output_queue_worker() {
   # As we operate on them, content types are removed from this
   # associative array. Data is keyed with the content type and the
   # value is true..ex: Array ( [blog] => 1 )
   #
   $flaggedCTypes = variable_get('caching_xml_output_ctype_flags',array());
   # We track the views we have generated output for so we dont double our
   # work across content types
   $generatedViews = array();

   if( count($flaggedCTypes) > 0 ) {

   # Start by impersonating admin in case we do any rendering
     global $user;
     $original_user = $user;
     $original_session_state = drupal_save_session();
     drupal_save_session(FALSE);
     $user = user_load(1);

	foreach($flaggedCTypes as $cTypeKey => $cTypeValue) {
	  $monitoredViews = variable_get('caching_xml_output_ctype_categories_' . $cTypeKey, array());

	  # Get the view data for the content type so we can figure out which we need to re-generate
          # output for.
	  foreach( $monitoredViews['views'] as $viewKey => $viewValue) {
		# Here is a content type we are supposed to monitor
                if( $viewValue != FALSE ) {
		  # Ensure it was not already re-generated in this run
		  if( ! isset($generatedViews[$viewKey]) ) {
		    caching_xml_output_gen_output($viewKey);
		    # Make an entry so we do not re-genreate this view output this run.
		    $generatedViews[$viewKey] = TRUE;
		  }
		}
	   }

	 # Update the stored content types to monitor to remove this entry. Its important to realize this main
         # loop may not complete all in one processing run. This is an expensive operation to read/write
         # but it would be even more expensive to reprocess ouptut over and over
           $freshFlaggedCTypes = variable_get('caching_xml_output_ctype_flags',array());
           if( isset( $freshFlaggedCTypes[$cTypeKey] ) ) {
             unset( $freshFlaggedCTypes[$cTypeKey] );
             # Write the structure back
             variable_set('caching_xml_output_ctype_flags', $freshFlaggedCTypes);
           }
        }

	# Set our user back to anonymous
	$user = $original_user;
	drupal_save_session($original_session_state);
   }

   # This all should block, so we can write back so that it is ready to process again
   # when cron runs. 
   variable_set('caching_xml_output_queue_running', FALSE);
}
############################################################
######### caching_xml_output Utility Functions #############
/**
* This function looks at a content type and checks to
* see if it is in the list of monitored types.
*
* If the type is being monitored, it will check if a
* flag has been set for it. If not, it will set a flag
* for the content type, so cache will be re-built for
* data using it during cron.
*
* @param contentType 
*   Should be a string with machine name content type
*/
function caching_xml_output_check_ctype_cache($contentType) {

   if(!isset($contentType) ) return;

   # Pull the listing of content types we are monitoring
   # Has a structure like
   # Array ( [blog] => blog [video] => 0 [page] => 0 [custom_content_type] => custom_content_type)
   # non selected values have an entry of 0
   $arr_monitorContentTypes = variable_get('caching_xml_output_ctypes', array());

   # We can use associative arrays to our advantage to quickly see without iterating if
   # this is a value we want to monitor 0 equates to FALSE in php
   if( isset( $arr_monitorContentTypes[$contentType] ) ) {
	if( $arr_monitorContentTypes[$contentType] != FALSE ) {
	  # We should set a flag in the database for this content type
	  $arr_flaggedCTypes = variable_get('caching_xml_output_ctype_flags',array());
	  $doFlag = TRUE;

	  if( isset( $arr_flaggedCTypes[$contentType] ) ) {
		if( $arr_flaggedCTypes[$contentType] == TRUE ) {
		  $doFlag = FALSE;
		}
          }
	
	  if($doFlag == TRUE) {
	    $arr_flaggedCTypes[$contentType] = TRUE;
	    variable_set('caching_xml_output_ctype_flags',$arr_flaggedCTypes);
	  }
        }
   }

}
/**
*
* This utility function is called from in our scheduler
* it triggers the real regeneration and output of our
* files.
*
* We programatically execute the views in here and write them to
* disk because calling
* something like wget requires open permissions on the content
* this was designed for private content.
*
* We copy that data to the following:
* caching_xml_output_gen_output/view_machine_name.xml
*
* We need to place a timestamped file at:
* caching_xml_output_gen_output/view_machine_name_timestamp.xml
*
* If you want templates to effect the output ensure they are in your
* admin and main themes!
* 
*/
function caching_xml_output_gen_output($viewMachineName) {
 
  # Ensure our output diirectory exhists or else the writes will fail
  $directoryOperationsResult = FALSE;

  $outputDirectory = 'public://caching_xml_output_gen_output';
  $directoryOperationsResult = file_prepare_directory($outputDirectory, FILE_CREATE_DIRECTORY);

  if($directoryOperationsResult == TRUE) {
	  # Initiate generation of the view programatically
          /*
          $wgetCommA = drupal_realpath( $outputDirectory . '/' . $viewMachineName . '.xml' );
          $wgetCommB = file_create_url('caching_xml_output_gen_input/' . $viewMachineName);

          if( $wgetCommA !== FALSE && $wgetCommB != FALSE ) {
		  $execCommand = 'wget -O ' . $wgetCommA . ' ';
		  $execCommand .= $wgetCommB;
		  exec( $execCommand );
	  }
          */
           
	   $myview = views_get_view( $viewMachineName );
           $viewOutputPath = $outputDirectory . '/' . $viewMachineName . '.xml';

           if($myview !== FALSE) {
		   $myview -> set_display('views_data_export_1');
		   $myview -> set_items_per_page(0);
		   $myview -> pre_execute();
		   $myview -> execute();
                   $viewResults = $myview -> render();
                   if($viewResults != NULL) {
		      file_save_data($viewResults, $viewOutputPath, FILE_EXISTS_REPLACE);
                   }
           }

	  # Generation of the timestamped xml file. Timestamp is number of milliseconds
	  # since Jan 1, 1970 0:00:000 GMT - The linux time stamp
	  $timestampXML = '<InfoXML><XMLBody>';
	  $timestampXML .= '<valueKeyWord><![CDATA[' . $viewMachineName . ']]></valueKeyWord>';
	  $timestampXML .= '<unixTimeStamp><![CDATA[' . time() . ']]></unixTimeStamp>';
	  $timestampXML .= '</XMLBody></InfoXML>';
	  # Try and write out the timestamp
	  $tsFilePath = $outputDirectory . '/' . $viewMachineName . '_timestamp.xml';
	  file_save_data($timestampXML, $tsFilePath, FILE_EXISTS_REPLACE);
  }
}
