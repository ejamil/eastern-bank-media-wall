﻿package code
{
	import com.bostonproductions.events.CMSEvent;
	import com.bostonproductions.data.commandConstants;
	import com.bostonproductions.net.cmsAPI;
	import com.bostonproductions.text.textFieldBaseFunctions;
	
	import flash.text.TextFormat;
	
	import com.greensock.*;
	
	import flash.events.MouseEvent;
	
	import flash.display.MovieClip;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	
	public class Document extends MovieClip
	{
		
		private var _api:cmsAPI;
		private var _savedFormat:TextFormat = null;
		
		public function Document()
		{
			// Save the text format on our alert box for forcing it to fit
			_savedFormat = mcAlertText.txtAlert.getTextFormat();
			
			createCMSAPI();
		}
		
		private function createCMSAPI():void {
			if (_api == null) {
				_api = new cmsAPI();
				_api.addEventListener(cmsAPI.INITCOMPLETE, handleCMSReady, false, 0, true);
				_api.addEventListener(cmsAPI.CMS_DATA, handleCMSData, false, 0, true);
				_api.addEventListener(cmsAPI.NEW_CMS_DATA, handleUnsolicitedNewCMSData, false, 0, true);
				_api.init();
			} else {
				destroyCMSAPI( true );
			}
		}
		
		private function destroyCMSAPI(optionalCallback:Boolean = false):void {
			if (_api != null) {
				_api.removeEventListener(cmsAPI.INITCOMPLETE, handleCMSReady);
				_api.removeEventListener(cmsAPI.CMS_DATA, handleCMSData);
				_api.removeEventListener(cmsAPI.NEW_CMS_DATA, handleUnsolicitedNewCMSData);
				_api.destroyInternals();
				_api = null;
				if (optionalCallback == true) createCMSAPI();
			}
		}
		
		private function handleCMSReady(e:Event):void {
			_api.removeEventListener(cmsAPI.INITCOMPLETE, handleCMSReady);
			
			// We are now able to request data from the CMS
			// If the application was waiting for startup initialization
			// this would be a good time to do it.
			
			// This event means the cms application has cached all content 
			// available for the configured exhibit. On a fresh install combined
			// with a CMS with a lot of content for the target exhibit this
			// may take several minutes for this event to fire. You can watch
			// caching progress on the UI of the caching application.
			btnPromotionsText.enabled = true;
			btnPromotionsVideos.enabled = true;
			btnPromotionsPhotos.enabled = true;
			btnPromotionsAll.enabled = true;
			btnMedGames.enabled = true;
			btnMedLife.enabled = true;
			btnMedBusiness.enabled = true;
			btnMedDreams.enabled = true;
			btnMedHome.enabled = true;
			btnMedAll.enabled = true;	
			/*
			btnVestAll.enabled = true;
			btnVestPhotos.enabled = true;
			btnVestText.enabled = true;
			btnComAll.enabled = true;
			btnComPhotos.enabled = true;
			btnComVideos.enabled = true;
			btnComtText.enabled = true;
			*/
			
			btnPromotionsText.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnPromotionsVideos.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnPromotionsPhotos.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnPromotionsAll.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnMedGames.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnMedLife.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnMedBusiness.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnMedDreams.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnMedHome.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnMedAll.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			/*
			btnVestAll.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnVestPhotos.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnVestText.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnComAll.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnComPhotos.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnComVideos.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			btnComtText.addEventListener(MouseEvent.CLICK, handleMouseClick, false, 0, true);
			*/
			txtData.text = 'Apps Connected..Press the buttons to see example api interaction..';
		}
		
		/**
		 * When you make method calls to the cmsAPI, responses
		 * will come back through this event handler.
		 * 
		 * You would want to parse xml and take action on the data here.
		 * XML will be in the e.Data.resultData property.
		 * @param	e
		 */
		private function handleCMSData(e:CMSEvent):void {
			switch(e.Data.command) {
				case commandConstants.PROMOTIONS_ALL:

					break;
				case commandConstants.PROMOTIONS_PHOTOS:

					break;
				case commandConstants.PROMOTIONS_TEXT:
				
					break;
				case commandConstants.PROMOTIONS_VIDEOS:
					
					break;
				case commandConstants.MEDIA_WALL_ALL:

					break;
				case commandConstants.MEDIA_WALL_BUSINESS:

					break;
				case commandConstants.MEDIA_WALL_DREAMS:
				
					break;
				case commandConstants.MEDIA_WALL_HOME:
					
					break;
				case commandConstants.MEDIA_WALL_LIFE:
				
					break;
				case commandConstants.MEDIA_WALL_GAMES:
					
					break;
				case commandConstants.VESTIBULE_ALL:
				
					break;
				case commandConstants.VESTIBULE_PHOTOS:
					
					break;
				case commandConstants.VESTIBULE_TEXT:
				
					break;
				case commandConstants.COMMUNITY_ALL:
					
					break;
				case commandConstants.COMMUNITY_PHOTOS:
					
					break;
				case commandConstants.COMMUNITY_VIDEOS:
					
					break;
				case commandConstants.COMMUNITY_TEXT:
					
					break;
			}
			
			txtData.text = "command:" + e.Data.command + "\n" + String( e.Data.resultData );
		}
		
		/**
		 * Be very careful here. Events will happen on this event handler unsolicited
		 * whenever new data is detected and cached from the CMS.
		 * 
		 * If you want to have a near real time synchronized display, you should program
		 * around handling these events.  The event comes with the ALL version of data
		 * from each configured display...so all the old and new xml data for each type of
		 * asset combined.
		 * 
		 * If you don't want to deal with parsing all the individual data types out
		 * of an ALL data response here, its perfectly acceptable to respond to an event
		 * like this with an api call to just get photo, video...etc data.
		 * @param	e
		 */
		private function handleUnsolicitedNewCMSData(e:CMSEvent):void {
			switch(e.Data.command) {
				case commandConstants.PROMOTIONS_EVENT:
					showAlertText('Unsolicited PROMOTIONS data from CMS! - (see function handleUnsolicitedNewCMSData)');
				break;
				case commandConstants.MEDIA_WALL_EVENT:
					showAlertText('Unsolicited MEDIA WALL data from CMS! - (see function handleUnsolicitedNewCMSData)');
				break;
				case commandConstants.MEDIA_WALL_GAMES_EVENT:
					showAlertText('Unsolicited MEDIA WALL GAMES data from CMS! - (see function handleUnsolicitedNewCMSData)');
				break;
				case commandConstants.COMMUNITY_EVENT:
					showAlertText('Unsolicited COMMUNITY data from CMS! - (see function handleUnsolicitedNewCMSData)');
				break;
				case commandConstants.VESTIBULE_EVENT:
					showAlertText('Unsolicited VESTIBULE data from CMS! - (see function handleUnsolicitedNewCMSData)');
				break;
			}
			
			txtData.text = " handleUnsolicitedNewCMSData command:" + e.Data.command + "\n" + String( e.Data.resultData );
		}
		
		// ####################################################
		// ######## UNINPORTANT UI CODE #######################
		// ####################################################
		private function handleMouseClick(e:MouseEvent):void {
			switch(e.target.name) {
				case 'btnPromotionsText':
					_api.get_promotions_text();
					break;
				case 'btnPromotionsVideos':
					_api.get_promotions_videos();
					break;
				case 'btnPromotionsPhotos':
					_api.get_promotions_photos();
					break;
				case 'btnPromotionsAll':
					_api.get_promotions_all();
					break;
				case 'btnMedGames':
					_api.get_media_wall_games();
					break;
				case 'btnMedLife':
					_api.get_media_wall_life();
					break;
				case 'btnMedBusiness':
					_api.get_media_wall_business();
					break;
				case 'btnMedDreams':
					_api.get_media_wall_dreams();
					break;
				case 'btnMedHome':
					_api.get_media_wall_home();
					break;
				case 'btnMedAll':
					_api.get_media_wall_all();
					break;
				case 'btnVestAll':
					_api.get_vestibule_all();
					break;
				case 'btnVestPhotos':
					_api.get_vestibule_photos();
					break;
				case 'btnVestText':
					_api.get_vestibule_text();
					break;
				case 'btnComAll':
					_api.get_community_all();
					break;
				case 'btnComPhotos':
					_api.get_community_photos();
					break;
				case 'btnComVideos':
					_api.get_community_videos();
					break;
				case 'btnComtText':
					_api.get_community_text();
					break;
			}
		}
		
		private function showAlertText(_message:String):void {
			TweenLite.killTweensOf(mcAlertText);
			mcAlertText.alpha = 0;
			// Restore the original text format
			mcAlertText.txtAlert.setTextFormat(_savedFormat);
			mcAlertText.txtAlert.text= _message;
			textFieldBaseFunctions.fitTextIntoField(mcAlertText.txtAlert, 1);
			TweenLite.to(mcAlertText, 0.5, {alpha:1, onComplete:hideAlert});
		}
		private function hideAlert():void {
			TweenLite.killTweensOf(mcAlertText);
			TweenLite.to(mcAlertText, 1, { delay:5, alpha:0 } );
		}
	}

}