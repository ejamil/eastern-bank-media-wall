package com.bostonproductions.net
{
	import com.bostonproductions.data.vo.cmsDataVO;
	import com.bostonproductions.events.CMSEvent;
	
	import com.technogumbo.net.StringSockets;
	
	import flash.errors.IOError;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.Socket;
	import flash.utils.Timer;
	
	import com.hurlant.util.Base64;
	
		
	/**
	 * This class handles the network link to the
	 * other application that handles CMS communication
	 * and asset caching.
	 * 
	 * Handles decoding and encoding of requests over
	 * the TCP socket connection to the other application.
	 * Data is sent between apps in Base64 so CMS content
	 * cannot mess up message delimeters.
	 * 
	 * Requests to the other application are base64 encoded
	 * methods such as "GET_PROMOTIONS_VIDEOS".
	 * The string sockets append a pipe to the end of each
	 * message used as a delimeter.
	 * 
	 * Responses from the other application come back in the
	 * form: GET_PROMOTIONS_VIDEOS,Full XML Data|
	 * 
	 * Both segments of the above message are base64 encoded uppon
	 * retrieval over sockets. We docode the messages appropriately.
	 */
	public class cmsLink extends EventDispatcher
	{
	    public static const DATA:String = "com.bostonproductions.net.cmsLink.DATA";
		
		private var _socket:StringSockets;
		private var _socketDataQueue:Vector.<String> = new Vector.<String>();
		
		public function cmsLink()
		{

		}
		
		public function init():void {
			createSocket();
		}
		/**
		 * During execution this class should be active
		 * at all times, but if for some reason you want
		 * to ensure it is completley gotten rid of,
		 * call this function right before nulling the
		 * instance value of the class. It will ensure
		 * all internal assets are cleaned up.
		 */
		public function destroyInternals():void {

		}
		
		public function sendSocketString(_Data:String):void {
			_socketDataQueue.push( _Data );
			
			emptySocketDataQueue();
		}
		
		private function emptySocketDataQueue():void {
			_socketDataQueue = _socketDataQueue.reverse();
			
			for (var i:int = _socketDataQueue.length - 1; i > -1; i--) {
				if (_socket != null) {
					if ( _socket.getConnectedStatus() == true ) {
						_socket.sendString( Base64.encode( _socketDataQueue[i] ) );
						
						_socketDataQueue.splice(i, 1);
					}
				}
			}
			
			// Just in case mid send there were any problems reverse back to restore the original order
			_socketDataQueue = _socketDataQueue.reverse();
		}
		
		private function createSocket():void {
			if (_socket == null) {
				_socket = new StringSockets("127.0.0.1", 32385, "|");
				_socket.addEventListener(StringSockets.CONNECTED, handleSocketConnected, false, 0, true);
				_socket.addEventListener(StringSockets.DISCONNECTED, handleSocketDisconnected, false, 0, true);
				_socket.addEventListener(StringSockets.ERROR, handleSocketError, false, 0, true);
				_socket.addEventListener(StringSockets.STRINGDATA, handleSocketData, false, 0, true);
				_socket.makeConnection();
			} else {
				destroySocket(true);
			}
		}
		
		private function destroySocket(optionalCallback:Boolean = false):void {
			if (_socket != null) {
				_socket.removeEventListener(StringSockets.CONNECTED, handleSocketConnected);
				_socket.removeEventListener(StringSockets.DISCONNECTED, handleSocketDisconnected);
				_socket.removeEventListener(StringSockets.ERROR, handleSocketError);
				_socket.removeEventListener(StringSockets.STRINGDATA, handleSocketData);
				_socket.destroyInternals();
				_socket = null;
				
				if (optionalCallback == true) createSocket();
			}
		}
		private function handleSocketConnected(e:Event):void {
			if (_socket != null) {
				if ( _socket.getConnectedStatus() == true ) {
					if (_socketDataQueue.length > 0) {
						emptySocketDataQueue();
					}
				}
			}
		}
		
		private function handleSocketDisconnected(e:Event):void {
			trace('cmsLink - socketdisconnected - starting auto reconnect sequence');
			destroySocket();
			createSocket();
		}
		
		private function handleSocketError(e:DataEvent):void {
			trace("cmsLink - SocketError (will auto try and reconnect) " + e.data);
			// IGNORE - the socket will continue to try and connect infinateley
		}
		
		/**
		 * Data will end up here in the following format for this application
		 * it will be one full string, but be comma delimeted base64 encoded strings.
		 * 
		 * The first element in the string before the comma is the original command
		 * that was issued echoed back.
		 * 
		 * The second element is the return data payload base64 encoded.
		 * 
		 * These are sockets, its unlikeley in this application that they will send
		 * data out of order, but it is possible. The StringSocket class handles out
		 * of order data based on a delimeter.
		 * @param	e
		 */
		private function handleSocketData(e:DataEvent):void {
			//trace("handleSocketData " + e.data);
			
			var returnData:Array = e.data.split(',');
			if (returnData.length == 2) {
				var Command:String = Base64.decode( returnData[0] );
				var Data:String = Base64.decode( returnData[1] );

				//trace('handleSocketData decoded command: ' + Command);
				//trace('handleSocketData decoded data: ' + Data);
				var finalObj:cmsDataVO = new cmsDataVO();
				finalObj.command = Command;
				finalObj.resultData = XML(Data);
				
				dispatchEvent(new CMSEvent(cmsLink.DATA, false, true, finalObj));
			}
		}
		
	}
}