package com.transcendingdigital.ui
{
	/**
	 * A sweet interface implemented by all the main sections
	 * so we can create and destroy them all with the same
	 * code
	 */
	public interface iMainSectionInterface
	{
		function destroyInternals():void;
		function init():void;
	}
}