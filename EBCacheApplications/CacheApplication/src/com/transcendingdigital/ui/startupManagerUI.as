package com.transcendingdigital.ui
{
	import assets.mcPhotoLoadProgressTimeline;
	import assets.mcStartupUI;
	
	import com.bostonproductions.text.textFieldBaseFunctions;
	import com.technogumbo.utils.NumberUtils;
	import com.transcendingdigital.data.CMSCache;
	import com.transcendingdigital.data.GlobalDataStorage;
	import com.transcendingdigital.events.UIMessageEvent;
	
	import flash.display.Sprite;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.text.TextFormat;
	import flash.utils.Timer;
	
	/**
	 * Unlike many other exhibits, this one needs to
	 * do a few things on startup.
	 * <ol>
	 * <li>Attempt to connect to the CMS and cache all guide data</li>
	 * <li>Download and cache any assets referenced in the CMS data</li>
	 * </ol>
	 * 
	 * It shows a UI as this sequence happens with status messages.
	 */
	public class startupManagerUI extends Sprite implements iMainSectionInterface
	{
		public static const NEW_CACHE_DATA:String = "com.transcendingdigital.ui.startupManagerUI.NEW_CACHE_DATA";
		private var delayTimer:Timer;
		private var startupUI:mcStartupUI;
		private var _loadVisualization:mcPhotoLoadProgressTimeline;
		
		private var _savedMsgFormat:TextFormat;
		
		private var _cache:CMSCache;
		
		private var initialStartupComplete:Boolean = false;
		
		public function startupManagerUI()
		{
			super();
		}
		
		public function init():void {
			createStartupUI();
			createStartupDelayTimer();
		}
		private function createStartupDelayTimer():void {
			if(delayTimer == null) {
				if(initialStartupComplete == false) {
					delayTimer = new Timer( 1000 , int( GlobalDataStorage.configXML.config.StartupDelayInSec.text()[0] ) );
					updateStatusMessage("Startup In: " + GlobalDataStorage.configXML.config.StartupDelayInSec.text()[0]);
				} else {
					var secondOffset:int = NumberUtils.getRandNumberInRange(1,60);
					var secondsToContentPull:int = ( int(GlobalDataStorage.configXML.config.CMSContentCheckMinutes.text()[0]) * 60) + secondOffset;
					delayTimer = new Timer( 1000 , secondsToContentPull );
					updateStatusMessage("Next content check in: " + secondsToContentPull );
				}
				delayTimer.addEventListener(TimerEvent.TIMER, handleStartupDelay, false, 0, true);
				delayTimer.addEventListener(TimerEvent.TIMER_COMPLETE, handleStartupDelayComplete, false, 0, true);
				delayTimer.start();
			} else {
				destroyStartupDelayTimer(true);
			}
		}
		private function destroyStartupDelayTimer(_OptionalCallback:Boolean = false):void {
			if(delayTimer != null) {
				delayTimer.stop();
				delayTimer.removeEventListener(TimerEvent.TIMER, handleStartupDelay);
				delayTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, handleStartupDelayComplete);
				delayTimer = null;
				if(_OptionalCallback == true) {
					createStartupDelayTimer();
				}
			}
		}
		private function handleStartupDelay(e:TimerEvent):void {
			if(initialStartupComplete == false) {
			 	updateStatusMessage("Startup In: " + String(delayTimer.repeatCount - delayTimer.currentCount) );
			} else {
				updateStatusMessage("Next content check in: " + String(delayTimer.repeatCount - delayTimer.currentCount) );
			}
		}
		private function handleStartupDelayComplete(e:TimerEvent):void {
			destroyStartupDelayTimer();
			deactivateButton();
			
			createLoadVisualization();
			createCMSCache();
			updateStatusMessage( "Attempting to cache all assets from the content management system. This may take a few minutes. Please wait." );
		}
		private function createStartupUI():void {
			if(startupUI == null) {
				startupUI = new mcStartupUI();
				// Resize everything to fit the current UI Size
				startupUI["txtHeader"].width = GlobalDataStorage.uiSize.x;
				startupUI["txtStatus"].width = GlobalDataStorage.uiSize.x;
				startupUI["txtPercentage"].width = GlobalDataStorage.uiSize.x;
				startupUI["txtProblems"].width = GlobalDataStorage.uiSize.x * 0.25;
				startupUI["bg"].width = GlobalDataStorage.uiSize.x;
				startupUI["bg"].height = GlobalDataStorage.uiSize.y;
				
				startupUI["txtHeader"].y = GlobalDataStorage.uiSize.y * 0.1;
				startupUI["txtStatus"].y = startupUI["txtHeader"].y + startupUI["txtHeader"].height + 5;
				startupUI["txtPercentage"].y = startupUI["txtStatus"].y + startupUI["txtStatus"].height + (GlobalDataStorage.uiSize.y * 0.005);
				
				startupUI["txtProblems"].x = 25;
				startupUI["txtProblems"].y = GlobalDataStorage.uiSize.y - startupUI["txtProblems"].height - 25;
				startupUI["bpiInfo"].x = GlobalDataStorage.uiSize.x - startupUI["bpiInfo"].width - 25;
				startupUI["bpiInfo"].y = GlobalDataStorage.uiSize.y - startupUI["bpiInfo"].height - 25;
					
				startupUI["btnCycleCache"].x = GlobalDataStorage.uiSize.x / 2 - startupUI["btnCycleCache"].width / 2;
				startupUI["btnCycleCache"].y = GlobalDataStorage.uiSize.y / 2 - startupUI["btnCycleCache"].height / 2;
				deactivateButton();
				
				startupUI["txtProblems"].visible = false;
				startupUI["txtPercentage"].visible = false;
				// Save the UI text message format
				_savedMsgFormat = startupUI["txtStatus"].getTextFormat();
				addChild(startupUI);
			} else {
				destroyStartupUI(true);
			}
		}
		private function destroyStartupUI(_optionalCallback:Boolean = false):void {
			if(startupUI != null) {
				if(contains(startupUI))removeChild(startupUI);
				startupUI = null;
				if(_optionalCallback == true) {
					createStartupUI();
				}
			}
		}
		private function createLoadVisualization():void {
			if(_loadVisualization == null) {
				_loadVisualization = new mcPhotoLoadProgressTimeline();
				addChild(_loadVisualization);
				// position in center
				_loadVisualization.x = GlobalDataStorage.uiSize.x / 2 - _loadVisualization.width / 4;
				_loadVisualization.y = GlobalDataStorage.uiSize.y  / 2 - _loadVisualization.height / 2;
			} else {
				destroyLoadVisualization(true);
			}
		}
		private function destroyLoadVisualization(_OptionalCallback:Boolean = false):void {
			if(_loadVisualization != null) {
				if( contains(_loadVisualization))removeChild(_loadVisualization);
				_loadVisualization.stop();
				_loadVisualization = null;
				
				if(_OptionalCallback == true) {
					createLoadVisualization();
				}
			}
		}
		private function createCMSCache():void {
			if(_cache == null) {
				_cache = new CMSCache();
				_cache.addEventListener(CMSCache.CACHE_COMPLETE, handleCacheComplete);
				_cache.addEventListener(CMSCache.CACHE_PROGRESS_MESSAGE, handleCacheMessage);
				_cache.addEventListener(CMSCache.FATAL_ERROR, handleCacheFatal);
				_cache.addEventListener(CMSCache.NEW_CACHE_DATA, handleNewCMSCacheData);
				_cache.init();
			}
		}
		private function destroyCMSCache():void {
			if(_cache != null) {
				_cache.removeEventListener(CMSCache.CACHE_COMPLETE, handleCacheComplete);
				_cache.removeEventListener(CMSCache.CACHE_PROGRESS_MESSAGE, handleCacheMessage);
				_cache.removeEventListener(CMSCache.FATAL_ERROR, handleCacheFatal);
				_cache.removeEventListener(CMSCache.NEW_CACHE_DATA, handleNewCMSCacheData);
				_cache.destroyInternals();
				_cache = null;
			}
		}
		private function handleCacheComplete(e:Event):void {
			updateStatusMessage("Cache operations complete. ");
			GlobalDataStorage.mostRecentCMSPullTime = new Date();
			completeCache();
		}
		private function handleCacheMessage(e:UIMessageEvent):void {
			updateStatusMessage( String(e.data) );
		}
		private function handleCacheFatal(e:Event):void {
			GlobalDataStorage.mostRecentCMSPullTime = new Date();
			// Cant do anything
			completeCache();
		}
		private function handleNewCMSCacheData(e:DataEvent):void {
			dispatchEvent(new DataEvent(startupManagerUI.NEW_CACHE_DATA, false, true, e.data));
		}
		private function completeCache():void {
			destroyCMSCache();
			
			if(initialStartupComplete == false) {
				initialStartupComplete = true;
				dispatchEvent( new Event(GlobalDataStorage.INITIAL_CACHE_COMPLETE, false, true) );	
			}
			
			destroyLoadVisualization();
			activateButton();
			createStartupDelayTimer();
		}
		private function updateStatusMessage(_Data:String):void {
			// Restore the text format
			if(startupUI) {
				startupUI["txtStatus"].setTextFormat(_savedMsgFormat);
				startupUI["txtStatus"].text = _Data;
				textFieldBaseFunctions.fitTextIntoField( startupUI["txtStatus"], 2 );
			}
		}
		private function activateButton():void {
			  if(startupUI["btnCycleCache"] != null) {
				  startupUI["btnCycleCache"].enabled = true;
				  startupUI["btnCycleCache"].alpha = 1;
				  startupUI["btnCycleCache"].addEventListener(MouseEvent.CLICK, handleButtonClick, false, 0, true);
			  }
		}
		private function deactivateButton():void {
			if(startupUI["btnCycleCache"] != null) {
				startupUI["btnCycleCache"].enabled = false;
				startupUI["btnCycleCache"].alpha = 0;
				startupUI["btnCycleCache"].removeEventListener(MouseEvent.CLICK, handleButtonClick);
			}
		}
		private function handleButtonClick(e:MouseEvent):void {
			deactivateButton();
			destroyStartupDelayTimer();
			
			createLoadVisualization();
			createCMSCache();
			updateStatusMessage( "Attempting to cache all assets from the content management system. This may take a few minutes. Please wait." );
		}
		public function destroyInternals():void {
			destroyCMSCache();
			destroyStartupDelayTimer();
			destroyLoadVisualization();
			destroyStartupUI();
		}
	}
}