package com.transcendingdigital.data.vo
{
	public class cacheFileSegment
	{
		public var dataKey:String = "";
		public var builtXMLOutput:String = "";
		public var dataElements:Vector.<cacheFileData> = null;
		
		public function initStructure(_key:String):void {
			dataKey = _key;
			dataElements = new Vector.<cacheFileData>();
			builtXMLOutput += '<?xml version="1.0" encoding="UTF-8" ?>' + "\n";
			builtXMLOutput += '<content>';
		}
		public function closeXMLStructure():void {
			builtXMLOutput += '</content>';
		}
	}
}