package com.transcendingdigital.data.vo
{
	public class cacheMediaWallPageData
	{
		public var title:String = "";
		public var bgPhotoURL:String = "";
		public var bgPhotoSize:Number = 0;
		public var tabData:Vector.<cacheMediaWallFileTabData>;
		
		public var builtXMLOutput:String = "";
		public var dataKey:String = "";
		
		public function initStructure(_key:String):void {
			dataKey = _key;
			builtXMLOutput += '<?xml version="1.0" encoding="UTF-8" ?>' + "\n";
			builtXMLOutput += '<content>';
		}
		public function closeXMLStructure():void {
			builtXMLOutput += '</content>';
		}
		
		public function cacheMediaWallPageData()
		{
		}
	}
}