package com.transcendingdigital.data.vo
{
	public class xmlCacheVO
	{
		public var timeStampPath:String = '';
		public var fullPath:String = '';
		public var broadcastContstant:String = '';
		
		public var drupalCMSBasePath:String = '';
		public var drupalCMSEndpointName:String = '';
		public var drupalCMSUserName:String = '';
		public var drupalCMSPassword:String = '';
		public var drupalLoggedIn:Boolean = false;
	}
}