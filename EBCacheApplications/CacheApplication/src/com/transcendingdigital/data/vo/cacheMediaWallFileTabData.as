package com.transcendingdigital.data.vo
{
	public class cacheMediaWallFileTabData
	{
		public var title:String = "";
		public var buttonText:String = "";
		public var bodyText:String = "";
		public var bodyPhotoURL:String = "";
		public var bodyPhotoSize:Number = 0;
		public var bodyVideoURL:String = "";
		public var bodyVideoSize:Number = 0;
		
		public function cacheMediaWallFileTabData()
		{
		}
	}
}