package com.transcendingdigital.data.vo
{
	public class cacheFileData
	{
		public var title:String = "";
		public var body:String = "";
		public var photoPath:String = "";
		public var photoByteSize:Number = 0;
		public var videoPath:String = "";
		public var videoByteSize:Number = 0;
	}
}