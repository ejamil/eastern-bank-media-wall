package com.transcendingdigital.data
{
	import com.bostonproductions.images.SinglePhotoCaching;
	import com.technogumbo.data.XMLLoader;
	import com.technogumbo.utils.NumberUtils;
	import com.transcendingdigital.data.Drupal7Sercies3RESTInterface;
	import com.transcendingdigital.data.vo.xmlCacheVO;
	import com.transcendingdigital.data.vo.xmlTimestampVO;
	import com.transcendingdigital.events.UIMessageEvent;
	
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	/**
	 * This class handles the actual caching of CMS elements.
	 * There may be a lot of them.
	 * 
	 * The goal is to attempt to download all xml assets,
	 * parse those, then download all potential image assets as well, so
	 * the end result is all CMS data stored locally.
	 * 
	 * I know its slower, but everything is ordered sequentially.
	 * When we get into loading the images I am concerned that massive
	 * simultanious loading will cause memory issues.
	 */
	public class CMSCache extends EventDispatcher
	{
		public static const CACHE_COMPLETE:String = "com.transcendingdigital.data.CMSCache.CACHE_COMPLETE";
		public static const FATAL_ERROR:String = "com.transcendingdigital.data.CMSCache.FATAL_ERROR";
		public static const CACHE_PROGRESS_MESSAGE:String = "com.transcendingdigital.data.CMSCache.CACHE_PROGRESS_MESSAGE";
		public static const NEW_CACHE_DATA:String = "com.transcendingdigital.data.CMSCache.NEW_CACHE_DATA";
		
		private const CACHING_TEXT:String = "CACHING_TEXT";
		private const CACHING_PHOTOS:String = "CACHING_PHOTOS";
		private const CACHING_VIDEOS:String = "CACHING_VIDEOS";
		
		private var _currentActiveXML:XMLLoader;
		private var _currentXMLPath:String = "";
		private var _currentXMLPathIndex:int = 0;
		private var _previousXMLUnixTimeStampString:String = "";
		
		private var _currentCacheFile:genericFileCaching;
		private var _currentCachePath:String = "";
		private var _currentCacheSize:Number = 0;
		private var _currentMediaCacheIndex:int = 0;
		private var _subMediaCacheIndex:int = 0;
		
		private var _newDataDispatchKey:String = "";
		
		private var _currentCacheOp:String = CACHING_TEXT;
		
		private var _mediaWallPageIndex:int = 0;
		private var _mediaWallTabIndex:int = 0;
		private var _mediaWallBGCached:Boolean = false;
		
		private var _drupal:Drupal7Sercies3RESTInterface;
		
		public function CMSCache()
		{
			
		}
		
		public function init():void {
			nextXMLTimestampCacheStep();
			
			
			//createDrupalInterface();
			
		}
		
		public function destroyInternals():void {
			destroyXMLLoader();
			destroyCacheFile();
		}
		
		private function createXMLLoader():void {
			if(_currentActiveXML == null) {
				_currentActiveXML = new XMLLoader(_currentXMLPath, true);
				_currentActiveXML.addEventListener(XMLLoader.DATALOADED, handleXMLLoaded);
				_currentActiveXML.addEventListener(IOErrorEvent.IO_ERROR, handleXMLIOError);
				_currentActiveXML.addEventListener(SecurityErrorEvent.SECURITY_ERROR, handleXMLSecurityError);
				_currentActiveXML.LoadXMLData();
			} else {
				destroyXMLLoader( true );
			}
		}
		
		private function destroyXMLLoader(_optionalCallback:Boolean = false):void {
			if(_currentActiveXML != null) {
				_currentActiveXML.removeEventListener(XMLLoader.DATALOADED, handleXMLLoaded);
				_currentActiveXML.removeEventListener(IOErrorEvent.IO_ERROR, handleXMLIOError);
				_currentActiveXML.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, handleXMLSecurityError);
				_currentActiveXML.destroyInternals();
				_currentActiveXML = null;
				if(_optionalCallback == true) {
					createXMLLoader();
				}
			}
		}
		
		private function handleXMLLoaded(e:Event):void {
			// IS THIS A TIMESTAMP OR FULL DATA FILE?
			if(_currentXMLPath == GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].timeStampPath) {
				// TIMESTAMP
				var currentVO:xmlTimestampVO = parseTimestampXML( _currentActiveXML.getLoadedXML() );
				// Compare the timestamp to the previous timestamp to know if we need to load the full xml
				if(_previousXMLUnixTimeStampString == currentVO.unixTimestamp) {
					if(GlobalDataStorage.guideXMLFiles.length < _currentXMLPathIndex) {
						dispatchEvent(new UIMessageEvent(CMSCache.CACHE_PROGRESS_MESSAGE,"Moving On. No update needed for data keyed: " + GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant, false, true));
					}
					// IF WE ARE LOGGED IN, WE NEED TO LOG OUT
					if( GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].drupalLoggedIn = true ) {
						_drupal.logout();
					} else {
						_currentXMLPathIndex += 1;
						nextXMLTimestampCacheStep();
					}
				} else {
					// No cached previous timestamp data was present or the timestamps do not match, download the full data
					// and re-cache assets
					dispatchEvent(new UIMessageEvent(CMSCache.CACHE_PROGRESS_MESSAGE,"Updating data and cache for key: " + GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant, false, true));
					_currentXMLPath = GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].fullPath;
					createXMLLoader();
				}
			} else {
				// FULL DATA
				// Will block while parsing
				var cmsParser:CMSParser = new CMSParser();
				cmsParser.parseData( GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant, _currentActiveXML.getLoadedXML() );
				// Now we need to download all the assets for the particular data set - GlobalDataStorage has been populated with the just loaded xml
				// The media wall data has to be parsed in a different mode than everything else unfortunately. That data gets written out in
				// much more complex structures
				initiateMediaCacheOperations();
			}
		}
		
		/**
		 * Timestamp xml has the following structure
		 * 
		 * <?xml version="1.0"?>
         * <InfoXML>
		 * <XMLBody>
		 * <valueKeyWord><![CDATA[promotions_data_export]]></valueKeyWord>
		 * <unixTimeStamp><![CDATA[1384870638]]></unixTimeStamp>
		 * </XMLBody>
		 * </InfoXML>
		 */
		private function parseTimestampXML(_xmlData:XML):xmlTimestampVO {
			var returnVO:xmlTimestampVO = new xmlTimestampVO();
			returnVO.identifier = _xmlData.XMLBody.valueKeyWord.text()[0];
			returnVO.unixTimestamp = _xmlData.XMLBody.unixTimeStamp.text()[0];
			return returnVO;
		}
		
		private function nextXMLTimestampCacheStep():void {
			
			// Should we dispatch that new cache data has just been loaded?
			// This likeley bubbles up and skips some classes. Will trigger messages sent over sockets to other
			// connected clients.
			if(_newDataDispatchKey != '') {
				dispatchEvent(new DataEvent(CMSCache.NEW_CACHE_DATA, true, true, _newDataDispatchKey));
				_newDataDispatchKey = '';
			}
			
			
			if( (GlobalDataStorage.guideXMLFiles.length > 0) && (_currentXMLPathIndex < GlobalDataStorage.guideXMLFiles.length) ) {
				// DOES THIS DATA REQUIRE AUTHENTICATION AS SPECIFIED BY THE CONFIG FILE? IF SO LOGIN FIRST
				if( (GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].drupalCMSBasePath != '') && (GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].drupalLoggedIn == false) ) {
					createDrupalInterface( GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex] );
					// Gotta wait for drupal to do async login process - will call back here when logged in.
				} else {
					// FIRST - See if we have a current cached on disk, save the timestamp
					// in the cached file before we download a new copy.
					_currentXMLPath = GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].timeStampPath;
					var fileNameOnly:String = NumberUtils.getFileNameOnly( _currentXMLPath );
					
					var testFile:File = new File( GlobalDataStorage.cacheDirectoryPrefix + "xmlCache/" + fileNameOnly );
					if(testFile.exists) {
					   try {
						   var stream:FileStream = new FileStream();
						   stream.open(testFile, FileMode.READ);
						   var fileContents:String = stream.readUTFBytes( testFile.size );
						   stream.close();
						   stream = null;
						   
						   var currentTSVO:xmlTimestampVO = parseTimestampXML( XML(fileContents) );
						   _previousXMLUnixTimeStampString = currentTSVO.unixTimestamp;
					   } catch(e:Error) {
						   // No one cares just sink it
					   }
	
					}
					testFile = null;
					createXMLLoader();
				}
			} else {
				dispatchEvent(new Event(CMSCache.CACHE_COMPLETE, false, true));
			}
		}
		/**
		 * With caching enabled it is important to understand
		 * that even with a load faliure, it will attempt to
		 * use anything in cache. If this fires, it means caching failed AND there
		 * is nothing in cache. IF CACHING IS ON.
		 */
		private function handleXMLIOError(e:IOErrorEvent):void {
			dispatchEvent(new UIMessageEvent(CMSCache.CACHE_PROGRESS_MESSAGE,"There was an error loading xml data from the CMS. There is also no data vailable in the local cache. We cannot start the exhibit: " + e.text, false, true));
			// We cant proceede with the application
			dispatchEvent(new Event(CMSCache.FATAL_ERROR, false, true));
		}
		private function handleXMLSecurityError(e:SecurityErrorEvent):void {
			dispatchEvent(new UIMessageEvent(CMSCache.CACHE_PROGRESS_MESSAGE,"There was an error loading xml data from the CMS. There is also no data vailable in the local cache. We cannot start the exhibit: " + e.text, false, true));
			// We cant proceede with the application
			dispatchEvent(new Event(CMSCache.FATAL_ERROR, false, true));
		}
		
		/**
		 * When parsing normal data
		 * Uses the data found in GlobalDataStorage.mostRecentTeamData
		 * and
		 * GlobalDataStorage.mostRecentPlayerData
		 * to go through all the images, check if they are present in
		 * cache and attempt download if not.  We do this sequentially.
		 * 
		 * When parsing media wall data it follows an entirely different
		 * path. A vector holds all the popups data we need to parse
		 * several data elements for each.
		 */
		private function initiateMediaCacheOperations():void {
			
			if( GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant == CMSParser.TYPE_MEDIAWALL_DATA ) {
				if(GlobalDataStorage.currentCachingMediaWallData.length > 0) {
					if(_mediaWallBGCached == false) {
						dispatchEvent(new UIMessageEvent(CMSCache.CACHE_PROGRESS_MESSAGE,"Downloading media wall data for: " + GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].title, false, true));
						GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += '<data>' + "\n";
						// Inject the common stuff like title
						GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += "<title><![CDATA[" + GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].title + "]]></title>\n";
						
						if(GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].bgPhotoURL != "") {
							_currentCachePath = GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].bgPhotoURL;
							_currentCacheSize = GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].bgPhotoSize;
							
							createCacheFile();
							
						} else {
							// Add an empty photo entry
							_mediaWallBGCached = true;
							GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += "<file><![CDATA[]]></file>\n";
							GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += "<file_size><![CDATA[]]></file_size>\n";
							
							determineNextCacheStep();
						}
					} else {
						if(GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].tabData.length > 0) {
							if(_currentCacheOp == CACHING_TEXT) {
								GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += '<tab>' + "\n";
								// Title for a tab
								GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += '<title><![CDATA[' + GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].tabData[_mediaWallTabIndex].title + "]]></title>\n";
								// Button text for a tab
								GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += '<button_text><![CDATA[' + GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].tabData[_mediaWallTabIndex].buttonText + "]]></button_text>\n";
								// Body text for a tab
								GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += '<body><![CDATA[' + GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].tabData[_mediaWallTabIndex].bodyText + "]]></body>\n";
								// Check for a photo
								_currentCacheOp = CACHING_PHOTOS;
								
								if( GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].tabData[_mediaWallTabIndex].bodyPhotoURL != "" ) {
									_currentCachePath = GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].tabData[_mediaWallTabIndex].bodyPhotoURL;
									_currentCacheSize = GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].tabData[_mediaWallTabIndex].bodyPhotoSize;
									createCacheFile();
								} else {
									// Put blank entries and move on
									GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += "<photo>\n";
									GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += "<file><![CDATA[]]></file>\n";
									GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += "<file_size><![CDATA[]]></file_size>\n";
									GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += "</photo>\n";
									
									determineNextCacheStep();
								}
								
							} else if(_currentCacheOp == CACHING_PHOTOS) {
								
								_currentCacheOp = CACHING_VIDEOS;
								
								// Check for a video
								if( GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].tabData[_mediaWallTabIndex].bodyVideoURL != "" ) {
									_currentCachePath = GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].tabData[_mediaWallTabIndex].bodyVideoURL;
									_currentCacheSize = GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].tabData[_mediaWallTabIndex].bodyVideoSize;
									createCacheFile();
								} else {
									// Put blank entries and move on
									GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += "<video>\n";
									GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += "<file><![CDATA[]]></file>\n";
									GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += "<file_size><![CDATA[]]></file_size>\n";
									GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += "</video>\n";
									
									determineNextCacheStep();
								}
							}
							
						}
					}
				}
				
				
			} else if( GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant == CMSParser.TYPE_MEDIAWALLGAME_DATA ) {
				GlobalDataStorage.currentCachingMediaWallGameData.builtXMLOutput += '<data>' + "\n";
				
				if(GlobalDataStorage.currentCachingMediaWallGameData.bgPhotoURL != "") {
					_currentCachePath = GlobalDataStorage.currentCachingMediaWallGameData.bgPhotoURL;
					_currentCacheSize = GlobalDataStorage.currentCachingMediaWallGameData.bgPhotoSize;
					
					dispatchEvent(new UIMessageEvent(CMSCache.CACHE_PROGRESS_MESSAGE,"Downloading media wall game data", false, true));
					createCacheFile();
				} else {
					// Add an empty photo entry
					GlobalDataStorage.currentCachingMediaWallGameData.builtXMLOutput += "<file><![CDATA[]]></file>\n";
					GlobalDataStorage.currentCachingMediaWallGameData.builtXMLOutput += "<file_size><![CDATA[]]></file_size>\n";
					
					determineNextCacheStep();
				}
			} else {
				switch( _currentCacheOp ) {
					case CACHING_TEXT:
						if(GlobalDataStorage.currentCachingText.dataElements.length > 0) {
							// Build on the entries xml file
							GlobalDataStorage.currentCachingText.builtXMLOutput += '<data>' + "\n";
							GlobalDataStorage.currentCachingText.builtXMLOutput += '<title><![CDATA[' + GlobalDataStorage.currentCachingText.dataElements[_currentMediaCacheIndex].title + "]]></title>\n";
							GlobalDataStorage.currentCachingText.builtXMLOutput += '<body><![CDATA[' + GlobalDataStorage.currentCachingText.dataElements[_currentMediaCacheIndex].body + "]]></body>\n";
							GlobalDataStorage.currentCachingText.builtXMLOutput += '<file><![CDATA[]]></file>' + "\n";
							GlobalDataStorage.currentCachingText.builtXMLOutput += '<file_size><![CDATA[]]></file_size>' + "\n";
							GlobalDataStorage.currentCachingText.builtXMLOutput += '</data>' + "\n";
						}
						
						dispatchEvent(new UIMessageEvent(CMSCache.CACHE_PROGRESS_MESSAGE,"Phase: " + GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant + " Caching Text: " + String(_currentMediaCacheIndex) + " of " + String(GlobalDataStorage.currentCachingText.dataElements.length), false, true));
						
						determineNextCacheStep();
						break;
					case CACHING_PHOTOS:
						if(GlobalDataStorage.currentCachingPhotos.dataElements.length > 0) {
							_currentCachePath = GlobalDataStorage.currentCachingPhotos.dataElements[_currentMediaCacheIndex].photoPath;
							_currentCacheSize = GlobalDataStorage.currentCachingPhotos.dataElements[_currentMediaCacheIndex].photoByteSize;
							dispatchEvent(new UIMessageEvent(CMSCache.CACHE_PROGRESS_MESSAGE,"Phase: " + GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant + " Caching Photo: " + String(_currentMediaCacheIndex) + " of " + String(GlobalDataStorage.currentCachingPhotos.dataElements.length), false, true));
							createCacheFile();
						} else {
							determineNextCacheStep();
						}
						break;
					case CACHING_VIDEOS:
						if(GlobalDataStorage.currentCachingVideos.dataElements.length > 0) {
							_currentCachePath = GlobalDataStorage.currentCachingVideos.dataElements[_currentMediaCacheIndex].videoPath;
							_currentCacheSize = GlobalDataStorage.currentCachingVideos.dataElements[_currentMediaCacheIndex].videoByteSize;
							
							dispatchEvent(new UIMessageEvent(CMSCache.CACHE_PROGRESS_MESSAGE,"Phase: " + GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant + " Caching Video: " + String(_currentMediaCacheIndex) + " of " + String(GlobalDataStorage.currentCachingVideos.dataElements.length), false, true));
							createCacheFile();
						} else {
							determineNextCacheStep();
						}
						break;
				}
			
			}
			
		}
		
		private function createCacheFile():void {
			if(_currentCacheFile == null) {
				var cacheSubDir:String = GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant.toLowerCase() + '/' + _currentCacheOp.toLowerCase();
				if( GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant == CMSParser.TYPE_MEDIAWALL_DATA && _mediaWallBGCached == false) {
					cacheSubDir = GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant.toLowerCase() + '/' + 'caching_photos';
				} else if( GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant == CMSParser.TYPE_MEDIAWALLGAME_DATA ) {
					cacheSubDir = GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant.toLowerCase() + '/' + 'caching_photos';
				}
				_currentCacheFile = new genericFileCaching();
				_currentCacheFile.addEventListener(genericFileCaching.LOADED, cacheFileLoaded, false, 0, true);
				_currentCacheFile.addEventListener(genericFileCaching.ERROR, cacheFileLoadError, false, 0, true);
				_currentCacheFile.init(_currentCachePath, _currentCacheSize, cacheSubDir);
			} else {
				destroyCacheFile( true );
			}
		}
		private function destroyCacheFile(optionalCallback:Boolean = false):void {
			if(_currentCacheFile != null) {
				_currentCacheFile.removeEventListener(genericFileCaching.LOADED, cacheFileLoaded);
				_currentCacheFile.removeEventListener(genericFileCaching.ERROR, cacheFileLoadError);
				_currentCacheFile.destroyInternals();
				_currentCacheFile = null;
				if(optionalCallback == true) createCacheFile();
			}
		}
		private function cacheFileLoaded(e:Event):void {
			// Build on the entries xml file
			if( GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant == CMSParser.TYPE_MEDIAWALL_DATA ) {
				if(_mediaWallBGCached == false) {
					GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += '<file><![CDATA[' + _currentCacheFile.getFinalDownloadPath() + "]]></file>\n";
					GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += '<file_size><![CDATA[' + String( _currentCacheFile.getFinalByteSize() ) + "]]></file_size>\n";
					_mediaWallBGCached = true;
				} else {
					switch( _currentCacheOp ) {
						case CACHING_PHOTOS:
							GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += '<photo>' + "\n";
							GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += '<file><![CDATA[' + _currentCacheFile.getFinalDownloadPath() + "]]></file>\n";
							GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += '<file_size><![CDATA[' + String( _currentCacheFile.getFinalByteSize() ) + "]]></file_size>\n";
							GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += '</photo>' + "\n";
							break;
						case CACHING_VIDEOS:
							GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += '<video>' + "\n";
							GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += '<file><![CDATA[' + _currentCacheFile.getFinalDownloadPath() + "]]></file>\n";
							GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += '<file_size><![CDATA[' + String( _currentCacheFile.getFinalByteSize() ) + "]]></file_size>\n";
							GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += '</video>' + "\n";
							break;
					}
				}
			} else if( GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant == CMSParser.TYPE_MEDIAWALLGAME_DATA ) {
				GlobalDataStorage.currentCachingMediaWallGameData.builtXMLOutput += '<file><![CDATA[' + _currentCacheFile.getFinalDownloadPath() + "]]></file>\n";
				GlobalDataStorage.currentCachingMediaWallGameData.builtXMLOutput += '<file_size><![CDATA[' + String( _currentCacheFile.getFinalByteSize() ) + "]]></file_size>\n";
			} else {
				switch( _currentCacheOp ) {
					case CACHING_PHOTOS:
						GlobalDataStorage.currentCachingPhotos.builtXMLOutput += '<data>' + "\n";
						GlobalDataStorage.currentCachingPhotos.builtXMLOutput += '<title><![CDATA[' + GlobalDataStorage.currentCachingPhotos.dataElements[_currentMediaCacheIndex].title + "]]></title>\n";
						GlobalDataStorage.currentCachingPhotos.builtXMLOutput += '<body><![CDATA[' + GlobalDataStorage.currentCachingPhotos.dataElements[_currentMediaCacheIndex].body + "]]></body>\n";
						GlobalDataStorage.currentCachingPhotos.builtXMLOutput += '<file><![CDATA[' + _currentCacheFile.getFinalDownloadPath() + "]]></file>\n";
						GlobalDataStorage.currentCachingPhotos.builtXMLOutput += '<file_size><![CDATA[' + String( _currentCacheFile.getFinalByteSize() ) + "]]></file_size>\n";
						GlobalDataStorage.currentCachingPhotos.builtXMLOutput += '</data>' + "\n";
				    break;
					case CACHING_VIDEOS:
						GlobalDataStorage.currentCachingVideos.builtXMLOutput += '<data>' + "\n";
						GlobalDataStorage.currentCachingVideos.builtXMLOutput += '<title><![CDATA[' + GlobalDataStorage.currentCachingVideos.dataElements[_currentMediaCacheIndex].title + "]]></title>\n";
						GlobalDataStorage.currentCachingVideos.builtXMLOutput += '<body><![CDATA[' + GlobalDataStorage.currentCachingVideos.dataElements[_currentMediaCacheIndex].body + "]]></body>\n";
						GlobalDataStorage.currentCachingVideos.builtXMLOutput += '<file><![CDATA[' + _currentCacheFile.getFinalDownloadPath() + "]]></file>\n";
						GlobalDataStorage.currentCachingVideos.builtXMLOutput += '<file_size><![CDATA[' + String( _currentCacheFile.getFinalByteSize() ) + "]]></file_size>\n";
						GlobalDataStorage.currentCachingVideos.builtXMLOutput += '</data>' + "\n";
					break;
				}
			}
			destroyCacheFile();
			determineNextCacheStep();
		}
		private function cacheFileLoadError(e:Event):void {
			// Write a blank entry in some cases
			if( GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant == CMSParser.TYPE_MEDIAWALL_DATA ) {
				GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += "<file><![CDATA[]]></file>\n";
				GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += "<file_size><![CDATA[]]></file_size>\n";
				
			} else if( GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant == CMSParser.TYPE_MEDIAWALLGAME_DATA ) {
				GlobalDataStorage.currentCachingMediaWallGameData.builtXMLOutput += "<file><![CDATA[]]></file>\n";
				GlobalDataStorage.currentCachingMediaWallGameData.builtXMLOutput += "<file_size><![CDATA[]]></file_size>\n";
			}
			
			destroyCacheFile();
			determineNextCacheStep();
		}
		private function determineNextCacheStep():void {
			
			if( GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant == CMSParser.TYPE_MEDIAWALL_DATA ) {
				if(_mediaWallTabIndex +1 == GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].tabData.length) {
					
					// Write out a file for the category
					if(_currentCacheOp == CACHING_VIDEOS) {
						GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += '</tab>' + "\n";
						
						GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += '</data>' + "\n";
						GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].closeXMLStructure();
						writeOutStringCacheFile( GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant.toLowerCase() + '/caching_' + GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].dataKey.toLowerCase() + '.xml', GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput);
						
						// IS THIS THE END?
						if( _mediaWallPageIndex +1 == GlobalDataStorage.currentCachingMediaWallData.length ) {
							// We are finished
							_mediaWallPageIndex = 0;
							_mediaWallTabIndex = 0;
							_currentCacheOp = CACHING_TEXT;
							_mediaWallBGCached = false;
							// Set the key for this cache operation so clients will be notified of new data over the sockets
							_newDataDispatchKey = GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant;
							
							if( GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].drupalLoggedIn = true ) {
								_drupal.logout();
							} else {
								// DONE - ON TO THE NEXT MAIN XML ELEMENT OR EXHIBIT TO CACHE ON
								_currentXMLPathIndex += 1;
								nextXMLTimestampCacheStep();
							}
							return;
							
						} else {
							_mediaWallPageIndex += 1;
							_mediaWallTabIndex = 0;
							_currentCacheOp = CACHING_TEXT;
							_mediaWallBGCached = false;
						}
					}

				} else {
					if(_currentCacheOp == CACHING_VIDEOS ) {
						_currentCacheOp = CACHING_TEXT;
						GlobalDataStorage.currentCachingMediaWallData[_mediaWallPageIndex].builtXMLOutput += '</tab>' + "\n";
						_mediaWallTabIndex += 1;
					}
				}
			} else if( GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant == CMSParser.TYPE_MEDIAWALLGAME_DATA ) {
				// ### CACHING MEDIA WALL GAME DATA ###
				GlobalDataStorage.currentCachingMediaWallGameData.builtXMLOutput += '<activatedGames>' + "\n";
				for(var mwGameIndx:int = 0; mwGameIndx < GlobalDataStorage.currentCachingMediaWallGameData.activatedGameList.length; mwGameIndx++) {
					if( GlobalDataStorage.currentCachingMediaWallGameData.activatedGameList[mwGameIndx] != "") {
						GlobalDataStorage.currentCachingMediaWallGameData.builtXMLOutput += '<game><![CDATA[' + GlobalDataStorage.currentCachingMediaWallGameData.activatedGameList[mwGameIndx] + "]]></game>\n";
					}
				}
				GlobalDataStorage.currentCachingMediaWallGameData.builtXMLOutput += '</activatedGames>' + "\n";
				GlobalDataStorage.currentCachingMediaWallGameData.builtXMLOutput += '</data>' + "\n";
				GlobalDataStorage.currentCachingMediaWallGameData.closeXMLStructure();
				
				writeOutStringCacheFile( GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant.toLowerCase() + '/' + 'caching_' + GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant.toLowerCase() + '.xml', GlobalDataStorage.currentCachingMediaWallGameData.builtXMLOutput);
				// Set the key for this cache operation so clients will be notified of new data over the sockets
				_newDataDispatchKey = GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant;
				
				if( GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].drupalLoggedIn = true ) {
					_drupal.logout();
				} else {
					// DONE - ON TO THE NEXT MAIN XML ELEMENT OR EXHIBIT TO CACHE ON
					_currentXMLPathIndex += 1;
					nextXMLTimestampCacheStep();
				}

				return;
				
			} else {
				switch( _currentCacheOp ) {
					case CACHING_TEXT:
						if( (_currentMediaCacheIndex + 1) < GlobalDataStorage.currentCachingText.dataElements.length) {
							_currentMediaCacheIndex += 1;
						} else {
							// Write out the xml for fulfilling client requests
							GlobalDataStorage.currentCachingText.closeXMLStructure();
							writeOutStringCacheFile( GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant.toLowerCase() + '/' + _currentCacheOp.toLowerCase() + '.xml', GlobalDataStorage.currentCachingText.builtXMLOutput);
							
							_currentCacheOp = CACHING_PHOTOS;
							_currentMediaCacheIndex = 0;
						}
						break;
					case CACHING_PHOTOS:
						if( (_currentMediaCacheIndex + 1) < GlobalDataStorage.currentCachingPhotos.dataElements.length) {
							_currentMediaCacheIndex += 1;
						} else {
							// Write out the xml for fulfilling client requests
							GlobalDataStorage.currentCachingPhotos.closeXMLStructure();
							writeOutStringCacheFile( GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant.toLowerCase() + '/' + _currentCacheOp.toLowerCase() + '.xml', GlobalDataStorage.currentCachingPhotos.builtXMLOutput);
							
							_currentCacheOp = CACHING_VIDEOS;
							_currentMediaCacheIndex = 0;
						}
						break;
					case CACHING_VIDEOS:
						if( (_currentMediaCacheIndex + 1) < GlobalDataStorage.currentCachingVideos.dataElements.length) {
							_currentMediaCacheIndex += 1;
						} else {
							// DONE - BUT DO WE NEED TO LOGOUT OF DRUPAL?
	
								// Write out the xml for fulfilling client requests
								GlobalDataStorage.currentCachingVideos.closeXMLStructure();
								writeOutStringCacheFile( GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant.toLowerCase() + '/' + _currentCacheOp.toLowerCase() + '.xml', GlobalDataStorage.currentCachingVideos.builtXMLOutput);
								
								_currentCacheOp = CACHING_TEXT;
								_currentMediaCacheIndex = 0;
		
								// Set the key for this cache operation so clients will be notified of new data over the sockets
								_newDataDispatchKey = GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].broadcastContstant;
								
								if( GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].drupalLoggedIn = true ) {
									_drupal.logout();
								} else {
								// DONE - ON TO THE NEXT MAIN XML ELEMENT OR EXHIBIT TO CACHE ON
								_currentXMLPathIndex += 1;
								 nextXMLTimestampCacheStep();
								}
								return;
							
						}
						break;
				}
			}
			
			initiateMediaCacheOperations();
			
		}
		private function writeOutStringCacheFile(_desiredPath:String, _content:String):void {
			var cachePrefix:String = "app-storage:/";			
			if(GlobalDataStorage['cacheDirectoryPrefix'] != undefined) {
				cachePrefix = GlobalDataStorage['cacheDirectoryPrefix'];
			}
			
			var outputFile:File = new File(cachePrefix + _desiredPath);
			try {
				var stream:FileStream = new FileStream();
				stream.open(outputFile, FileMode.WRITE);
				stream.writeUTFBytes(_content);
				stream.close();
				stream = null;
			} catch (e:Error) {
				// Dont do anything.  Its possible the cached file was open and we tried to over-wirte it.
				// Thats fine because we'll just allow loading of the previous cached file
			}
			outputFile = null;
		}
		
		private function createDrupalInterface(_loginInformation:xmlCacheVO):void {
			if(_drupal == null) {
				_drupal = new Drupal7Sercies3RESTInterface(_loginInformation.drupalCMSBasePath, _loginInformation.drupalCMSEndpointName);
				_drupal.addEventListener(Drupal7Sercies3RESTInterface.LOGGED_IN, handleDrupalLoggedIn, false, 0, true);
				_drupal.addEventListener(Drupal7Sercies3RESTInterface.LOGGED_OUT, handleDrupalLoggedOut, false, 0, true);
				_drupal.addEventListener(Drupal7Sercies3RESTInterface.UNRECOVERABLE_ERROR, handleDrupalError, false, 0, true);
				_drupal.login( _loginInformation.drupalCMSUserName, _loginInformation.drupalCMSPassword );
				// Wait for login event to proceede
			} else {
				destroyDrupalInterface( true, _loginInformation );
			}
		}
		private function destroyDrupalInterface(_optionalCallback:Boolean = false, _loginInfo:xmlCacheVO = null):void {
			if(_drupal != null) {
				_drupal.removeEventListener(Drupal7Sercies3RESTInterface.LOGGED_IN, handleDrupalLoggedIn);
				_drupal.removeEventListener(Drupal7Sercies3RESTInterface.LOGGED_OUT, handleDrupalLoggedOut);
				_drupal.removeEventListener(Drupal7Sercies3RESTInterface.UNRECOVERABLE_ERROR, handleDrupalError);
				_drupal.destroyInternals();
				_drupal = null;
				if(_optionalCallback == true) createDrupalInterface(_loginInfo);
			}
		}
		private function handleDrupalLoggedIn(e:Event):void {
			GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].drupalLoggedIn = true;
			nextXMLTimestampCacheStep();
		}
		private function handleDrupalLoggedOut(e:Event):void {
			destroyDrupalInterface();
			GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].drupalLoggedIn = false;
			// Move on to the next step
			_currentXMLPathIndex += 1;
			nextXMLTimestampCacheStep();
		}
		/**
		 * We should destroy the instance and move on here. There may have been a network error,
		 * who knows. It will be attempted again next cache check anyway.
		 */
		private function handleDrupalError(e:Event):void {
			// We should destroy the instance and move on
			destroyDrupalInterface();
			GlobalDataStorage.guideXMLFiles[_currentXMLPathIndex].drupalLoggedIn = false;
			// Move on to the next step
			_currentXMLPathIndex += 1;
			nextXMLTimestampCacheStep();
		}
	}
}