package com.transcendingdigital.data
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	/**
	 * For interfacing with Drupal 7 Services 3 REST
	 * server. The Services module has been very active lateley
	 * with breaking changes.
	 * 
	 * This was authored in November 2013 against versions:
	 * core: 7.23
	 * services: 7.x-2.3
	 * views: 7.x-3.7
	 * 
	 * On Windows anyway, AIR shares session data with Internet Explorer. If you
	 * log in, then do not log out, you may find you are still logged in
	 * next application run, and various other shenanigans.
	 * 
	 * We just use this for being logged in and out to be able to acess private files
	 * which are unpublished when doing cache operations.
	 */
	public class Drupal7Sercies3RESTInterface extends EventDispatcher
	{
		public static const LOGGED_IN:String = "com.transcendingdigital.data.Drupal7Sercies3RESTInterface.LOGGED_IN";
		public static const LOGGED_OUT:String = "com.transcendingdigital.data.Drupal7Sercies3RESTInterface.LOGGED_OUT";
		public static const UNRECOVERABLE_ERROR:String = "com.transcendingdigital.data.Drupal7Sercies3RESTInterface.UNRECOVERABLE_ERROR";
		
		private const DRUPAL_LOGIN_JSON:String = "/user/login.json";
		private const DRUPAL_LOGOUT_JSON:String = "/user/logout.json";
		private const DRUPAL_CSRF_TOKEN_JSON:String = "services/session/token";
		
		private var _DrupalURL:String = "";
		private var _DrupalServiceEndpointName:String = "";
		
		
		private var _urlRequest:URLRequest = null;
		private var _urlLoader:URLLoader = null;
		
		private var _latestMethodString:String = "";
		private var _latestResponseData:Object = null;
		private var _loggedInObject:Object = null;
		private var _activeCSRFToken:String = "";
		
		public function Drupal7Sercies3RESTInterface(_DrupalBaseURL:String, _DrupalEndpointName:String)
		{
			_DrupalURL = _DrupalBaseURL;
			_DrupalServiceEndpointName = _DrupalEndpointName;
			
			super(null);
		}
		public function destroyInternals():void {
			destroyRequest();
		}
		
		/**
		 * Because manual cookie management would just be so tough, flash
		 * does it annoyingly for us. If this suceeds we are logged in
		 * just as a normal browser.
		 */
		public function login(_username:String, _password:String):void {
			
			createRequest();
			var loginData:Object = {
				username: _username, 
				password: _password
			};
			
			var jsonEncodedLoginData:String = JSON.stringify(loginData);
			_urlRequest.data = jsonEncodedLoginData;
			_urlRequest.method = URLRequestMethod.POST;
			_urlRequest.url = _DrupalURL + '/' + _DrupalServiceEndpointName + DRUPAL_LOGIN_JSON;
			_latestMethodString = DRUPAL_LOGIN_JSON;
			
			try {
				_urlLoader.load( _urlRequest );
			} catch(e:Error) {
				trace("Drupal7Services3RESTInterface - login error: " + e);
			}
		}
		
		public function logout():void {
			if(_loggedInObject != null) {
				createRequest();
				_urlRequest.method = URLRequestMethod.POST;
				_urlRequest.url = _DrupalURL + '/' + _DrupalServiceEndpointName + DRUPAL_LOGOUT_JSON;
				_latestMethodString = DRUPAL_LOGOUT_JSON;
				
				try {
					_urlLoader.load( _urlRequest );
				} catch(e:Error) {
					trace("Drupal7Services3RESTInterface - logout error: " + e);
				}
			} else {
				dispatchEvent( new Event(Drupal7Sercies3RESTInterface.LOGGED_OUT, false, true));
			}
		}
		
		/**
		 * Starting in Services 3.4, we need a CSRF token to help
		 * prevent cross site request forgery. 
		 * 
		 * As of the shaky services 3.5 this is needed for any other
		 * POST operations right after login...so logout needs it.
		 * 
		 * I may be stupid, but
		 * doing this after a login session is already obtained seems
		 * really stupid. How the heck will that help? A token and
		 * session should have been obtained BEFORE random POST calls
		 * like login can hammer a server...I have a feeling this
		 * will be changed in future services releases.
		 * 
		 * I also have a feeling they put this in for NON SESSION AUTHENTICATED
		 * service calls. Most of the examples online indicate people just
		 * have no clue in terms of security..like allowing anonymous
		 * user creation..etc.
		 */
		private function requestCSRFToken():void {
			if(_loggedInObject != null) {
				createRequest();
				_urlRequest.method = URLRequestMethod.GET;
				_urlRequest.url = _DrupalURL + '/' + DRUPAL_CSRF_TOKEN_JSON;
				_latestMethodString = DRUPAL_CSRF_TOKEN_JSON;
				
				try {
					_urlLoader.load( _urlRequest );
				} catch(e:Error) {
					trace("Drupal7Services3RESTInterface - logout error: " + e);
				}
			}
		}
		
		private function createRequest():void {
			if(_urlLoader == null) {
				_latestResponseData = null;
				_urlLoader = new URLLoader();
				_urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
				_urlLoader.addEventListener(Event.COMPLETE, handleRequestComplete, false, 0, true);
				_urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, handleRequestSecurityError, false, 0, true);
				_urlLoader.addEventListener(IOErrorEvent.IO_ERROR, handleRequestIOError, false, 0, true);
				
				_urlRequest = new URLRequest();
				_urlRequest.contentType = 'application/json';
				// As of Services 3.4 We need a CSRF token header, check for its presence and set it, if present.
				if(_activeCSRFToken != "") {
					var addHeaderArray:Array = new Array(new URLRequestHeader("X-CSRF-Token", _activeCSRFToken));
					_urlRequest.requestHeaders = addHeaderArray;
				}
			} else {
				destroyRequest( true );
			}
		}
		
		private function destroyRequest(_optionalCallback:Boolean = false):void {
			if(_urlLoader != null) {
				try {
					_urlLoader.close();
				} catch(e:Error) {
					// Who cares just sink it
				}
				
				_urlRequest = null;
				_urlLoader.removeEventListener(Event.COMPLETE, handleRequestComplete);
				_urlLoader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, handleRequestSecurityError);
				_urlLoader.removeEventListener(IOErrorEvent.IO_ERROR, handleRequestIOError);
				_urlLoader = null;
				
				if(_optionalCallback == true) createRequest();
			}
		}
		
		private function handleRequestComplete(e:Event):void {
			_latestResponseData = e.target.data;
			destroyRequest();
			handleDrupalResponse();
		}
		private function handleRequestSecurityError(e:SecurityErrorEvent):void {
			trace("Drupal7Services3RESTInterface - handleRequestSecurityError - : " + e);
			
			_latestResponseData = e.target.data;
			destroyRequest();
			handleDrupalResponse();
		}
		private function handleRequestIOError(e:IOErrorEvent):void {
			trace("Drupal7Services3RESTInterface - handleRequestIOError - : " + e);
			
			_latestResponseData = e.target.data;
			destroyRequest();
			handleDrupalResponse();
		}
		private function handleDrupalResponse():void {
			
			switch(_latestMethodString) {
				case DRUPAL_LOGIN_JSON:
					trace("Drupal7Services3RESTInterface - login response: " + _latestResponseData);
					if(_latestResponseData != null) {
						try {
						
							var responseText:String = String( _latestResponseData );
							_loggedInObject = JSON.parse(responseText);
							
							requestCSRFToken();
							
						} catch (e:Error) {
							dispatchEvent(new Event(Drupal7Sercies3RESTInterface.UNRECOVERABLE_ERROR, false, true));
						}
					}
				break;
				case DRUPAL_LOGOUT_JSON:
					trace("Drupal7Services3RESTInterface - logout response: " + _latestResponseData);
					if(_latestResponseData != null) {
						// Do check for valid response - valid response does not matter
						// we need to move on as if we were logged out
						var logoutResponse:String = String( _latestResponseData );
						var responseObj:Object = JSON.parse(logoutResponse);
						if(responseObj[0] == true) {
							_loggedInObject = null;
							_activeCSRFToken = "";
						}
						
						dispatchEvent( new Event(Drupal7Sercies3RESTInterface.LOGGED_OUT, false, true));
					}
				break;
				case DRUPAL_CSRF_TOKEN_JSON:
					trace("Drupal7Services3RESTInterface - csrf token response: " + _latestResponseData);
					if(_latestResponseData != null) {
						_activeCSRFToken = String(_latestResponseData);
						// We are logged in, ready to make calls
						dispatchEvent( new Event(Drupal7Sercies3RESTInterface.LOGGED_IN, false, true) );
					}
				break;
			}
			
		}
	}
}