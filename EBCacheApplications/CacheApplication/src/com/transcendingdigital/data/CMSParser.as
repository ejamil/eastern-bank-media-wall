package com.transcendingdigital.data
{
	import com.transcendingdigital.data.vo.cacheFileData;
	import com.transcendingdigital.data.vo.cacheFileSegment;
	import com.transcendingdigital.data.vo.cacheMediaWallFileTabData;
	import com.transcendingdigital.data.vo.cacheMediaWallGameData;
	import com.transcendingdigital.data.vo.cacheMediaWallPageData;

	/**
	 * This class parses data from the CMS and
	 * places it in data structures applicable to the
	 * application. 
	 * 
	 * For the final data structures available to the application. Assets
	 * need to reside local, so any output xml guide data should also
	 * be pointing to local assets.
	 * 
	 * Shouldnt take too much horespower and this all just blocks
	 * so when it finishes the data will be ready.
	 */
	public class CMSParser
	{
		public static const TYPE_PROMOTIONS_DATA:String = "PROMOTIONS";
		public static const TYPE_VESTIBULE_DATA:String ="VESTIBULE";
		public static const TYPE_MEDIAWALL_DATA:String = "MEDIAWALL";
		public static const TYPE_MEDIAWALLGAME_DATA:String = "MEDIAWALLGAME";
		
		private var sortPropertyName:String = "_PlayerLastName";
		
		public function CMSParser()
		{
			
		}
		
		public function parseData(_TypeConst:String, _rawData:XML):void {
			
			// Reset volitle storage data structures
			GlobalDataStorage.currentCachingText = new cacheFileSegment();
			GlobalDataStorage.currentCachingText.initStructure(_TypeConst);
			
			GlobalDataStorage.currentCachingPhotos = new cacheFileSegment();
			GlobalDataStorage.currentCachingPhotos.initStructure(_TypeConst);
			
			GlobalDataStorage.currentCachingVideos = new cacheFileSegment();
			GlobalDataStorage.currentCachingVideos.initStructure(_TypeConst);

			GlobalDataStorage.currentCachingMediaWallGameData = new cacheMediaWallGameData();
			GlobalDataStorage.currentCachingMediaWallData = new Vector.<cacheMediaWallPageData>();
			
			
			switch(_TypeConst) {
				case TYPE_PROMOTIONS_DATA:
					parsePromotionsData( _rawData );
					break;
				case TYPE_VESTIBULE_DATA:
					parseVestibuleData( _rawData );
					break;
				case TYPE_MEDIAWALL_DATA:
					parseMediaWallData( _rawData );
					break;
				case TYPE_MEDIAWALLGAME_DATA:
					parseMediaWallGameData( _rawData );
					break;
			}
		}
		
		/**
		 * The structure for this data follows a mixed
		 * paradigm. In any data node we may have,
		 * a paragraph of text, a photo, or a video.
		 * One node may have all three or just one.
		 * 
		 * For simplicity in the final output it would be
		 * smart to divide up the photo, video, and text only data.
		 * 
		 * <?xml version="1.0" encoding="UTF-8" ?>
		 * <content>
		 * <data>
    	 * <title><![CDATA[Promotions Nov 19 Entry]]></title>
         * <photo>
         * <url><![CDATA[]]></url>
         * <file_size><![CDATA[]]></file_size>
         * </photo>
         * <video>
         * <url><![CDATA[]]></url>
         * <file_size><![CDATA[]]></file_size>
         * </video>
         * <content><![CDATA[On November 19 there will be new free debit cards issues to all patrons!]]></content>
         * </data>
		 * </content>
		 * 
		 */
		private function parsePromotionsData(_data:XML):void {
			
			for each( var targetNode:XML in _data.data ) {
				//trace("CMSParser - targetNode: " + targetNode);
				var currentData:cacheFileData = new cacheFileData();
				currentData.title = targetNode.title.text()[0];
				currentData.body = targetNode.content.text()[0];
				currentData.photoPath = targetNode.photo.url.text()[0];
				currentData.photoByteSize = Number( targetNode.photo.file_size.text()[0] );
				currentData.videoPath = targetNode.video.url.text()[0];
				currentData.videoByteSize = targetNode.video.file_size.text()[0];
				
				// Determine what structure it goes in
				if(currentData.photoPath == "" && currentData.videoPath == "" && currentData.body != "") {
					GlobalDataStorage.currentCachingText.dataElements.push( currentData );
				} else if(currentData.photoPath != "" && currentData.videoPath == "") {
					GlobalDataStorage.currentCachingPhotos.dataElements.push( currentData );
				} else if(currentData.videoPath != "" && currentData.photoPath == "") {
					GlobalDataStorage.currentCachingVideos.dataElements.push( currentData );
				}
			}
		}
		
		private function parseVestibuleData(_data:XML):void {
			
		}
		
		/**
		 * Media wall data is special compared to the other
		 * types of data. Its a special pain in the rear.  The
		 * xml data looks like the following:
		 * <content>
		 * <data>
		 * <title>Business</title>
		 * <photo>
		 * 	<url></url>
		 * 	<file_size></file_size>
		 * </photo>
		 * <tab1_button_text></tab1_button_text>
		 *  <tab1_title><![CDATA[]]></tab1_title>
    <tab1_body><![CDATA[]]></tab1_body>
    <tab1_photo>
         <url><![CDATA[]]></url>
         <file_size><![CDATA[]]></file_size>
	</tab1_photo>
    <tab1_video>
         <url><![CDATA[]]></url>
         <file_size><![CDATA[]]></file_size>
	</tab1_video>
    <tab2_button_text><![CDATA[]]></tab2_button_text>
    <tab2_title><![CDATA[]]></tab2_title>
    <tab2_body><![CDATA[]]></tab2_body>
    <tab2_photo>
         <url><![CDATA[]]></url>
         <file_size><![CDATA[]]></file_size>
	</tab2_photo>
    <tab2_video>
         <url><![CDATA[]]></url>
         <file_size><![CDATA[]]></file_size>
	</tab2_video>
    <tab3_button_text><![CDATA[]]></tab3_button_text>
    <tab3_title><![CDATA[]]></tab3_title>
    <tab3_body><![CDATA[]]></tab3_body>
    <tab3_photo>
         <url><![CDATA[]]></url>
         <file_size><![CDATA[]]></file_size>
	</tab3_photo>
    <tab3_video>
         <url><![CDATA[]]></url>
         <file_size><![CDATA[]]></file_size>
	</tab3_video>
    <tab4_button_text><![CDATA[]]></tab4_button_text>
    <tab4_title><![CDATA[]]></tab4_title>
    <tab4_body><![CDATA[]]></tab4_body>
    <tab4_photo>
         <url><![CDATA[]]></url>
         <file_size><![CDATA[]]></file_size>
	</tab4_photo>
    <tab4_video>
         <url><![CDATA[]]></url>
         <file_size><![CDATA[]]></file_size>
	</tab4_video>
   			 <tab5_button_text><![CDATA[]]></tab5_button_text>
    		<tab5_title><![CDATA[]]></tab5_title>
    		<tab5_body><![CDATA[]]></tab5_body>
    		<tab5_photo>
        	 <url><![CDATA[]]></url>
         	<file_size><![CDATA[]]></file_size>
			</tab5_photo>
   			 <tab5_video>
        	 <url><![CDATA[]]></url>
        	 <file_size><![CDATA[]]></file_size>
			</tab5_video>
   			 <tab6_button_text><![CDATA[]]></tab6_button_text>
    		<tab6_title><![CDATA[]]></tab6_title>
   			 <tab6_body><![CDATA[]]></tab6_body>
    		<tab6_photo>
         	<url><![CDATA[]]></url>
        	 <file_size><![CDATA[]]></file_size>
			</tab6_photo>
    		<tab6_video>
         	<url><![CDATA[]]></url>
        	 <file_size><![CDATA[]]></file_size>
			</tab6_video>
		 * </data>
		 * <data>
		 * The entire sequence above repeats for each
		 * category.
		 * </data>
		 * </content>
		 */
		private function parseMediaWallData(_data:XML):void {
			for each( var targetNode:XML in _data.data ) {
				//trace("CMSParser - targetNode: " + targetNode);
				var currentData:cacheMediaWallPageData = new cacheMediaWallPageData();
				currentData.title = targetNode.title.text()[0];
				currentData.initStructure( currentData.title.toLowerCase() );
				currentData.bgPhotoURL = targetNode.photo.url.text()[0];
				currentData.bgPhotoSize = Number( targetNode.photo.file_size.text()[0] );
				currentData.tabData = new Vector.<cacheMediaWallFileTabData>();
				
				for(var i:int = 1; i < 7; i++) {
					var thisTab:cacheMediaWallFileTabData = new cacheMediaWallFileTabData();
					thisTab.buttonText = targetNode['tab' + String(i) + '_button_text'].text()[0];
					thisTab.title = targetNode['tab' + String(i) + '_title'].text()[0];
					thisTab.bodyText = targetNode['tab' + String(i) + '_body'].text()[0];
					thisTab.bodyPhotoURL = targetNode['tab' + String(i) + '_photo'].url.text()[0];
					thisTab.bodyPhotoSize = Number( targetNode['tab' + String(i) + '_photo'].file_size.text()[0] );
					thisTab.bodyVideoURL = targetNode['tab' + String(i) + '_video'].url.text()[0];
					thisTab.bodyVideoSize = Number( targetNode['tab' + String(i) + '_video'].file_size.text()[0] );
					currentData.tabData.push( thisTab );
				}
				
				GlobalDataStorage.currentCachingMediaWallData.push( currentData );
			}
			
		}
		
		/**
		 * Media wall game data is unique. Has a very small and
		 * simple structure like the following. Activated Games show up in
		 * a pipe | delimeted list:
		 * <?xml version="1.0" encoding="UTF-8" ?><content>
  			<data>
    			<background_photo>
  				<url><![CDATA[http://dev-bpidevops.gotpantheon.com/system/files/media_wall/PlayPopupBckg.png]]></url>
 				 <file_size><![CDATA[18908]]></file_size>
 				</background_photo>
   				 <game_selection><![CDATA[Drawing App|Bill Ninja]]></game_selection>
  			</data>
			</content>
		 * 
		 */
		private function parseMediaWallGameData(_data:XML):void {
				GlobalDataStorage.currentCachingMediaWallGameData.initStructure(TYPE_MEDIAWALLGAME_DATA);
				
				if(_data.data.background_photo.url != undefined) {
					GlobalDataStorage.currentCachingMediaWallGameData.bgPhotoURL = _data.data.background_photo.url.text()[0];
				}
				if(_data.data.background_photo.file_size != undefined) {
					GlobalDataStorage.currentCachingMediaWallGameData.bgPhotoSize = Number(_data.data.background_photo.file_size.text()[0]);
				}
				if(_data.data.game_selection != undefined) {
					GlobalDataStorage.currentCachingMediaWallGameData.activatedGameList = String(_data.data.game_selection.text()[0]).split('|');
				}
		}
		
		private function sortOnProperty(obj1:Object, obj2:Object):Number {
			
			var a:Object = String(obj1[sortPropertyName]).toLowerCase();
			var b:Object = String(obj2[sortPropertyName]).toLowerCase();
			
			if (a < b)
				return -1;
			if (a > b)
				return 1;
			return 0;
		}

	}
}