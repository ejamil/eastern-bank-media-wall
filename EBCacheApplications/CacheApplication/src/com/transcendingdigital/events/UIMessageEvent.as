package com.transcendingdigital.events
{
	import flash.events.Event;
	
	public class UIMessageEvent extends Event
	{
		public var data:* = null;
		
		public function UIMessageEvent(type:String, _data:* = null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			data = _data;
			
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event {
			return new UIMessageEvent(type, data, bubbles, cancelable);
		}
	}
}