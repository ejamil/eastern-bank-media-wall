package com.technogumbo.data
{
	import com.transcendingdigital.data.GlobalDataStorage;
	
	import flash.errors.IOError;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	
	public class XMLLoader extends EventDispatcher
	{
		public static var DATALOADED:String = "com.technogumbo.data.XMLLoader_DATALOADED";
		private var _finalData:String;
		private var _theLoader:URLLoader;
		private var _xmlPath:String;
		
		private var _cacheFile:Boolean = false;
		private var _cacheSize:Number = 0;
		private var remoteFileLoader:URLLoader;
		
		public function XMLLoader(_LoadPath:String, _CacheFile:Boolean = false, _expectedCacheSize:Number = 0)
		{
			_xmlPath = _LoadPath;
			_cacheFile = _CacheFile;
			_cacheSize = _expectedCacheSize;
		}
		
		public function LoadXMLData():void {
			if(_cacheFile == true) {
				initiateCacheOps();
			} else {
				createLoader();
				_theLoader.load( getURLRequestWithRandomPropertyToPreventCache(_xmlPath) );
			}
		}
		
		public function getRawString():String {
			return _finalData;
		}
		
		public function getLoadedXML():XML {
			return XML(_finalData);
		}
		
		public function destroyInternals():void {
			destroyRemoteFileLoader();
			destroyLoader();
			_finalData = null;
		}
		
		private function handleXMLLoaded(e:Event):void {
			_finalData = String(e.target.data);
			dispatchEvent(new Event(XMLLoader.DATALOADED, false, true));
		}
		
		private function handleLoadError(e:IOErrorEvent):void {
			dispatchEvent(e);
		}
		private function handleSecurityError(e:SecurityErrorEvent):void {
			dispatchEvent(e);
		}
		private function createLoader():void {
			if (_theLoader == null) {
			_theLoader = new URLLoader();
			_theLoader.addEventListener(Event.COMPLETE, handleXMLLoaded, false, 0, true);
			_theLoader.addEventListener(IOErrorEvent.IO_ERROR, handleLoadError, false, 0, true);
			_theLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, handleLoadError, false, 0, true);
			} else {
				destroyLoader(true);
			}
		}
		
		private function destroyLoader(_OptionalCallback:Boolean = false):void {
			if (_theLoader != null) {
				try {
					_theLoader.close();
				} catch (e:Error) {
					// Who cares
				}
				_theLoader.removeEventListener(Event.COMPLETE, handleXMLLoaded);
				_theLoader.removeEventListener(IOErrorEvent.IO_ERROR, handleLoadError);
				_theLoader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, handleLoadError);
				_theLoader = null;
				
				if (_OptionalCallback == true) {
					createLoader();
				}
			}
		}
		
		private function initiateCacheOps():void {
			var fileNameOnly:String = getFileNameOnly(_xmlPath);
			var tmpFile:File;
			
			//trace("Trying to check for file: " + "app-storage:" + File.separator + "imageCache" + File.separator + fileNameOnly);
			// Check the video cache directory - THINK...cross platform you have to use backslashes here.  I get argument exception on windows using File.separator or \
			tmpFile = new File(GlobalDataStorage.cacheDirectoryPrefix + "xmlCache/" + fileNameOnly);
			var tmpFileSize:int = 0;
			
			// Get the file size of the file if it exists
			if (tmpFile.exists) 
			{
				tmpFileSize = tmpFile.size;
			}
			
			if (tmpFile.exists && (tmpFileSize == _cacheSize) && (_cacheSize != 0) ) 
			{
				// Same file as remote - load the local file
				_xmlPath = tmpFile.url;
				
				createLoader();
				_theLoader.load(getURLRequestWithRandomPropertyToPreventCache(_xmlPath));
			} else {
				// Get on loading the remote file
				createRemoteFileLoader();
			}
		}
		/*
		* @private
		* Takes a string of a path like http://192.168.0.10/drupal/sites/default/files/factory_Cam/videos/Sequence1-TURKEYHILL-MPEG-4.mov
		* or videos/Sequence1-TURKEYHILL-MPEG-4.mov
		* or \\ENTERPRISE\Shared_Documents\Sequence1-TURKEYHILL-MPEG-4.mov
		* or Sequence1-TURKEYHILL-MPEG-4.mov
		* and returns everything after the last slash, or just everything if there is no slash
		*/
		private function getFileNameOnly(_FullPath:String):String {
			var returnString:String = "";
			
			// Find the last slash
			var lastSlashPos:int = -1;
			
			// Are they back slashes?
			lastSlashPos = _FullPath.lastIndexOf("/");
			
			// Ok, no dice try the other direction
			if (lastSlashPos == -1) {
				lastSlashPos = _FullPath.lastIndexOf("\\");
			}
			
			// If we got a match on something, shorten the string
			if (lastSlashPos != -1) {
				returnString = _FullPath.substr((lastSlashPos + 1), _FullPath.length - lastSlashPos);
			}
			
			return returnString;
		}
		
		/*
		* @private
		* Begins the process of loading a remote file then writing a copy of that file into the local cache
		*/
		private function createRemoteFileLoader():void {
			if(remoteFileLoader == null) 
			{
				//trace("creating remote file loader");
				remoteFileLoader = new URLLoader();
				remoteFileLoader.addEventListener(Event.COMPLETE, handleRemoteLoadCoplete);
				remoteFileLoader.addEventListener(IOErrorEvent.IO_ERROR, handleRemoteLoadError, false, 0, true);
				remoteFileLoader.dataFormat = URLLoaderDataFormat.BINARY;
				var request:URLRequest = new URLRequest(_xmlPath);
				request.cacheResponse = false;
				
				remoteFileLoader.load( request );
			} else {
				destroyRemoteFileLoader(true);
			}
		}
		
		/*
		* @private
		* Gets rid of the remote file loader
		*/
		private function destroyRemoteFileLoader(_OptionalCallback:Boolean = false):void {
			if (remoteFileLoader != null) {
				try {
					remoteFileLoader.close();
				} catch (e:Error) {
					// No big deal, no load was in progress.  Just sink it
				}
				
				remoteFileLoader.removeEventListener(Event.COMPLETE, handleRemoteLoadCoplete);
				remoteFileLoader.removeEventListener(IOErrorEvent.IO_ERROR, handleRemoteLoadError);
				remoteFileLoader = null;
				
				if (_OptionalCallback == true) {
					createRemoteFileLoader();
				}
			}
		}
		
		/*
		* @private
		* Event for handling the end of file loading
		* on this projects CMS (UTES Football). The CMS
		* has problems where it generates blank files for xml
		* we need to NOT cache if thats the case.
		*/
		private function handleRemoteLoadCoplete(e:Event):void {
			//trace("SinglePhotoCaching - Remote load complete");
			
			var justFileName:String = getFileNameOnly(_xmlPath);
			var newCacheFile:File  = new File(GlobalDataStorage.cacheDirectoryPrefix + "xmlCache/" + justFileName);
			
			if(remoteFileLoader.bytesLoaded > 0) {
				try {
					var stream:FileStream = new FileStream();
					stream.open(newCacheFile, FileMode.WRITE);
					stream.writeBytes(remoteFileLoader.data);
					stream.close();
					stream = null;
				} catch (e:Error) {
					// Dont do anything.  Its possible the cached file was open and we tried to over-wirte it.
					// Thats fine because we'll just allow loading of the previous cached file
				}
			}
			destroyRemoteFileLoader();
			
			// Cache file has been created.
			// Have the xml load the cache file
			_xmlPath = newCacheFile.url;
			
			// Initiate normal load operations
			createLoader();
			_theLoader.load(getURLRequestWithRandomPropertyToPreventCache(_xmlPath));
		}
		
		/*
		* @private
		* Event for handling file load error
		*/
		private function handleRemoteLoadError(e:IOErrorEvent):void {
			destroyRemoteFileLoader();
			// Bummer. We need to check if there is an older version of this file in
			// cache; if so, we use that.
			var fileNameOnly:String = getFileNameOnly(_xmlPath);
			var tmpFile:File = new File(GlobalDataStorage.cacheDirectoryPrefix + "xmlCache/" + fileNameOnly);
			if(tmpFile.exists) {
				_xmlPath = tmpFile.url;
				
				// Initiate normal load operations
				createLoader();
				_theLoader.load( getURLRequestWithRandomPropertyToPreventCache(_xmlPath) );
			} else {
				throw(e);
			}
		}
		
		private function getURLRequestWithRandomPropertyToPreventCache(_URL:String):URLRequest {
			var randomPropDate:Date = new Date();
			var returnRequest:URLRequest = new URLRequest( _URL );
			returnRequest.cacheResponse = false;
			
			return returnRequest;
			//return new URLRequest( _URL + "?timestamp=" + String(randomPropDate.valueOf()) );
		}
	}
}