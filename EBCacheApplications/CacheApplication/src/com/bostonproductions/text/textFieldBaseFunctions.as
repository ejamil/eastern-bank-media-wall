﻿package com.bostonproductions.text {
	
	import flash.display.DisplayObject;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	public class textFieldBaseFunctions {
	
		/*
		 * @public
		 * This will attempt to remove any html tags and new lines.
		 * Ideal for text coming out of a CMS
		 */
		public static function stripHTMLTags(_InputText:String):String {
			return _InputText.replace(/<[pP].*?>|<\/[pP]>|\n|\r/gism, "");
		}
		
		/*
		 * @public
		 * Gets rid of any white space on the front of a string
		 */
		public static function trimFront(_InputString:String):String {
			return _InputString.replace(/^[ \t]+/gi, "");
		}
		
		public static function removeJackedChars(_InputString:String):String {
			
			_InputString = _InputString.replace(/‘/gism, '\'');

			return _InputString;
		}
		
		// This takes a text field and forces it to a certain size
		public static function forceTextSize(_TxtObj:TextField, _NewSize:int):void {
			   var _CurrentFormat:TextFormat = _TxtObj.getTextFormat();
			  _CurrentFormat.size = _NewSize;
			  _TxtObj.setTextFormat(_CurrentFormat);
			  _TxtObj.defaultTextFormat = _CurrentFormat;
		}
		
		// This takes a text field and forces it to a certain size
		public static function forceLineSpacing(_TxtObj:TextField, _NewSize:int):void {
			   var _CurrentFormat:TextFormat = _TxtObj.getTextFormat();
			  _CurrentFormat.leading = _NewSize;
			  _TxtObj.setTextFormat(_CurrentFormat);
			  _TxtObj.defaultTextFormat = _CurrentFormat;
		}
		
		// This takes a text field and forces it to a certain alignment
		public static function forceTextAlighnment(_TxtObj:TextField, _Alignment:String= "left"):void {
			   
			var _CurrentFormat:TextFormat = _TxtObj.getTextFormat();
			switch(_Alignment) {
				case "left":
					_CurrentFormat.align = TextFormatAlign.LEFT;
				break;
				case "right":
					_CurrentFormat.align = TextFormatAlign.RIGHT;
				break;
				case "center":
					_CurrentFormat.align = TextFormatAlign.CENTER;
				break;
			}

			  _TxtObj.setTextFormat(_CurrentFormat);
			  _TxtObj.defaultTextFormat = _CurrentFormat;
		}
		
		
		// This takes a multiline text field and makes sure whatever text is in it is resized until it fits on only one line
		// Returns the new font size
		public static function fitTextIntoField(_TxtObj:TextField, _NumberOfLines:int):int {
			 //Do we need to decrease the font size?
			var ResizeCnt:int = 0;
			
			// This is how many points we will try and reduce the font by before having to give up
			var MaxCnt:int = 50;
			
			var tmpFormat:TextFormat = _TxtObj.getTextFormat();
			
			var returnSize:int = int( tmpFormat.size);
			
//trace("textFieldBaseFunctions - fitTextIntoField - " + _TxtObj.text + " lines: " + _TxtObj.numLines + " lines: " + _NumberOfLines);
	         while ((_TxtObj.numLines > _NumberOfLines) && (ResizeCnt < MaxCnt) ) {

		      // Decrease object font size until it gets visible
                ResizeCnt += 1;
		      // Move the text field down by one pixel every time to reduce the size by one pixel
			    //_TxtObj.y += .4;
//trace("textFieldBaseFunctions - resizing - round: " + ResizeCnt);
			   var _CurrentFormat:TextFormat = _TxtObj.getTextFormat();
			  _CurrentFormat.size = int(_CurrentFormat.size) - 1;
			  returnSize = int(_CurrentFormat.size);
			  _TxtObj.setTextFormat(_CurrentFormat);
			  _TxtObj.defaultTextFormat = _CurrentFormat;
	         }
//trace("textFieldBaseFunctions - fitTextIntoField - RETURN SIZE: " + returnSize);
			 return returnSize;
		}
		
		// This takes a multiline text field and makes sure whatever text is in it is resized until it fits on only one line
		// Returns the new font size
		public static function fitTextIntoWidth(_TxtObj:TextField, _DesiredWidth:Number):int {
			 //Do we need to decrease the font size?
			var ResizeCnt:int = 0;
			
			// This is how many points we will try and reduce the font by before having to give up
			var MaxCnt:int = 25;
			
			var tmpFormat:TextFormat = _TxtObj.getTextFormat();
			
			var returnSize:int = int( tmpFormat.size);
//trace("textFieldBaseFunctions - fitTextIntoWidth real: " + _TxtObj.textWidth + " desired: " + _DesiredWidth);
	         while ((_TxtObj.textWidth > _DesiredWidth) && (ResizeCnt < MaxCnt) ) {
//trace("textFieldBaseFunctions - LOOP - fitTextIntoWidth real: " + _TxtObj.textWidth + " desired: " + _DesiredWidth);
		      // Decrease object font size until it gets visible
                ResizeCnt += 1;
		      // Move the text field down by one pixel every time to reduce the size by one pixel
			    _TxtObj.y += .4;
			   var _CurrentFormat:TextFormat = _TxtObj.getTextFormat();
			  _CurrentFormat.size = int(_CurrentFormat.size) - 1;
			  returnSize = int(_CurrentFormat.size);
			  _TxtObj.setTextFormat(_CurrentFormat);
			  _TxtObj.defaultTextFormat = _CurrentFormat;
	         }
			 
			 return returnSize;
		}
		
	}
	
}