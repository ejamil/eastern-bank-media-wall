package com.bostonproductions.data
{
	import com.hurlant.util.Base64;
	import com.technogumbo.data.XMLLoader;
	import com.transcendingdigital.data.GlobalDataStorage;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.utils.ByteArray;

	/**
	 * This is for parsing and responding to commands sent as strings
	 * Commands come in the form:
	 * 
	 * GET_Display Key_What to get
	 * 
	 * So an example of getting promotions photos would look
	 * like:
	 * GET_PROMOTIONS_PHOTOS
	 * 
	 * An example of getting all data would look like:
	 * GET_PROMOTIONS_ALL
	 * 
	 * 
	 */
	public class commandAPI extends EventDispatcher
	{
		public static var DATA_READY:String = "com.bostonproductions.data.commandAPI.DATA_READY";
		
		private var _currentDataKey:String = "";
		private var _currentContentKey:String = "";
		private var _filesToLoad:Vector.<String> = null;
		private var _fileIndex:int = 0;
		
		private var _finalContent:String = "";
		private var _originalCommand:String = "";
		
		private var _currentActiveXML:XMLLoader;
		
		public function commandAPI()
		{
			
		}
		
		public function getFinalData():String {
			return _finalContent;
		}
		/**
		 * Assembles a comma delimeted, base64 encoded version of our 
		 * command and data for sending over the network
		 * 
		 */
		public function getTransmittableFinalData():String {
			return Base64.encode( _originalCommand ) + "," + Base64.encode( _finalContent );
		}
		/**
		 * Commands come in here as a base64 encoded
		 * string or not depending if _decodeString is set.
		 */
		public function parseCommand(_command:String, _decodeString:Boolean = true):void {
			
			
			if(_decodeString == true) {
				_originalCommand = Base64.decode(_command);
			} else {
				_originalCommand = _command;
			}

			var commandParts:Array = _originalCommand.split('_');
			
			if(commandParts.length == 3) {
				var dataKey:String = commandParts[1];
				var contentKey:String = commandParts[2];
				if(dataKey != "" && contentKey != "") {
					_currentDataKey = dataKey;
					_currentContentKey = contentKey;
					_filesToLoad = new Vector.<String>();
					_fileIndex = 0;
					_finalContent = '';
					
					if( contentKey == "ALL" ) {
						if( _originalCommand == commandConstants.PROMOTIONS_ALL || _originalCommand == commandConstants.PROMOTIONS_EVENT) {
								_filesToLoad.push(GlobalDataStorage.configXML.config.LocalCacheDirectory.text()[0] +  dataKey.toLowerCase() + '/caching_photos.xml');
								_filesToLoad.push(GlobalDataStorage.configXML.config.LocalCacheDirectory.text()[0] +  dataKey.toLowerCase() + '/caching_text.xml');
								_filesToLoad.push(GlobalDataStorage.configXML.config.LocalCacheDirectory.text()[0] +  dataKey.toLowerCase() + '/caching_videos.xml');
						} else if(_originalCommand == commandConstants.MEDIA_WALL_ALL || _originalCommand == commandConstants.MEDIA_WALL_EVENT) {
								_filesToLoad.push(GlobalDataStorage.configXML.config.LocalCacheDirectory.text()[0] +  dataKey.toLowerCase() + '/caching_business.xml');
								_filesToLoad.push(GlobalDataStorage.configXML.config.LocalCacheDirectory.text()[0] +  dataKey.toLowerCase() + '/caching_dreams.xml');
								_filesToLoad.push(GlobalDataStorage.configXML.config.LocalCacheDirectory.text()[0] +  dataKey.toLowerCase() + '/caching_life.xml');
								_filesToLoad.push(GlobalDataStorage.configXML.config.LocalCacheDirectory.text()[0] +  dataKey.toLowerCase() + '/caching_home.xml');
								_filesToLoad.push(GlobalDataStorage.configXML.config.LocalCacheDirectory.text()[0] +  'mediawallgame/caching_mediawallgame.xml');
						} else if(_originalCommand == commandConstants.MEDIA_WALL_GAMES || _originalCommand == commandConstants.MEDIA_WALL_GAMES_EVENT) {
								_filesToLoad.push(GlobalDataStorage.configXML.config.LocalCacheDirectory.text()[0] +  'mediawallgame/caching_mediawallgame.xml');
						}
					} else {
						_filesToLoad.push(GlobalDataStorage.configXML.config.LocalCacheDirectory.text()[0] +  dataKey.toLowerCase() + '/caching_' + contentKey.toLowerCase() + '.xml');
					}
					
					createXMLLoader();
				} else {
					// ERROR
					generateEmptyData();
					dispatchEvent(new Event(commandAPI.DATA_READY, false,true));
				}
			} else {
				// Trigger some sort of error
				generateEmptyData();
				dispatchEvent(new Event(commandAPI.DATA_READY, false,true));
			}
		}
		
		private function createXMLLoader():void {
			if(_currentActiveXML == null) {
				_currentActiveXML = new XMLLoader( _filesToLoad[_fileIndex] , false);
				_currentActiveXML.addEventListener(XMLLoader.DATALOADED, handleXMLLoaded);
				_currentActiveXML.addEventListener(IOErrorEvent.IO_ERROR, handleXMLIOError);
				_currentActiveXML.addEventListener(SecurityErrorEvent.SECURITY_ERROR, handleXMLSecurityError);
				_currentActiveXML.LoadXMLData();
			} else {
				destroyXMLLoader( true );
			}
		}
		
		private function destroyXMLLoader(_optionalCallback:Boolean = false):void {
			if(_currentActiveXML != null) {
				_currentActiveXML.removeEventListener(XMLLoader.DATALOADED, handleXMLLoaded);
				_currentActiveXML.removeEventListener(IOErrorEvent.IO_ERROR, handleXMLIOError);
				_currentActiveXML.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, handleXMLSecurityError);
				_currentActiveXML.destroyInternals();
				_currentActiveXML = null;
				if(_optionalCallback == true) {
					createXMLLoader();
				}
			}
		}
		
		private function handleXMLLoaded(e:Event):void {
			
			if(_filesToLoad.length == 1) {
				_finalContent = _currentActiveXML.getRawString();
			} else {
				var tmpContent:String = _currentActiveXML.getRawString();
				// Pull off the xml header
				tmpContent = tmpContent.replace('<?xml version="1.0" encoding="UTF-8" ?>','');
				tmpContent = tmpContent.replace('<content>','');
				tmpContent = tmpContent.replace('</content>','');
				_finalContent += tmpContent;
				
				// If this is the end we need the header and closing tag
				if( _fileIndex + 1 == _filesToLoad.length) {
					_finalContent = '<?xml version="1.0" encoding="UTF-8" ?>' + 
						            "\n<content>" + _finalContent + "\n</content>";
				}
			}
			
			nextXMLStep();
		}
		/**
		 * With caching enabled it is important to understand
		 * that even with a load faliure, it will attempt to
		 * use anything in cache. If this fires, it means caching failed AND there
		 * is nothing in cache. IF CACHING IS ON.
		 */
		private function handleXMLIOError(e:IOErrorEvent):void {
			if(_filesToLoad.length == 1) {
				generateEmptyData();
			}
			nextXMLStep();
		}
		private function handleXMLSecurityError(e:IOErrorEvent):void {
			if(_filesToLoad.length == 1) {
				generateEmptyData();
			}
			nextXMLStep();
		}
		private function nextXMLStep():void {
			if( _fileIndex + 1 < _filesToLoad.length) {
				_fileIndex += 1;
				createXMLLoader();
			} else {
				// Reset everything
				destroyXMLLoader();
				_filesToLoad = null;
				_fileIndex = 0;
				
				// Dispatch Complete
				dispatchEvent(new Event(commandAPI.DATA_READY, false,true));
			}
		}
		private function generateEmptyData():void {
			_finalContent = '<?xml version="1.0" encoding="UTF-8" ?>'
			_finalContent += "\n";
			_finalContent += "<content></content>\n";
		}
	}
}