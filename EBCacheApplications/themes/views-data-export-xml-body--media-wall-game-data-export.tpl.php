<?php
/**
 * @file views-data-export-xml-body--media-wall-data-export.tpl.php
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $rows: An array of row items. Each row is an array of content
 *   keyed by field ID.
 * - $header: an array of headers(labels) for fields.
 * - $themed_rows: a array of rows with themed fields.
 * @ingroup views_templates
 */
?>
<?php
  # Defines some of the potential field names that
  # could be in use across CMS implementations for at
  # least a little portability
  if(!isset($this_PhotoFieldNames) ) {
    $this_PhotoFieldNames = array('field_content_photo', 'field_content_photo_1', 'field_content_photo_2', 'field_content_photo_3', 'field_content_photo_4', 'field_content_photo_5', 'field_content_photo_6');
  }

  if(!isset($this_VideoFieldNames) ) {
    $this_VideoFieldNames = array('field_content_video', 'field_content_video_1', 'field_content_video_2', 'field_content_video_3', 'field_content_video_4', 'field_content_video_5', 'field_content_video_6');
  }

  if(!function_exists ('_photo_video_output')) {

   function _photo_video_output($c_item, $xmlTag, $ident='') {
    $xmlNodeSize = 0;
    $outputURL = '';

    if(isset($c_item['raw'])) {
      $xmlNodeSize = trim($c_item['raw']['filesize']);
      $outputURL = file_create_url( trim($c_item['raw']['uri']) );
    }

    echo "<{$xmlTag}>\n";
    echo "{$ident}{$ident}<url><![CDATA[" . $outputURL . "]]></url>\n";
    echo "{$ident}{$ident}<file_size><![CDATA[" . $xmlNodeSize . "]]></file_size>\n";
    echo "{$ident}</{$xmlTag}>";
   }

  }

  if(!function_exists ('_output_photo_video_default')) {
   function _output_photo_video_default($_Tag) {
            echo "<{$_Tag}>\n";
            echo "         <url><![CDATA[]]></url>\n";
            echo "         <file_size><![CDATA[]]></file_size>\n";
            echo "</{$_Tag}>";
   }
  }
  echo '<?xml version="1.0" encoding="UTF-8" ?>';
?>
<content>
<?php foreach ($themed_rows as $count => $row): ?>
  <<?php print 'data'; ?>>
<?php foreach ($row as $field => $content): ?>
    <?php 
      $FieldProcessed = FALSE;
	  # echo '<br/>CURRENT FIELD NAME: ' . $field . '<br/>';
      # Check potential photo types
      for( $photo_indx = 0; $photo_indx < count($this_PhotoFieldNames); $photo_indx++) {
        if($field == $this_PhotoFieldNames[$photo_indx]) {

          if( is_array($rows[$count] -> { 'field_' . $this_PhotoFieldNames[$photo_indx] } ) && ( count($rows[$count] -> { 'field_' . $this_PhotoFieldNames[$photo_indx] } ) > 0 ) ) {
            foreach ( $rows[$count] -> { 'field_' . $this_PhotoFieldNames[$photo_indx]} as $key=>$c_item ) {
              _photo_video_output($c_item, $xml_tag[$field], ' ');
            }
          } else {
            _output_photo_video_default( $xml_tag[$field] );
          }

          $FieldProcessed = TRUE;
          break;
        }
      }
      # Check potential video types
      if($FieldProcessed == FALSE) {
       for( $video_indx = 0; $video_indx < count($this_VideoFieldNames); $video_indx++) {
        if($field == $this_VideoFieldNames[$video_indx]) {

          if( is_array($rows[$count] -> { 'field_' . $this_VideoFieldNames[$video_indx] } ) && ( count($rows[$count] -> { 'field_' . $this_VideoFieldNames[$video_indx] } ) > 0 ) ) {
            foreach ( $rows[$count] -> { 'field_' . $this_VideoFieldNames[$video_indx]} as $key=>$c_item ) {
              _photo_video_output($c_item, $xml_tag[$field], '     ');
            }
          } else {
            _output_photo_video_default( $xml_tag[$field] );
          }

          $FieldProcessed = TRUE;
          break;
        }
       }
      }
      # Everything else
      if($FieldProcessed == FALSE) {
 	 echo '<' . $xml_tag[$field] . '><![CDATA[' . $content . ']]></' . $xml_tag[$field] . '>';
      }

    ?>

<?php endforeach; ?>
  </<?php print 'data'; ?>>
<?php endforeach; ?>
</content>
