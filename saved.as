﻿package code {
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	public class Document extends MovieClip {
		
		private var playIcon:Icons;
		private var dreamIcon:Icons;
		private var businessIcon:Icons;
		private var homeIcon:Icons;
		private var lifeIcon:Icons;
		
		private var iconArray:Array;
		
		private var buffer:Number = 200;
		
		
		
		public function Document()
		{
			addIcon(playIcon);
			playIcon.gotoAndStop(1);
			addIcon(dreamIcon);
			dreamIcon.gotoAndStop(2);
			addIcon(businessIcon);
			businessIcon.gotoAndStop(3);
			addIcon(homeIcon);
			homeIcon.gotoAndStop(4);
			addIcon(lifeIcon);
			lifeIcon.gotoAndStop(5);
			
			iconArray = new Array(playIcon, dreamIcon, businessIcon, homeIcon, lifeIcon);
		}
		
		private function addIcon(mc:Icons):void
		{
			var tempX:Number = stage.stageWidth;
			var tempY:Number = stage.stageHeight;
			mc = new Icons(tempX, tempY, 15, 3, 7);
			addChild(mc);
			
			mc.addEventListener(MouseEvent.CLICK, pauseIcon);
		}
		
		private function pauseIcon(e:MouseEvent):void
		{
			e.target.removeEventListener(MouseEvent.CLICK, pauseIcon);
			e.target.addEventListener(MouseEvent.CLICK, startIcon);
			e.target.pauseThis();
		}
		
		private function startIcon(e:MouseEvent):void
		{
			e.target.addEventListener(MouseEvent.CLICK, pauseIcon);
			e.target.removeEventListener(MouseEvent.CLICK, startIcon);
			e.target.playThis();
		}
	}
	
}
package  {
	
	public class saved {

		public function saved() {
			// constructor code
		}

	}
	
}
