﻿package  
{	
	import flash.display.*;				// import display class
	import flash.events.*;				// import events class
	import flash.text.*;					// import text class
	import flash.utils.getTimer;		// import timer class
	import flash.events.MouseEvent;
	import flash.ui.Mouse;
	import flash.geom.Point;

	import PlatformGame;
		
	public class Document extends MovieClip
	{	
		//private var ninjaGame:BillNinja;
		
		public function Document() 
		{
			//stage.addEventListener(MouseEvent.MOUSE_DOWN, mDown);
			//addEventListener(Event.ENTER_FRAME, update);
			createPlatformGame(0,0);
			//createNinjaWorld();
		}
		
		private function createPlatformGame(xPos:Number, yPos:Number)
		{
			var platformGame:PlatformGame = new PlatformGame();//xPos, yPos, this
			addChild(platformGame);
		}		
	}
}
