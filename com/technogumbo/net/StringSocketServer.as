package com.technogumbo.net
{
	import com.technogumbo.net.StringSockets;
	
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.ServerSocketConnectEvent;
	import flash.events.TimerEvent;
	import flash.net.ServerSocket;
	import flash.net.Socket;
	import flash.sampler.NewObjectSample;
	import flash.utils.Timer;
	
	/**
	 * OK - we need to send acknowledgements
	 * of messages we recieve.
	 */
	public class StringSocketServer extends EventDispatcher
	{
		public static var PARSEDDATA:String = "com.technogumbo.net_StringSocketServer_PARSEDDATA";
		public static var ERROR:String = "com.technogumbo.net_StringSocketServer_ERROR";
		
		protected var _theServer:ServerSocket;
		protected var _targetPort:int;
		protected var _activeSockets:Vector.<StringSockets>;
		protected var _messageDelimeter:String;
	
		protected var _volatileIncomingSocketData:String = "";
		protected var _delayTimer:Timer = null;
		
		public function StringSocketServer(_ListenPort:int, _MessageDelimeter:String="/r/n")
		{
			_targetPort = _ListenPort;
			_messageDelimeter = _MessageDelimeter;
			createSocketServer();
		}
		
		public function destroyInternals():void {
			destroySocketServer();
		}
		
		public function sendAllClientsString(_inputData:String):void {
			if(_activeSockets != null) {
				for (var i:int = 0; i < _activeSockets.length; i++) 
				{
					_activeSockets[i].sendString( _inputData, true );
				}
			}
		}
		/* Internal Functions */
		private function createSocketServer():void {
			if(_theServer == null) {
				_theServer = new ServerSocket();
				_theServer.addEventListener(ServerSocketConnectEvent.CONNECT, handleClientConnection, false, 0, true);
				_theServer.addEventListener(Event.CLOSE, handleServerSocketClose, false, 0, true);
				try {
					// Binding to IPv4 by default on all addresses of the computer
//trace("StringSocketServer binding to: " + _targetPort);
					_theServer.bind(_targetPort);
					// Starts listening
					_theServer.listen();
				} catch (e:Error) {
					// Oh well, try again to infinity.. this would be a good one to
					// throw on I think
					dispatchEvent( new DataEvent(StringSocketServer.ERROR, false, true, e.toString() ) );
					//throw(e);
					// Make the timer and wait a bit before trying again so we dont have infinate loop fun
					_delayTimer = new Timer(3000);
					_delayTimer.addEventListener(TimerEvent.TIMER, handleRetryDelayUp, false, 0, true);
					_delayTimer.start();
				}
				
			} else {
				destroySocketServer(true);
			}
		}
		
		private function destroySocketServer(_OptionalCallback:Boolean = false):void {
			if(_theServer != null) {
				// First off, purge any client connections
				destroyAnyClientConnections();
				_theServer.removeEventListener(ServerSocketConnectEvent.CONNECT, handleClientConnection);
				_theServer.removeEventListener(Event.CLOSE, handleServerSocketClose);
				// If this was a bad socket to begin with, this will throw
				try {
					_theServer.close();
				} catch(e:Error) {
					dispatchEvent( new DataEvent(StringSocketServer.ERROR, false, true, e.toString() ) );
				}
				_theServer = null;
				
				if(_OptionalCallback == true) {
					createSocketServer();
				}
			}
		}
		
		/**
		 * When destroying the socket server, we need to rip through any open clients
		 * and clean them up as properly as possible.
		 */
		private function destroyAnyClientConnections():void {
			if(_activeSockets != null) {
				for (var i:int = 0; i < _activeSockets.length; i++) 
				{
					destroySingleClientConnection(i);
				}
				
				_activeSockets = null;
			}
		}
		
		private function destroySingleClientConnection(i:int):void {
			if(_activeSockets != null) {
				
				_activeSockets[i].removeEventListener(StringSockets.CONNECTED, handleSocketConnectSuccess);
				_activeSockets[i].removeEventListener(StringSockets.DISCONNECTED, handleSocketDisconnectSuccess);
				_activeSockets[i].removeEventListener(StringSockets.ERROR, handleSocketError);
				_activeSockets[i].removeEventListener(StringSockets.STRINGDATA, handleSocketData);
				_activeSockets[i].disconnect();
				_activeSockets[i] = null;
				
				// Removes the slot in the vector
				_activeSockets.splice(i,1);
			}
		}
		
		private function locateClientSocketIndexWithRef(_InputRef:StringSockets):int {
			var returnIndex:int = -1;
			if(_activeSockets != null) {
				for (var i:int = 0; i < _activeSockets.length; i++) 
				{
					if(_activeSockets[i] == _InputRef) {
						returnIndex = i;
						break;
					}
				}
			}
			
			return returnIndex;
		}
		/* Event Handlers */
		//---------- EVENTS FOR SOCKET SERVER -------------------------
		protected function handleClientConnection(e:ServerSocketConnectEvent):void {
			// Push the socket onto our active socket vector
			if(_activeSockets == null) {
				_activeSockets = new Vector.<StringSockets>;
			}
			
			//trace("StringSocketServer - Got a socket connection: ");
			var tmpSocket:StringSockets = new StringSockets(e.socket.remoteAddress,e.socket.remotePort,_messageDelimeter);
			tmpSocket.addEventListener(StringSockets.CONNECTED, handleSocketConnectSuccess, false, 0, true);
			tmpSocket.addEventListener(StringSockets.DISCONNECTED, handleSocketDisconnectSuccess, false, 0, true);
			tmpSocket.addEventListener(StringSockets.ERROR, handleSocketError, false, 0, true);
			tmpSocket.addEventListener(StringSockets.STRINGDATA, handleSocketData, false, 0, true);
			tmpSocket.makeConnection(e.socket);
			_activeSockets.push(tmpSocket);
		}
		
		protected function handleServerSocketClose(e:Event):void {
			// Heck I don't know what to do here
			// Im going to have it automatically attempt to re-create itself
			// If this is called when the socket server.close method is called we DO NOT
			// want to call destroy socket server here.
			destroySocketServer(true);
		}
		// -------------- EVENTS FROM SOCKET CLIENTS --------------------
		protected function handleSocketConnectSuccess(e:Event):void {
			// eh..who cares right now
			dispatchEvent( new DataEvent(StringSocketServer.ERROR, false, true, e.toString() ) );
		}
		protected function handleSocketDisconnectSuccess(e:Event):void {
			trace("StringSocketServer - a client connection was closed");
			// Well, here I need to purge the socket from the list
			var refIndex:int = locateClientSocketIndexWithRef( e.target as StringSockets );
			// If we couldnt find the ref, were screwed
			if(refIndex != -1) {
				destroySingleClientConnection(refIndex);
			}
		}
		
		protected function handleSocketError(e:DataEvent):void {
			// Well, here I need to purge the socket from the list
			var refIndex:int = locateClientSocketIndexWithRef( e.target as StringSockets );
			// If we couldnt find the ref, were screwed
			if(refIndex != -1) {
				destroySingleClientConnection(refIndex);
			}
		}
		protected function handleSocketData(e:DataEvent):void {
			trace("StringSocketServer - Data from a connected Socket: " + e.data);

			// This data is Base 64 encoded to prevent users of the cms from accidentally triggering our
			// delimeters. It is arranged as follows:
			// BASE64 ENCODED COMMAND, BASE64 ENCODED OPTIONAL DATA|
			//
			// So the command, data |
			//
			// The two delimeters here are a comma and a pipe
				
			dispatchEvent(new DataEvent(StringSocketServer.PARSEDDATA, false, true, e.data));
		}
		
		protected function handleRetryDelayUp(e:TimerEvent):void {
			_delayTimer.stop();
			_delayTimer.removeEventListener(TimerEvent.TIMER, handleRetryDelayUp);
			_delayTimer = null;
			
			destroySocketServer(true);
		}
	}
}