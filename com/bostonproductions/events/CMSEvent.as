﻿package com.bostonproductions.events
{
	import flash.events.Event;
	import com.bostonproductions.data.vo.cmsDataVO;
	
	/**
	 * A custom event for sending a number
	 */
	public class CMSEvent extends Event
	{
		public var Data:cmsDataVO;
		public function CMSEvent(type:String, bubbles:Boolean, cancelable:Boolean, _Data:cmsDataVO):void
		{
			super(type, bubbles, cancelable);
			Data = _Data;
		}	
	}
}